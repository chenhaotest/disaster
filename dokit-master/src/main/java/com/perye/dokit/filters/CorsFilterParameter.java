package com.perye.dokit.filters;


import com.perye.dokit.utils.RedisUtils;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter(urlPatterns = {"/app*"}, filterName = "corsFilterParameter", asyncSupported = true)//urlPatterns过滤的哪些url
public class CorsFilterParameter implements Filter {
    private String filterName;
    @Autowired
    RedisUtils redisUtils;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @SneakyThrows
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String path = ((HttpServletRequest) request).getRequestURI();//请求路径

        if (path.startsWith("/app/sendMqttMessage") || path.startsWith("/app/fanlog/getMqttFanData") || path.startsWith("/app/fanlog/testWebSocket")
                || path.startsWith("/app/strategy/queryStrategyList")
                || path.startsWith("/app/fanlog/findDeviceByCode")
                || path.startsWith("/app/testHerb/test")

        ) {// 排除的url
            /*if (true) {*/
            chain.doFilter(request, response);
        } else {
            RequestWrapper requestWrapper = new RequestWrapper(request, request.getParameterMap());
            chain.doFilter(requestWrapper, response);
        }


    }

    @Override
    public void destroy() {
    }

}
