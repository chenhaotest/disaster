package com.perye.dokit.filters;


import com.perye.dokit.utils.Encrypt3Utils;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

@Slf4j
public class RequestWrapper extends HttpServletRequestWrapper {

    Map<String, String[]> params = null;

    public RequestWrapper(HttpServletRequest request, Map inParam) throws Exception {
        super(request);
        params = new HashMap(inParam);
        getJSONParam(params);
    }

    public void setParameter(String key, String value) {
        params.put(key, new String[]{value});
    }

    public void setParameter(String key, String[] values) {
        params.put(key, values);
    }

    @Override
    public String getParameter(String name) {
        Object v = params.get(name);
        if (v == null) {
            return null;
        } else if (v instanceof String[]) {
            String[] strArr = (String[]) v;
            if (strArr.length > 0) {
                return strArr[0];
            } else {
                return null;
            }
        } else {
            return v.toString();
        }
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return params;
    }

    @Override
    public Enumeration<String> getParameterNames() {
        Vector l = new Vector(params.keySet());
        return l.elements();
    }

    @Override
    public String[] getParameterValues(String name) {
        return params.get(name);
    }
    public  void getJSONParam(Map<String, String[]> parameterMap) throws Exception {
//        Map<String, String[]> parameterMap = request.getParameterMap();
//        JSONObject returnObject = new JSONObject();
        for (Map.Entry<String, String[]> entry : parameterMap.entrySet()) {
            String value = "";
            String[] values = entry.getValue();
            if (values != null){
                for (String s : values) {
                    value = s + ",";
                }
                value = value.substring(0, value.length() - 1);
            }

          value= Encrypt3Utils.decode(value);//解密

            setParameter(entry.getKey(),value);
        }
    }
}
