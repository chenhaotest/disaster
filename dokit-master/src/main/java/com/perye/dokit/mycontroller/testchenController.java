package com.perye.dokit.mycontroller;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.config.DataScope;
import com.perye.dokit.dto.DeptDto;
import com.perye.dokit.entity.Dept;
import com.perye.dokit.entity.Test;
import com.perye.dokit.exception.BadRequestException;
import com.perye.dokit.query.DeptQueryCriteria;
import com.perye.dokit.query.TestQueryCriteria;
import com.perye.dokit.service.ChenTestService;
import com.perye.dokit.service.DeptService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.ThrowableUtil;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * ceshi
 */
@RestController
@Api(tags = "系统：部门管理")
@RequestMapping("/api/testchen")
public class testchenController extends BaseController {
    @Autowired
    private ChenTestService chenTestService;
    private final DeptService deptService;

    private final DataScope dataScope;

    private static final String ENTITY_NAME = "testchen";

    public testchenController(DeptService deptService, DataScope dataScope) {
        this.deptService = deptService;
        this.dataScope = dataScope;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('testchen:list')")
    public void download(HttpServletResponse response) throws Exception {
        PageData pageData = this.getPageData();
        List<PageData> pageData1 = chenTestService.queryAll(pageData);
        chenTestService.download(pageData1, response);
    }

     @Log("查询testchen")
        @ApiOperation("查询testchen")
        @GetMapping
        @PreAuthorize("@dokit.check('testchen:list')")
  /*  @RequestMapping("/list")
    @ResponseBody*/
    public ResponseEntity<Object> getDepts() throws Exception {
        // 数据权限

        PageData pageData = this.getPageData();
        // PageData pageData = new PageData();
        //  PageHelper.startPage(pageable.getPageNumber()+1,  pageable.getPageSize());//页面的
        Page p = PageHelper.startPage(Integer.parseInt(pageData.getString("page")), Integer.parseInt(pageData.getString("size")), true);
        List<PageData> deptDtos = chenTestService.queryAll(pageData);
         Integer count = chenTestService.getCount(pageData);

        PageData pageData1 = ArrayAssembly.pageList(deptDtos, count);
        return new ResponseEntity<>(pageData1, HttpStatus.OK);
    }


    @Log("新增testchen")
    @ApiOperation("新增testchen")
    @PostMapping
    @PreAuthorize("@dokit.check('testchen:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PageData pageData) throws Exception {

        pageData.put("create_time", DateUtils.getDate());
        //pageData.put("id",this.get32UUID());
        chenTestService.create(pageData);
        return new ResponseEntity<>(pageData, HttpStatus.CREATED);
    }

    @Log("修改test")
    @ApiOperation("修改test")
    @PutMapping
    @PreAuthorize("@dokit.check('testchen:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PageData pageData) throws Exception {
        chenTestService.update(pageData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


    @Log("删除test")
    @ApiOperation("删除test")
    @PreAuthorize("@dokit.check('test:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Integer[] ids) throws Exception {
        chenTestService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping("/test")
    @ResponseBody
    public ResponseEntity<Object> test() throws Exception {
        PageData pageData = this.getPageData();
        System.out.println(pageData);
       // GenerateImage(pageData.getString("id"));
        Page p = PageHelper.startPage(Integer.parseInt(pageData.getString("page")), Integer.parseInt(pageData.getString("size")), true);
        List<PageData> deptDtos = chenTestService.queryAll(pageData);
      //  PageData pageData1 = ArrayAssembly.pageList(deptDtos, p);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    //base64字符串转化成图片
   /* public static boolean GenerateImage(String imgStr) {   //对字节数组字符串进行Base64解码并生成图片
        if (imgStr == null) //图像数据为空
            return false;
     //   BASE64Decoder decoder = new BASE64Decoder();
        try {
            //Base64解码
        //    byte[] b = decoder.decodeBuffer(imgStr);
            for (int i = 0; i < b.length; ++i) {
                if (b[i] < 0) {//调整异常数据
                    b[i] += 256;
                }
            }
            //生成jpeg图片
            String imgFilePath = "D:\\new23233.png";//新生成的图片
            OutputStream out = new FileOutputStream(imgFilePath);
            out.write(b);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }*/


    public static void tr(String imageString) {
        BufferedImage image = null;
        byte[] imageByte = null;

        try {
            imageByte = DatatypeConverter.parseBase64Binary(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(new ByteArrayInputStream(imageByte));
            bis.close();

            File outputfile = new File("D:\\sealImg.bmp");
            ImageIO.write(image, "bmp", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
