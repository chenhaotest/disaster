package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpAccidentStatisticsDto implements Serializable {

    private String id;

    private String manufacturerName;

    private String personName;

    private String personPhone;

    private String manufacturersAddress;

    private String country;

    private String badCredit;

    private String equipmentVendorsType;

    private Integer isDelete;

    private String createId;

    private Timestamp createTime;

    private Timestamp updateTime;
}
