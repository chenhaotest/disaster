package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpHotWorkDto implements Serializable {

    private String id;

    private String workDefinition;

    private String workName;

    private String useTool;

    private String workAddressRange;

    private Timestamp workBeginTime;

    private Timestamp workEndTime;

    private String workUnit;

    private String workPeopleType;

    private String workPeoples;

    private String inform;

    private String workSupervise;

    private String regulatoryRecords;

    private String approvalStatus;

    private String approvalOpinions;

    private String approvalBy;

    private Integer isDelete;

    private Timestamp createTime;

    private Timestamp updateTime;

    private String createId;
}
