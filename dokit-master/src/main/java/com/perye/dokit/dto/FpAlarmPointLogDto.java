package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpAlarmPointLogDto implements Serializable {

    private String id;

    private String pointName;

    private String pointType;

    private String mappingType;

    private String registerAddress;

    private String registerAddressByte;

    private String modbusId;

    private String resRailwayId;

    private String resTunnelId;

    private String resRegionId;

    private String resSysId;

    private String equipmentId;

    private String batch;

    private String state;

    private String registerAddressVal;

    private Timestamp updateTime;

    private Timestamp createTime;
}
