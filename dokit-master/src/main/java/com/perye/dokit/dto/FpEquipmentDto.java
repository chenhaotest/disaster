package com.perye.dokit.dto;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;



@Data
public class FpEquipmentDto implements Serializable {

    private String id;

    private String assetNumber;

    private String deviceName;

    private String deviceType;

    private String specification;

    private String manufacturer;

    private String department;

    private String serviceLife;

    private String usageCondition;

    private Timestamp purchaseDate;

    private String mileageNumbe;

    private String remoteControl;

    private String downLines;

    private String enable;

    private String resRailwayId;

    private String resTunnelId;

    private String resRegionId;

    private String resSysId;

    private Integer isDelete;

    private String createId;

    private Timestamp createTime;

    private Timestamp updateTime;

    /**
     * 下次保养日期
     */
    private Timestamp nextMaintenanceDate;

    /**
     * 保养周期(天)
     */
    private String maintenanceCycle;
}
