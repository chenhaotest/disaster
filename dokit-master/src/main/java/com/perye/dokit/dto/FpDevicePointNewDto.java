package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpDevicePointNewDto implements Serializable {

    private String id;

    private String pointName;

    private String pointType;

    private String mappingType;

    private String registerAddress;

    private String registerAddressByte;

    private String modbusId;

    private String resRailwayId;

    private String resTunnelId;

    private String resRegionId;

    private String resSysId;

    private String equipmentId;

    private String connectionType;

    private String enable;

    private String isAlarm;

    private Timestamp createTime;

    private Timestamp updateTime;

    private Integer isDelete;

    private String type;

    private String registerAddressVal;
    private String registerType;
}
