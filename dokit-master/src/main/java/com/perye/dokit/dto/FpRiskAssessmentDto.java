package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpRiskAssessmentDto implements Serializable {

    private String id;

    private Timestamp evaluationTime;

    private String assessor;

    private String contactPhone;

    private String rectificationSituation;

    private String assessmentReport;

    private String evaluationUnit;

    private String evaluate;

    private String createId;

    private Timestamp createTime;

    private Timestamp updateTime;

    private Integer isDelete;
}
