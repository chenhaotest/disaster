package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpModubsDto implements Serializable {

    private String id;

    private String networkAddress;

    private String ports;

    private String resRailwayId;

    private String resTunnelId;

    private String state;

    private String description;

    private Integer readMax;

    private Integer isDelete;

    private String createId;

    private Timestamp createTime;

    private Timestamp updateTime;
}
