package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpFireDrillDto implements Serializable {

    private String id;

    private Timestamp exerciseTime;

    private String exerciseAddr;

    private String exerciseDescribe;
    private String contact;
    private String contactInfo;

    private String state;

    private String participateNum;

    private Timestamp createTime;

    private Timestamp updateTime;

    private String createId;
    private Integer isDelete;
}
