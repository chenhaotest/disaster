package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class TestLiDto implements Serializable {

    private String id;

    private String name;

    private Timestamp createTime;

    private Integer isDelete;
}
