package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpPersonnelDto implements Serializable {

    private String id;

    private String name;

    private String sex;

    private String departmentId;

    private String personnelType;

    private String idcard;

    private String phoneNumber;

    private String emergencyContact;

    private String emergencyContactPhone;

    private String state;

    private String affiliatedCompany;

    private Integer isDelete;

    private String createId;

    private Timestamp createTime;

    private Timestamp updateTime;
    //岗位
    private String peoplePost;
}
