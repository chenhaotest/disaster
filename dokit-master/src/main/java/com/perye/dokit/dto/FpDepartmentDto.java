package com.perye.dokit.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;



@Data
public class FpDepartmentDto implements Serializable {

    private String id;

    private String departmentName;

    private String departmentType;

    private String state;

    private String description;

    private Integer isDelete;

    private String createId;

    private Timestamp createTime;

    private Timestamp updateTime;
}
