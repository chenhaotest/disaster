package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpRiskAssessment;
import com.perye.dokit.service.FpRiskAssessmentService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api(tags = "fpRiskAssessment管理")
@RestController
@RequestMapping("/api/fpRiskAssessment")
public class FpRiskAssessmentController extends BaseController {

    private final FpRiskAssessmentService fpRiskAssessmentService;

    public FpRiskAssessmentController(FpRiskAssessmentService fpRiskAssessmentService) {
        this.fpRiskAssessmentService = fpRiskAssessmentService;
    }


    @Log("查询fpRiskAssessment")
    @ApiOperation("查询fpRiskAssessment")
    @GetMapping()
    public ResponseEntity<Object> getFpHotWorks(){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpRiskAssessmentService.fpRiskAssessmentList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增fpRiskAssessment")
    @ApiOperation("新增fpRiskAssessment")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpRiskAssessment resources){
        PageData result = new PageData();
        result.put("data",fpRiskAssessmentService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.CREATED);
    }

    @Log("修改fpRiskAssessment")
    @ApiOperation("修改fpRiskAssessment")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpRiskAssessment resources){
    fpRiskAssessmentService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("删除fpRiskAssessment")
    @ApiOperation("删除fpRiskAssessment")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
    fpRiskAssessmentService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
