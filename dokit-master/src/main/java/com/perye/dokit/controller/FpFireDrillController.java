package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpFireDrill;
import com.perye.dokit.query.FpFireDrillQueryCriteria;
import com.perye.dokit.service.FpFireDrillService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "FireDrill管理")
@RestController
@RequestMapping("/api/fpFireDrill")
public class FpFireDrillController extends BaseController {

    private final FpFireDrillService fpFireDrillService;

    public FpFireDrillController(FpFireDrillService fpFireDrillService) {
        this.fpFireDrillService = fpFireDrillService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpFireDrill:list')")
    public void download(HttpServletResponse response, FpFireDrillQueryCriteria criteria) throws IOException {
        fpFireDrillService.download(fpFireDrillService.queryAll(criteria), response);
    }

    @Log("查询FireDrill")
    @ApiOperation("查询FireDrill")
    @GetMapping()
    public ResponseEntity<Object> getFpFireDrills(){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpFireDrillService.fpFireDrillList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增FireDrill")
    @ApiOperation("新增FireDrill")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpFireDrill resources){

        PageData  result = new PageData();
        result.put("data",fpFireDrillService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改FireDrill")
    @ApiOperation("修改FireDrill")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpFireDrill resources){
          fpFireDrillService.update(resources);
            PageData  result = new PageData();
            result.put("data","");
            result.put("code",HttpStatus.OK.value());
            result.put("message", "修改成功");
            return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("删除FireDrill")
    @ApiOperation("删除FireDrill")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {

        PageData  result = new PageData();
        fpFireDrillService.deleteAll(ids);
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "删除成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

}
