package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpHotWork;
import com.perye.dokit.query.FpHotWorkQueryCriteria;
import com.perye.dokit.service.FpHotWorkService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "HotWork管理")
@RestController
@RequestMapping("/api/fpHotWork")
public class FpHotWorkController extends BaseController {

    private final FpHotWorkService fpHotWorkService;

    public FpHotWorkController(FpHotWorkService fpHotWorkService) {
        this.fpHotWorkService = fpHotWorkService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpHotWork:list')")
    public void download(HttpServletResponse response, FpHotWorkQueryCriteria criteria) throws IOException {
        fpHotWorkService.download(fpHotWorkService.queryAll(criteria), response);
    }

    @Log("查询HotWork")
    @ApiOperation("查询HotWork")
    @GetMapping()
    public ResponseEntity<Object> getFpHotWorks(){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpHotWorkService.fpHotWorklList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增HotWork")
    @ApiOperation("新增HotWork")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpHotWork resources){
        PageData result = new PageData();
        result.put("data",fpHotWorkService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.CREATED);
    }

    @Log("修改HotWork")
    @ApiOperation("修改HotWork")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpHotWork resources){
        fpHotWorkService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }
    @Log("审批")
    @ApiOperation("审批")
    @PostMapping("/updataApprovalStatusById")
    public ResponseEntity<Object> updataApprovalStatusById(@Validated @RequestBody FpHotWork resources){
        fpHotWorkService.updataApprovalStatusById(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "审批成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }
    @Log("删除HotWork")
    @ApiOperation("删除HotWork")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
    fpHotWorkService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
