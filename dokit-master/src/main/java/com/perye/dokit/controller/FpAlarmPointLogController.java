package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpAlarmPointLog;
import com.perye.dokit.query.FpAlarmPointLogQueryCriteria;
import com.perye.dokit.service.FpAlarmPointLogService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "FpAlarmPointLog管理")
@RestController
@RequestMapping("/api/fpAlarmPointLog")
public class FpAlarmPointLogController extends BaseController {

    private final FpAlarmPointLogService fpAlarmPointLogService;

    public FpAlarmPointLogController(FpAlarmPointLogService fpAlarmPointLogService) {
        this.fpAlarmPointLogService = fpAlarmPointLogService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    public void download(HttpServletResponse response, FpAlarmPointLogQueryCriteria criteria) throws IOException {
        fpAlarmPointLogService.download(fpAlarmPointLogService.queryAll(criteria), response);
    }

    @Log("查询FpAlarmPointLog")
    @ApiOperation("查询FpAlarmPointLog")
    @GetMapping()
    public ResponseEntity<Object> getFpAlarmPointLogs(FpAlarmPointLogQueryCriteria criteria, Pageable pageable){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpAlarmPointLogService.fpAlarmPointLogList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增FpAlarmPointLog")
    @ApiOperation("新增FpAlarmPointLog")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpAlarmPointLog resources){
        PageData  result = new PageData();
        result.put("data",fpAlarmPointLogService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改FpAlarmPointLog")
    @ApiOperation("修改FpAlarmPointLog")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpAlarmPointLog resources){
    fpAlarmPointLogService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除FpAlarmPointLog")
    @ApiOperation("删除FpAlarmPointLog")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
    fpAlarmPointLogService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
