package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpDepartment;
import com.perye.dokit.query.FpDepartmentQueryCriteria;
import com.perye.dokit.service.FpDepartmentService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "Fpdepartment管理")
@RestController
@RequestMapping("/api/fpDepartment")
public class FpDepartmentController extends BaseController {

    private final FpDepartmentService fpDepartmentService;

    public FpDepartmentController(FpDepartmentService fpDepartmentService) {
        this.fpDepartmentService = fpDepartmentService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpDepartment:list')")
    public void download(HttpServletResponse response, FpDepartmentQueryCriteria criteria) throws IOException {
        fpDepartmentService.download(fpDepartmentService.queryAll(criteria), response);
    }

    @Log("查询Fpdepartment")
    @ApiOperation("查询Fpdepartment")
    @GetMapping()
    public ResponseEntity<Object> getFpDepartments(FpDepartmentQueryCriteria criteria, Pageable pageable){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpDepartmentService.fpDepartmentList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增Fpdepartment")
    @ApiOperation("新增Fpdepartment")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpDepartment resources){
        PageData  result = new PageData();
        result.put("data",fpDepartmentService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改Fpdepartment")
    @ApiOperation("修改Fpdepartment")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpDepartment resources){
        fpDepartmentService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    }

    @Log("删除Fpdepartment")
    @ApiOperation("删除Fpdepartment")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
        PageData  result = new PageData();
        fpDepartmentService.deleteAll(ids);
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "删除成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

}
