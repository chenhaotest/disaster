package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpEquipment;
import com.perye.dokit.query.FpEquipmentQueryCriteria;
import com.perye.dokit.service.FpEquipmentService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "Fpequipment管理")
@RestController
@RequestMapping("/api/fpEquipment")
public class FpEquipmentController extends BaseController {

    private final FpEquipmentService fpEquipmentService;

    public FpEquipmentController(FpEquipmentService fpEquipmentService) {
        this.fpEquipmentService = fpEquipmentService;
    }

    /**
     * 查询数据分页
     * @param criteria criteria
     */
    @Log("不分页查询设备列表")
    @ApiOperation("不分页查询设备列表")
    @PostMapping("/fpEquipmentListAll")
    public  ResponseEntity<Object>  fpEquipmentListAll(FpEquipmentQueryCriteria criteria) throws Exception{
        //只查询启用的设备 未删除状态[0：未删除；1：已删除]
        PageData  result = new PageData();
        criteria.setEnable("1");
        criteria.setIsDelete(0);

        result.put("data",fpEquipmentService.queryAll(criteria));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "查询成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    };

    @Log("下载模板文件")
    @ApiOperation("下载模板文件")
    @GetMapping("/downLoadExcel")
    public void downLoadExcel(HttpServletResponse response, HttpServletRequest request,  String fileName) throws IOException{
        fpEquipmentService.downLoadExcel(response,request,fileName);
    }

    @Log("导入设备")
    @ApiOperation("导入设备")
    @PostMapping(value = "/importFpEquipment")
    public ResponseEntity<Object>  importFpEquipment(@RequestBody MultipartFile file) throws Exception {
        PageData pd = fpEquipmentService.importFpEquipment(file);
        return new ResponseEntity<>(pd,HttpStatus.OK);

    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    public void download(HttpServletResponse response, FpEquipmentQueryCriteria criteria) throws IOException {
        fpEquipmentService.download(fpEquipmentService.queryAll(criteria), response);
    }

    @Log("查询Fpequipment")
    @ApiOperation("查询Fpequipment")
    @GetMapping()
    public ResponseEntity<Object> getFpEquipments(FpEquipmentQueryCriteria criteria, Pageable pageable){

        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpEquipmentService.fpEquipmentList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);

    }

    @Log("新增Fpequipment")
    @ApiOperation("新增Fpequipment")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpEquipment resources){
        PageData  result = new PageData();
        result.put("data",fpEquipmentService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改Fpequipment")
    @ApiOperation("修改Fpequipment")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpEquipment resources){

        fpEquipmentService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    }

    @Log("删除Fpequipment")
    @ApiOperation("删除Fpequipment")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
        PageData  result = new PageData();
        fpEquipmentService.deleteAll(ids);
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "删除成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    }

}
