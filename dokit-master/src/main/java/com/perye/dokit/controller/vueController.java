package com.perye.dokit.controller;


import com.perye.dokit.base.BaseController;
import com.perye.dokit.newservice.vue1Service;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/vuedemo")
@Api(tags = "系统:访问记录管理")
public class vueController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(vueController.class);
    @Autowired
    private vue1Service vue1Service;

    @RequestMapping("/list")
    @ResponseBody
    public ResponseEntity<Object> list() throws Exception {
        PageData result = new PageData();
        PageData pageData = getPageData();
        PageData pageData2 = new PageData();

        try {

            List<PageData> list = vue1Service.getUserList(pageData);
            result.put("data", list);
            result.put("status", 0);
            result.put("msg", "查询成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @CrossOrigin
    @RequestMapping("/findUser")
    @ResponseBody
    public PageData findUser() throws Exception {
        PageData result = new PageData();
        PageData pageData = getPageData();
        PageData pageData2 = new PageData();
        try {
            List<PageData> list = vue1Service.findUser(pageData);

            result.put("data", list);
            result.put("code", 1);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
        return result;
    }


    @CrossOrigin
    @RequestMapping("/findRole")
    @ResponseBody
    public PageData findRole() throws Exception {
        PageData result = new PageData();
        PageData pageData = getPageData();
        PageData pageData2 = new PageData();
        try {
            List<PageData> list = vue1Service.findRole(pageData);
            for (PageData role : list) {
                String role_ids = role.getString("role_ids");
                String[] split = role_ids.split(",");
                pageData.put("role_ids", split);
                List<PageData> menu = vue1Service.findRoleMenu(pageData);
                role.put("erpMemberPermissions", menu);
            }
            result.put("data", list);
            result.put("code", 1);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
        return result;
    }


    @CrossOrigin
    @RequestMapping("/findMenu")
    @ResponseBody
    public PageData findMenu() throws Exception {
        PageData result = new PageData();
        PageData pageData = getPageData();
        PageData pageData2 = new PageData();
        try {
            List<PageData> list = vue1Service.findMenu(pageData);

            result.put("data", list);
            result.put("code", 1);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
        return result;
    }

    @CrossOrigin
    @RequestMapping("/findAllMenu")
    @ResponseBody
    public PageData findAllMenu() throws Exception {
        PageData result = new PageData();
        PageData pageData = getPageData();
        PageData pageData2 = new PageData();
        List<PageData> listMenu = new ArrayList<>();
        String data = "";
        try {
            //得到所有菜单数据
            List<PageData> list = vue1Service.findAllMenu(pageData);
            for (int i = 0; i < list.size(); i++) {
                PageData menu = list.get(i);
                //获取顶级菜单
                String parentId = menu.get("parentId").toString();
                if (StringUtils.equals(parentId, "0")) {
                    listMenu.add(menu);
                    UtilsSplit(i + "", data);
                }


            }

            String[] split = data.split(",");
            for (String index : split) {
                list.remove(index);
                data="";
            }

            //遍历等级菜单把二级菜单放入一级菜单里面

            for (int i = 0; i < listMenu.size(); i++) {
                PageData menu = listMenu.get(i);
                String id = menu.get("id").toString();
                List<PageData> erpMemberPermissions = new ArrayList<>();

                for (PageData menuFevel : list) {
                    String parentId = menuFevel.get("parentId").toString();
                    if (StringUtils.equals(id, parentId)) {
                        menuFevel.put("permissionLevel",null);
                        menuFevel.remove("resourceLevel");
                        erpMemberPermissions.add(menuFevel);
                        UtilsSplit(i + "", data);
                    }
                }
                menu.put("erpMemberPermissions", erpMemberPermissions);
                listMenu.remove(i);
                listMenu.add(i, menu);

            }

            for (String index : data.split(",")) {
                list.remove(index);
                data="";
            }

            //遍历等级菜单 把三级菜单放入二级菜单里面

            for (int i = 0; i < listMenu.size(); i++) {
                PageData pageMenu = listMenu.get(i);
                //获得二级菜单数据集合
                List<PageData> erpMemberPermissions = (List<PageData>) pageMenu.get("erpMemberPermissions");
                //遍历二级菜单数据集合
                for (PageData menu : erpMemberPermissions) {
                    String id = menu.get("id").toString();
                    List<PageData> erpMemberPermissions2 = new ArrayList<>();
                    //遍历所有数据
                    for (PageData menuFevel : list) {

                        String parentId = menuFevel.get("parentId").toString();
                        if (StringUtils.equals(id, parentId)) {
                            erpMemberPermissions2.add(menuFevel);
                            menuFevel.remove("resourceLevel");
                            menuFevel.put("permissionLevel",null);
                            UtilsSplit(i + "", data);
                        }
                    }
                    if (erpMemberPermissions2.size() > 0) {
                        menu.put("erpMemberPermissions", erpMemberPermissions2);
                    }


                }


            }
            for (String index : data.split(",")) {
                list.remove(index);
                data="";
            }
            result.put("data", listMenu);
            result.put("code", 1);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
        return result;
    }

    public String UtilsSplit(String str, String data) {
        String[] split = data.split(",");
        if (split.length == 0) {
            data = str;
        } else {
            data = data + "," + str;
        }
        return data;

    }




    @CrossOrigin
    @RequestMapping("/login")
    @ResponseBody
    public PageData login() throws Exception {
        PageData result = new PageData();
        PageData pageData = getPageData();
        PageData pageData2 = new PageData();
        try {
            List<PageData> list = vue1Service.findMenu(pageData);
            pageData2.put("token","eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwiaWF0IjoxNTkwMzkyMDExLCJleHAiOjE1OTA0Nzg0MTF9.r2734a9iQYCRgcekw9dA52u62Edbwm8B7ayPYeAxkV0");
            result.put("message", "获取token成功");
            result.put("code", 1);
            result.put("data", pageData2);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
        return result;
    }
    @CrossOrigin
    @RequestMapping("/info")
    @ResponseBody
    public PageData info() throws Exception {
        PageData result = new PageData();
        PageData pageData = getPageData();
        PageData pageData2 = new PageData();
        try {
            String [] role=new String[]{};
            String [] data=new String[]{};
            pageData2.put("avatar","https://randy168.com/1533262153771.gif");
            pageData2.put("name","putong");
            String roles="putong";
            role = roles.split(",");
         String datas="admin,ceshi,ceshi,ceshi1-1,ceshi1-2,permission,user-manage,role-manage,menu-manage";
            //   String datas="admin,ceshi,ceshi2,ceshi1-1,ceshi1-2,Order22";
           /* role[0]="admin";
            data[0]="ceshi";
            data[1]="ceshi2";
            data[2]="ceshi1-1";
            data[3]="ceshi1-2";
            data[4]="permission";
            data[5]="user-manage";
            data[6]="role-manage";
            data[7]="menu-manage";*/
            data = datas.split(",");
            pageData2.put("roles",role);
            pageData2.put("data",data);
            result.put("code", 1);
            result.put("data", pageData2);
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw new Exception(e.getMessage());
        }
        return result;
    }
}
