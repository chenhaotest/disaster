package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpDevicePointNew;
import com.perye.dokit.query.FpDevicePointNewQueryCriteria;
import com.perye.dokit.service.FpDevicePointNewService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "FpDevicePointNew管理")
@RestController
@RequestMapping("/api/fpDevicePointNew")
public class FpDevicePointNewController extends BaseController {

    private final FpDevicePointNewService fpDevicePointNewService;

    public FpDevicePointNewController(FpDevicePointNewService fpDevicePointNewService) {
        this.fpDevicePointNewService = fpDevicePointNewService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpDevicePointNew:list')")
    public void download(HttpServletResponse response, FpDevicePointNewQueryCriteria criteria) throws IOException {
        fpDevicePointNewService.download(fpDevicePointNewService.queryAll(criteria), response);
    }

    @Log("查询FpDevicePointNew")
    @ApiOperation("查询FpDevicePointNew")
    @GetMapping()
    public ResponseEntity<Object> getFpDevicePointNews(FpDevicePointNewQueryCriteria criteria, Pageable pageable){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpDevicePointNewService.fpDevicePointNewList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);


    }

    @Log("新增FpDevicePointNew")
    @ApiOperation("新增FpDevicePointNew")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpDevicePointNew resources){
        PageData  result = new PageData();
        result.put("data",fpDevicePointNewService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改FpDevicePointNew")
    @ApiOperation("修改FpDevicePointNew")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpDevicePointNew resources){
        fpDevicePointNewService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    }

    @Log("删除FpDevicePointNew")
    @ApiOperation("删除FpDevicePointNew")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
        PageData  result = new PageData();
        fpDevicePointNewService.deleteAll(ids);
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "删除成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    }

}
