package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpAccidentStatistics;
import com.perye.dokit.query.FpAccidentStatisticsQueryCriteria;
import com.perye.dokit.service.FpAccidentStatisticsService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "AccidentStatistics管理")
@RestController
@RequestMapping("/api/fpAccidentStatistics")
public class FpAccidentStatisticsController extends BaseController {


    private final FpAccidentStatisticsService fpAccidentStatisticsService;

    public FpAccidentStatisticsController(FpAccidentStatisticsService fpAccidentStatisticsService) {
        this.fpAccidentStatisticsService = fpAccidentStatisticsService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    public void download(HttpServletResponse response, FpAccidentStatisticsQueryCriteria criteria) throws IOException {
        fpAccidentStatisticsService.download(fpAccidentStatisticsService.queryAll(criteria), response);
    }
    /**
     * 查询数据分页
     * @param criteria criteria
     */
    @Log("不分页查询设备商和维修商")
    @ApiOperation("不分页查询设备商和维修商")
    @PostMapping("/fpAccidentStatisticsListAll")
    public  ResponseEntity<Object>  fpAccidentStatisticsListAll(@RequestBody FpAccidentStatisticsQueryCriteria criteria) throws Exception{
        //未删除状态[0：未删除；1：已删除]
        PageData  result = new PageData();

        criteria.setIsDelete(0);

        result.put("data",fpAccidentStatisticsService.queryAll(criteria));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "查询成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    };

    @Log("查询设备，维修商表")
    @ApiOperation("查询设备，维修商表")
    @GetMapping()
    public ResponseEntity<Object> getFpAccidentStatisticss(FpAccidentStatisticsQueryCriteria criteria, Pageable pageable){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpAccidentStatisticsService.fpAccidentStatisticsList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增AccidentStatistics")
    @ApiOperation("新增AccidentStatistics")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpAccidentStatistics resources){
        PageData  result = new PageData();
        result.put("data",fpAccidentStatisticsService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改AccidentStatistics")
    @ApiOperation("修改AccidentStatistics")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpAccidentStatistics resources){
      fpAccidentStatisticsService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("删除AccidentStatistics")
    @ApiOperation("删除AccidentStatistics")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
        PageData  result = new PageData();
       fpAccidentStatisticsService.deleteAll(ids);
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "删除成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

}
