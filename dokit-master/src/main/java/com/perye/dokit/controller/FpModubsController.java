package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpModubs;
import com.perye.dokit.query.FpModubsQueryCriteria;
import com.perye.dokit.service.FpModubsService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "FpModubs管理")
@RestController
@RequestMapping("/api/fpModubs")
public class FpModubsController extends BaseController {

    private final FpModubsService fpModubsService;

    public FpModubsController(FpModubsService fpModubsService) {
        this.fpModubsService = fpModubsService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpModubs:list')")
    public void download(HttpServletResponse response, FpModubsQueryCriteria criteria) throws IOException {
        fpModubsService.download(fpModubsService.queryAll(criteria), response);
    }

    @Log("查询FpModubs")
    @ApiOperation("查询FpModubs")
    @GetMapping()
    public ResponseEntity<Object> getFpModubss(FpModubsQueryCriteria criteria, Pageable pageable){

        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpModubsService.fpModubsList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增FpModubs")
    @ApiOperation("新增FpModubs")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpModubs resources){
        PageData  result = new PageData();
        result.put("data",fpModubsService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改FpModubs")
    @ApiOperation("修改FpModubs")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpModubs resources){

        fpModubsService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    }

    @Log("删除FpModubs")
    @ApiOperation("删除FpModubs")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
        PageData  result = new PageData();
        fpModubsService.deleteAll(ids);
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "删除成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

}
