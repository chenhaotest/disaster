package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpPointLog;
import com.perye.dokit.query.FpPointLogQueryCriteria;
import com.perye.dokit.service.FpPointLogService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "FpPointLog管理")
@RestController
@RequestMapping("/api/fpPointLog")
public class FpPointLogController extends BaseController {

    private final FpPointLogService fpPointLogService;

    public FpPointLogController(FpPointLogService fpPointLogService) {
        this.fpPointLogService = fpPointLogService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    public void download(HttpServletResponse response, FpPointLogQueryCriteria criteria) throws IOException {
        fpPointLogService.download(fpPointLogService.queryAll(criteria), response);
    }

    @Log("查询FpPointLog")
    @ApiOperation("查询FpPointLog")
    @GetMapping()
    public ResponseEntity<Object> getFpPointLogs(FpPointLogQueryCriteria criteria, Pageable pageable){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpPointLogService.fpPointLogNewList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);

    }

    @Log("新增FpPointLog")
    @ApiOperation("新增FpPointLog")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpPointLog resources){
        PageData  result = new PageData();
        result.put("data",fpPointLogService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改FpPointLog")
    @ApiOperation("修改FpPointLog")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpPointLog resources){
        fpPointLogService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    }

    @Log("删除FpPointLog")
    @ApiOperation("删除FpPointLog")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
    fpPointLogService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
