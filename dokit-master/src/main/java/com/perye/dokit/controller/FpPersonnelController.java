package com.perye.dokit.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.entity.FpPersonnel;
import com.perye.dokit.query.FpPersonnelQueryCriteria;
import com.perye.dokit.service.FpPersonnelService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "Fppersonnel管理")
@RestController
@RequestMapping("/api/fpPersonnel")
public class FpPersonnelController extends BaseController {

    private final FpPersonnelService fpPersonnelService;

    public FpPersonnelController(FpPersonnelService fpPersonnelService) {
        this.fpPersonnelService = fpPersonnelService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    public void download(HttpServletResponse response, FpPersonnelQueryCriteria criteria) throws IOException {
        fpPersonnelService.download(fpPersonnelService.queryAll(criteria), response);
    }

    @Log("查询Fppersonnel")
    @ApiOperation("查询Fppersonnel")
    @GetMapping()
    public ResponseEntity<Object> getFpPersonnels(FpPersonnelQueryCriteria criteria, Pageable pageable){
        PageData pageDataList = null;
        try {
            PageData pageData = this.getPageData();
            int page = Integer.parseInt(pageData.getString("page")) + 1;
            PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

            List<PageData> deptDtos = fpPersonnelService.fpPersonnelList(pageData);
            PageInfo<PageData> pageInfo = new PageInfo<PageData>(deptDtos);
            int count = (int) pageInfo.getTotal();
            pageDataList = ArrayAssembly.pageList(deptDtos,count);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return new ResponseEntity<>(pageDataList, HttpStatus.OK);

    }

    @Log("新增Fppersonnel")
    @ApiOperation("新增Fppersonnel")
    @PostMapping
    public ResponseEntity<Object> create(@Validated @RequestBody FpPersonnel resources){

        PageData  result = new PageData();
        result.put("data",fpPersonnelService.create(resources));
        result.put("code",HttpStatus.OK.value());
        result.put("message", "新增成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

    @Log("修改Fppersonnel")
    @ApiOperation("修改Fppersonnel")
    @PutMapping
    public ResponseEntity<Object> update(@Validated @RequestBody FpPersonnel resources){
        fpPersonnelService.update(resources);
        PageData  result = new PageData();
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "修改成功");
        return new ResponseEntity<>(result,HttpStatus.OK);

    }

    @Log("删除Fppersonnel")
    @ApiOperation("删除Fppersonnel")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {

        PageData  result = new PageData();
        fpPersonnelService.deleteAll(ids);
        result.put("data","");
        result.put("code",HttpStatus.OK.value());
        result.put("message", "删除成功");
        return new ResponseEntity<>(result,HttpStatus.OK);
    }

}
