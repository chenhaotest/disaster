package com.perye.dokit.query;

import lombok.Data;
import java.util.List;
import com.perye.dokit.annotation.Query;

@Data
public class FpModubsQueryCriteria{

    // 模糊
    @Query(type = Query.Type.INNER_LIKE)
    private String networkAddress;
}
