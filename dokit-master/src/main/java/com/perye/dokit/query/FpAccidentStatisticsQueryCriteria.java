package com.perye.dokit.query;

import com.perye.dokit.annotation.Query;
import lombok.Data;

@Data
public class FpAccidentStatisticsQueryCriteria{
    /**
     * 删除状态[0：未删除；1：已删除]
     */
    @Query(type = Query.Type.EQUAL)
    private Integer isDelete;

    /**
     *设备维保商类型[0：运行；1：维护；2：保养,3生产]
     */
    @Query(type = Query.Type.INNER_LIKE)
    private String equipmentVendorsType;

}
