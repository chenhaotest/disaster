package com.perye.dokit.query;

import com.perye.dokit.annotation.Query;
import lombok.Data;

@Data
public class FpEquipmentQueryCriteria{
    /**
     * 删除状态[0：未删除；1：已删除]
     */
    @Query(type = Query.Type.EQUAL)
    private Integer isDelete;
    /**
     * 相等
     */
    @Query(type = Query.Type.EQUAL)
    private String enable;
}
