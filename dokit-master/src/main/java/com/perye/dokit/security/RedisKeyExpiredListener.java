package com.perye.dokit.security;/**
 * @Description //TODO
 * @Date 2023/7/3 11:51
 * @Author chenhao
 **/

import com.perye.dokit.utils.RedisUtils;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * @author chenhao
 * @date 2023/7/3   11:51
 */


@Component
public class RedisKeyExpiredListener extends KeyExpirationEventMessageListener {
    @Autowired
    private RedisUtils redisUtils;

    private static Logger logger = LoggerFactory.getLogger(RedisKeyExpiredListener.class);

    public RedisKeyExpiredListener(RedisMessageListenerContainer listenerContainer) {
        super(listenerContainer);
    }

    @SneakyThrows
    @Override
    public void onMessage(Message message, byte[] pattern) {
        String key = new String(message.getBody(), StandardCharsets.UTF_8);

        if (key.contains("msgSendError")) {

            Map<Object, Object> redisData = redisUtils.hmget(key);

        }


    }


}
