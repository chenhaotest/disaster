package com.perye.dokit.analysis;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.newservice.DeviceLogService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.UuidUtil;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * 气体灭火监控系统json解析类
 */
@Component
public class GasFireExtinguishing {

    @Autowired
    private transient DeviceLogService service;

    private static DeviceLogService deviceLogService;

    @PostConstruct
    public void init() {
        deviceLogService = service;
    }


    public static void main(String[] args) throws Exception {

        PageData info = JSONObject.parseObject(jsonStr, PageData.class);

        jsonMain(info);


    }

    public static PageData jsonMain(PageData pageData) throws Exception {
        PageData result = new PageData();
        String batchCode = UuidUtil.get32UUID();

        //json解析-正线火灾-气体灭火设备管理机
        List<PageData> managementMachine = managementMachine(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(managementMachine)) {
            System.out.println("气体灭火设备管理机:" + managementMachine);
        }

        //json解析-正线火灾-通风设备控制箱----废弃
        /*List<PageData> ventilationEquipmentcontrolBox = ventilationEquipmentcontrolBox(pageData);
        if (CollectionUtil.isNotEmpty(ventilationEquipmentcontrolBox)) {
            deviceLogService.insertManualButtonLog(ventilationEquipmentcontrolBox);
        }*/

        //json解析-正线火灾-悬挂式气体灭火装置
        List<PageData> suspendedGasFireExtinguishing = suspendedGasFireExtinguishing(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(suspendedGasFireExtinguishing)) {
            System.out.println("悬挂式气体灭火装置:" + suspendedGasFireExtinguishing);
        }

        //json解析-正线火灾-手动按钮
        List<PageData> manualButton = manualButton(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(manualButton)) {
            System.out.println("手动按钮:" + manualButton);
        }

        //json解析-正线火灾-声光报警器
        List<PageData> audibleVisualAlarm = audibleVisualAlarm(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(audibleVisualAlarm)) {
            System.out.println("声光报警器:" + audibleVisualAlarm);
        }

        //json解析-正线火灾-放气指示灯
        List<PageData> bleedIndicatorLight = bleedIndicatorLight(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(bleedIndicatorLight)) {
            System.out.println("放气指示灯:" + bleedIndicatorLight);
        }

        //json解析-正线火灾-温感
        List<PageData> warmthSensation = warmthSensation(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(warmthSensation)) {
            System.out.println("温感:" + warmthSensation);
        }

        //json解析-正线火灾-烟感
        List<PageData> smokeDetector = smokeDetector(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(smokeDetector)) {
            System.out.println("烟感:" + smokeDetector);
        }

        //json解析-正线火灾-图像型火灾探测器
        PageData imageTypeFireDetector = imageTypeFireDetector(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(imageTypeFireDetector)) {
            List<PageData> imgList = (List<PageData>) imageTypeFireDetector.get("imgList");
            List<PageData> list = (List<PageData>) imageTypeFireDetector.get("list");
            if (CollectionUtil.isNotEmpty(list)) {
                System.out.println("图像型火灾探测器:" + list);
            }
            if (CollectionUtil.isNotEmpty(imgList)) {
                System.out.println("图片:" + imgList);
            }
        }

        //json解析-正线火灾-人体识别传感器
        List<PageData> humanBodyRecognitionSensor = humanBodyRecognitionSensor(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(humanBodyRecognitionSensor)) {
            System.out.println("人体识别传感器:" + humanBodyRecognitionSensor);
        }

        //json解析-正线火灾-语音提示器
        List<PageData> voiceReminder = voiceReminder(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(voiceReminder)) {
            System.out.println("语音提示器:" + voiceReminder);
        }

        //json解析-正线火灾-风阀
        //json解析-正线火灾-风机

        return result;
    }

    public static PageData jsonAnalysis(PageData pageData, String batchCode) throws Exception {
        PageData result = new PageData();


        //json解析-正线火灾-气体灭火设备管理机
        List<PageData> managementMachine = managementMachine(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(managementMachine)) {
            deviceLogService.insertGasFireExtinguishingLog(managementMachine);
        }

        //json解析-正线火灾-通风设备控制箱----废弃
        /*List<PageData> ventilationEquipmentcontrolBox = ventilationEquipmentcontrolBox(pageData);
        if (CollectionUtil.isNotEmpty(ventilationEquipmentcontrolBox)) {
            deviceLogService.insertManualButtonLog(ventilationEquipmentcontrolBox);
        }*/

        //json解析-正线火灾-悬挂式气体灭火装置
        List<PageData> suspendedGasFireExtinguishing = suspendedGasFireExtinguishing(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(suspendedGasFireExtinguishing)) {
            deviceLogService.insertSuspendedFireExtinguishingLog(suspendedGasFireExtinguishing);
        }

        //json解析-正线火灾-手动按钮
        List<PageData> manualButton = manualButton(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(manualButton)) {
            deviceLogService.insertManualButtonLog(manualButton);
        }

        //json解析-正线火灾-声光报警器
        List<PageData> audibleVisualAlarm = audibleVisualAlarm(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(audibleVisualAlarm)) {
            deviceLogService.insertAudibleVisualAlarmLog(audibleVisualAlarm);
        }

        //json解析-正线火灾-放气指示灯
        List<PageData> bleedIndicatorLight = bleedIndicatorLight(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(bleedIndicatorLight)) {
            deviceLogService.insertBleedIndicatorLightLog(bleedIndicatorLight);
        }

        //json解析-正线火灾-温感
        List<PageData> warmthSensation = warmthSensation(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(warmthSensation)) {
            deviceLogService.insertWarmthSensationLog(warmthSensation);
        }

        //json解析-正线火灾-烟感
        List<PageData> smokeDetector = smokeDetector(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(smokeDetector)) {
            deviceLogService.insertSmokeDetectorLog(smokeDetector);
        }

        //json解析-正线火灾-图像型火灾探测器
        PageData imageTypeFireDetector = imageTypeFireDetector(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(imageTypeFireDetector)) {
            List<PageData> imgList = (List<PageData>) imageTypeFireDetector.get("imgList");
            List<PageData> list = (List<PageData>) imageTypeFireDetector.get("list");
            if (CollectionUtil.isNotEmpty(list)) {
                deviceLogService.insertImageTypeFireDetectorLog(list);
            }
            if (CollectionUtil.isNotEmpty(imgList)) {
                deviceLogService.insertImg(imgList);
            }
        }

        //json解析-正线火灾-人体识别传感器
        List<PageData> humanBodyRecognitionSensor = humanBodyRecognitionSensor(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(humanBodyRecognitionSensor)) {
            deviceLogService.insertHumanBodyRecognitionSensorLog(humanBodyRecognitionSensor);
        }

        //json解析-正线火灾-语音提示器
        List<PageData> voiceReminder = voiceReminder(pageData, batchCode);
        if (CollectionUtil.isNotEmpty(voiceReminder)) {
            result.put("voiceReminder", voiceReminder);
            deviceLogService.insertVoiceReminderLog(voiceReminder);
        }

        //json解析-正线火灾-风阀
        //json解析-正线火灾-风机

        return result;
    }

    //json解析-正线火灾-气体灭火设备管理机
    private static List<PageData> managementMachine(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                PageData info = new PageData();
                info.put("id", UuidUtil.get32UUID());
                info.put("code", data.getString("code"));
                info.put("state", data.getString("state"));
                info.put("isAbnormal", "0");
                info.put("warningLevel", data.getString("wl"));
                info.put("isDelete", 0);
                info.put("createTime", DateUtils.getTime());
                info.put("updateTime", DateUtils.getTime());
                info.put("batchCode", batchCode);
                list.add(info);
            }
        }
        return list;
    }

    //json解析-正线火灾-通风设备控制箱------废弃
    /*private static List<PageData> ventilationEquipmentcontrolBox(PageData pageData) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String vecbStr = JSONObject.toJSONString(gfec.get("vecb"));
        List<PageData> vecb = JSONObject.parseArray(vecbStr, PageData.class);

        if (CollectionUtil.isNotEmpty(vecb)) {
            for (PageData data : vecb) {
                PageData info = new PageData();
                info.put("id", UuidUtil.get32UUID());
                info.put("deviceCode", data.getString("code"));
                info.put("deviceState", data.getString("state"));
                info.put("isDelete", 0);
                info.put("createTime", DateUtils.getTime());
                info.put("updateTime", DateUtils.getTime());
                list.add(info);
            }
        }


        return list;
    }*/

    //json解析-正线火灾-悬挂式气体灭火装置
    private static List<PageData> suspendedGasFireExtinguishing(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String feStr = JSONObject.toJSONString(data.get("fe"));
                List<PageData> fe = JSONObject.parseArray(feStr, PageData.class);
                if (CollectionUtil.isNotEmpty(fe)) {
                    for (PageData da : fe) {
                        PageData info = new PageData();
                        info.put("id", UuidUtil.get32UUID());
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);
                    }
                }
            }
        }


        return list;
    }

    //json解析-正线火灾-手动按钮
    private static List<PageData> manualButton(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String mbStr = JSONObject.toJSONString(data.get("mb"));
                List<PageData> mb = JSONObject.parseArray(mbStr, PageData.class);
                if (CollectionUtil.isNotEmpty(mb)) {
                    for (PageData da : mb) {
                        PageData info = new PageData();
                        info.put("id", UuidUtil.get32UUID());
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);
                    }
                }
            }
        }


        return list;
    }

    //json解析-正线火灾-声光报警器
    private static List<PageData> audibleVisualAlarm(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String avaStr = JSONObject.toJSONString(data.get("ava"));
                List<PageData> ava = JSONObject.parseArray(avaStr, PageData.class);
                if (CollectionUtil.isNotEmpty(ava)) {
                    for (PageData da : ava) {
                        PageData info = new PageData();
                        info.put("id", UuidUtil.get32UUID());
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);
                    }
                }
            }
        }


        return list;
    }

    //json解析-正线火灾-放气指示灯
    private static List<PageData> bleedIndicatorLight(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);


        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String bilStr = JSONObject.toJSONString(data.get("bil"));
                List<PageData> bil = JSONObject.parseArray(bilStr, PageData.class);
                if (CollectionUtil.isNotEmpty(bil)) {
                    for (PageData da : bil) {
                        PageData info = new PageData();
                        info.put("id", UuidUtil.get32UUID());
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);
                    }
                }
            }
        }
        return list;
    }

    //json解析-正线火灾-温感
    private static List<PageData> warmthSensation(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String wsStr = JSONObject.toJSONString(data.get("ws"));
                List<PageData> ws = JSONObject.parseArray(wsStr, PageData.class);
                if (CollectionUtil.isNotEmpty(ws)) {
                    for (PageData da : ws) {
                        PageData info = new PageData();
                        info.put("id", UuidUtil.get32UUID());
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("temperatureValue", da.getString("tv"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);
                    }
                }
            }
        }


        return list;
    }

    //json解析-正线火灾-烟感
    private static List<PageData> smokeDetector(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String sdStr = JSONObject.toJSONString(data.get("sd"));
                List<PageData> sd = JSONObject.parseArray(sdStr, PageData.class);
                if (CollectionUtil.isNotEmpty(sd)) {
                    for (PageData da : sd) {
                        PageData info = new PageData();
                        info.put("id", UuidUtil.get32UUID());
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("concentration", da.getString("sd"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);
                    }
                }
            }
        }


        return list;
    }

    //json解析-正线火灾-图像型火灾探测器
    private static PageData imageTypeFireDetector(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> imgList = new ArrayList<PageData>();
        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String cameraStr = JSONObject.toJSONString(data.get("camera"));
                List<PageData> camera = JSONObject.parseArray(cameraStr, PageData.class);
                if (CollectionUtil.isNotEmpty(camera)) {
                    for (PageData da : camera) {
                        PageData info = new PageData();
                        String id = UuidUtil.get32UUID();
                        info.put("id", id);
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);

                        String imgStr = JSONObject.toJSONString(da.get("img"));
                        List<PageData> img = JSONObject.parseArray(imgStr, PageData.class);
                        if (CollectionUtil.isNotEmpty(img)) {
                            for (PageData i : img) {
                                String imgUrl = i.getString("url");
                                if (StringUtils.isNotBlank(imgUrl)) {
                                    PageData imgInfo = new PageData();
                                    imgInfo.put("id", UuidUtil.get32UUID());
                                    imgInfo.put("imgType", "0");
                                    imgInfo.put("source", "0");
                                    imgInfo.put("deviceType", "0");
                                    imgInfo.put("deviceId", id);
                                    imgInfo.put("deviceCode", codeStr + "_" + da.getString("code"));
                                    imgInfo.put("url", i.getString("url"));
                                    imgInfo.put("captureTime", i.getString("time"));
                                    imgInfo.put("isDelete", 0);
                                    imgInfo.put("createTime", DateUtils.getTime());
                                    imgInfo.put("updateTime", DateUtils.getTime());
                                    imgList.add(imgInfo);
                                }
                            }

                        }
                    }
                }
            }
        }

        result.put("imgList", imgList);
        result.put("list", list);
        return result;
    }

    //json解析-正线火灾-人体识别传感器
    private static List<PageData> humanBodyRecognitionSensor(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String hbrStr = JSONObject.toJSONString(data.get("hbr"));
                List<PageData> hbr = JSONObject.parseArray(hbrStr, PageData.class);
                if (CollectionUtil.isNotEmpty(hbr)) {
                    for (PageData da : hbr) {
                        PageData info = new PageData();
                        info.put("id", UuidUtil.get32UUID());
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);
                    }
                }
            }
        }


        return list;
    }

    //json解析-正线火灾-语音提示器
    private static List<PageData> voiceReminder(PageData pageData, String batchCode) {
        List<PageData> list = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String vpStr = JSONObject.toJSONString(data.get("vp"));
                List<PageData> vp = JSONObject.parseArray(vpStr, PageData.class);
                if (CollectionUtil.isNotEmpty(vp)) {
                    for (PageData da : vp) {
                        PageData info = new PageData();
                        info.put("id", UuidUtil.get32UUID());
                        info.put("code", codeStr + "_" + da.getString("code"));
                        info.put("address", da.getString("code"));
                        info.put("state", da.getString("state"));
                        info.put("isAbnormal", da.getString("ia"));
                        info.put("playContent", da.getString("pc"));
                        info.put("isDelete", 0);
                        info.put("createTime", DateUtils.getTime());
                        info.put("updateTime", DateUtils.getTime());
                        info.put("batchCode", batchCode);
                        list.add(info);
                    }
                }
            }
        }


        return list;
    }

    //json解析-正线火灾-风阀
    //json解析-正线火灾-风机


    static String jsonStr = "{\n" +
            "    \"gfec\":{\n" +
            "        \"gfe\":[\n" +
            "            {\n" +
            "                \"code\":\"JF4TFG4365TGER\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ws\":[\n" +
            "                    {\n" +
            "                        \"code\":\"0\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"tv\":\"37\",\n" +
            "                        \"ia\":\"0\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"sd\":[\n" +
            "                    {\n" +
            "                        \"code\":\"0\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"sd\":\"10\",\n" +
            "                        \"ia\":\"0\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"camera\":[\n" +
            "                    {\n" +
            "                        \"code\":\"K545G54ETE5TET4E4F3\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"0\",\n" +
            "                        \"img\":[\n" +
            "                            {\n" +
            "                                \"url\":\"https://lmg.jj20.com/up/allimg/4k/s/02/2109250006343S5-0-lp.jpg\",\n" +
            "                                \"time\":\"2023-08-17 14:11:30\"\n" +
            "                            }\n" +
            "                        ]\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"hbr\":[\n" +
            "                    {\n" +
            "                        \"code\":\"L465FF34HY3H3B43S\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"0\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"vp\":[\n" +
            "                    {\n" +
            "                        \"code\":\"AT65TTE4STW57Y6YHY6J\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"0\",\n" +
            "                        \"pc\":\"第一条\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            }\n" +
            "        ],\n" +
            "        \"fe\":[\n" +
            "            {\n" +
            "                \"code\":\"MVCB45T4RFS5YGERW\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ia\":\"0\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"mb\":[\n" +
            "            {\n" +
            "                \"code\":\"T6UYHSFG4RFSFSFG\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ia\":\"0\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"ava\":[\n" +
            "            {\n" +
            "                \"code\":\"MCVXG43TAED34TGD4R\",\n" +
            "                \"state\":\"0 \",\n" +
            "                \"ia\":\"0\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"bil\":[\n" +
            "            {\n" +
            "                \"code\":\"LKFGJ57Y355TFE4423\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ia\":\"0\"\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "}\n";
}
