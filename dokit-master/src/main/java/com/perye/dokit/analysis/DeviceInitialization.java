package com.perye.dokit.analysis;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.UuidUtil;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 设备初始化  第一次存入数据库
 */
@Component
public class DeviceInitialization {


    @Autowired
    private DeviceService deviceService;

    private static DeviceService service;


    @PostConstruct
    public void init() {
        service = deviceService;

    }

    public static int maintenanceTime=120;//维保日期 默认120天

    public static void main(String[] args) throws Exception {

        PageData info = JSONObject.parseObject(jsonStr, PageData.class);
        jsonMain(info);

    }

    public static PageData jsonMain(PageData pageData) throws Exception {
        PageData result = new PageData();
        String batchCode = UuidUtil.get32UUID();

        //json解析-正线火灾-气体灭火设备管理机
        List<PageData> managementMachine = managementMachine(pageData, batchCode);
        return result;
    }

    public static PageData jsonAnalysis(PageData pageData, String batchCode) throws Exception {
        PageData result = new PageData();


        //json解析-正线火灾-气体灭火设备管理机
        List<PageData> managementMachine = managementMachine(pageData, batchCode);


        return result;
    }


    public static PageData toDeviceInit(List<PageData> list,String keyV) throws Exception {


        /**
         * 设备角色：0：管理机，1：温感，2：烟感，3：图像型火灾探测器，4：人体识别传感器，5：语音提示器，
         * 6：悬挂式气体灭火装置，7：手动按钮，8：声光报警器，9：放气指示灯
         *
         *
         *
         */

        PageData pdRedisGli = new PageData();
        pdRedisGli.put("gfe", "气体灭火设备管理机");


        PageData pdRedisDev = new PageData();
        pdRedisDev.put("ws", "1:温感");
        pdRedisDev.put("sd", "2:烟感");
        pdRedisDev.put("camera", "3:图像型火灾探测器");
        pdRedisDev.put("hbr", "4:人体识别传感器");
        pdRedisDev.put("vp", "5:语音提示器");
        pdRedisDev.put("fe", "6:气体灭火装置");
        pdRedisDev.put("mb", "7:手动按钮");
        pdRedisDev.put("ava", "8:声光报警器");
        pdRedisDev.put("bil", "9:放气指示灯");


        PageData pd = new PageData();

        List<PageData> deviceAddList = new ArrayList<>();
        for (PageData data : list) {

            for (Object key : data.keySet()) {
                String KeyName = key + "";
                Object value = data.get(KeyName);
                String Id = "";//管理机id

                String code = data.getString("code");
                PageData pDDevice = new PageData();
                pDDevice.put("deviceCode", code);
                //管理机
                if (value instanceof String) {
                    //进行你的逻辑处理
                    if ("code".equals(KeyName)) {


                        // List<PageData> deviceList = (List<PageData>) dao.findForList("DeviceMapper.findDevicelist", pDDevice);


                        List<PageData> deviceList = service.findDevicelist(pDDevice);
                        if (deviceList.size() == 0) {
                            Id = IdUtil.simpleUUID();
                            pDDevice.put("id", Id);
                            pDDevice.put("railwayId", "3135b93dc3394705acbb9176e699d6cb");
                            pDDevice.put("tunnelId", "ea2915dde40146f5b88f2d38f021d5d3");
                            pDDevice.put("deviceRole", 0);//设备角色：0：管理机，1：温感，2：烟感，3：图像型火灾探测器，4：人体识别传感器，5：语音提示器，6：悬挂式气体灭火装置，7：手动按钮，8：声光报警器，9：放气指示灯
                            pDDevice.put("deviceType", 2);//设备类型：0：监测设备，1：响应设备，2：其他设备
                            pDDevice.put("deviceName", pdRedisGli.get(keyV));//设备名称
                            pDDevice.put("deviceState", 0);//设备状态(0：正常状态；1：离线状态；2：故障状态；-1：停用状态)
                            pDDevice.put("devicePosition", "T100+60");//设备位置
                            pDDevice.put("deviceVersion", 1);//设备版本
                            pDDevice.put("deviceIp", "192.168.1.59");//设备 IP
                            pDDevice.put("deviceMac", "NU54Y543BT54B311");//设备 MAC 地址
                            pDDevice.put("isAbnormal", 0);//是否异常
                            /**
                             * 是否异常，不同的设备角色，代表的含义也不一样
                             * 管理机：是否异常：0：正常，1：异常
                             * 温感：是否监测到异常：0：正常，1：异常
                             * 烟感：是否喷洒灭火气体：0：未喷洒，1：已喷洒
                             * 人体识别传感器：是否有人：0：没有，1：有
                             * 语音提示器：是否正在播放：0：正常，1：没有播放
                             * 悬挂式气体灭火：是否喷洒：0：正常，1：已喷洒
                             * 手动按钮：是否被使用：0：正常，1：已被使用
                             * 声光报警器：是否报警：0：正常，1：报警中
                             * 放气指示灯：是否亮灯：0：未亮，1：亮灯中
                             */
                            pDDevice.put("mmId", 0);//上层管理机id
                            pDDevice.put("warningLevel", 0);//无预警为：0，依次递增
                            pDDevice.put("address", 0);//二总线地址. 管理器为0
                            pDDevice.put("createTime", DateUtils.getTime());
                            pDDevice.put("updateTime", DateUtils.getTime());

                            pDDevice.put("mStartDate", DateUtils.getTime());
                            pDDevice.put("mEndDate", DateUtils.increaseDays(120));

                            service.addDevice(pDDevice);
                        } else {
                            break;
                        }


                    }
                }
                //管理机下的小设备
                if (value instanceof List) {
                    List<JSONObject> sourceList = (List<JSONObject>) value;
                    for (JSONObject device : sourceList) {
                        PageData params = new PageData();
                        String[] devData = pdRedisDev.getString(KeyName).split(":");
                        params.put("id", IdUtil.simpleUUID());
                        params.put("deviceCode", code + "_" + device.get("code"));
                        params.put("railwayId", "3135b93dc3394705acbb9176e699d6cb");
                        params.put("tunnelId", "ea2915dde40146f5b88f2d38f021d5d3");
                        params.put("deviceRole", devData[0]);//设备角色：0：管理机，1：温感，2：烟感，3：图像型火灾探测器，4：人体识别传感器，5：语音提示器，6：悬挂式气体灭火装置，7：手动按钮，8：声光报警器，9：放气指示灯
                        params.put("deviceType", 0);//设备类型：0：监测设备，1：响应设备，2：其他设备
                        params.put("deviceName", devData[1]);//设备名称
                        params.put("deviceState", 0);//设备状态(0：正常状态；1：离线状态；2：故障状态；-1：停用状态)
                        params.put("devicePosition", "T100+60");//设备位置
                        params.put("deviceVersion", 1);//设备版本
                        params.put("deviceIp", "192.168.1.59");//设备 IP
                        params.put("deviceMac", "NU54Y543BT54B311");//设备 MAC 地址
                        params.put("isAbnormal", 0);//是否异常

                        /***
                         *   /**
                         *                          * 是否异常，不同的设备角色，代表的含义也不一样
                         *                          * 管理机：是否异常：0：正常，1：异
                         *
                         */

                        params.put("mmId", Id);//上层管理机id
                        params.put("warningLevel", 0);//无预警为：0，依次递增
                        params.put("address", device.get("code"));//二总线地址. 管理器为0
                        params.put("createTime", DateUtils.getTime());
                        params.put("updateTime", DateUtils.getTime());
                        params.put("mStartDate", DateUtils.getTime());
                        params.put("mEndDate", DateUtils.increaseDays(120));
                        deviceAddList.add(params);

                    }



                }

            }


        }

        if(deviceAddList.size()>0){
            service.addDeviceList(deviceAddList);
        }
        return pd;
    }

    //json解析-正线火灾-气体灭火设备管理机
    private static List<PageData> managementMachine(PageData pageData, String batchCode) throws Exception {
        List<PageData> list = new ArrayList<PageData>();



        for (Object key : pageData.keySet()) {
            System.out.println(key);

            pageData.get(key);
            JSONObject data=   (JSONObject) pageData.get(key);
            for (String keyl : data.keySet()) {
                List<PageData> gfeList = JSONObject.parseArray( data.get(keyl) + "", PageData.class);
                toDeviceInit(gfeList,keyl);


            }



        }


        return list;
    }

    static String jsonStr = "{\n" +
            "    \"gfec\":{\n" +
            "        \"gfe\":[\n" +
            "            {\n" +
            "                \"code\":\"JF4TFG4365TGER\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ia\":\"1\",\n" +
            "                \"wl\":\"3\",\n" +
            "                \"ws\":[\n" +
            "                    {\n" +
            "                        \"code\":\"1_13\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"0\",\n" +
            "                        \"tv\":\"25\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"sd\":[\n" +
            "\n" +
            "                ],\n" +
            "                \"camera\":[\n" +
            "\n" +
            "                ],\n" +
            "                \"hbr\":[\n" +
            "\n" +
            "                ],\n" +
            "                \"vp\":[\n" +
            "\n" +
            "                ],\n" +
            "                \"fe\":[\n" +
            "                    {\n" +
            "                        \"code\":\"1_11\",\n" +
            "                        \"state\":\"1\",\n" +
            "                        \"ia\":\"0\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"mb\":[\n" +
            "                    {\n" +
            "                        \"code\":\"1_7\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"1\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"ava\":[\n" +
            "                    {\n" +
            "                        \"code\":\"1_4\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"1\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"bil\":[\n" +
            "                    {\n" +
            "                        \"code\":\"1_10\",\n" +
            "                        \"state\":\"1\",\n" +
            "                        \"ia\":\"0\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "}";

    static String jsonStr2 = "{\n" +
            "    \"gfec\":{\n" +
            "        \"gfe\":[\n" +
            "            {\n" +
            "                \"code\":\"JF4TFG4365TGER\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ws\":[\n" +
            "                    {\n" +
            "                        \"code\":\"0\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"tv\":\"37\",\n" +
            "                        \"ia\":\"0\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"sd\":[\n" +
            "                    {\n" +
            "                        \"code\":\"0\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"sd\":\"10\",\n" +
            "                        \"ia\":\"0\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"camera\":[\n" +
            "                    {\n" +
            "                        \"code\":\"K545G54ETE5TET4E4F3\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"0\",\n" +
            "                        \"img\":[\n" +
            "                            {\n" +
            "                                \"url\":\"https://lmg.jj20.com/up/allimg/4k/s/02/2109250006343S5-0-lp.jpg\",\n" +
            "                                \"time\":\"2023-08-17 14:11:30\"\n" +
            "                            }\n" +
            "                        ]\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"hbr\":[\n" +
            "                    {\n" +
            "                        \"code\":\"L465FF34HY3H3B43S\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"0\"\n" +
            "                    }\n" +
            "                ],\n" +
            "                \"vp\":[\n" +
            "                    {\n" +
            "                        \"code\":\"AT65TTE4STW57Y6YHY6J\",\n" +
            "                        \"state\":\"0\",\n" +
            "                        \"ia\":\"0\",\n" +
            "                        \"pc\":\"第一条\"\n" +
            "                    }\n" +
            "                ]\n" +
            "            }\n" +
            "        ],\n" +
            "        \"fe\":[\n" +
            "            {\n" +
            "                \"code\":\"MVCB45T4RFS5YGERW\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ia\":\"0\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"mb\":[\n" +
            "            {\n" +
            "                \"code\":\"T6UYHSFG4RFSFSFG\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ia\":\"0\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"ava\":[\n" +
            "            {\n" +
            "                \"code\":\"MCVXG43TAED34TGD4R\",\n" +
            "                \"state\":\"0 \",\n" +
            "                \"ia\":\"0\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"bil\":[\n" +
            "            {\n" +
            "                \"code\":\"LKFGJ57Y355TFE4423\",\n" +
            "                \"state\":\"0\",\n" +
            "                \"ia\":\"0\"\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "}\n";
}
