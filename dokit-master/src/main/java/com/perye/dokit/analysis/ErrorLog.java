package com.perye.dokit.analysis;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.newservice.DeviceErrorLogService;
import com.perye.dokit.newservice.DeviceLogService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.RedisUtils;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.UuidUtil;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.websocket.MsgType;
import com.perye.dokit.websocket.SocketMsg;
import com.perye.dokit.websocket.WebSocketServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ErrorLog {

    @Autowired
    private transient DeviceErrorLogService service;

    @Autowired
    private transient RedisUtils redis;

    @Autowired
    private WebSocketServer webSocket;

    private static DeviceErrorLogService deviceErrorLogService;

    private static RedisUtils redisUtils;

    private static WebSocketServer webSocketServer;

    @PostConstruct
    public void init() {
        deviceErrorLogService = service;
        redisUtils = redis;
        webSocketServer = webSocket;
    }


    public static PageData jsonAnalysis(PageData pageData, String batchCode) throws Exception {
        PageData result = new PageData();

        List<PageData> updateDevList = new ArrayList<PageData>();

        //json解析-正线火灾-气体灭火设备管理机
        PageData managementMachineInfo = managementMachine(pageData, batchCode);
        List<PageData> managementMachine = (List<PageData>) managementMachineInfo.get("list");
        if (CollectionUtil.isNotEmpty(managementMachine)) {
            deviceErrorLogService.insertGasFireExtinguishingLog(managementMachine);
        }
        List<PageData> mmList = (List<PageData>) managementMachineInfo.get("devList");
        updateDevList.addAll(mmList);

        List<PageData> wlList = (List<PageData>) managementMachineInfo.get("wlList");
        if (CollectionUtil.isNotEmpty(wlList)) {
            deviceErrorLogService.insertWarningLevelLog(wlList);
        }

        //json解析-正线火灾-悬挂式气体灭火装置
        PageData suspendedGasFireExtinguishingInfo = suspendedGasFireExtinguishing(pageData, batchCode);
        List<PageData> suspendedGasFireExtinguishing = (List<PageData>) suspendedGasFireExtinguishingInfo.get("list");
        if (CollectionUtil.isNotEmpty(suspendedGasFireExtinguishing)) {
            deviceErrorLogService.insertSuspendedFireExtinguishingLog(suspendedGasFireExtinguishing);
        }
        List<PageData> sgList = (List<PageData>) suspendedGasFireExtinguishingInfo.get("devList");
        updateDevList.addAll(sgList);


        //json解析-正线火灾-手动按钮
        PageData manualButtonInfo = manualButton(pageData, batchCode);
        List<PageData> manualButton = (List<PageData>) manualButtonInfo.get("list");
        if (CollectionUtil.isNotEmpty(manualButton)) {
            deviceErrorLogService.insertManualButtonLog(manualButton);
        }
        List<PageData> mbList = (List<PageData>) manualButtonInfo.get("devList");
        updateDevList.addAll(mbList);

        //json解析-正线火灾-声光报警器
        PageData audibleVisualAlarmInfo = audibleVisualAlarm(pageData, batchCode);
        List<PageData> audibleVisualAlarm = (List<PageData>) audibleVisualAlarmInfo.get("list");
        if (CollectionUtil.isNotEmpty(audibleVisualAlarm)) {
            deviceErrorLogService.insertAudibleVisualAlarmLog(audibleVisualAlarm);
        }
        List<PageData> avList = (List<PageData>) audibleVisualAlarmInfo.get("devList");
        updateDevList.addAll(avList);

        //json解析-正线火灾-放气指示灯
        PageData bleedIndicatorLightInfo = bleedIndicatorLight(pageData, batchCode);
        List<PageData> bleedIndicatorLight = (List<PageData>) bleedIndicatorLightInfo.get("list");
        if (CollectionUtil.isNotEmpty(bleedIndicatorLight)) {
            deviceErrorLogService.insertBleedIndicatorLightLog(bleedIndicatorLight);
        }
        List<PageData> biList = (List<PageData>) bleedIndicatorLightInfo.get("devList");
        updateDevList.addAll(biList);

        //json解析-正线火灾-温感
        PageData warmthSensationInfo = warmthSensation(pageData, batchCode);
        List<PageData> warmthSensation = (List<PageData>) warmthSensationInfo.get("list");
        if (CollectionUtil.isNotEmpty(warmthSensation)) {
            deviceErrorLogService.insertWarmthSensationLog(warmthSensation);
        }
        List<PageData> wsList = (List<PageData>) warmthSensationInfo.get("devList");
        updateDevList.addAll(wsList);

        //json解析-正线火灾-烟感
        PageData smokeDetectorInfo = smokeDetector(pageData, batchCode);
        List<PageData> smokeDetector = (List<PageData>) smokeDetectorInfo.get("list");
        if (CollectionUtil.isNotEmpty(smokeDetector)) {
            deviceErrorLogService.insertSmokeDetectorLog(smokeDetector);
        }
        List<PageData> sdList = (List<PageData>) smokeDetectorInfo.get("devList");
        updateDevList.addAll(sdList);

        //json解析-正线火灾-图像型火灾探测器
        PageData imageTypeFireDetector = imageTypeFireDetector(pageData, batchCode);
        List<PageData> imgList = new ArrayList<PageData>();
        Map<String, List<PageData>> imgMap = new HashMap<String, List<PageData>>();
        if (CollectionUtil.isNotEmpty(imageTypeFireDetector)) {
            imgList = (List<PageData>) imageTypeFireDetector.get("imgList");
            List<PageData> list = (List<PageData>) imageTypeFireDetector.get("list");
            List<PageData> imgDevList = (List<PageData>) imageTypeFireDetector.get("devList");
            imgMap = (Map<String, List<PageData>>) imageTypeFireDetector.get("imgMap");
            if (CollectionUtil.isNotEmpty(list)) {
                deviceErrorLogService.insertImageTypeFireDetectorLog(list);
            }
            if (CollectionUtil.isNotEmpty(imgList)) {
                deviceErrorLogService.insertImg(imgList);
            }
            updateDevList.addAll(imgDevList);
        }

        //json解析-正线火灾-人体识别传感器
        PageData humanBodyRecognitionSensorInfo = humanBodyRecognitionSensor(pageData, batchCode);
        List<PageData> humanBodyRecognitionSensor = (List<PageData>) humanBodyRecognitionSensorInfo.get("list");
        if (CollectionUtil.isNotEmpty(humanBodyRecognitionSensor)) {
            deviceErrorLogService.insertHumanBodyRecognitionSensorLog(humanBodyRecognitionSensor);
        }
        List<PageData> hbList = (List<PageData>) humanBodyRecognitionSensorInfo.get("devList");
        updateDevList.addAll(hbList);

        //json解析-正线火灾-语音提示器
        PageData voiceReminderInfo = voiceReminder(pageData, batchCode);
        List<PageData> voiceReminder = (List<PageData>) voiceReminderInfo.get("list");
        if (CollectionUtil.isNotEmpty(voiceReminder)) {
            result.put("voiceReminder", voiceReminder);
            deviceErrorLogService.insertVoiceReminderLog(voiceReminder);
        }
        List<PageData> vrList = (List<PageData>) voiceReminderInfo.get("devList");
        updateDevList.addAll(vrList);


        if (CollectionUtil.isNotEmpty(updateDevList)) {
            deviceErrorLogService.updateDeviceList(updateDevList);
        }

        List<PageData> redisList = new ArrayList<PageData>();
        List<String> keyList = redisUtils.scan("runStatusLog:changeState:*");
        if (CollectionUtil.isNotEmpty(keyList)) {
            Map<String, PageData> deviceMap = deviceErrorLogService.queryDeviceMap(new PageData());
            for (String key : keyList) {
                String state = (String) redisUtils.get(key);
                String[] keyArr = key.split(":");
                String devCode = keyArr[keyArr.length - 1];
                PageData info = deviceMap.get(devCode);
                if (CollectionUtil.isEmpty(info)) {
                    continue;
                }
                info.put("state", state);
                info.put("createTime", DateUtils.getTime());
                List<PageData> ll = imgMap.get(devCode);
                if (CollectionUtil.isEmpty(ll)) {
                    ll = new ArrayList<PageData>();
                }
                info.put("imgList", ll);


                redisList.add(info);
                redisUtils.del(key);
            }


            if (CollectionUtil.isNotEmpty(wlList)) {

                for (PageData wl : wlList) {
                    PageData webInfo = new PageData();
                    webInfo.put("type", "1");
                    PageData data = new PageData();
                    data.put("state", wl.getString("warningLevel"));
                    if (CollectionUtil.isNotEmpty(imgList)) {
                        data.put("imgUrl", imgList);
                    } else {
                        data.put("imgUrl", "");
                    }

                    webInfo.put("data", data);
                    String stringJSON = JSON.toJSONString(webInfo);
                    SocketMsg socketMsg = new SocketMsg(stringJSON, MsgType.CONNECT);
                    webSocketServer.sendInfo(socketMsg, "3dThree");
                }
            }


            PageData webInfo = new PageData();
            webInfo.put("type", "2");
            webInfo.put("data", redisList);
            if (CollectionUtil.isNotEmpty(imgList)) {
                webInfo.put("imgUrl", imgList);
            } else {
                webInfo.put("imgUrl", "");
            }
            String stringJSON = JSON.toJSONString(webInfo);
            SocketMsg socketMsg = new SocketMsg(stringJSON, MsgType.CONNECT);
            webSocketServer.sendInfo(socketMsg, "3dThree");

            PageData info = new PageData();
            info.put("type", "3");
            PageData datas = deviceMap.get("errorNum");
            info.put("data", datas);
            String sj = JSON.toJSONString(info);
            SocketMsg sm = new SocketMsg(sj, MsgType.CONNECT);
            webSocketServer.sendInfo(sm, "3dThree");
        }
        return result;
    }

    //json解析-正线火灾-气体灭火设备管理机
    private static PageData managementMachine(PageData pageData, String batchCode) throws Exception {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();
        List<PageData> wlList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                PageData dev = new PageData();

                String state = data.getString("state");
                String ia = data.getString("ia");
                //判断是否有预警
                String warningLevel = data.getString("wl");
                if (StringUtils.isNotBlank(warningLevel)) {
                    PageData wlInfo = new PageData();
                    wlInfo.put("id", UuidUtil.get32UUID());


                    if (StringUtils.isNotBlank(data.getString("wl"))) {
                        if ("0".equals(data.getString("wl"))) {
                            wlInfo.put("warningState", "0");
                        } else {
                            wlInfo.put("warningState", "1");
                        }
                    }

                    String code = data.getString("code");
                    String value = (String) redisUtils.get("warningLevel:" + code);

                    if (StringUtils.isBlank(value)) {
                        //查询当前管理机的预警值
                        PageData device = new PageData();
                        device.put("code", code);
                        PageData extinguishingInfo = deviceErrorLogService.queryExtinguishingInfo(device);

                        //判断查询出来的预警值与当前的预警值是否一样
                        if (CollectionUtil.isNotEmpty(extinguishingInfo)) {
                            value = extinguishingInfo.getString("warningLevel");
                            if (StringUtils.isBlank(value)) {
                                value = "0";
                            }
                        }
                    }

                    if (!StringUtils.equals(value, warningLevel)) {
                        dev.put("code", data.getString("code"));
                        dev.put("warningLevel", warningLevel);
                        wlInfo.put("code", data.getString("code"));
                        wlInfo.put("warningLevel", warningLevel);
                        wlInfo.put("isDelete", 0);
                        wlInfo.put("createTime", DateUtils.getTime());
                        wlInfo.put("updateTime", DateUtils.getTime());
                        wlList.add(wlInfo);
                    }

                    redisUtils.set("warningLevel:" + code, warningLevel);
                }

                //判断是否有异常
                String num = "0";
                if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                    num = "1";
                    PageData info = new PageData();
                    info.put("id", UuidUtil.get32UUID());
                    info.put("code", data.getString("code"));
                    info.put("state", data.getString("state"));
                    info.put("isAbnormal", data.getString("ia"));
                    info.put("warningLevel", data.getString("wl"));
                    info.put("isDelete", 0);
                    info.put("createTime", DateUtils.getTime());
                    info.put("updateTime", DateUtils.getTime());
                    info.put("batchCode", batchCode);
                    list.add(info);
                }

                PageData info = recordRedis(data.getString("code"), data.getString("code"), num, data.getString("state"), data.getString("ia"));
                if (CollectionUtil.isNotEmpty(info)) {
                    dev.put("code", info.getString("code"));
                    dev.put("state", info.getString("state"));
                    dev.put("isAbnormal", info.getString("isAbnormal"));
                }

                if (CollectionUtil.isNotEmpty(dev)) {
                    devList.add(dev);
                }
            }
        }
        result.put("list", list);
        result.put("wlList", wlList);
        result.put("devList", devList);
        return result;
    }

    //json解析-正线火灾-悬挂式气体灭火装置
    private static PageData suspendedGasFireExtinguishing(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);


        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String feStr = JSONObject.toJSONString(data.get("fe"));
                List<PageData> fe = JSONObject.parseArray(feStr, PageData.class);
                if (CollectionUtil.isNotEmpty(fe)) {
                    for (PageData da : fe) {
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            info.put("id", UuidUtil.get32UUID());
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);
                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }
                    }
                }
            }
        }


        result.put("list", list);
        result.put("devList", devList);
        return result;
    }

    //json解析-正线火灾-手动按钮
    private static PageData manualButton(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);


        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String mbStr = JSONObject.toJSONString(data.get("mb"));
                List<PageData> mb = JSONObject.parseArray(mbStr, PageData.class);
                if (CollectionUtil.isNotEmpty(mb)) {
                    for (PageData da : mb) {
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            info.put("id", UuidUtil.get32UUID());
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);
                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }

                    }
                }
            }
        }


        result.put("list", list);
        result.put("devList", devList);
        return result;
    }

    //json解析-正线火灾-声光报警器
    private static PageData audibleVisualAlarm(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);


        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String avaStr = JSONObject.toJSONString(data.get("ava"));
                List<PageData> ava = JSONObject.parseArray(avaStr, PageData.class);
                if (CollectionUtil.isNotEmpty(ava)) {
                    for (PageData da : ava) {
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            info.put("id", UuidUtil.get32UUID());
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);
                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }

                    }
                }
            }
        }


        result.put("list", list);
        result.put("devList", devList);
        return result;
    }

    //json解析-正线火灾-放气指示灯
    private static PageData bleedIndicatorLight(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String bilStr = JSONObject.toJSONString(data.get("bil"));
                List<PageData> bil = JSONObject.parseArray(bilStr, PageData.class);
                if (CollectionUtil.isNotEmpty(bil)) {
                    for (PageData da : bil) {
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            info.put("id", UuidUtil.get32UUID());
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);
                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }

                    }
                }
            }
        }


        result.put("list", list);
        result.put("devList", devList);
        return result;
    }

    //json解析-正线火灾-温感
    private static PageData warmthSensation(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String wsStr = JSONObject.toJSONString(data.get("ws"));
                List<PageData> ws = JSONObject.parseArray(wsStr, PageData.class);
                if (CollectionUtil.isNotEmpty(ws)) {
                    for (PageData da : ws) {
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            info.put("id", UuidUtil.get32UUID());
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("temperatureValue", da.getString("tv"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);

                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }

                    }
                }
            }
        }


        result.put("list", list);
        result.put("devList", devList);
        return result;
    }

    //json解析-正线火灾-烟感
    private static PageData smokeDetector(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String sdStr = JSONObject.toJSONString(data.get("sd"));
                List<PageData> sd = JSONObject.parseArray(sdStr, PageData.class);
                if (CollectionUtil.isNotEmpty(sd)) {
                    for (PageData da : sd) {
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            info.put("id", UuidUtil.get32UUID());
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("concentration", da.getString("sd"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);
                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }

                    }
                }
            }
        }


        result.put("list", list);
        result.put("devList", devList);
        return result;
    }

    //json解析-正线火灾-图像型火灾探测器
    private static PageData imageTypeFireDetector(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();
        List<PageData> imgList = new ArrayList<PageData>();
        Map<String, List<PageData>> imgMap = new HashMap<String, List<PageData>>();
        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String cameraStr = JSONObject.toJSONString(data.get("camera"));
                List<PageData> camera = JSONObject.parseArray(cameraStr, PageData.class);
                if (CollectionUtil.isNotEmpty(camera)) {
                    for (PageData da : camera) {
                        String codeStr = data.getString("code");
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            String id = UuidUtil.get32UUID();
                            info.put("id", id);
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);

                            String imgStr = JSONObject.toJSONString(da.get("img"));
                            List<PageData> img = JSONObject.parseArray(imgStr, PageData.class);
                            if (CollectionUtil.isNotEmpty(img)) {
                                List<PageData> ll = new ArrayList<PageData>();
                                for (PageData i : img) {
                                    String imgUrl = i.getString("url");
                                    if (StringUtils.isNotBlank(imgUrl)) {
                                        PageData imgInfo = new PageData();
                                        imgInfo.put("id", UuidUtil.get32UUID());
                                        imgInfo.put("imgType", "0");
                                        imgInfo.put("deviceType", "0");
                                        imgInfo.put("source", "1");
                                        imgInfo.put("deviceId", id);
                                        imgInfo.put("deviceCode", codeStr + "_" + da.getString("code"));
                                        imgInfo.put("url", i.getString("url"));
                                        imgInfo.put("captureTime", i.getString("time"));
                                        imgInfo.put("isDelete", 0);
                                        imgInfo.put("createTime", DateUtils.getTime());
                                        imgInfo.put("updateTime", DateUtils.getTime());
                                        imgList.add(imgInfo);
                                        PageData ii = new PageData();
                                        ii.put("imgUrl", imgUrl);
                                        ii.put("createTime", i.getString("time"));
                                        ll.add(ii);
                                    }
                                }
                                imgMap.put(codeStr + "_" + da.getString("code"), ll);
                            }
                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }

                    }
                }
            }
        }

        result.put("imgList", imgList);
        result.put("devList", devList);
        result.put("list", list);
        result.put("imgMap", imgMap);
        return result;
    }

    //json解析-正线火灾-人体识别传感器
    private static PageData humanBodyRecognitionSensor(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String hbrStr = JSONObject.toJSONString(data.get("hbr"));
                List<PageData> hbr = JSONObject.parseArray(hbrStr, PageData.class);
                if (CollectionUtil.isNotEmpty(hbr)) {
                    for (PageData da : hbr) {
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            info.put("id", UuidUtil.get32UUID());
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);
                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }

                    }
                }
            }
        }

        result.put("list", list);
        result.put("devList", devList);
        return result;
    }

    //json解析-正线火灾-语音提示器
    private static PageData voiceReminder(PageData pageData, String batchCode) {
        PageData result = new PageData();
        List<PageData> list = new ArrayList<PageData>();
        List<PageData> devList = new ArrayList<PageData>();

        //解析控制器
        String gfecStr = JSONObject.toJSONString(pageData.get("gfec"));
        PageData gfec = JSONObject.parseObject(gfecStr, PageData.class);

        String gfeStr = JSONObject.toJSONString(gfec.get("gfe"));
        List<PageData> gfe = JSONObject.parseArray(gfeStr, PageData.class);

        if (CollectionUtil.isNotEmpty(gfe)) {
            for (PageData data : gfe) {
                String codeStr = data.getString("code");
                String vpStr = JSONObject.toJSONString(data.get("vp"));
                List<PageData> vp = JSONObject.parseArray(vpStr, PageData.class);
                if (CollectionUtil.isNotEmpty(vp)) {
                    for (PageData da : vp) {
                        String state = da.getString("state");
                        String ia = da.getString("ia");
                        String num = "0";
                        if (!StringUtils.equals("0", state) || !StringUtils.equals("0", ia)) {
                            num = "1";
                            PageData info = new PageData();
                            info.put("id", UuidUtil.get32UUID());
                            info.put("code", codeStr + "_" + da.getString("code"));
                            info.put("address", da.getString("code"));
                            info.put("state", da.getString("state"));
                            info.put("isAbnormal", da.getString("ia"));
                            info.put("playContent", da.getString("pc"));
                            info.put("isDelete", 0);
                            info.put("createTime", DateUtils.getTime());
                            info.put("updateTime", DateUtils.getTime());
                            info.put("batchCode", batchCode);
                            list.add(info);
                        }

                        PageData dev = recordRedis(data.getString("code"), da.getString("code"), num, da.getString("state"), da.getString("ia"));
                        if (CollectionUtil.isNotEmpty(dev)) {
                            devList.add(dev);
                        }
                    }
                }
            }
        }


        result.put("list", list);
        result.put("devList", devList);
        return result;
    }


    private static PageData recordRedis(String supCode, String code, String num, String state, String ia) {
        if (!supCode.equals(code)) {
            code = supCode + "_" + code;
        }
        PageData dev = new PageData();
        String key = "runStatusLog:recordData:" + supCode + ":" + code;
        String value = (String) redisUtils.get(key);

        dev.put("code", code);
        dev.put("state", state);
        dev.put("isAbnormal", ia);

        if (StringUtils.isNotBlank(value)) {
            if (num.equals(value)) {
                return dev;
            } else {
                String changeState = (String) redisUtils.get("runStatusLog:changeState:" + code);
                if (StringUtils.isNotBlank(changeState)) {
                    redisUtils.del("runStatusLog:changeState:" + code);
                }
                redisUtils.set("runStatusLog:changeState:" + code, num);
                redisUtils.del(key);
            }
        } else {
            if ("1".equals(num)) {
                String changeState = (String) redisUtils.get("runStatusLog:changeState:" + code);
                if (StringUtils.isNotBlank(changeState)) {
                    redisUtils.del("runStatusLog:changeState:" + code);
                }
                redisUtils.set("runStatusLog:changeState:" + code, num);
            }
        }
        redisUtils.set(key, num);


        return dev;
    }
}
