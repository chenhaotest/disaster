package com.perye.dokit.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author libo
* @date 2024-03-28
*/
@Getter
@Setter
public class FpequipmentExcel implements Serializable {


    @Excel(name = "设备编号",width = 20)
    private String assetNumber;


    @Excel(name = "设备名称",width = 20)
    private String deviceName;


    @Excel(name = "设备类型",width = 20)
    private String deviceType;


    @Excel(name = "规格型号",width = 20)
    private String specification;


    @Excel(name = "厂商",width = 20)
    private String manufacturer;


    @Excel(name = "管理部门",width = 20)
    private String department;


    @Excel(name = "使用年限",width = 20)
    private String serviceLife;


    @Excel(name = "使用情况",width = 20)
    private String usageCondition;


    @Excel(name = "购置日期",width = 20)
    private Timestamp purchaseDate;


    @Excel(name = "里程号",width = 20)
    private String mileageNumbe;


    @Excel(name = "是否可远控",width = 20)
    private String remoteControl;


    @Excel(name = "上下行线",width = 20)
    private String downLines;


    @Excel(name = "是否启用",replace = {"禁用_0", "启用_1"},width = 20 )
    private String enable;


    @Excel(name = "所属铁路",width = 20)
    private String resRailwayId;


    @Excel(name = "所属站防",width = 20)
    private String resTunnelId;


    @Excel(name = "所属区域",width = 20)
    private String resRegionId;


    @Excel(name = "所属子系统",width = 20)
    private String resSysId;


}
