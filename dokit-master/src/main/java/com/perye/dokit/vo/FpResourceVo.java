package com.perye.dokit.vo;

import com.perye.dokit.utils.tree.TreeEntity;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@Accessors(chain = true)
@Builder
public class FpResourceVo extends TreeEntity<FpResourceVo, String> implements Serializable {

    private static final long serialVersionUID = 1L;

    public String id;
    public String code;
    public String name;
    public String startPosition;
    public String endPosition;
    public String totalLength;
    public String downLines;
    public String downLinesStr;
    public String parentId;
    public String oldParentId;
    public String treeGrade;
    public String treePath;
    public String type;
    public String typeStr;
    public String subSystemType;
    public String subSystemTypeStr;
    public int isDelete;
    public String createId;
    public Timestamp createTime;
    public Timestamp updateTime;


}
