package com.perye.dokit.config.mqtt.util;


/**
 * @author gaoyf
 * @since 2020/06/14 0014 14:34
 * 数据包
 */
public class DataPacket {
    /**
     * 网关唯一序列号(8B)
     */
    private String mac = "";
    /**
     * 时间戳(4B)
     */
    private int timestamp;
    /**
     * 命令(1B)
     */
    private byte command;
    /**
     * 长度(1B)
     */
    private int length;
    /**
     * 数据
     */
    private byte[] data = new byte[0];
    /**
     * CRC8(1B)
     */
    private byte crc8;
    /**
     * CRC8校验结果
     */
    private boolean checkFlag;

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public byte getCommand() {
        return command;
    }

    public void setCommand(byte command) {
        this.command = command;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public byte getCrc8() {
        return crc8;
    }

    public void setCrc8(byte crc8) {
        this.crc8 = crc8;
    }

    public boolean isCheckFlag() {
        return checkFlag;
    }

    public void setCheckFlag(boolean checkFlag) {
        this.checkFlag = checkFlag;
    }

    public static DataPacket getDataPacket(byte[] data) {
        DataPacket dataPacket = new DataPacket();
        //[网关唯一序列号(8B)  时间戳(4B)  命令(1B)  长度(1B)  数据 CRC8(1B)  ]
        byte[] macBytes = new byte[DataPacketEnum.DP_GATEWAY_MAC.getLength()];
        byte[] timeBytes = new byte[DataPacketEnum.DP_TIMESTAMP.getLength()];
        System.arraycopy(data, DataPacketEnum.DP_GATEWAY_MAC.getIndex(), macBytes, 0, DataPacketEnum.DP_GATEWAY_MAC.getLength());
        dataPacket.setMac(HexStringUtil.bytesToHex(macBytes));
        System.arraycopy(data, DataPacketEnum.DP_TIMESTAMP.getIndex(), timeBytes, 0, DataPacketEnum.DP_TIMESTAMP.getLength());
        dataPacket.setTimestamp(ByteIntConverter.toIntHH(timeBytes));
        byte commandByte = data[DataPacketEnum.DP_COMMAND.getIndex()];
        dataPacket.setCommand(commandByte);
        byte lengthByte = data[DataPacketEnum.DP_LENGTH.getIndex()];
        dataPacket.setLength(ByteIntConverter.byteToInt(lengthByte));
        byte[] dataBytes = new byte[dataPacket.getLength()];
        System.arraycopy(data, DataPacketEnum.DP_DATA.getIndex(), dataBytes, 0, dataPacket.getLength());
        dataPacket.setData(dataBytes);
        byte crc8Byte = data[data.length - 1];
        dataPacket.setCrc8(crc8Byte);
        byte[] crc8Data = new byte[data.length - 1];
        System.arraycopy(data, 0, crc8Data, 0, data.length - 1);
        byte CRC8 = CRC8Util.calcCrc(crc8Data);
        dataPacket.setCheckFlag(crc8Byte == CRC8);
        return dataPacket;
    }
}
