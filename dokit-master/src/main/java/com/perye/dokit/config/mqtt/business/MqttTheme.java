package com.perye.dokit.config.mqtt.business;
/**
 * @Description //TODO
 * @Date 2023/7/27 10:58
 * @Author chenhao
 **/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author chenhao
 * @date 2023/7/27   10:58
 */
public class MqttTheme {
    //消息等级，和主题数组一一对应，服务端将按照指定等级给订阅了主题的客户端推送消息
    public static int[] qosArr = {};  //主题级别
    public static String[] topicsArr = {}; //主题
    private static List<String> topicsList = new ArrayList<String>();
    private static List<Object> topicsQo = new ArrayList();

    public static String SHARED_TOPIC_PREFIX = "$share/group/";
    /**
     * 气体灭火监控系统
     * <p>
     * 气体灭火日志
     * <p>
     * 气体灭火策略下发
     */
    public static String TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_ORDINARY = "/tunnel/main_line_fire_monitoring_system/initialization";  //通电后 初始化协议
    public static String TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_LOG_ORDINARY = "/tunnel/main_line_fire_monitoring_system/log/ordinary";
    public static String TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_DISTRIBUTE_STRATEGY = "/tunnel/main_line_fire_monitoring_system/distribute/strategy";
    public static String TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_DISTRIBUTE_FIRERESPONSE = "/tunnel/main_line_fire_monitoring_system/distribute/fireresponse";
    public static String TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_DISTRIBUTE_MUTE = "/tunnel/main_line_fire_monitoring_system/distribute/mute";


    /**
     * 防灾 系统
     *
     *
     *
     */
    /**
     * 安全防护门终端数据上报
     * <p>
     * 安全防护门终端数据响应
     */
    public static String   TUNNEL_PROTECTIVE_DOOR_INTELLIGENT_MONITORING_SERVER_DATA_REPORT="/tunnel/protective_door_intelligent_monitoring/server/dataReport";
    public static String   TUNNEL_PROTECTIVE_DOOR_INTELLIGENT_MONITORING_MONITORING_CLIENT_RESPONSE="/tunnel/protective_door_intelligent_monitoring/client/";

    static {

        /**
         * 防火灾
         */
        topicsList.add(SHARED_TOPIC_PREFIX + TUNNEL_PROTECTIVE_DOOR_INTELLIGENT_MONITORING_SERVER_DATA_REPORT);//
        topicsList.add(SHARED_TOPIC_PREFIX + TUNNEL_PROTECTIVE_DOOR_INTELLIGENT_MONITORING_MONITORING_CLIENT_RESPONSE);//

        topicsList.add(SHARED_TOPIC_PREFIX + TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_ORDINARY);//通电后 初始化协议
        topicsList.add(SHARED_TOPIC_PREFIX + TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_LOG_ORDINARY);
        topicsList.add(TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_DISTRIBUTE_STRATEGY);

        topicsArr = topicsList.toArray(new String[topicsList.size()]);
        for (int i = 0; i < topicsArr.length; i++) {
            topicsQo.add(1);
        }
        Integer t1[] = topicsQo.toArray(new Integer[topicsQo.size()]);
        qosArr = Arrays.stream(t1).mapToInt(Integer::valueOf).toArray();
    }

    public static void main(String[] args) {
        System.out.println(topicsArr.length);
        System.out.println(qosArr.length);
    }
}
