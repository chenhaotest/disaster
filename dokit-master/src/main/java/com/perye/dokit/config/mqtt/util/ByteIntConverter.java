package com.perye.dokit.config.mqtt.util;

public class ByteIntConverter {

    /**
     * byte转int类型
     * 如果byte是负数，则转出的int型是正数
     *
     * @param b
     * @return
     */
    public static int byteToInt(byte b) {
        int x = b & 0xff;
        return x;
    }

    /**
     * int 类型转换为byte 类型
     * 截取int类型的最后8位,与 0xff
     *
     * @param x
     * @return
     */
    public static byte intToByte(int x) {
        return (byte) (x & 0xff);
    }

    /**
     * 把整数转换为字节数组：整数是32位，8位一个字节，依次读取8位，转化为字节数组
     * 整数与0xff,取得最后8位,生成整数,再强转为第3个byte
     * 整数右移8位,与0xff,取得倒数第二组8位,生成整数,再强转为第2个byte
     * 整数右移16位,与0xff,取得倒数第3组8位,生成整数,再强转为第1个byte
     * 整数右移24位,与0xff,取得倒数第4组8位,生成整数,再强转为第0个byte
     */
    public static byte[] intToByteArr(int x) {
        byte[] arr = new byte[4];
        arr[3] = (byte) (x & 0xff);
        arr[2] = (byte) (x >> 8 & 0xff);
        arr[1] = (byte) (x >> 16 & 0xff);
        arr[0] = (byte) (x >> 24 & 0xff);

        return arr;
    }

    public static byte[] intToByteArr2(int x) {
        byte[] arr = new byte[2];
        arr[1] = (byte) (x & 0xff);
        arr[0] = (byte) (x >> 8 & 0xff);
        return arr;
    }

    /**
     * byte数组转换为整数
     * 第0个byte与上0xff,生成整数,在右移24位，取得一个整数
     * 第1个byte与上0xff,生成整数,在右移16位，取得一个整数
     * 第2个byte与上0xff,生成整数,在右移8位，取得一个整数
     * 第3个byte与上0xff,生成整数
     * 把四个整数做或操作,转换为已整数
     */
    public static int byteArrToInt(byte[] arr) {
        int x = 0;
        int len = -1;
        if (arr.length > 4) {
            len = 4;
        } else {
            len = arr.length;
        }
        for (int i = 0; i < len; i++) {
            x |= (arr[len - i - 1] & 0xFF) << (8 * i);
        }
        return x;
    }

    /**
     * byte数组转换为整数
     * 第0个byte与上0xff,生成整数,在右移24位，取得一个整数
     * 第1个byte与上0xff,生成整数,在右移16位，取得一个整数
     * 第2个byte与上0xff,生成整数,在右移8位，取得一个整数
     * 第3个byte与上0xff,生成整数
     * 把四个整数做或操作,转换为已整数
     */
    public static int byteArrToInt(byte[] arr, int start, int length) {
        int x = 0;
        int len = -1;
        if (length > 4) {
            len = 4;
        } else {
            len = length;
        }
        for (int i = 0; i < len; i++) {
            x |= (arr[len - i - 1 + start] & 0xFF) << (8 * i);
        }
        return x;
    }

    /**
     * byte数组转换为整数
     * 第1个byte与上0xff,生成整数,在右移8位，取得一个整数
     * 第2个byte与上0xff,生成整数
     * 把2个整数做或操作,转换为已整数
     */
    public static int byteArrToInt(byte high, byte low) {
        int x = 0;
        x = ((high & 0xFF) << 8) | (low & 0xFF);
        return x;
    }

    /**
     * byte数组转换为整数(带符号)
     */
    public static int byteArrToIntSymbol(byte high, byte low) {
        int x = 0;
        x = (high << 8) | low ;
        return x;
    }

    /**
     * int 转 byte[]   低字节在前（低字节序）
     *
     * @param n
     * @return
     */
    public static byte[] toLH(int n) {
        byte[] b = new byte[4];
        b[0] = (byte) (n & 0xff);
        b[1] = (byte) (n >> 8 & 0xff);
        b[2] = (byte) (n >> 16 & 0xff);
        b[3] = (byte) (n >> 24 & 0xff);
        return b;
    }

    /**
     * int 转 byte[]   高字节在前（高字节序）
     *
     * @param n
     * @return
     */
    public static byte[] toHH(int n) {
        byte[] b = new byte[4];
        b[3] = (byte) (n & 0xff);
        b[2] = (byte) (n >> 8 & 0xff);
        b[1] = (byte) (n >> 16 & 0xff);
        b[0] = (byte) (n >> 24 & 0xff);
        return b;
    }

    /**
     * byte[] 转 int 低字节在前（低字节序）
     * @param b
     * @return
     */
    public int toIntLH(byte[] b){
        int res = 0;
        for(int i=0;i<b.length;i++){
            res += (b[i] & 0xff) << (i*8);
        }
        return res;
    }

    /**
     *  byte[] 转 int 高字节在前（高字节序）
     * @param b
     * @return
     */
    public static int toIntHH(byte[] b){
        int res = 0;
        for(int i=0;i<b.length;i++){
            res += (b[i] & 0xff) << ((3-i)*8);
        }
        return res;
    }
}
