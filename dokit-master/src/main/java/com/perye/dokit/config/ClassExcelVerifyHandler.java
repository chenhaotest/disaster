package com.perye.dokit.config;
import cn.afterturn.easypoi.excel.entity.result.ExcelVerifyHandlerResult;
import cn.afterturn.easypoi.handler.inter.IExcelVerifyHandler;
import cn.hutool.core.util.ObjectUtil;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.Map;
    /**
     * 判断easyPoi 对象中属性是否全部为null
     */

    @Component
    public class ClassExcelVerifyHandler implements IExcelVerifyHandler<Object> {
        @SneakyThrows
        @Override
        public ExcelVerifyHandlerResult verifyHandler(Object obj) {
            ExcelVerifyHandlerResult result = new ExcelVerifyHandlerResult(true);
            if (ObjectUtil.isNotNull(obj)) {
                //判断对象属性是否全部为空
                boolean b = checkFieldAllNull(obj);
                result.setSuccess(!b);
            }
            return result;
        }

        /***
         * 判断对象所有属性是否为空
         *
         * @param object 对象
         * @return boolean
         * @throws Exception
         */

        public static boolean checkFieldAllNull(Object object) throws IllegalAccessException {
            for (Field f : object.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                if (Modifier.isFinal(f.getModifiers()) && Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                if (!isEmpty(f.get(object))) {
                    return false;
                }
                f.setAccessible(false);
            }
            for (Field f : object.getClass().getFields()) {
                f.setAccessible(true);
                if (Modifier.isFinal(f.getModifiers()) && Modifier.isStatic(f.getModifiers())) {
                    continue;
                }
                if (!isEmpty(f.get(object))) {
                    return false;
                }
                f.setAccessible(false);
            }
            return true;
        }

        private static boolean isEmpty(Object object) {
            if (object == null) {
                return true;
            }
            if (object instanceof String && (object.toString().equals(""))) {
                return true;
            }
            if (object instanceof Collection && ((Collection) object).isEmpty()) {
                return true;
            }
            if (object instanceof Map && ((Map) object).isEmpty()) {
                return true;
            }
            if (object instanceof Object[] && ((Object[]) object).length == 0) {
                return true;
            }
            return false;
        }
    }


