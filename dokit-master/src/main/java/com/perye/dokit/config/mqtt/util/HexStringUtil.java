package com.perye.dokit.config.mqtt.util;

import org.apache.commons.lang3.StringUtils;

/**
 * @author gaoyf
 * @since 2020/7/6 0006 17:53
 * <p>
 * 十六进制（Hex）与byte数组之间的转换
 */
public class HexStringUtil {

    /**
     * 字节转十六进制
     *
     * @param b 需要进行转换的byte字节
     * @return 转换后的Hex字符串
     */
    public static String byteToHex(byte b) {
        String hex = Integer.toHexString(b & 0xFF);
        if (hex.length() < 2) {
            hex = "0" + hex;
        }
        return hex.toUpperCase();
    }

    /**
     * 字节数组转16进制
     *
     * @param bytes 需要转换的byte数组
     * @return 转换后的Hex字符串
     */
    public static String bytesToHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            String hex = Integer.toHexString(aByte & 0xFF);
            if (hex.length() < 2) {
                sb.append(0);
            }
            sb.append(hex);
        }
        return sb.toString().toUpperCase();
    }
    /**
     * 字节数组转16进制
     *
     * @param bytes 需要转换的byte数组
     * @return 转换后的Hex字符串
     */
    public static String bytesToHexSplit(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte aByte : bytes) {
            String hex = Integer.toHexString(aByte & 0xFF);
            if (hex.length() < 2) {
                sb.append(0);
            }
            sb.append(hex);
            sb.append("-");
        }
        return sb.toString().toUpperCase();
    }

    /**
     * Hex字符串转byte
     *
     * @param inHex 待转换的Hex字符串
     * @return 转换后的byte
     */
    public static byte hexToByte(String inHex) {
        return (byte) Integer.parseInt(inHex, 16);
    }

    /**
     * hex字符串转byte数组
     *
     * @param inHex 待转换的Hex字符串
     * @return 转换后的byte数组结果
     */
    public static byte[] hexToByteArray(String inHex) {
        int hexlen = inHex.length();
        byte[] result;
        if (hexlen % 2 == 1) {
            //奇数
            hexlen++;
            result = new byte[(hexlen / 2)];
            inHex = "0" + inHex;
        } else {
            //偶数
            result = new byte[(hexlen / 2)];
        }
        int j = 0;
        for (int i = 0; i < hexlen; i += 2) {
            result[j] = hexToByte(inHex.substring(i, i + 2));
            j++;
        }
        return result;
    }


    /**
     * <编码>
     * <数字字符串编成BCD格式字节数组>
     *
     * @param bcd 数字字符串
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static byte[] str2bcd(String bcd) {
        if (StringUtils.isEmpty(bcd)) {
            return null;
        } else {
            // 获取字节数组长度
            int size = bcd.length() / 2;
            int remainder = bcd.length() % 2;

            // 存储BCD码字节
            byte[] bcdByte = new byte[size + remainder];

            // 转BCD码
            for (int i = 0; i < size; i++) {
                int low = Integer.parseInt(bcd.substring(2 * i, 2 * i + 1));
                int high = Integer.parseInt(bcd.substring(2 * i + 1, 2 * i + 2));
                bcdByte[i] = (byte) ((high << 4) | low);
            }

            // 如果存在余数，需要填F
            if (remainder > 0) {
                int low = Integer.parseInt(bcd.substring(bcd.length() - 1));
                bcdByte[bcdByte.length - 1] = (byte) ((0xf << 4) | low);
            }

            // 返回BCD码字节数组
            return bcdByte;
        }
    }

    /**
     * <解码>
     * <BCD格式的字节数组解成数字字符串>
     *
     * @param bcd 字节数组
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String bcd2str(byte[] bcd) {
        if (null == bcd || bcd.length == 0) {
            return "";
        } else {
            // 存储转码后的字符串
            StringBuilder sb = new StringBuilder();

            // 循环数组解码
            for (int i = 0; i < bcd.length; i++) {
                // 转换低字节
                int low = (bcd[i] & 0x0f);
                sb.append(low);

                // 转换高字节
                int high = ((bcd[i] & 0xf0) >> 4);

                // 如果高字节等于0xf说明是补的字节，直接抛掉
                if (high != 0xf) {
                    sb.append(high);
                }
            }

            // 返回解码字符串
            return sb.toString();
        }
    }

}
