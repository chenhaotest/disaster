package com.perye.dokit.config.mqtt;

/**
 * @author chenhao
 * @date 2023/7/26   10:12
 * @version 消费者
 */


import com.perye.dokit.config.mqtt.business.MqttTheme;
import com.perye.dokit.config.mqtt.util.DataPacket;
import com.perye.dokit.config.mqtt.util.DataPacketEnum;
import com.perye.dokit.config.mqtt.util.HexStringUtil;
import com.perye.dokit.config.mqtt.util.ImgUtil;
import com.perye.dokit.newservice.HqPhotoService;
import com.perye.dokit.newservice.HqVideoService;
import com.perye.dokit.newservice.MqttThemeService;
import com.perye.dokit.newservice.PersionService;
import com.perye.dokit.utils.SpringContextUtil;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MqttConsumerCallBack implements MqttCallback {

    @Autowired
    private MqttThemeService mqttThemeService;
    @Autowired
    private PersionService service;

    @Autowired
    private HqPhotoService photoService;

    @Autowired
    private HqVideoService videoService;

    private String clientId;

    private MqttClient client;

    public void aaa(MqttClient client, String clientId) {
        this.client = client;
        this.clientId = clientId;
    }

    /**
     * 与服务器断开的回调
     */
    @Override
    public void connectionLost(Throwable cause) {
        System.out.println(clientId + "与服务器断开连接");
        long reconnectTimes = 1;
        while (true) {
            try {
                if (client.isConnected()) {
                    client.subscribe(MqttTheme.topicsArr, MqttTheme.qosArr);
                    System.out.println("mqtt reconnect success end");
                    break;
                }
                if (reconnectTimes == 50) {
                    //当重连次数达到10次时，就抛出异常，不在重连
                    System.out.println("mqtt reconnect error");
                    return;
                }
                System.out.println("mqtt reconnect times = {} try again...：" + reconnectTimes++);
                client.reconnect();
            } catch (Exception e) {
                System.out.println(e);
            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    /*@Override
    public void connectComplete(boolean reconnect, String serverURI) {
        try {
            //如果监测到有,号，说明要订阅多个主题

            //单主题
            client.subscribe(mqttTopic);

            log.info("----TAG", "connectComplete: 订阅主题成功");
        } catch (Exception e) {
            e.printStackTrace();
            log.info("----TAG", "error: 订阅主题失败");
        }
    }*/


    /**
     * 消息到达的回调
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {


        try {
            SpringContextUtil.getBean(MqttThemeService.class).toMqttDataTopic(topic, message);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }

    /**
     * 消息发布成功的回调
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

}


