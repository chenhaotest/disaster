package com.perye.dokit.utils.rsa_aes;


import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author
 * @program
 * @description
 * @create 2022/10/17 10:57
 **/
@Data
public class RequestApiParam {


    /**
     * 商户id (非空)
     */
    @NotBlank(message = "appid不能为空")
    private String appId;

    /**
     * 参数签名 (非空)
     */
    @NotBlank(message = "参数签名不能为空")
    private String sign;

    /**
     * 对称加密key (非空)
     */
    @NotBlank(message = "对称加密key不能为空")
    private String aseKey;

    /**
     * 时间戳，精确到毫秒 (非空)
     */
    private Long timestamp;

    /**
     * 请求的业务参数(AES加密后传入，可空)
     */
    @NotBlank(message = "请求的业务参数不能为空")
    private String body;

}
