package com.perye.dokit.utils.tree;

/**
 * @author: libo
 * @description: TODO
 * @date: 2024/3/26 0026 19:54
 */
import io.swagger.annotations.ApiModelProperty;

import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;

public class SuperEntity<T> implements Serializable {
    public static final String ID_FIELD = "id";
    public static final String CREATED_TIME = "createdTime";
    public static final String CREATED_TIME_FIELD = "created_time";
    public static final String CREATED_BY = "createdBy";
    public static final String CREATED_BY_FIELD = "created_by";
    public static final String CREATED_ORG_ID = "createdOrgId";
    public static final String SYNC = "sync";
    public static final String CREATED_ORG_ID_FIELD = "created_org_id";
    private static final long serialVersionUID = -4603650115461757622L;
//    @TableId(
//            value = "id",
//            type = IdType.INPUT
//    )
//    @ApiModelProperty("主键")
//    @NotNull(
//            message = "id不能为空",
//            groups = {SuperEntity.Update.class}
//    )
    protected T id;
    @ApiModelProperty("创建时间")
//    @TableField(
//            value = "created_time",
//            fill = FieldFill.INSERT
//    )
    protected LocalDateTime createdTime;
    @ApiModelProperty("创建人ID")
//    @TableField(
//            value = "created_by",
//            fill = FieldFill.INSERT
//    )
    protected T createdBy;

    public T getId() {
        return this.id;
    }

    public LocalDateTime getCreatedTime() {
        return this.createdTime;
    }

    public T getCreatedBy() {
        return this.createdBy;
    }

    public SuperEntity<T> setId(T id) {
        this.id = id;
        return this;
    }

    public SuperEntity<T> setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
        return this;
    }

    public SuperEntity<T> setCreatedBy(T createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public SuperEntity() {
    }

    public SuperEntity(T id, LocalDateTime createdTime, T createdBy) {
        this.id = id;
        this.createdTime = createdTime;
        this.createdBy = createdBy;
    }


    public interface Update extends Default {
    }

    public interface Save extends Default {
    }
}