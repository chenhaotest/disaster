package com.perye.dokit.utils;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;


/**
 * \* Created with IntelliJ IDEA.
 * \* User: chenhao
 * \* Date: 2023/6/27
 * \* Time: 10:18
 * \* Description:   阿里短信集成
 * \
 */


public class AliSms {

    public static String aliSendMes(String phoneNumbers, String setSignName, String templateCode, String templateParam) throws ClientException {
        String flash = "0";//0代表发送失败1代表发送成功
        //设置超时时间-可自行调整
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化ascClient需要的几个参数
        final String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
        final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
        //替换成你的AK    //智慧佳的
//        final String accessKeyId ="LTAI5t9Y3Prvhw3hZYp99kvv";
//        final String  accessKeySecret="S70TeWq0vzjeni4EGhb8fduUj9IbzO";


        //齐会诊
        final String accessKeyId = "LTAI5tKBsME6zdy7wVoXirTa";
        final String accessKeySecret = "jkFX1ksDRFiS6pRtPl9V6PGdKubGQW";

        //初始化ascClient,暂时不支持多region（请勿修改）
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象
        SendSmsRequest request = new SendSmsRequest();
        //使用post提交
        request.setMethod(MethodType.POST);
        //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式；发送国际/港澳台消息时，接收号码格式为00+国际区号+号码，如“0085200000000”
        request.setPhoneNumbers(phoneNumbers);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(setSignName);
        //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
        request.setTemplateCode(templateCode);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
        //request.setTemplateParam("{\"code\":\"988756\"}"); request.setTemplateParam("{\"code\":\"" + msgCode + "\"}");
        request.setTemplateParam(templateParam);
        //请求失败这里会抛ClientException异常
        SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
        if (sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
            //请求成功
            flash = "1";

        } else {

            flash = "0";
        }


        return flash;
    }
    //这是测试用例
    public static void main(String[] args) throws ClientException {
//        String a=aliSendMes("18600406985", "北京汇智医康科技有限公司", "SMS_221121898", "{\"code\":\"988756\"}");
        String a=aliSendMes("18310595658", "济南则合信息科技", "SMS_205990182", "{\"code\":\"1236\"}");

        System.out.println("+++++++++"+a);
        String code="0002xnkBB20200710E0094174";
//              code=code.substring(code.length()-4,code.length());
////        String mess="{\"adress\":\"" +"北京"+ "\",\"code\":\""+code+"\"}";
//        String mess="{\"aboutsubscribe\":\"" +"2020-09-10"+ "\",\"aboutduan\":\""+"10:10-12:45"+"\",\"hospatilName\":\""+"济南医院"+"\",\"departmentName\":\""+"肠胃科"+"\",\"roomName\":\""+"肠胃科诊室1"+"\"}";
//
//        String a=AliSms.aliSendMes("18548923428", "互联网医院用户端", "SMS_195872212", mess);

    }


}
