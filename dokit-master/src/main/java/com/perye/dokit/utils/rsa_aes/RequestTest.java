package com.perye.dokit.utils.rsa_aes;


import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @program
 * @description
 * @create 2022/10/17 10:57
 **/
public  class RequestTest {

    /**
     * 公钥key
     */
    public static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCJAs7g-BKF15KUKxec-H2GC9Q22FMyho_AyByGqrM2uNWnBRQudE3wEWloi2PVGQ62vsTFUclNef2_xvdYWjF4TusUv_e9qwEypOyqQIeNUYmuUbJd_6iCUr2M9kqwj_KT2J106N1bkwRYSu0pCT_JoW7hTomRYXM3P235Bn8L-QIDAQAB";

    /**
     * 私钥KEY
     */
    public static final String PRIVATE_KEY = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAIkCzuD4EoXXkpQrF5z4fYYL1DbYUzKGj8DIHIaqsza41acFFC50TfARaWiLY9UZDra-xMVRyU15_b_G91haMXhO6xS_972rATKk7KpAh41Ria5Rsl3_qIJSvYz2SrCP8pPYnXTo3VuTBFhK7SkJP8mhbuFOiZFhczc_bfkGfwv5AgMBAAECgYAscZ_ANFh12C7xJ2VagvTc5btqKxQZ4LKC-6bdPEQ0LRMJQcmBJDMf-9Xwlr7EooM2Xn2f8F7XZgodCqKzo1ZGUH9ZXusQKcEXr6b6sfREfIuHIuMoeksMEfCyV-7yZG2k2aHtExfn7rz-LJO-uX28HWvhomKxOlyfbpBhM60eAQJBANri0-Z39bpFs73Dq8ZghqaoV-5T_mhGTkQuWs7VhL83AnF2OZOECW9cwcP20nV5w5_Tm2izSqVmX_6Hq5UEGsECQQCgPgjHblTMHyx6lPIu4Oyr0b84b34f0yDi37qneOZ2EX78yhEQ3dSLwh4ToBcNlZ6Zn8_rZX74SSxsjdIuXdc5AkAKRE1wJ_xMqmxN4Fm0Sl7GfmrxcNskBPgy1oHH5df-KBRjQgpdUvomURF9oZwpinaUjpZhiNddfrnN9IslyDWBAkBC98n5ds2vhPO0wxy3pL6kTAMrVcUTDcw3pKLALNG71DIK-XPKZbrXMagaMZSQ08-1ikl9qiw3HXWYZGjLgeJZAkEAp1U6zcjarlH2xy2NGbdqBelebufxl9MXi6swtSqRE8HewxPgiAuSzDNwjM4S0vNYtsdSB-Zhi8mDXyckitM2zQ";

    /**
     * appid
     */
    public static final String APP_ID = "445d748d-91fd-48a5-a1a5-1dbf14b63fb7";

    public static void main(String[] args) throws Exception {
        /*****调用方（请求方）*****/
        //业务参数
        Map<String, Object> businessParams = new HashMap<>();
        businessParams.put("name", "testName");
        businessParams.put("content", "testContent");

        RequestApiParam jsonRequest = new RequestApiParam();
        jsonRequest.setAppId(APP_ID);
        long currentTimeMillis = System.currentTimeMillis();
        jsonRequest.setTimestamp(currentTimeMillis);
        //使用AES密钥，并对密钥进行rsa公钥加密
        String aseKey = AESUtils.generateKey();
        String aseKeyStr = RSAUtils.publicEncrypt(aseKey, RSAUtils.getPublicKey(PUBLIC_KEY));
        jsonRequest.setAseKey(aseKeyStr);
        //请求的业务参数进行加密
        String body = "";
        try {
            body = AESUtils.encode(aseKey, JSONObject.toJSONString(businessParams));
        } catch (Exception e) {
            throw new RuntimeException("报文加密异常", e);
        }
        jsonRequest.setBody(body);
        //json转map
        Map<String, Object> paramMap = RSAUtils.beanToMap(jsonRequest);
        paramMap.remove("sign");
        //私钥签名
        String sign = RSAUtils.sign(RSAUtils.hexStringToBytes(APP_ID + currentTimeMillis), PRIVATE_KEY);
        jsonRequest.setSign(sign);
        System.out.println("请求参数：" + jsonRequest);

        /*****调用方*****/




        /*****接收方*****/
        //验签
        Map<String, Object> paramMap2 = RSAUtils.beanToMap(jsonRequest);
        paramMap2.remove("sign");
        //签名验证 通过appid以及时间戳验签
        boolean verify = RSAUtils.verify(RSAUtils.hexStringToBytes("" + paramMap2.get("appId") + paramMap2.get("timestamp")), PUBLIC_KEY, jsonRequest.getSign());
        if (!verify) {
            throw new RuntimeException("签名验证失败");
        }
        //私钥解密，获取aseKey
        String aseKey2 = RSAUtils.privateDecrypt(jsonRequest.getAseKey(), RSAUtils.getPrivateKey(PRIVATE_KEY));
        if (!StringUtils.isEmpty(jsonRequest.getBody())) {
            // 解密请求报文
            String requestBody = "";
            try {
                requestBody = AESUtils.decode(aseKey2, jsonRequest.getBody());
            } catch (Exception e) {
                throw new RuntimeException("请求参数解密异常");
            }
            System.out.println("业务参数解密结果：" + requestBody);
        }
        /*****接收方*****/
    }
}

