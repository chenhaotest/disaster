package com.perye.dokit.utils.tree;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

/**
 * @author: libo
 * @description: TODO
 * @date: 2024/3/26 0026 19:53
 */
public class Entity<T> extends SuperEntity<T> {
        public static final String UPDATED_TIME = "updatedTime";
        public static final String UPDATED_BY = "updatedBy";
        public static final String UPDATED_TIME_FIELD = "updated_time";
        public static final String UPDATED_BY_FIELD = "updated_by";
        private static final long serialVersionUID = 5169873634279173683L;
        @ApiModelProperty("最后修改时间")
//        @TableField(
//                value = "updated_time",
//                fill = FieldFill.INSERT_UPDATE
//        )
        protected LocalDateTime updatedTime;
        @ApiModelProperty("最后修改人ID")
//        @TableField(
//                value = "updated_by",
//                fill = FieldFill.INSERT_UPDATE
//        )
        protected T updatedBy;

        public Entity(T id, LocalDateTime createdTime, T createdBy, LocalDateTime updatedTime, T updatedBy) {
            super(id, createdTime, createdBy);
            this.updatedTime = updatedTime;
            this.updatedBy = updatedBy;
        }

        public Entity() {
        }

        public LocalDateTime getUpdatedTime() {
            return this.updatedTime;
        }

        public T getUpdatedBy() {
            return this.updatedBy;
        }

        public Entity<T> setUpdatedTime(LocalDateTime updatedTime) {
            this.updatedTime = updatedTime;
            return this;
        }

        public Entity<T> setUpdatedBy(T updatedBy) {
            this.updatedBy = updatedBy;
            return this;
        }

//        public String toString() {
//            String var10000 = super.toString();
//            return "Entity(super=" + var10000 + ", updatedTime=" + this.getUpdatedTime() + ", updatedBy=" + this.getUpdatedBy() + ")";
//        }
    }

