package com.perye.dokit.utils.tree;

public interface DefValConstants {
    Long DEF_TENANT_ID = 1L;
    /**
     * 默认的树节点 分隔符
     */
    String TREE_PATH_SPLIT = StrPool.SLASH;

    /**
     * 默认树层级
     */
    Integer TREE_GRADE = 0;
    /**
     * 默认的父id
     */
    String PARENT_ID = StrPool.DEF_PARENT_ID;
    /**
     * 默认的排序
     */
    Integer SORT_VALUE = 0;
    /**
     * 字典占位符
     */
    @Deprecated
    String DICT_PLACEHOLDER = "###";
    /**
     * 默认增加的层级
     */
    Integer DefValLevel = 1;
}
