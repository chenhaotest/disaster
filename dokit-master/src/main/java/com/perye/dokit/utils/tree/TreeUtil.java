package com.perye.dokit.utils.tree;

/**
 * @author: libo
 * @description: TODO
 * @date: 2024/3/26 0026 19:43
 */

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

import java.io.Serializable;
import java.util.*;

public final class TreeUtil {
    public static final String TREE_SPLIT = "/";

    private TreeUtil() {
    }

    public static String getTreePath(String parentTreePath, String parentId) {
        return "/" + parentId + parentTreePath;
    }

    public static String buildTreePath(String id) {
        return "/" + id + "/";
    }

    public static boolean isRoot(String id) {
        return StrUtil.isBlank(id) || Long.valueOf(StrPool.DEF_PARENT_ID).equals(id);
    }

    public static <E extends TreeEntity<E, ? extends Serializable>> List<E> buildTree(Collection<E> treeList) {
        if (CollUtil.isEmpty(treeList)) {
            return Collections.emptyList();
        } else {
            Map<Serializable, List<E>> childrenMap = new HashMap();

            Iterator var2;
            TreeEntity node;
            Serializable id;
            for(var2 = treeList.iterator(); var2.hasNext(); ((List)childrenMap.get(id)).add(node)) {
                node = (TreeEntity)var2.next();
                id = node.getParentId();
                if (!childrenMap.containsKey(id)) {
                    childrenMap.put(id, new ArrayList());
                }
            }

            var2 = treeList.iterator();

            while(var2.hasNext()) {
                node = (TreeEntity)var2.next();
                id = (Serializable)node.getId();
                if (childrenMap.containsKey(id)) {
                    node.setChildren((List)childrenMap.get(id));
                }
            }

            List<E> trees = new ArrayList();
            Set<Serializable> allIds = new HashSet();
            Iterator var8 = treeList.iterator();

            TreeEntity node2;
            while(var8.hasNext()) {
                node2 = (TreeEntity)var8.next();
                allIds.add((Serializable)node2.getId());
            }

            var8 = treeList.iterator();

            while(var8.hasNext()) {
                node2 = (TreeEntity)var8.next();
                if (!allIds.contains(node2.getParentId())) {
                    trees.add((E)node2);
                }
            }

            return trees;
        }
    }
}
