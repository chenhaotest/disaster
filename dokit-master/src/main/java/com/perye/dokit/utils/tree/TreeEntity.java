package com.perye.dokit.utils.tree;

/**
 * @author: libo
 * @description: TODO
 * @date: 2024/3/26 0026 19:26
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TreeEntity<E, T extends Serializable> extends Entity<T> {
    public static final String LABEL = "label";
    public static final String PARENT_ID = "parentId";
    public static final String SORT_VALUE = "sortValue";
    public static final String PARENT_ID_FIELD = "parent_id";
    public static final String SORT_VALUE_FIELD = "sort_value";
    @ApiModelProperty("父ID")
//    @TableField("parent_id")
    protected T parentId;
    @ApiModelProperty("排序号")
//    @TableField("sort_value")
    protected Integer sortValue;
    @ApiModelProperty(
            value = "子节点",
            hidden = true
    )
//    @TableField(
//            exist = false
//    )
    protected List<E> children;

    public TreeEntity() {
    }

    @JsonIgnore
    public void initChildren() {
        if (this.getChildren() == null) {
            this.setChildren(new ArrayList());
        }

    }

    @JsonIgnore
    public void addChildren(E child) {
        this.initChildren();
        this.children.add(child);
    }

    public T getParentId() {
        return this.parentId;
    }

    public Integer getSortValue() {
        return this.sortValue;
    }

    public List<E> getChildren() {
        return this.children;
    }

    public TreeEntity<E, T> setParentId(T parentId) {
        this.parentId = parentId;
        return this;
    }

    public TreeEntity<E, T> setSortValue(Integer sortValue) {
        this.sortValue = sortValue;
        return this;
    }

    public TreeEntity<E, T> setChildren(List<E> children) {
        this.children = children;
        return this;
    }

}
