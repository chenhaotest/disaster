package com.perye.dokit.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ListUtil {
    /**
     * @description: java判断有没有重复元素
     */
    public static List<String> getDuplicateElements(List<String> originalList) {
        Set<String> uniqueElements = new HashSet<>();
        List<String> duplicateElements = new ArrayList<>();

        for (String element : originalList) {
            if (!uniqueElements.add(element)) {
                duplicateElements.add(element);
            }
        }

        return duplicateElements;
    }
}
