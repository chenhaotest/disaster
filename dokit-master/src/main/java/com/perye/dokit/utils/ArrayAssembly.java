package com.perye.dokit.utils;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.utils.pageData.PageData;
import io.jsonwebtoken.lang.Classes;

import java.util.List;

public class ArrayAssembly {

    public static PageData pageList(List<PageData> list, Integer count) {
        PageData pageData = new PageData();

        PageInfo<PageData> pageInfo = new PageInfo<PageData>(list);
        pageInfo.setList(list);
        pageData.put("totalElements", count);
        pageData.put("content", list);
        return pageData;
    }

    public static void getPage(PageData pageData) {
        int a = Integer.parseInt(pageData.getString("page")) + 1;
        PageHelper.startPage(a, Integer.parseInt(pageData.getString("size")));
    }

    public static PageData getDataToObject(List<PageData> list) {

        PageData result = new PageData();
        PageInfo<PageData> pageInfo = new PageInfo<PageData>(list);
        int count = (int) pageInfo.getTotal();
        result = ArrayAssembly.pageList(list, count);
        return result;
    }

}
