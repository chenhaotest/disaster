package com.perye.dokit.utils.rsa_aes;


import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author
 * @program
 * @description
 * @create 2022/10/17 10:57
 **/
public  class RequestTest2 {

    /**
     * 公钥key
     */
    public static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCRJWVTc31Dkah2lSPAoU2VWtRz81z9T38BRfJ_VRDnoG23y3IZA6WXeonzW2QqFt_OFxsc0pjIccQEqadqpiCXUrfVuYPdWtWXCW5K174oOq38K-4oBfkjjC8C6dGRLIu90Bdpw2UrQOCSQX8Q9u9oZXDLi9GWb_WyFVFsTUNubwIDAQAB";

    /**
     * 私钥KEY
     */
    public static final String PRIVATE_KEY = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJElZVNzfUORqHaVI8ChTZVa1HPzXP1PfwFF8n9VEOegbbfLchkDpZd6ifNbZCoW384XGxzSmMhxxASpp2qmIJdSt9W5g91a1ZcJbkrXvig6rfwr7igF-SOMLwLp0ZEsi73QF2nDZStA4JJBfxD272hlcMuL0ZZv9bIVUWxNQ25vAgMBAAECgYBQrqbqgpIpEzi_4htjosaLtVfPtIIGVUBWJtmrqk-hUe5rcm3UNdDAqV8xpaXCN8DkwLdTiEVhdsvtg5kKkZofaXKVgxoWTclmDzhTk2UB2udh6QPBRxJGOKwh5IUebpeVya_2jI1S99ycCq8gOz-MWN5sfb5Oo5USSYMDFlB9YQJBAOpp63mpY-brG426LqLFHxTTsOUtix1udwcOzTYJX9lQU2SmN1huMvu_ivAjPPv_Kv11IH0YvU5yrrbKuwEBXHkCQQCegxNuTkzt87P9zU3VtVO_vTlTgIXlRSqCuFSBPEZhbUciY7BlvMYsB-oE-qn0gMqtdl3_D1YBLLwC3lwBuxgnAkEApPsJNOZIHqTIDrbDzeFon7Lh5Fpqd7tgYIJFYcu1PvBu0P6_IyjkDxTfR2W1rTII4rwFsqWvzr-eQZ5rgSihGQJAZkmpWU4X4JmZ9dbtOlI3yAxgmNzgCpI2m4I9rePqeuZct_lilFRcAX_d6Pakg840e_ldMMGBu2JVtPguoUNNkwJAUOYsjnawslCDRD-1CR0RvsPQgF2K1HcjVfyuKEnbpJWFVXkeXDQEDwG3-a7CozF2tdkGd1uyB4ARgQpM2H-gyg";

    /**
     * appid
     */
    public static final String APP_ID = "108970485210";

    public static void main(String[] args) throws Exception {
        /*****调用方（请求方）*****/
        //业务参数
        Map<String, Object> businessParams = new HashMap<>();
        businessParams.put("name", "testName");
        businessParams.put("content", "testContent");

        RequestApiParam jsonRequest = new RequestApiParam();
        jsonRequest.setAppId(APP_ID);
        long currentTimeMillis = System.currentTimeMillis();
        jsonRequest.setTimestamp(currentTimeMillis);
        //使用AES密钥，并对密钥进行rsa公钥加密
        String aseKey = AESUtils.generateKey();
        String aseKeyStr = RSAUtils.publicEncrypt(aseKey, RSAUtils.getPublicKey(PUBLIC_KEY));
        jsonRequest.setAseKey(aseKeyStr);
        //请求的业务参数进行加密
        String body = "";
        try {
            body = AESUtils.encode(aseKey, JSONObject.toJSONString(businessParams));
        } catch (Exception e) {
            throw new RuntimeException("报文加密异常", e);
        }
        jsonRequest.setBody(body);
        //json转map
        Map<String, Object> paramMap = RSAUtils.beanToMap(jsonRequest);
        paramMap.remove("sign");
        //私钥签名
        String sign = RSAUtils.sign(RSAUtils.hexStringToBytes(APP_ID + currentTimeMillis), PRIVATE_KEY);
        jsonRequest.setSign(sign);
        System.out.println("请求参数：" + jsonRequest);

        /*****调用方*****/




        /*****接收方*****/
        //验签
        Map<String, Object> paramMap2 = RSAUtils.beanToMap(jsonRequest);
        paramMap2.remove("sign");
        //签名验证 通过appid以及时间戳验签
        boolean verify = RSAUtils.verify(RSAUtils.hexStringToBytes("" + paramMap2.get("appId") + paramMap2.get("timestamp")), PUBLIC_KEY, jsonRequest.getSign());
        if (!verify) {
            throw new RuntimeException("签名验证失败");
        }
        //私钥解密，获取aseKey
        String aseKey2 = RSAUtils.privateDecrypt(jsonRequest.getAseKey(), RSAUtils.getPrivateKey(PRIVATE_KEY));
        if (!StringUtils.isEmpty(jsonRequest.getBody())) {
            // 解密请求报文
            String requestBody = "";
            try {
                requestBody = AESUtils.decode(aseKey2, jsonRequest.getBody());
            } catch (Exception e) {
                throw new RuntimeException("请求参数解密异常");
            }
            System.out.println("业务参数解密结果：" + requestBody);
        }
        /*****接收方*****/
    }
}

