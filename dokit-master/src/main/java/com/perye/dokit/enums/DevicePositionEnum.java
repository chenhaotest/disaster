package com.perye.dokit.enums;

public enum DevicePositionEnum {
    UP_LINE(0,"上行线"),
    DOWN_LINE(1,"下行线"),
    ;

    private int code;

    private String content;

    DevicePositionEnum(int code, String content) {
        this.code = code;
        this.content = content;
    }

    public int getCode() {
        return code;
    }

    public byte getByteCode() {
        return (byte) (code & 0x000000FF);
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
