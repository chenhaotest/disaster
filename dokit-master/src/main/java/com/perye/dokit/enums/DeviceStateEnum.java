package com.perye.dokit.enums;

/**
 * 设备状态枚举值
 *
 * @author Administrator
 */
public enum DeviceStateEnum {

    /**
     * 正常状态
     */
    NORMAL(0),
    /**
     * 离线状态
     */
    DISCONNECT(1),
    /**
     * 故障状态
     */
    FAULT(2),
    /**
     * 停用状态
     */
    STOP(-1);

    private int code;

    DeviceStateEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
