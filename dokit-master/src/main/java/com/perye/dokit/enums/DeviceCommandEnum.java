package com.perye.dokit.enums;

/**
 * 设备发送指令
 */
public enum DeviceCommandEnum {

    /**
     * 设备升级
     */
    DEVICE_UPGRADE("DEVICE_UPGRADE"),
    /**
     * 设备就绪
     */
    DEVICE_READY("DEVICE_READY"),

    /**
     * 设备配置
     */
    DEVICE_CONFIG("DEVICE_CONFIG"),
    ;

    private String code;

    DeviceCommandEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
