package com.perye.dokit.enums.mqtt;

/**
 * 铁路MQTT主题
 *
 * @author wangjie
 */
public enum RailwayMqttTopicEnum {

    /**
     * 共享主题前缀
     */
    SHARED_TOPIC_PREFIX("$share/group/"),

    /**
     * 安全防护门终端数据上报
     * <p>
     * 安全防护门终端数据响应
     */
    TUNNEL_PROTECTIVE_DOOR_INTELLIGENT_MONITORING_SERVER_DATA_REPORT("/tunnel/protective_door_intelligent_monitoring/server/dataReport"),
    TUNNEL_PROTECTIVE_DOOR_INTELLIGENT_MONITORING_MONITORING_CLIENT_RESPONSE("/tunnel/protective_door_intelligent_monitoring/client/"),

    /**
     * 风机监测终端数据上报
     * <p>
     * 风机监测终端数据响应
     */
    TUNNEL_FAN_INTELLIGENT_MONITORING_SERVER_DATA_REPORT("/tunnel/fan_intelligent_monitoring/server/dataReport"),
    TUNNEL_FAN_INTELLIGENT_MONITORING_CLIENT_RESPONSE("/tunnel/fan_intelligent_monitoring/client/"),

    /**
     * 应急照明监测终端数据上报
     * <p>
     * 应急照明监测终端数据响应
     */
    TUNNEL_EMERGENCY_LIGHTING_INTELLIGENT_SERVER_DATA_REPORT("/tunnel/emergency_lighting_intelligent_monitoring/server/dataReport"),
    TUNNEL_EMERGENCY_LIGHTING_INTELLIGENT_CLIENT_RESPONSE("/tunnel/emergency_lighting_intelligent_monitoring/client/"),

    /**
     * 风阀监测终端数据上报
     * <p>
     * 风阀监测终端数据响应
     */
    TUNNEL_AIR_VALVE_INTELLIGENT_MONITORING_SERVER_DATA_REPORT("/tunnel/air_valve_intelligent_monitoring/server/dataReport"),
    TUNNEL_AIR_VALVE_INTELLIGENT_MONITORING_CLIENT_RESPONSE("/tunnel/air_valve_intelligent_monitoring/client/"),


    /**
     * 消火栓智能监测装置 数据上报
     * <p>
     * 消火栓智能监测装置 数据响应
     */
    TUNNEL_FIRE_HYDRANT_INTELLIGENT_MONITORING_SERVER_DATA_REPORT("/tunnel/fire_hydrant_intelligent_monitoring/server/dataReport"),
    TUNNEL_FIRE_HYDRANT_INTELLIGENT_MONITORING_CLIENT_RESPONSE("/tunnel/fire_hydrant_intelligent_monitoring/client/"),
    ;

    private String value;

    RailwayMqttTopicEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}

