package com.perye.dokit.enums.mqtt;

/**
 * 门控状态枚举
 */
public enum DoorControlStateEnum {

    /**
     * 门控状态正常
     */
    DOOR_CONTROL_STATE_NORMAL(0),
    /**
     * 门控状态异常
     */
    DOOR_CONTROL_STATE_EXCEPTION(1),
    /**
     * 门控状态(开门)
     */
    DOOR_CONTROL_STATE_OPEN(1),
    /**
     * 门控状态(关门)
     */
    DOOR_CONTROL_STATE_CLOSE(2),
    /**
     * 图片上传成功
     */
    IMG_UPLOAD_STATE_SUCCESS(1),
    /**
     * 图片上传失败
     */
    IMG_UPLOAD_STATE_FAIL(0);

    private int code;

    DoorControlStateEnum(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
