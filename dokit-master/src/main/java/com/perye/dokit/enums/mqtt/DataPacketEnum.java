package com.perye.dokit.enums.mqtt;

/**
 * @author wangjie
 */
public enum DataPacketEnum {
    //网关唯一序列号(8B)
    DP_GATEWAY_MAC(0, 8),
    //时间戳(4B)
    DP_TIMESTAMP(8, 4),
    //命令(1B)
    DP_COMMAND(12, 1),
    //长度(1B)
    DP_LENGTH(13, 1),
    //数据
    DP_DATA(14, 0),
    //卡号长度
    DP_CARD(0, 4),
    //CRC8
    DP_CRC8(0, 1);

    private int index;

    private int length;

    DataPacketEnum(int index, int length) {
        this.index = index;
        this.length = length;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
