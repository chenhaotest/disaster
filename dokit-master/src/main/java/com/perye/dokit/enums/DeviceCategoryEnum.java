package com.perye.dokit.enums;

/**
 * 设备类别枚举
 *
 * @author wangjie
 */
public enum DeviceCategoryEnum {

    /**
     * 隧道安全防护门监测
     */
    TUNNEL_PROTECTIVE_DOOR_INTELLIGENT_MONITORING("TUNNEL_PROTECTIVE_DOOR_INTELLIGENT_MONITORING"),

    /**
     * 隧道风阀监测
     */
    TUNNEL_AIR_VALVE_MONITORING("TUNNEL_AIR_VALVE_MONITORING"),

    /**
     * 隧道风机监测
     */
    TUNNEL_FAN_MONITORING("TUNNEL_FAN_MONITORING"),

    /**
     * 隧道应急照明监测
     */
    TUNNEL_EMERGENCY_LIGHTING_MONITORING("TUNNEL_EMERGENCY_LIGHTING_MONITORING"),


    /**
     * 隧道消火栓监测
     */
    TUNNEL_FIRE_HYDRANT_MONITORING("TUNNEL_FIRE_HYDRANT_MONITORING"),
    ;

    private String code;

    DeviceCategoryEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
