package com.perye.dokit.enums.mqtt;

/**
 * 防护门状态码枚举值
 *
 * @author wangjie
 */
public enum DoorControlCommandEnum {

    /**
     * 终端上电启动上报工作状态
     */
    //终端上电启动上报工作状态（终端>>>服务器）
    CLIENT_DEVICE_POWER_ON_REPORT(0x01,"防护门终端上电启动上报工作状态"),
    //终端上报上电数据响应（服务器>>>终端）
    SERVER_CLIENT_POWER_ON_REPORT_RESPONSE(0xA1,"防护门终端上报上电数据响应"),

    /**
     * 防护门终端发送心跳
     */
    //防护门终端发送心跳（终端 >>> 服务器）
    CLIENT_DEVICE_HEARTBEAT_SEND(0x02,"防护门终端发送心跳"),
    //防护门终端心跳响应（服务器>>>终端）
    SERVER_DEVICE_HEARTBEAT_RESPONSE(0xA2,"防护门终端心跳响应"),

    /**
     * 刷卡信息
     */
    //刷卡信息（终端>>>服务器）
    CLIENT_DEVICE_PAY_CARD(0x03,"防护门刷卡信息"),
    //刷卡信息响应（服务器>>>终端）
    SERVER_DEVICE_PAY_CARD_RESPONSE(0xA3,"防护门刷卡信息响应"),

    /**
     * 门控状态
     */
    //门控状态(终端 >>> 服务器)
    CLIENT_DEVICE_DOOR_CONTROL_STATE(0x04,"防护门门控状态"),
    //门控状态响应(服务器>>>终端)
    SERVER_CLIENT_DEVICE_DOOR_CONTROL_STATE(0xA4,"防护门门控状态响应"),

    /**
     * 门控状态图片上传
     */
    //门控状态图片上传(终端 >>> 服务器)
    CLIENT_DEVICE_DOOR_CONTROL_IMG_STATE(0x05,"防护门门控状态图片上传"),
    //门控状态图片上传响应(服务器>>>终端)
    SERVER_CLIENT_DEVICE_DOOR_CONTROL_IMG_STATE(0xA5,"防护门门控状态图片上传响应"),

    /**
     * /门控状态故障上报
     */
    //门控状态故障上报(终端 >>> 服务器)
    CLIENT_DEVICE_DOOR_CONTROL_FAULT_REPORT(0x06,"防护门门控状态故障上报"),
    //门控状态故障上报响应(服务器>>>终端)
    SERVER_CLIENT_DEVICE_DOOR_CONTROL_FAULT_REPORT(0xA6,"防护门门控状态故障上报响应"),

    /**
     * OTA升级
     */
    //设备OTA升级通知
    SERVER_DEVICE_UPGRADE_NOTICE(0xA7,"防护门设备OTA升级通知"),
    //设备OTA升级就绪
    CLIENT_DEVICE_UPGRADE_READY(0x07,"防护门设备OTA升级就绪"),
    //设备OTA升级失败
    SERVER_DEVICE_UPGRADE_FAIL(0x08,"防护门设备OTA升级失败"),

    /**
     * 服务器下发终端配置
     */
    //服务器下发终端配置(服务器>>>终端)
    SERVER_CONFIG_SEND(0xA9,"服务器下发配置信息"),
    //服务器下发终端配置(终端>>>服务器)
    CLIENT_CONFIG_RESPONSE(0x09,"服务器下发配置信息响应"),

    /**
     * 防护门温湿度上报
     */
    //防护门温湿度上报(服务器>>>终端)
    TEMPERATURE_AND_HUMIDITY_SEND(0x10,"服务器下发配置信息"),
    //防护门温湿度上报响应(终端>>>服务器)
    TEMPERATURE_AND_HUMIDITY_RESPONSE(0xB1,"服务器下发配置信息响应"),
    ;

    private int code;

    private String content;

    DoorControlCommandEnum(int code,String content) {
        this.code = code;
        this.content =content;
    }

    public int getCode() {
        return code;
    }

    public byte getByteCode() {
        return (byte) (code & 0x000000FF);
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}


