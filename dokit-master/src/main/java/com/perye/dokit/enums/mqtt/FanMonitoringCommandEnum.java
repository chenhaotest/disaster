package com.perye.dokit.enums.mqtt;

/**
 * 风机监测状态码枚举值
 *
 * @author wangjie
 */
public enum FanMonitoringCommandEnum {

    /**
     * 终端上电启动上报工作状态
     */
    //终端上电启动上报工作状态（终端>>>服务器）
    CLIENT_DEVICE_POWER_ON_REPORT(0x01,"风机监测终端上电启动上报工作状态"),
    //终端上报上电数据响应（服务器>>>终端）
    SERVER_CLIENT_POWER_ON_REPORT_RESPONSE(0xA1,"风机监测终端上报上电数据响应"),

    /**
     * 终端发送心跳
     */
    //终端发送心跳（终端 >>> 服务器）
    CLIENT_DEVICE_HEARTBEAT_SEND(0x02,"风机监测终端发送心跳"),
    //终端心跳响应（服务器>>>终端）
    SERVER_DEVICE_HEARTBEAT_RESPONSE(0xA2,"风机监测终端发送心跳"),

    /**
     * 业务上报
     */
    //业务上报（终端>>>服务器）
    CLIENT_DEVICE_DATA_REPORT(0x03,"风机监测终端业务上报"),
    //业务上报响应（服务器>>>终端）
    SERVER_DEVICE_DATA_REPORT_RESPONSE(0xA3,"风机监测终端业务上报响应"),

    /**
     * 业务完成上报
     */
    //业务完成上报（终端>>>服务器）
    CLIENT_DEVICE_DATA_FAULT_REPORT(0x05,"风机监测终端业务完成上报"),
    //业务完成上报响应（服务器>>>终端）
    SERVER_DEVICE_DATA_REPORT_FAULT_RESPONSE(0xA5,"风机监测终端业务完成上报响应"),

    /**
     * 异常上报
     */
    //异常上报（终端>>>服务器）
    CLIENT_DEVICE_FAULT_REPORT(0x04,"风机监测终端异常上报"),
    //异常上报响应（服务器>>>终端）
    SERVER_DEVICE_FAULT_REPORT_RESPONSE(0xA4,"风机监测终端异常上报响应"),
    ;

    private int code;

    private String content;

    FanMonitoringCommandEnum(int code,String content) {
        this.code = code;
        this.content =content;
    }

    public int getCode() {
        return code;
    }

    public byte getByteCode() {
        return (byte) (code & 0x000000FF);
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
