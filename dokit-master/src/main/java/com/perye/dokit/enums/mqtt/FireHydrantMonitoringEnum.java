package com.perye.dokit.enums.mqtt;
/**
 * 消火栓智能监测装置 枚举值
 */
public enum FireHydrantMonitoringEnum {
    /**
     * 终端上电启动上报工作状态
     */
    //终端上电启动上报工作状态（终端>>>服务器）
    CLIENT_DEVICE_POWER_ON_REPORT(0x01,"消火栓智能监测装置上电启动上报工作状态"),
    //终端上报上电数据响应（服务器>>>终端）
    SERVER_CLIENT_POWER_ON_REPORT_RESPONSE(0xA1,"消火栓智能监测装置上报上电数据响应"),

    /**
     * 终端发送心跳
     */
    //终端发送心跳（终端 >>> 服务器）
    CLIENT_DEVICE_HEARTBEAT_SEND(0x02,"消火栓智能监测装置发送心跳"),
    //终端心跳响应（服务器>>>终端）
    SERVER_DEVICE_HEARTBEAT_RESPONSE(0xA2,"消火栓智能监测装置发送心跳"),

    /**
     * 业务上报
     */
    //业务上报（终端>>>服务器）
    CLIENT_DEVICE_DATA_REPORT(0x03,"消火栓智能监测装置业务上报"),
    //业务上报响应（服务器>>>终端）
    SERVER_DEVICE_DATA_REPORT_RESPONSE(0xA3,"消火栓智能监测装置业务上报响应"),

    /**
     * 异常上报
     */
    //异常上报（终端>>>服务器）
    CLIENT_DEVICE_FAULT_REPORT(0x04,"消火栓智能监测装置异常上报"),
    //异常上报响应（服务器>>>终端）
    SERVER_DEVICE_FAULT_REPORT_RESPONSE(0xA4,"消火栓智能监测装置异常上报响应"),
    ;

    private int code;

    private String content;

    FireHydrantMonitoringEnum(int code,String content) {
        this.code = code;
        this.content =content;
    }

    public int getCode() {
        return code;
    }

    public byte getByteCode() {
        return (byte) (code & 0x000000FF);
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
