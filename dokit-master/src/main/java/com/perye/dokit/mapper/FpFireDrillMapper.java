package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpFireDrill;
import com.perye.dokit.dto.FpFireDrillDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpFireDrillMapper extends BaseMapper<FpFireDrillDto, FpFireDrill> {

}
