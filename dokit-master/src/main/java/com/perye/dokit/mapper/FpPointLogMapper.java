package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpPointLog;
import com.perye.dokit.dto.FpPointLogDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpPointLogMapper extends BaseMapper<FpPointLogDto, FpPointLog> {

}
