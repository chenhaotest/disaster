package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpPersonnel;
import com.perye.dokit.dto.FpPersonnelDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpPersonnelMapper extends BaseMapper<FpPersonnelDto, FpPersonnel> {

}
