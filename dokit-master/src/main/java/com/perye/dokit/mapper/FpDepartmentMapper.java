package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpDepartment;
import com.perye.dokit.dto.FpDepartmentDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpDepartmentMapper extends BaseMapper<FpDepartmentDto, FpDepartment> {

}
