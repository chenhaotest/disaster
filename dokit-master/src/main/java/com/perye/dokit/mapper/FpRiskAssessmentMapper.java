package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpRiskAssessment;
import com.perye.dokit.dto.FpRiskAssessmentDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpRiskAssessmentMapper extends BaseMapper<FpRiskAssessmentDto, FpRiskAssessment> {

}
