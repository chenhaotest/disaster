package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpHotWork;
import com.perye.dokit.dto.FpHotWorkDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpHotWorkMapper extends BaseMapper<FpHotWorkDto, FpHotWork> {

}
