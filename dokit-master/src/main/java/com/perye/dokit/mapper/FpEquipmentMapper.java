package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpEquipment;
import com.perye.dokit.dto.FpEquipmentDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpEquipmentMapper extends BaseMapper<FpEquipmentDto, FpEquipment> {

}
