package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpAlarmPointLog;
import com.perye.dokit.dto.FpAlarmPointLogDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpAlarmPointLogMapper extends BaseMapper<FpAlarmPointLogDto, FpAlarmPointLog> {

}
