package com.perye.dokit.mapper;

import com.perye.dokit.base.BaseMapper;
import com.perye.dokit.entity.FpAccidentStatistics;
import com.perye.dokit.dto.FpAccidentStatisticsDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FpAccidentStatisticsMapper extends BaseMapper<FpAccidentStatisticsDto, FpAccidentStatistics> {

}
