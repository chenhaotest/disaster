package com.perye.dokit.service;

import com.perye.dokit.utils.pageData.PageData;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public interface ChenTestService {


    List<PageData> queryAll(PageData pageData) throws Exception;
    List<PageData> queryAllLi(PageData pageData) throws Exception;

    void create(PageData pageData) throws Exception;

    void update(PageData pageData) throws Exception;

    void deleteAll(Integer[] ids) throws Exception;

    void download(List<PageData> pageData1, HttpServletResponse response) throws IOException;

    Integer getCount(PageData pageData) throws Exception;
}
