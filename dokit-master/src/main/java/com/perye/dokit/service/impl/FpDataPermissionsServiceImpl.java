package com.perye.dokit.service.impl;

import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.modbus.door.util.DateUtil;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpDataPermissionsService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.SecurityUtils;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.UuidUtil;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.utils.tree.TreeUtil;
import com.perye.dokit.vo.FpResourceVo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName FpDataPermissionsServiceImpl
 * @Description 数据权限管理实现类
 * @Author zd
 * @Date 2024/4/29 13:10
 * @Version 1.0
 **/
@Service
public class FpDataPermissionsServiceImpl implements FpDataPermissionsService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private DictDetailService dictDetailService;

    @Override
    //@Cacheable
    public List<FpResourceVo> queryAll(PageData pageData) throws Exception {

        List<FpResourceVo> pageList = (List<FpResourceVo>) dao.findForList("FpResourceMapper.queryResourceList", pageData);
        if (CollectionUtils.isNotEmpty(pageList)) {
            Map<String, String> downLinesMap = dictDetailService.queryDictDetailByName("down_lines", "map");
            Map<String, String> resourceTypeMap = dictDetailService.queryDictDetailByName("resource_type", "map");
            Map<String, String> subSystemTypeMap = dictDetailService.queryDictDetailByName("sub_system_type", "map");
            for (FpResourceVo data : pageList) {
                String downLines = data.getDownLines();
                data.setDownLinesStr(downLinesMap.get(downLines));
                String resourceType = data.getType();
                data.setTypeStr(resourceTypeMap.get(resourceType));
                String subSystemType = data.getSubSystemType();
                data.setSubSystemTypeStr(subSystemTypeMap.get(subSystemType));

                data.setOldParentId(data.getParentId());
            }
        }
        List<FpResourceVo> list = TreeUtil.buildTree(pageList);
        return list;
    }

    @Override
    public List<PageData> queryDataPermissions(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpDataPermissionsMapper.queryDataPermissions", pageData);
    }

    @Override
    public int editDataPermissions(PageData pageData) throws Exception {
        int num = 0;

        String userId = pageData.getString("userId");
        List<String> selectedDataList = (List<String>) pageData.get("selectedDataList");
        List<String> oldDataList = (List<String>) pageData.get("oldDataList");


        List<PageData> addList = new ArrayList<PageData>();
        List<PageData> delList = new ArrayList<PageData>();


        //oldDataList为空，selectedDataList不为空就是新增
        if (CollectionUtils.isEmpty(oldDataList) && CollectionUtils.isNotEmpty(selectedDataList)) {
            for (String sourceId : selectedDataList) {
                PageData info = new PageData();
                info.put("id", UuidUtil.get32UUID());
                info.put("userId", userId);
                info.put("sourceId", sourceId);
                info.put("isDelete", 0);
                info.put("createId", SecurityUtils.getCurrentUserId().toString());
                info.put("createTime", DateUtils.getTime());
                info.put("updateTime", DateUtils.getTime());
                addList.add(info);
            }
        }


        //oldDataList不为空，selectedDataList为空就是删除
        if (CollectionUtils.isNotEmpty(oldDataList) && CollectionUtils.isEmpty(selectedDataList)) {
            for (String sourceId : selectedDataList) {
                PageData info = new PageData();
                info.put("userId", userId);
                info.put("sourceId", sourceId);
                delList.add(info);
            }
        }


        //oldDataList不为空，selectedDataList不为空就是编辑
        if (CollectionUtils.isNotEmpty(oldDataList) && CollectionUtils.isNotEmpty(selectedDataList)) {

            Map<String, String> map = new HashMap<String, String>();
            for (String sourceId : selectedDataList) {
                map.put(sourceId, sourceId);
            }

            for (String sourceId : oldDataList) {
                String value = map.get(sourceId);
                if (StringUtils.isBlank(value)) {
                    PageData info = new PageData();
                    info.put("userId", userId);
                    info.put("sourceId", sourceId);
                    delList.add(info);
                } else {
                    map.remove(sourceId);
                }
            }


            map.forEach((key, Value) -> {
                PageData info = new PageData();
                info.put("id", UuidUtil.get32UUID());
                info.put("userId", userId);
                info.put("sourceId", key);
                info.put("isDelete", 0);
                info.put("createId", SecurityUtils.getCurrentUserId().toString());
                info.put("createTime", DateUtils.getTime());
                info.put("updateTime", DateUtils.getTime());
                addList.add(info);
            });

        }

        if (CollectionUtils.isNotEmpty(delList)) {
            num += (int) dao.update("FpDataPermissionsMapper.delDataPermissions", delList);
        }

        if (CollectionUtils.isNotEmpty(addList)) {
            num += (int) dao.update("FpDataPermissionsMapper.insertDataPermissions", addList);
        }

        return num;
    }


}
