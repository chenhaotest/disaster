package com.perye.dokit.service;

import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.vo.FpResourceVo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface FpResourceService {


    /**
     * 查询所有数据
     *
     * @param pageData 条件参数
     * @return List<FpResourceDto>
     */
    List<FpResourceVo> queryAll(PageData pageData) throws Exception;

    PageData queryResourceInfo(PageData pageData) throws Exception;

    /**
     * 创建
     *
     * @param pageData /
     * @return FpResourceDto
     */
    Boolean create(PageData pageData) throws Exception;

    /**
     * 编辑
     *
     * @param pageData /
     */
    void update(PageData pageData) throws Exception;

    /**
     * 导出数据
     *
     * @param all      待导出的数据
     * @param response /
     * @throws IOException /
     */
    void download(List<PageData> all, HttpServletResponse response) throws IOException;

    /**
     * 多选删除
     *
     * @param ids /
     */
    void deleteAll(String[] ids) throws Exception;

    List<PageData> queryDropdownBoxList(PageData pageData) throws Exception;

    List<PageData> queryResourceById(PageData pageData) throws Exception;

    List<FpResourceVo> queryResourceListByType(List<String> list) throws Exception;

}
