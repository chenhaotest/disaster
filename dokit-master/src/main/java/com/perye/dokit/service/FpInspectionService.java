package com.perye.dokit.service;



import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Pageable;
import java.util.Map;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public interface FpInspectionService {


    List<PageData> queryAll(PageData pageData) throws Exception;

    void add(PageData pageData) throws Exception;

    void update(PageData pageData) throws Exception;
}