package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpRepairService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpRepair")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpRepairServiceImpl implements FpRepairService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private DictDetailService dictDetailService;

    @Override
    //@Cacheable
    public List<PageData> queryAll(PageData pageData) throws Exception {
        List<PageData> repairList = (List<PageData>) dao.findForList("FpRepairMapper.selectByPrimaryKey", pageData);
        if (CollectionUtils.isNotEmpty(repairList)) {
            Map<String, String> repairStateMap = dictDetailService.queryDictDetailByName("repair_state", "map");
            Map<String, String> faultCategoryMap = dictDetailService.queryDictDetailByName("fault_category", "map");
            Map<String, String> downLinesMap = dictDetailService.queryDictDetailByName("down_lines", "map");

            for (PageData data : repairList) {
                String repairState = data.getString("repairState");
                String faultCategory = data.getString("faultCategory");
                String downLines = data.getString("downLines");
                if (StringUtils.isNotBlank(repairState)) {
                    data.put("repairStateStr", repairStateMap.get(repairState));
                }
                if (StringUtils.isNotBlank(faultCategory)) {
                    data.put("faultCategoryStr", faultCategoryMap.get(faultCategory));
                }
                if (StringUtils.isNotBlank(downLines)) {
                    data.put("downLinesStr", downLinesMap.get(downLines));
                }
            }
        }
        return repairList;

    }


    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void create(PageData pageData) throws Exception {
        pageData.put("id", IdUtil.simpleUUID());
        pageData.put("repairOrder", randomOrderCode());
        pageData.put("createId", SecurityUtils.getCurrentUserId().toString());
        pageData.put("isDelete", 0);
        pageData.put("createTime", DateUtils.getTime());
        pageData.put("updateTime", DateUtils.getTime());
        dao.save("FpRepairMapper.insert", pageData);
    }


    public static String randomOrderCode() {
        SimpleDateFormat dmDate = new SimpleDateFormat("yyyyMMddHHmmss");
        String randata = getRandom(6);
        Date date = new Date();
        String dateran = dmDate.format(date);
        String Xsode = "WX" + dateran + randata;
        if (Xsode.length() < 24) {
            Xsode = Xsode + 0;
        }
        return Xsode;
    }

    public static String getRandom(int len) {
        Random r = new Random();
        StringBuilder rs = new StringBuilder();
        for (int i = 0; i < len; i++) {
            rs.append(r.nextInt(10));
        }
        return rs.toString();
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(PageData pageData) throws Exception {
        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpRepairMapper.updateByPrimaryKeySelective", pageData);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) throws Exception {
        List<String> list = Arrays.asList(ids);
        dao.update("FpRepairMapper.updateByPrimaryKey", list);
    }

    @Override
    public void download(List<PageData> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PageData fpRepair : all) {
            /*Map<String, Object> map = new LinkedHashMap<>();
            map.put(" repairOrder", fpRepair.getRepairOrder());
            map.put(" repairSource", fpRepair.getRepairSource());
            map.put(" repairDate", fpRepair.getRepairDate());
            map.put(" repairState", fpRepair.getRepairState());
            map.put(" equipmentId", fpRepair.getEquipmentId());
            map.put(" resRailwayId", fpRepair.getResRailwayId());
            map.put(" resTunnelId", fpRepair.getResTunnelId());
            map.put(" resRegionId", fpRepair.getResRegionId());
            map.put(" mileageNumbe", fpRepair.getMileageNumbe());
            map.put(" downLines", fpRepair.getDownLines());
            map.put(" repairPeople", fpRepair.getRepairPeople());
            map.put(" faultDescription", fpRepair.getFaultDescription());
            map.put(" maintenanceUnit", fpRepair.getMaintenanceUnit());
            map.put(" faultCategory", fpRepair.getFaultCategory());
            map.put(" repairBeginTime", fpRepair.getRepairBeginTime());
            map.put(" repairEndTime", fpRepair.getRepairEndTime());
            map.put(" faultAnalysis", fpRepair.getFaultAnalysis());
            map.put(" isDelete", fpRepair.getIsDelete());
            map.put(" createId", fpRepair.getCreateId());
            map.put(" createTime", fpRepair.getCreateTime());
            map.put(" updateTime", fpRepair.getUpdateTime());
            list.add(map);*/
        }
        FileUtil.downloadExcel(list, response);
    }

}
