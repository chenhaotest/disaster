package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.dto.FpRiskAssessmentDto;
import com.perye.dokit.entity.FpRiskAssessment;
import com.perye.dokit.mapper.FpRiskAssessmentMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpRiskAssessmentQueryCriteria;
import com.perye.dokit.repository.FpRiskAssessmentRepository;
import com.perye.dokit.service.FpRiskAssessmentService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpRiskAssessment")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpRiskAssessmentServiceImpl implements FpRiskAssessmentService {

    private final FpRiskAssessmentRepository fpRiskAssessmentRepository;

    private final FpRiskAssessmentMapper fpRiskAssessmentMapper;
    @Resource(name = "daoSupport")
    private DaoSupport dao;

    public FpRiskAssessmentServiceImpl(FpRiskAssessmentRepository fpRiskAssessmentRepository, FpRiskAssessmentMapper fpRiskAssessmentMapper) {
        this.fpRiskAssessmentRepository = fpRiskAssessmentRepository;
        this.fpRiskAssessmentMapper = fpRiskAssessmentMapper;
    }

    /**
     * 查询数据分页
     * @param pageData 条件
     * @return Map<String,Object>
     */

    @Override
    public List<PageData> fpRiskAssessmentList(PageData pageData) throws Exception {
        List<PageData> pageList = (List<PageData>) dao.findForList("fpRiskAssessmentMapper.selectPageList", pageData);
        return pageList;
    }


    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpRiskAssessmentQueryCriteria criteria, Pageable pageable){
        Page<FpRiskAssessment> page = fpRiskAssessmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpRiskAssessmentMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpRiskAssessmentDto> queryAll(FpRiskAssessmentQueryCriteria criteria){
        return fpRiskAssessmentMapper.toDto(fpRiskAssessmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpRiskAssessmentDto findById(String id) {
        FpRiskAssessment fpRiskAssessment = fpRiskAssessmentRepository.findById(id).orElseGet(FpRiskAssessment::new);
        ValidationUtil.isNull(fpRiskAssessment.getId(),"FpRiskAssessment","id",id);
        return fpRiskAssessmentMapper.toDto(fpRiskAssessment);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpRiskAssessmentDto create(FpRiskAssessment resources) {
        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());
        return fpRiskAssessmentMapper.toDto(fpRiskAssessmentRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpRiskAssessment resources) {
        FpRiskAssessment fpRiskAssessment = fpRiskAssessmentRepository.findById(resources.getId()).orElseGet(FpRiskAssessment::new);
        ValidationUtil.isNull( fpRiskAssessment.getId(),"FpRiskAssessment","id",resources.getId());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        fpRiskAssessment.copy(resources);
        fpRiskAssessmentRepository.save(fpRiskAssessment);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("fpRiskAssessmentMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpRiskAssessmentDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpRiskAssessmentDto fpRiskAssessment : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" evaluationTime",  fpRiskAssessment.getEvaluationTime());
                         map.put(" assessor",  fpRiskAssessment.getAssessor());
                         map.put(" contactPhone",  fpRiskAssessment.getContactPhone());
                         map.put(" rectificationSituation",  fpRiskAssessment.getRectificationSituation());
                         map.put(" assessmentReport",  fpRiskAssessment.getAssessmentReport());
                         map.put(" evaluationUnit",  fpRiskAssessment.getEvaluationUnit());
                         map.put(" evaluate",  fpRiskAssessment.getEvaluate());
                         map.put(" createId",  fpRiskAssessment.getCreateId());
                         map.put(" createTime",  fpRiskAssessment.getCreateTime());
                         map.put(" updateTime",  fpRiskAssessment.getUpdateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
