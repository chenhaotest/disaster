package com.perye.dokit.service;


import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface FpAlarmSuspendPointService {


    List<PageData> getAll(PageData pageData) throws Exception;

    void add(PageData pageData) throws Exception;

    void update(PageData pageData) throws Exception;

    void deleteAll(String[] ids) throws Exception;
}