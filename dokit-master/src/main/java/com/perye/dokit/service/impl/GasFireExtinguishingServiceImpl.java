package com.perye.dokit.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.GasFireExtinguishingService;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GasFireExtinguishingServiceImpl implements GasFireExtinguishingService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private DictDetailService dictDetailService;

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        List<PageData> logList = (List<PageData>) dao.findForList("GasFireExtinguishingMapper.getAll", pageData);

        Map<String, String> stateMap = dictDetailService.queryDictDetailByName("device_state", "map");
        Map<String, String> gfMap = dictDetailService.queryDictDetailByName("gf_is_abnormal", "map");
        Map<String, String> avMap = dictDetailService.queryDictDetailByName("av_is_abnormal", "map");
        Map<String, String> biMap = dictDetailService.queryDictDetailByName("bi_is_abnormal", "map");
        Map<String, String> hbMap = dictDetailService.queryDictDetailByName("hb_is_abnormal", "map");
        Map<String, String> imgMap = dictDetailService.queryDictDetailByName("img_is_abnormal", "map");
        Map<String, String> mbMap = dictDetailService.queryDictDetailByName("mb_is_abnormal", "map");
        Map<String, String> sdMap = dictDetailService.queryDictDetailByName("sd_is_abnormal", "map");
        Map<String, String> sfMap = dictDetailService.queryDictDetailByName("sf_is_abnormal", "map");
        Map<String, String> vrMap = dictDetailService.queryDictDetailByName("vr_is_abnormal", "map");
        Map<String, String> wsMap = dictDetailService.queryDictDetailByName("ws_is_abnormal", "map");

        if (CollectionUtil.isNotEmpty(logList)) {

            Map<String, List> map = new HashMap<>();
            List<PageData> imgList = (List<PageData>) dao.findForList("GasFireExtinguishingMapper.queryImgList", pageData);
            if (CollectionUtil.isNotEmpty(imgList)) {
                for (PageData data : imgList) {
                    String deviceId = data.getString("deviceId");
                    List<String> list = map.get(deviceId);
                    if (CollectionUtil.isEmpty(list)) {
                        list = new ArrayList<String>();
                    }
                    if (StringUtils.isNotBlank(data.getString("url"))) {
                        list.add(data.getString("url"));
                        map.put(deviceId, list);
                    }
                }
            }

            for (PageData log : logList) {

                String id = log.getString("id");
                List<String> list = map.get(id);
                if (CollectionUtil.isNotEmpty(list)) {
                    log.put("imgList", list);
                }

                String state = log.getString("state");
                log.put("deviceState", "");
                if (StringUtils.isNotBlank(state)) {
                    log.put("deviceState", stateMap.get(state));
                }

                //设备角色：0：管理机，1：温感，2：烟感，3：图像型火灾探测器，4：人体识别传感器，5：语音提示器，6：悬挂式气体灭火装置，7：手动按钮，8：声光报警器，9：放气指示灯
                String deviceRole = log.getString("deviceRole");
                String isAbnormal = log.getString("isAbnormal");
                if ("0".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", gfMap.get(isAbnormal));
                    }
                } else if ("1".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", wsMap.get(isAbnormal));
                    }
                } else if ("2".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", sdMap.get(isAbnormal));
                    }
                } else if ("3".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", imgMap.get(isAbnormal));
                    }
                } else if ("4".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", hbMap.get(isAbnormal));
                    }
                } else if ("5".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", vrMap.get(isAbnormal));
                    }
                } else if ("6".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", sfMap.get(isAbnormal));
                    }
                } else if ("7".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", mbMap.get(isAbnormal));
                    }
                } else if ("8".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", avMap.get(isAbnormal));
                    }
                } else if ("9".equals(deviceRole)) {
                    log.put("isAbnormal", "");
                    if (StringUtils.isNotBlank(isAbnormal)) {
                        log.put("isAbnormalStr", biMap.get(isAbnormal));
                    }
                }


            }
        }
        return logList;
    }

    @Override
    public List<PageData> findAllDevce(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("DeviceErrorLogMapper.queryDeviceAll", pageData);
    }

    @Override
    public PageData findWarningLevel(PageData pageData) throws Exception {
        PageData result = (PageData) dao.findForObject("DeviceErrorLogMapper.findWarningLevel", pageData);
        return result;
    }

    @Override
    public List<PageData> findWarningTimeLevel(PageData pageData) throws Exception {
        List<PageData> list = (List<PageData>) dao.findForList("DeviceErrorLogMapper.findWarningTimeLevel", pageData);
        if (list.size() == 0) {
            PageData pd = new PageData();
            pd.put("level", 1);
            pd.put("value", 0);
            list.add(pd);
            pd.put("level", 2);
            list.add(pd);
            pd.put("level", 3);
            list.add(pd);
            return list;
        }

        List<PageData> newlist = new ArrayList<>();

        for (int i = 1; i < 3; i++) {
            boolean flag = false;
            for (int j = 0; j < list.size(); j++) {
                PageData pj = list.get(j);
                if (StringUtils.equals(i + "", pj.get("level") + "")) {
                    newlist.add(pj);
                    flag = true;
                }

            }

            if (!flag) {
                PageData pd = new PageData();
                pd.put("level", i);
                pd.put("value", 0);
                newlist.add(pd);
            }

        }

        return list;
    }

    @Override
    public List<PageData> findDeviceNumEchart(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("DeviceErrorLogMapper.findDeviceNumEchart", pageData);
    }

    @Override
    public PageData findDeviceByCode(PageData pageData) throws Exception {
        PageData result = (PageData) dao.findForObject("DeviceErrorLogMapper.findDeviceByCode", pageData);
        return result;
    }

    @Override
    public List<  List<PageData>> findDevFireImg(PageData pageData) throws Exception {
        List<  List<PageData>> listPd=new ArrayList<>();
        List<PageData> list= (List<PageData>) dao.findForList("DeviceErrorLogMapper.findDevFireImg", pageData);

        if(list.size()>0){
            List<PageData> paramsPd = new ArrayList<>();
            //循环数据
            for (int i = 0; i <list.size() ; i++) {
                PageData pd = list.get(i);
                paramsPd.add(pd);
                //当集合里放了两个图片，或者 总的图片和 循环数据一样。
                if (paramsPd.size() == 2 || (list.size()-1) == i) {
                    listPd.add(paramsPd);
                    paramsPd = new ArrayList<>();
                }

            }

        }


        return listPd;
    }
}
