package com.perye.dokit.service.impl;

import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.ChenTestService;
import com.perye.dokit.utils.FileUtil;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "test")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class ChenTestServiceImpl implements ChenTestService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Override
    @Cacheable(value="list")
    public List<PageData> queryAll(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("vueMapper.getList", pageData);
    }

    @Override

    public List<PageData> queryAllLi(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("vueMapper.queryAllLi", pageData);
    }

    @Override
    @CacheEvict(value="list",allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void create(PageData pageData) throws Exception {
        dao.save("vueMapper.create",pageData);
    }

    @Override
    public void update(PageData pageData) throws Exception {
        dao.update("vueMapper.update",pageData);
    }

    @Override
    public void deleteAll(Integer[] ids) throws Exception {
        dao.delete("vueMapper.deleteAll",ids);
    }

    @Override
    public void download(List<PageData> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PageData test : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("邮箱", test.getString("email"));
            map.put("用户名",  test.getString("username"));
            map.put("创建时间",  test.get("createTime"));
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public Integer getCount(PageData pageData) throws Exception {
        return (Integer) dao.findForObject("vueMapper.getCount", pageData);
    }


}
