package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.perye.dokit.dto.FpModubsDto;
import com.perye.dokit.entity.FpModubs;
import com.perye.dokit.mapper.FpModubsMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpModubsQueryCriteria;
import com.perye.dokit.repository.FpModubsRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpModubsService;
import com.perye.dokit.service.FpResourceService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.vo.FpResourceVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpModubs")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpModubsServiceImpl implements FpModubsService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private DictDetailService dictDetailService;
    private final FpModubsRepository fpModubsRepository;

    private final FpModubsMapper fpModubsMapper;

    @Autowired
    private FpResourceService fpResourceService;
    public FpModubsServiceImpl(FpModubsRepository fpModubsRepository, FpModubsMapper fpModubsMapper) {
        this.fpModubsRepository = fpModubsRepository;
        this.fpModubsMapper = fpModubsMapper;
    }
    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */

    @Override
    public List<PageData> fpModubsList(PageData pageData) throws Exception {
        List<PageData> pageList = (List<PageData>) dao.findForList("fpModubsMapper.selectPageList", pageData);

        //是否启用
        Map<String,String> dmap2 = dictDetailService.queryDictDetailByName("universal_activation","map");
        //资源类型[0铁路，1隧道/站房，2区域/几楼站厅,3子系统(如果是子系统则有子系统类型否则没有)]
        List<String> data =new ArrayList<>();
        data.add("0");
        data.add("1");
        data.add("2");
        Map<String, String> map = fpResourceService.queryResourceListByType(data).stream().collect(Collectors.toMap(FpResourceVo::getId, FpResourceVo::getName,(key1 , key2)-> key2 ));

        for (int i = 0; i <pageList.size() ; i++) {
            //区域隧道值
            if(StrUtil.isNotBlank((pageList.get(i).getString("resRailwayId")))){
                pageList.get(i).put("resRailwayIdLabel", map.get(pageList.get(i).getString("resRailwayId")));
            }
            //站房
            if(StrUtil.isNotBlank((pageList.get(i).getString("resTunnelId")))){
                //获取隧道 站房  区域 子系统 的值
                pageList.get(i).put("resTunnelIdLabel", map.get(pageList.get(i).getString("resTunnelId")));
            }

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("state")))){
                pageList.get(i).put("stateLabel", dmap2.get(pageList.get(i).get("state")));

            }
        }
        return pageList;
    }



    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpModubsQueryCriteria criteria, Pageable pageable){
        Page<FpModubs> page = fpModubsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpModubsMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpModubsDto> queryAll(FpModubsQueryCriteria criteria){
        return fpModubsMapper.toDto(fpModubsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpModubsDto findById(String id) {
        FpModubs fpModubs = fpModubsRepository.findById(id).orElseGet(FpModubs::new);
        ValidationUtil.isNull(fpModubs.getId(),"FpModubs","id",id);
        return fpModubsMapper.toDto(fpModubs);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpModubsDto create(FpModubs resources) {
        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());
        return fpModubsMapper.toDto(fpModubsRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpModubs resources) {
        FpModubs fpModubs = fpModubsRepository.findById(resources.getId()).orElseGet(FpModubs::new);
        ValidationUtil.isNull( fpModubs.getId(),"FpModubs","id",resources.getId());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        fpModubs.copy(resources);
        fpModubsRepository.save(fpModubs);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("fpModubsMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpModubsDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpModubsDto fpModubs : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" networkAddress",  fpModubs.getNetworkAddress());
                         map.put(" ports",  fpModubs.getPorts());
                         map.put(" resRailwayId",  fpModubs.getResRailwayId());
                         map.put(" resTunnelId",  fpModubs.getResTunnelId());
                         map.put(" state",  fpModubs.getState());
                         map.put(" description",  fpModubs.getDescription());
                         map.put(" readMax",  fpModubs.getReadMax());
                         map.put(" isDelete",  fpModubs.getIsDelete());
                         map.put(" createId",  fpModubs.getCreateId());
                         map.put(" createTime",  fpModubs.getCreateTime());
                         map.put(" updateTime",  fpModubs.getUpdateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
