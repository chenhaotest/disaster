package com.perye.dokit.service;

import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface FpAlarmConfirmationService {

     /**
     * 查询数据分页
     * @return Map<String,Object>
     */
    List<PageData> getAll(PageData pageData) throws Exception;
/*
    Map<String,Object> queryAll(FpAlarmConfirmationQueryCriteria criteria, Pageable pageable);

    *//**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<FpAlarmConfirmationDto>
    *//*
    List<FpAlarmConfirmationDto> queryAll(FpAlarmConfirmationQueryCriteria criteria);

    *//**
    * 根据ID查询
    * @param id ID
    * @return FpAlarmConfirmationDto
    *//*
    FpAlarmConfirmationDto findById(String id);

    *//**
    * 创建
    * @param resources /
    * @return FpAlarmConfirmationDto
    *//*
    FpAlarmConfirmationDto create(FpAlarmConfirmation resources);

    *//**
    * 编辑
    * @param resources /
    *//*
    void update(FpAlarmConfirmation resources);

    *//**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    *//*
    void download(List<FpAlarmConfirmationDto> all, HttpServletResponse response) throws IOException;

    *//**
    * 多选删除
    * @param ids /
    *//*
    void deleteAll(String[] ids);*/
}