package com.perye.dokit.service.impl;


import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.FpAlarmSuspendPointService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpAlarmSuspendPoint")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpAlarmSuspendPointServiceImpl implements FpAlarmSuspendPointService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpAlarmSuspendPointMapper.queryAll", pageData);
    }

    @Override
    public void add(PageData pageData) throws Exception {
        String id = IdUtil.simpleUUID();
        pageData.put("id", id);
        pageData.put("createTime", DateUtils.getTime());
        dao.save("FpAlarmSuspendPointMapper.insert", pageData);
    }

    @Override
    public void update(PageData pageData) throws Exception {
        dao.update("FpAlarmSuspendPointMapper.update", pageData);
    }

    @Override
    public void deleteAll(String[] ids) throws Exception {
        dao.update("FpAlarmSuspendPointMapper.deleteAll", ids);
    }
}
