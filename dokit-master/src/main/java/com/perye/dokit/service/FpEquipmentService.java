package com.perye.dokit.service;

import com.perye.dokit.dto.FpEquipmentDto;
import com.perye.dokit.entity.FpEquipment;
import com.perye.dokit.query.FpEquipmentQueryCriteria;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface FpEquipmentService {


    public PageData importFpEquipment(MultipartFile file)throws Exception;
    /**
     * 下载
     *
     * @param fileName 文件名称
     * @param response
     * @param request excel数据
     */
    public  void downLoadExcel( HttpServletResponse response, HttpServletRequest request,String fileName)throws IOException;


    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */

    public List<PageData> fpEquipmentList(PageData pageData) throws Exception;
    /**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(FpEquipmentQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<FpEquipmentDto>
    */
    List<FpEquipmentDto> queryAll(FpEquipmentQueryCriteria criteria);

    /**
    * 根据ID查询
    * @param id ID
    * @return FpEquipmentDto
    */
    FpEquipmentDto findById(String id);

    /**
    * 创建
    * @param resources /
    * @return FpEquipmentDto
    */
    FpEquipmentDto create(FpEquipment resources);

    /**
    * 编辑
    * @param resources /
    */
    void update(FpEquipment resources);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<FpEquipmentDto> all, HttpServletResponse response) throws IOException;

    /**
    * 多选删除
    * @param ids /
    */
    void deleteAll(String[] ids);
}
