package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.perye.dokit.dto.FpPointLogDto;
import com.perye.dokit.entity.FpPointLog;
import com.perye.dokit.mapper.FpPointLogMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpPointLogQueryCriteria;
import com.perye.dokit.repository.FpPointLogRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpPointLogService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpPointLog")
@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Exception.class)
public class FpPointLogServiceImpl implements FpPointLogService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private DictDetailService dictDetailService;
    private final FpPointLogRepository fpPointLogRepository;

    private final FpPointLogMapper fpPointLogMapper;

    public FpPointLogServiceImpl(FpPointLogRepository fpPointLogRepository, FpPointLogMapper fpPointLogMapper) {
        this.fpPointLogRepository = fpPointLogRepository;
        this.fpPointLogMapper = fpPointLogMapper;
    }
    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */
    @Override
    public List<PageData> fpPointLogNewList(PageData pageData) throws Exception {
        List<PageData> pageList = (List<PageData>) dao.findForList("FpPointLogMapper.selectPageList", pageData);
        //属性点类型：1风机，2风阀等
        Map<String,String> pointType = dictDetailService.queryDictDetailByName("point_type","map");

        //映射类型：1开，2关等
        Map<String,String> mappingType = dictDetailService.queryDictDetailByName("mapping_type","map");


        //是否告警：1是、0否
        Map<String,String> isAlarm = dictDetailService.queryDictDetailByName("yes_or_not","map");




        for (int i = 0; i <pageList.size() ; i++) {
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("pointType")))){
                pageList.get(i).put("pointTypeLabel", pointType.get(pageList.get(i).get("pointType")));
            }

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("mappingType")))){
                pageList.get(i).put("mappingTypeLabel", mappingType.get(pageList.get(i).get("mappingType")));
            }



            if(ObjectUtil.isNotEmpty((pageList.get(i).get("isAlarm")))){
                pageList.get(i).put("isAlarmLabel", isAlarm.get(pageList.get(i).get("isAlarm")));
            }

        }
        return pageList;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpPointLogQueryCriteria criteria, Pageable pageable){
        Page<FpPointLog> page = fpPointLogRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpPointLogMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpPointLogDto> queryAll(FpPointLogQueryCriteria criteria){
        return fpPointLogMapper.toDto(fpPointLogRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpPointLogDto findById(String id) {
        FpPointLog fpPointLog = fpPointLogRepository.findById(id).orElseGet(FpPointLog::new);
        ValidationUtil.isNull(fpPointLog.getId(),"FpPointLog","id",id);
        return fpPointLogMapper.toDto(fpPointLog);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpPointLogDto create(FpPointLog resources) {
        resources.setId(IdUtil.simpleUUID());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        return fpPointLogMapper.toDto(fpPointLogRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpPointLog resources) {
        FpPointLog fpPointLog = fpPointLogRepository.findById(resources.getId()).orElseGet(FpPointLog::new);
        ValidationUtil.isNull( fpPointLog.getId(),"FpPointLog","id",resources.getId());
        fpPointLog.copy(resources);
        fpPointLogRepository.save(fpPointLog);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        for (String id : ids) {
            fpPointLogRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<FpPointLogDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpPointLogDto fpPointLog : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" pointName",  fpPointLog.getPointName());
                         map.put(" pointType",  fpPointLog.getPointType());
                         map.put(" mappingType",  fpPointLog.getMappingType());
                         map.put(" registerAddress",  fpPointLog.getRegisterAddress());
                         map.put(" registerAddressByte",  fpPointLog.getRegisterAddressByte());
                         map.put(" modbusId",  fpPointLog.getModbusId());
                         map.put(" resRailwayId",  fpPointLog.getResRailwayId());
                         map.put(" resTunnelId",  fpPointLog.getResTunnelId());
                         map.put(" resRegionId",  fpPointLog.getResRegionId());
                         map.put(" resSysId",  fpPointLog.getResSysId());
                         map.put(" equipmentId",  fpPointLog.getEquipmentId());
                         map.put(" batch",  fpPointLog.getBatch());
                         map.put(" connectionType",  fpPointLog.getConnectionType());
                         map.put(" isAlarm",  fpPointLog.getIsAlarm());
                         map.put(" registerAddressVal",  fpPointLog.getRegisterAddressVal());
                         map.put(" updateTime",  fpPointLog.getUpdateTime());
                         map.put(" createTime",  fpPointLog.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
