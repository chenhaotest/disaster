package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.perye.dokit.dto.FpHotWorkDto;
import com.perye.dokit.entity.FpHotWork;
import com.perye.dokit.mapper.FpHotWorkMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpHotWorkQueryCriteria;
import com.perye.dokit.repository.FpHotWorkRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpHotWorkService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpHotWork")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpHotWorkServiceImpl implements FpHotWorkService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private DictDetailService dictDetailService;
    private final FpHotWorkRepository fpHotWorkRepository;

    private final FpHotWorkMapper fpHotWorkMapper;

    public FpHotWorkServiceImpl(FpHotWorkRepository fpHotWorkRepository, FpHotWorkMapper fpHotWorkMapper) {
        this.fpHotWorkRepository = fpHotWorkRepository;
        this.fpHotWorkMapper = fpHotWorkMapper;
    }
    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */

    @Override
    public List<PageData> fpHotWorklList(PageData pageData) throws Exception {
        List<PageData> pageList = (List<PageData>) dao.findForList("fpHotWorkMapper.selectPageList", pageData);

        //动火作业人员类型[0内部，1外部]
        Map<String,String> wptMap = dictDetailService.queryDictDetailByName("work_people_type","map");
        //审批状态[0:审批中，1拒绝，2驳回修改，3审批通过]
        Map<String,String> asMap = dictDetailService.queryDictDetailByName("approval_status","map");

        for (int i = 0; i <pageList.size() ; i++) {

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("workPeopleType")))){
                pageList.get(i).put("workPeopleTypeLabel", wptMap.get(pageList.get(i).get("workPeopleType")));
            }
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("approvalStatus")))){
                pageList.get(i).put("approvalStatusLabel", asMap.get(pageList.get(i).get("approvalStatus")));
            }
        }
        return pageList;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpHotWorkQueryCriteria criteria, Pageable pageable){
        Page<FpHotWork> page = fpHotWorkRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpHotWorkMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpHotWorkDto> queryAll(FpHotWorkQueryCriteria criteria){
        return fpHotWorkMapper.toDto(fpHotWorkRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpHotWorkDto findById(String id) {
        FpHotWork fpHotWork = fpHotWorkRepository.findById(id).orElseGet(FpHotWork::new);
        ValidationUtil.isNull(fpHotWork.getId(),"FpHotWork","id",id);
        return fpHotWorkMapper.toDto(fpHotWork);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpHotWorkDto create(FpHotWork resources) {
        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());

        //'审批状态[0:审批中，1拒绝，2驳回修改，3审批通过]',
        resources.setApprovalStatus("0");
        return fpHotWorkMapper.toDto(fpHotWorkRepository.save(resources));

    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpHotWork resources) {
        FpHotWork fpHotWork = fpHotWorkRepository.findById(resources.getId()).orElseGet(FpHotWork::new);
        ValidationUtil.isNull( fpHotWork.getId(),"FpHotWork","id",resources.getId());
        fpHotWork.copy(resources);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        //'审批状态[0:审批中，1拒绝，2驳回修改，3审批通过]',
        resources.setApprovalStatus("0");
        fpHotWorkRepository.save(fpHotWork);
    }

    /**
     * 更改审批状态
     * @param
     * @return
     */
    @Override
//@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void updataApprovalStatusById(FpHotWork resources) {
            FpHotWork fpHotWork = fpHotWorkRepository.findById(resources.getId()).orElseGet(FpHotWork::new);
            ValidationUtil.isNull( fpHotWork.getId(),"FpHotWork","id",resources.getId());

        resources.setApprovalBy(SecurityUtils.getCurrentUsername());
            fpHotWork.copy(resources);
            Timestamp timestamp= new Timestamp(System.currentTimeMillis());
            resources.setUpdateTime(timestamp);
            //'审批状态[0:审批中，1拒绝，2驳回修改，3审批通过]',

            fpHotWorkRepository.save(fpHotWork);
        }





    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("fpHotWorkMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpHotWorkDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpHotWorkDto fpHotWork : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" workDefinition",  fpHotWork.getWorkDefinition());
                         map.put(" workName",  fpHotWork.getWorkName());
                         map.put(" useTool",  fpHotWork.getUseTool());
                         map.put(" workAddressRange",  fpHotWork.getWorkAddressRange());
                         map.put(" workBeginTime",  fpHotWork.getWorkBeginTime());
                         map.put(" workEndTime",  fpHotWork.getWorkEndTime());
                         map.put(" workUnit",  fpHotWork.getWorkUnit());
                         map.put(" workPeopleType",  fpHotWork.getWorkPeopleType());
                         map.put(" workPeoples",  fpHotWork.getWorkPeoples());
                         map.put(" inform",  fpHotWork.getInform());
                         map.put(" workSupervise",  fpHotWork.getWorkSupervise());
                         map.put(" regulatoryRecords",  fpHotWork.getRegulatoryRecords());
                         map.put(" approvalStatus",  fpHotWork.getApprovalStatus());
                         map.put(" approvalOpinions",  fpHotWork.getApprovalOpinions());
                         map.put(" approvalBy",  fpHotWork.getApprovalBy());
                         map.put(" isDelete",  fpHotWork.getIsDelete());
                         map.put(" createTime",  fpHotWork.getCreateTime());
                         map.put(" updateTime",  fpHotWork.getUpdateTime());
                         map.put(" createId",  fpHotWork.getCreateId());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
