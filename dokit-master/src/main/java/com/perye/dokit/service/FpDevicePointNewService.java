package com.perye.dokit.service;

import com.perye.dokit.dto.FpDevicePointNewDto;
import com.perye.dokit.entity.FpDevicePointNew;
import com.perye.dokit.query.FpDevicePointNewQueryCriteria;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface FpDevicePointNewService {
    /**
     * 分组查询数据
     * @param pageData pageData
     * @return Map<String,Object>
     */

    public List<PageData> selectListGroupBy(PageData pageData) throws Exception;

    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */

    public List<PageData> fpDevicePointNewList(PageData pageData) throws Exception;
    /**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(FpDevicePointNewQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<FpDevicePointNewDto>
    */
    List<FpDevicePointNewDto> queryAll(FpDevicePointNewQueryCriteria criteria);

    /**
    * 根据ID查询
    * @param id ID
    * @return FpDevicePointNewDto
    */
    FpDevicePointNewDto findById(String id);

    /**
    * 创建
    * @param resources /
    * @return FpDevicePointNewDto
    */
    FpDevicePointNewDto create(FpDevicePointNew resources);

    /**
    * 编辑
    * @param resources /
    */
    void update(FpDevicePointNew resources);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<FpDevicePointNewDto> all, HttpServletResponse response) throws IOException;

    /**
    * 多选删除
    * @param ids /
    */
    void deleteAll(String[] ids);
}
