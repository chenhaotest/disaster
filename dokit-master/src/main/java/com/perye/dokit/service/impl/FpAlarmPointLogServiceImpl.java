package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.perye.dokit.dto.FpAlarmPointLogDto;
import com.perye.dokit.entity.FpAlarmPointLog;
import com.perye.dokit.mapper.FpAlarmPointLogMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpAlarmPointLogQueryCriteria;
import com.perye.dokit.repository.FpAlarmPointLogRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpAlarmPointLogService;
import com.perye.dokit.utils.FileUtil;
import com.perye.dokit.utils.PageUtil;
import com.perye.dokit.utils.QueryHelp;
import com.perye.dokit.utils.ValidationUtil;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpAlarmPointLog")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpAlarmPointLogServiceImpl implements FpAlarmPointLogService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private DictDetailService dictDetailService;
    private final FpAlarmPointLogRepository fpAlarmPointLogRepository;

    private final FpAlarmPointLogMapper fpAlarmPointLogMapper;

    public FpAlarmPointLogServiceImpl(FpAlarmPointLogRepository fpAlarmPointLogRepository, FpAlarmPointLogMapper fpAlarmPointLogMapper) {
        this.fpAlarmPointLogRepository = fpAlarmPointLogRepository;
        this.fpAlarmPointLogMapper = fpAlarmPointLogMapper;
    }
    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */

    @Override
    public List<PageData> fpAlarmPointLogList(PageData pageData) throws Exception {
        List<PageData> pageList = (List<PageData>) dao.findForList("FpPointAlarmLogMapper.selectPageList", pageData);
        //属性点类型：1风机，2风阀等
        Map<String,String> pointType = dictDetailService.queryDictDetailByName("point_type","map");

        //映射类型：1开，2关等
        Map<String,String> mappingType = dictDetailService.queryDictDetailByName("mapping_type","map");


        //'状态：1未开始，2进行中，3已结束',
        Map<String,String> state = dictDetailService.queryDictDetailByName("alarm_state","map");




        for (int i = 0; i <pageList.size() ; i++) {
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("pointType")))){
                pageList.get(i).put("pointTypeLabel", pointType.get(pageList.get(i).get("pointType")));
            }

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("mappingType")))){
                pageList.get(i).put("mappingTypeLabel", mappingType.get(pageList.get(i).get("mappingType")));
            }



            if(ObjectUtil.isNotEmpty((pageList.get(i).get("state")))){
                pageList.get(i).put("stateLabel", state.get(pageList.get(i).get("state")));
            }

        }
        return pageList;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpAlarmPointLogQueryCriteria criteria, Pageable pageable){
        Page<FpAlarmPointLog> page = fpAlarmPointLogRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpAlarmPointLogMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpAlarmPointLogDto> queryAll(FpAlarmPointLogQueryCriteria criteria){
        return fpAlarmPointLogMapper.toDto(fpAlarmPointLogRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpAlarmPointLogDto findById(String id) {
        FpAlarmPointLog fpAlarmPointLog = fpAlarmPointLogRepository.findById(id).orElseGet(FpAlarmPointLog::new);
        ValidationUtil.isNull(fpAlarmPointLog.getId(),"FpAlarmPointLog","id",id);
        return fpAlarmPointLogMapper.toDto(fpAlarmPointLog);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpAlarmPointLogDto create(FpAlarmPointLog resources) {
        resources.setId(IdUtil.simpleUUID());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        return fpAlarmPointLogMapper.toDto(fpAlarmPointLogRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpAlarmPointLog resources) {
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        FpAlarmPointLog fpAlarmPointLog = fpAlarmPointLogRepository.findById(resources.getId()).orElseGet(FpAlarmPointLog::new);
        ValidationUtil.isNull( fpAlarmPointLog.getId(),"FpAlarmPointLog","id",resources.getId());
        fpAlarmPointLog.copy(resources);

        fpAlarmPointLogRepository.save(fpAlarmPointLog);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        for (String id : ids) {
            fpAlarmPointLogRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<FpAlarmPointLogDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpAlarmPointLogDto fpAlarmPointLog : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" pointName",  fpAlarmPointLog.getPointName());
                         map.put(" pointType",  fpAlarmPointLog.getPointType());
                         map.put(" mappingType",  fpAlarmPointLog.getMappingType());
                         map.put(" registerAddress",  fpAlarmPointLog.getRegisterAddress());
                         map.put(" registerAddressByte",  fpAlarmPointLog.getRegisterAddressByte());
                         map.put(" modbusId",  fpAlarmPointLog.getModbusId());
                         map.put(" resRailwayId",  fpAlarmPointLog.getResRailwayId());
                         map.put(" resTunnelId",  fpAlarmPointLog.getResTunnelId());
                         map.put(" resRegionId",  fpAlarmPointLog.getResRegionId());
                         map.put(" resSysId",  fpAlarmPointLog.getResSysId());
                         map.put(" equipmentId",  fpAlarmPointLog.getEquipmentId());
                         map.put(" batch",  fpAlarmPointLog.getBatch());
                         map.put(" state",  fpAlarmPointLog.getState());
                         map.put(" registerAddressVal",  fpAlarmPointLog.getRegisterAddressVal());
                         map.put(" updateTime",  fpAlarmPointLog.getUpdateTime());
                         map.put(" createTime",  fpAlarmPointLog.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
