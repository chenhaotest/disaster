package com.perye.dokit.service;

import com.perye.dokit.dto.DictDetailDto;
import com.perye.dokit.query.DictDetailQueryCriteria;
import com.perye.dokit.entity.DictDetail;
import org.springframework.data.domain.Pageable;

import java.util.Map;

public interface DictDetailService {

    /**
     * 根据ID查询
     *
     * @param id /
     * @return /
     */
    DictDetailDto findById(Long id);

    /**
     * 创建
     *
     * @param resources /
     * @return /
     */
    DictDetailDto create(DictDetail resources);

    /**
     * 编辑
     *
     * @param resources /
     */
    void update(DictDetail resources);

    /**
     * 删除
     *
     * @param id /
     */
    void delete(Long id);

    /**
     * 分页查询
     *
     * @param criteria 条件
     * @param pageable 分页参数
     * @return /
     */
    Map queryAll(DictDetailQueryCriteria criteria, Pageable pageable);

    /**
     * @Description 根据父级字典的name来查询该父级下所有的子级
     * @Date 2022/4/18 16:00
     * @Param name：父级字典的名称；
     * @Param type：想要返回的类型(目前支持：map、list)，type传的是什么，返回值的类型就是什么
     * @Return 返回值有list和map两种形式，如果type传的是map，则返回的是map类型的参数；如果type传的是list，则返回的是list类型的参数
     * @Exception
     */
    <T> T queryDictDetailByName(String name, String type);
}
