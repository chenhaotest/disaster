package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.perye.dokit.dto.FpPersonnelDto;
import com.perye.dokit.entity.FpPersonnel;
import com.perye.dokit.mapper.FpPersonnelMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpPersonnelQueryCriteria;
import com.perye.dokit.repository.FpPersonnelRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpPersonnelService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpPersonnel")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpPersonnelServiceImpl implements FpPersonnelService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private DictDetailService dictDetailService;

    private final FpPersonnelRepository fpPersonnelRepository;

    private final FpPersonnelMapper fpPersonnelMapper;

    public FpPersonnelServiceImpl(FpPersonnelRepository fpPersonnelRepository, FpPersonnelMapper fpPersonnelMapper) {
        this.fpPersonnelRepository = fpPersonnelRepository;
        this.fpPersonnelMapper = fpPersonnelMapper;
    }

    /**
     * 查询数据分页
     * @param pageData pageData
     * @return List<PageData>
     */
    @Override
    public List<PageData> fpPersonnelList(PageData pageData) throws Exception {

        List<PageData> pageList = (List<PageData>) dao.findForList("fpPersonnelMapper.selectPageList", pageData);

        //是否启用 通用状态
        Map<String,String> dmap2 = dictDetailService.queryDictDetailByName("universal_activation","map");

        //性别[0女；1男]
        Map<String,String> sexMap = dictDetailService.queryDictDetailByName("sex","map");

        //人员类型[0维修，1值班，2 巡检，3行政]
        Map<String,String> personnelTypeMap = dictDetailService.queryDictDetailByName("personnel_type","map");

        for (int i = 0; i <pageList.size() ; i++) {

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("state")))){
                pageList.get(i).put("stateLabel", dmap2.get(pageList.get(i).get("state")));

            }
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("state")))){
                pageList.get(i).put("sexLabel", sexMap.get(pageList.get(i).get("sex")));

            }
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("state")))){
                pageList.get(i).put("personnelTypeLabel", personnelTypeMap.get(pageList.get(i).get("personnelType")));

            }
        }
        return pageList;
    }




    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpPersonnelQueryCriteria criteria, Pageable pageable){
        Page<FpPersonnel> page = fpPersonnelRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpPersonnelMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpPersonnelDto> queryAll(FpPersonnelQueryCriteria criteria){
        return fpPersonnelMapper.toDto(fpPersonnelRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpPersonnelDto findById(String id) {
        FpPersonnel fpPersonnel = fpPersonnelRepository.findById(id).orElseGet(FpPersonnel::new);
        ValidationUtil.isNull(fpPersonnel.getId(),"FpPersonnel","id",id);
        return fpPersonnelMapper.toDto(fpPersonnel);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpPersonnelDto create(FpPersonnel resources) {
        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());
        return fpPersonnelMapper.toDto(fpPersonnelRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpPersonnel resources) {
        FpPersonnel fpPersonnel = fpPersonnelRepository.findById(resources.getId()).orElseGet(FpPersonnel::new);
        ValidationUtil.isNull( fpPersonnel.getId(),"FpPersonnel","id",resources.getId());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        fpPersonnel.copy(resources);
        fpPersonnelRepository.save(fpPersonnel);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("fpPersonnelMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpPersonnelDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpPersonnelDto fpPersonnel : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" name",  fpPersonnel.getName());
                         map.put(" sex",  fpPersonnel.getSex());
                         map.put(" departmentId",  fpPersonnel.getDepartmentId());
                         map.put(" personnelType",  fpPersonnel.getPersonnelType());
                         map.put(" idcard",  fpPersonnel.getIdcard());
                         map.put(" phoneNumber",  fpPersonnel.getPhoneNumber());
                         map.put(" emergencyContact",  fpPersonnel.getEmergencyContact());
                         map.put(" emergencyContactPhone",  fpPersonnel.getEmergencyContactPhone());
                         map.put(" state",  fpPersonnel.getState());
                         map.put(" affiliatedCompany",  fpPersonnel.getAffiliatedCompany());
                         map.put(" isDelete",  fpPersonnel.getIsDelete());
                         map.put(" createId",  fpPersonnel.getCreateId());
                         map.put(" createTime",  fpPersonnel.getCreateTime());
                         map.put(" updateTime",  fpPersonnel.getUpdateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
