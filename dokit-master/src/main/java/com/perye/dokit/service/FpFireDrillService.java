package com.perye.dokit.service;

import com.perye.dokit.dto.FpFireDrillDto;
import com.perye.dokit.entity.FpFireDrill;
import com.perye.dokit.query.FpFireDrillQueryCriteria;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface FpFireDrillService {


    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */

    public List<PageData> fpFireDrillList(PageData pageData) throws Exception;

    /**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(FpFireDrillQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<FpFireDrillDto>
    */
    List<FpFireDrillDto> queryAll(FpFireDrillQueryCriteria criteria);

    /**
    * 根据ID查询
    * @param id ID
    * @return FpFireDrillDto
    */
    FpFireDrillDto findById(String id);

    /**
    * 创建
    * @param resources /
    * @return FpFireDrillDto
    */
    FpFireDrillDto create(FpFireDrill resources);

    /**
    * 编辑
    * @param resources /
    */
    void update(FpFireDrill resources);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<FpFireDrillDto> all, HttpServletResponse response) throws IOException;

    /**
    * 多选删除
    * @param ids /
    */
    void deleteAll(String[] ids);
}
