package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.perye.dokit.dto.FpFireDrillDto;
import com.perye.dokit.entity.FpFireDrill;
import com.perye.dokit.mapper.FpFireDrillMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpFireDrillQueryCriteria;
import com.perye.dokit.repository.FpFireDrillRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpFireDrillService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpFireDrill")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpFireDrillServiceImpl implements FpFireDrillService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    private final FpFireDrillRepository fpFireDrillRepository;

    private final FpFireDrillMapper fpFireDrillMapper;
    @Autowired
    private DictDetailService dictDetailService;

    public FpFireDrillServiceImpl(FpFireDrillRepository fpFireDrillRepository, FpFireDrillMapper fpFireDrillMapper) {
        this.fpFireDrillRepository = fpFireDrillRepository;
        this.fpFireDrillMapper = fpFireDrillMapper;
    }
    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */
    @Override
    public List<PageData> fpFireDrillList(PageData pageData) throws Exception {
        //动火作业人员类型[0内部，1外部]
        Map<String,String> fdsMap = dictDetailService.queryDictDetailByName("fire_drill_state","map");
        List<PageData> pageList = (List<PageData>) dao.findForList("fpFireDrillMapper.selectPageList", pageData);

        for (int i = 0; i <pageList.size() ; i++) {

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("state")))){
                pageList.get(i).put("stateLable", fdsMap.get(pageList.get(i).get("state")));
            }
        }
        return pageList;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpFireDrillQueryCriteria criteria, Pageable pageable){
        Page<FpFireDrill> page = fpFireDrillRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpFireDrillMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpFireDrillDto> queryAll(FpFireDrillQueryCriteria criteria){
        return fpFireDrillMapper.toDto(fpFireDrillRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpFireDrillDto findById(String id) {
        FpFireDrill fpFireDrill = fpFireDrillRepository.findById(id).orElseGet(FpFireDrill::new);
        ValidationUtil.isNull(fpFireDrill.getId(),"FpFireDrill","id",id);
        return fpFireDrillMapper.toDto(fpFireDrill);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpFireDrillDto create(FpFireDrill resources) {
        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());
        return fpFireDrillMapper.toDto(fpFireDrillRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpFireDrill resources) {
        FpFireDrill fpFireDrill = fpFireDrillRepository.findById(resources.getId()).orElseGet(FpFireDrill::new);
        ValidationUtil.isNull( fpFireDrill.getId(),"FpFireDrill","id",resources.getId());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        fpFireDrill.copy(resources);
        fpFireDrillRepository.save(fpFireDrill);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("fpFireDrillMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpFireDrillDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpFireDrillDto fpFireDrill : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" exerciseTime",  fpFireDrill.getExerciseTime());
                         map.put(" exerciseAddr",  fpFireDrill.getExerciseAddr());
                         map.put(" exerciseDescribe",  fpFireDrill.getExerciseDescribe());
                         map.put(" contactInfo",  fpFireDrill.getContactInfo());
                         map.put(" state",  fpFireDrill.getState());
                         map.put(" participateNum",  fpFireDrill.getParticipateNum());
                         map.put(" createTime",  fpFireDrill.getCreateTime());
                         map.put(" updateTime",  fpFireDrill.getUpdateTime());
                         map.put(" createId",  fpFireDrill.getCreateId());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
