package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.dto.FpDevicePointNewDto;
import com.perye.dokit.entity.FpDevicePointNew;
import com.perye.dokit.mapper.FpDevicePointNewMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpDevicePointNewQueryCriteria;
import com.perye.dokit.repository.FpDevicePointNewRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpDevicePointNewService;
import com.perye.dokit.utils.FileUtil;
import com.perye.dokit.utils.PageUtil;
import com.perye.dokit.utils.QueryHelp;
import com.perye.dokit.utils.ValidationUtil;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpDevicePointNew")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpDevicePointNewServiceImpl implements FpDevicePointNewService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private DictDetailService dictDetailService;

    private final FpDevicePointNewRepository fpDevicePointNewRepository;

    private final FpDevicePointNewMapper fpDevicePointNewMapper;

    public FpDevicePointNewServiceImpl(FpDevicePointNewRepository fpDevicePointNewRepository, FpDevicePointNewMapper fpDevicePointNewMapper) {
        this.fpDevicePointNewRepository = fpDevicePointNewRepository;
        this.fpDevicePointNewMapper = fpDevicePointNewMapper;
    }
    /**
     * 分组查询数据
     * @param pageData pageData
     * @return Map<String,Object>
     */
    @Override
    public List<PageData> selectListGroupBy(PageData pageData) throws Exception {
        List<PageData> pageList = (List<PageData>) dao.findForList("FpDevicePointNewMapper.selectListGroupBy", pageData);
        return pageList;
    }

    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */
    @Override
    public List<PageData> fpDevicePointNewList(PageData pageData) throws Exception {
        List<PageData> pageList = (List<PageData>) dao.findForList("FpDevicePointNewMapper.selectPageList", pageData);
//        //属性点类型：1风机，2风阀等
//        Map<String,String> pointType = dictDetailService.queryDictDetailByName("point_type","map");
//
//        //映射类型：1开，2关等
//        Map<String,String> mappingType = dictDetailService.queryDictDetailByName("mapping_type","map");
//
//
//        //状态：1启用、0禁用
//        Map<String,String> enable = dictDetailService.queryDictDetailByName("universal_activation","map");
//
//        //是否告警：1是、0否
//        Map<String,String> isAlarm = dictDetailService.queryDictDetailByName("yes_or_not","map");
//
//      //'类型：1写，2读',
//        Map<String,String> type = dictDetailService.queryDictDetailByName("point_val_type","map");
//
//
//        for (int i = 0; i <pageList.size() ; i++) {
//            if(ObjectUtil.isNotEmpty((pageList.get(i).get("pointType")))){
//                pageList.get(i).put("pointTypeLabel", pointType.get(pageList.get(i).get("pointType")));
//            }
//
//            if(ObjectUtil.isNotEmpty((pageList.get(i).get("mappingType")))){
//                pageList.get(i).put("mappingTypeLabel", mappingType.get(pageList.get(i).get("mappingType")));
//            }
//
//
//            if(ObjectUtil.isNotEmpty((pageList.get(i).get("enable")))){
//                pageList.get(i).put("enableLabel", enable.get(pageList.get(i).get("enable").toString()));
//            }
//
//
//            if(ObjectUtil.isNotEmpty((pageList.get(i).get("isAlarm")))){
//                pageList.get(i).put("isAlarmLabel", isAlarm.get(pageList.get(i).get("isAlarm")));
//            }
//
//            if(ObjectUtil.isNotEmpty((pageList.get(i).get("type")))){
//                pageList.get(i).put("typeLabel", type.get(pageList.get(i).get("type")));
//            }
//        }
        return pageList;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpDevicePointNewQueryCriteria criteria, Pageable pageable){
        Page<FpDevicePointNew> page = fpDevicePointNewRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpDevicePointNewMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpDevicePointNewDto> queryAll(FpDevicePointNewQueryCriteria criteria){
        return fpDevicePointNewMapper.toDto(fpDevicePointNewRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpDevicePointNewDto findById(String id) {
        FpDevicePointNew fpDevicePointNew = fpDevicePointNewRepository.findById(id).orElseGet(FpDevicePointNew::new);
        ValidationUtil.isNull(fpDevicePointNew.getId(),"FpDevicePointNew","id",id);
        return fpDevicePointNewMapper.toDto(fpDevicePointNew);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpDevicePointNewDto create(FpDevicePointNew resources) {
        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
//        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());
        return fpDevicePointNewMapper.toDto(fpDevicePointNewRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpDevicePointNew resources) {
        FpDevicePointNew fpDevicePointNew = fpDevicePointNewRepository.findById(resources.getId()).orElseGet(FpDevicePointNew::new);
        ValidationUtil.isNull( fpDevicePointNew.getId(),"FpDevicePointNew","id",resources.getId());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        fpDevicePointNew.copy(resources);
        fpDevicePointNewRepository.save(fpDevicePointNew);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("FpDevicePointNewMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpDevicePointNewDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpDevicePointNewDto fpDevicePointNew : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" pointName",  fpDevicePointNew.getPointName());
                         map.put(" pointType",  fpDevicePointNew.getPointType());
                         map.put(" mappingType",  fpDevicePointNew.getMappingType());
                         map.put(" registerAddress",  fpDevicePointNew.getRegisterAddress());
                         map.put(" registerAddressByte",  fpDevicePointNew.getRegisterAddressByte());
                         map.put(" modbusId",  fpDevicePointNew.getModbusId());
                         map.put(" resRailwayId",  fpDevicePointNew.getResRailwayId());
                         map.put(" resTunnelId",  fpDevicePointNew.getResTunnelId());
                         map.put(" resRegionId",  fpDevicePointNew.getResRegionId());
                         map.put(" resSysId",  fpDevicePointNew.getResSysId());
                         map.put(" equipmentId",  fpDevicePointNew.getEquipmentId());
                         map.put(" connectionType",  fpDevicePointNew.getConnectionType());
                         map.put(" enable",  fpDevicePointNew.getEnable());
                         map.put(" isAlarm",  fpDevicePointNew.getIsAlarm());
                         map.put(" createTime",  fpDevicePointNew.getCreateTime());
                         map.put(" updateTime",  fpDevicePointNew.getUpdateTime());
                         map.put(" isDelete",  fpDevicePointNew.getIsDelete());
                         map.put(" type",  fpDevicePointNew.getType());
                         map.put(" registerAddressVal",  fpDevicePointNew.getRegisterAddressVal());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
