package com.perye.dokit.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.perye.dokit.exception.BadRequestException;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.modbus.door.util.DateUtil;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpResourceService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.utils.tree.DefValConstants;
import com.perye.dokit.utils.tree.TreeUtil;
import com.perye.dokit.vo.FpResourceVo;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpResource")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpResourceServiceImpl implements FpResourceService {


    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private DictDetailService dictDetailService;


    @Override
    //@Cacheable
    public List<FpResourceVo> queryAll(PageData pageData) throws Exception {


        List<FpResourceVo> pageList = (List<FpResourceVo>) dao.findForList("FpResourceMapper.queryResourceList", pageData);
        if (CollectionUtils.isNotEmpty(pageList)) {
            Map<String, String> downLinesMap = dictDetailService.queryDictDetailByName("down_lines", "map");
            Map<String, String> resourceTypeMap = dictDetailService.queryDictDetailByName("resource_type", "map");
            Map<String, String> subSystemTypeMap = dictDetailService.queryDictDetailByName("sub_system_type", "map");
            for (FpResourceVo data : pageList) {
                String downLines = data.getDownLines();
                data.setDownLinesStr(downLinesMap.get(downLines));
                String resourceType = data.getType();
                data.setTypeStr(resourceTypeMap.get(resourceType));
                String subSystemType = data.getSubSystemType();
                data.setSubSystemTypeStr(subSystemTypeMap.get(subSystemType));

                data.setOldParentId(data.getParentId());
            }
        }
        List<FpResourceVo> list = TreeUtil.buildTree(pageList);
        return list;

    }

    @Override
    public PageData queryResourceInfo(PageData pageData) throws Exception {

        PageData result = new PageData();
        String id = pageData.getString("id");
        String code = pageData.getString("code");
        if (StringUtils.isBlank(id) && StringUtils.isBlank(code)) {
            result.put("code", "01");
            result.put("msg", "资源id和资源编码不能都为空");
            return result;
        }


        PageData info = (PageData) dao.findForObject("FpResourceMapper.queryResourceListById", pageData);
        if (info != null) {
            result.put("code", "00");
            result.put("msg", "查询成功");
            result.put("data", info);
        } else {
            result.put("code", "01");
            result.put("msg", "该id未查询到对应的信息");
        }


        return result;
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public Boolean create(PageData pageData) throws Exception {

        pageData.put("id", IdUtil.simpleUUID());
        pageData.put("resourceCode", pageData.getString("code"));
        pageData.put("createId", SecurityUtils.getCurrentUserId().toString());
        pageData.put("isDelete", 0);
        pageData.put("createTime", DateUtils.getTime());
        pageData.put("updateTime", DateUtils.getTime());
        try {
            PageData pageData1 = builTreePath(pageData);
            if ("01".equals(pageData1.get("code"))) {
                return false;
            }
            dao.save("FpResourceMapper.insert", pageData1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private PageData builTreePath(PageData pageData) {
        pageData.put("code", "");
        pageData.put("mas", "");
        try {
            if (ObjectUtil.isNull(pageData.get("parentId")) || pageData.get("parentId").equals("0")) {
                pageData.put("parentId", DefValConstants.PARENT_ID);
                pageData.put("treePath", DefValConstants.TREE_PATH_SPLIT);
                pageData.put("treeGrade", DefValConstants.PARENT_ID);

            } else {
                //根据父id查询id对象
                PageData objPage = (PageData) dao.findForObject("FpResourceMapper.queryParentInfo", pageData);
                if (ObjectUtil.isNull(objPage)) {
                    pageData.put("code", "01");
                    pageData.put("mas", "请正确填写父级名称或者编号");
                }

                pageData.put("treeGrade", Integer.parseInt(objPage.get("treeGrade").toString()) + 1);

                pageData.put("treePath", TreeUtil.getTreePath(objPage.get("treePath").toString(), objPage.get("id").toString()));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return pageData;
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(PageData pageData) throws Exception {

        String oldParentId = pageData.getString("oldParentId");
        String parentId = pageData.getString("parentId");
        List<String> ids = new ArrayList<String>();
        ids.add(pageData.getString("id"));
        List<PageData> list = (List<PageData>) dao.findForList("FpResourceMapper.queryResourceInfoByParentId", ids);

        if (CollectionUtils.isNotEmpty(list) && !oldParentId.equals(parentId)) {
            throw new BadRequestException("该资源下存在子资源信息，无法更换资源路径");
        }

        pageData.put("resourceCode", pageData.getString("code"));
        pageData.put("createId", SecurityUtils.getCurrentUserId().toString());
        pageData.put("updateTime", DateUtils.getTime());
        PageData pageData1 = builTreePath(pageData);

        dao.update("FpResourceMapper.updateByPrimaryKeySelective", pageData1);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) throws Exception {
        List<String> list = Arrays.asList(ids);

        List<PageData> resourceList = (List<PageData>) dao.findForList("FpResourceMapper.queryResourceInfoByParentId", list);
        if (CollectionUtils.isNotEmpty(resourceList)) {
            throw new BadRequestException("有资源下存在子资源信息，无法删除");
        }
        dao.update("FpResourceMapper.updateByPrimaryKey", list);
    }

    @Override
    public List<PageData> queryDropdownBoxList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpResourceMapper.queryDropdownBoxList", pageData);
    }

    @Override
    public List<PageData> queryResourceById(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpResourceMapper.queryResourceById", pageData);
    }

    @Override
    public List<FpResourceVo> queryResourceListByType(List<String> list) throws Exception {
        List<FpResourceVo> resourceList = new ArrayList<FpResourceVo>();
        if (CollectionUtil.isEmpty(list)) {
            return resourceList;
        }
        return (List<FpResourceVo>) dao.findForList("FpResourceMapper.queryResourceListByType", list);
    }

    @Override
    public void download(List<PageData> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PageData fpResource : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put(" code", fpResource.getString(""));
            map.put(" name", fpResource.getString(""));
            map.put(" startPosition", fpResource.getString(""));
            map.put(" endPosition", fpResource.getString(""));
            map.put(" totalLength", fpResource.getString(""));
            map.put(" downLines", fpResource.getString(""));
            map.put(" parentId", fpResource.getString(""));
            map.put(" treeGrade", fpResource.getString(""));
            map.put(" treePath", fpResource.getString(""));
            map.put(" type", fpResource.getString(""));
            map.put(" subSystemType", fpResource.getString(""));
            map.put(" isDelete", fpResource.getString(""));
            map.put(" createId", fpResource.getString(""));
            map.put(" createTime", DateUtils.getTime());
            map.put(" updateTime", DateUtils.getTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

}
