package com.perye.dokit.service.impl;


import cn.hutool.core.util.IdUtil;
import com.perye.dokit.entity.User;
import com.perye.dokit.mapper.dao.DaoSupport;

import com.perye.dokit.repository.UserRepository;
import com.perye.dokit.service.AdministratorService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.util.List;


// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "wfSchoolAdministrator")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class AdministratorServiceImpl implements AdministratorService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private TransactionTemplate transactionTemplate;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        User user = userRepository.findById(SecurityUtils.getCurrentUserId()).orElseGet(User::new);
        pageData.put("sourceId", user.getSourceId());
        return (List<PageData>) dao.findForList("AdministratorMapper.getAll", pageData);
    }

    @Override
    public void add(PageData pageData) throws Exception {
/*
        PageData result = new PageData();
        User userData = userRepository.findById(SecurityUtils.getCurrentUserId()).orElseGet(User::new);
        pageData.put("sourceId", userData.getSourceId());
        if (StringUtils.equals(null, userData.getSourceId())) {

            throw new Exception("该账户不能维护此模块信息");

        }
        PageData school = new PageData();
        school.put("id", userData.getSourceId());
        List<PageData> schoolList = (List<PageData>) dao.findForList("WfSchoolMapper.getSchoolList", school);
        if (schoolList.size() != 1) {

            throw new Exception("信息找不到，请联系管理员");

        }*/
       // pageData.put("tunnelId", schoolList.get(0).getString("tunnelId"));


        PageData shcool = new PageData();
        String shcoolId = IdUtil.simpleUUID();
        shcool.put("id", shcoolId);
        shcool.put("railwayId", pageData.getString("railwayId"));
        shcool.put("tunnelId", pageData.getString("tunnelId"));
        shcool.put("pushMessage", pageData.getString("pushMessage"));
        shcool.put("sourceId", pageData.getString("sourceId"));
        shcool.put("createTime", DateUtils.getTime());
        shcool.put("updateTime", DateUtils.getTime());
        shcool.put("remarks", pageData.getString("remarks"));
        dao.save("AdministratorMapper.add", shcool);

        shcool.put("enabled", pageData.getString("enabled"));
        shcool.put("phone", pageData.getString("phone"));

        //保存用户表
        PageData user = new PageData();
        user.put("email", pageData.get("email"));//邮箱
        user.put("enabled", "true".equals(pageData.getString("enabled")) ? 1 : 0);//状态：1启用、0禁用
        user.put("password", passwordEncoder.encode(pageData.getString("password")));
        user.put("username", pageData.getString("username"));
        user.put("deptId", 1); //默认
        user.put("jobId", 2);//默认
        user.put("phone", pageData.getString("phone"));
        user.put("createTime", DateUtils.getTime());
        user.put("nickName", pageData.getString("nickName")); //昵称
        user.put("sex", pageData.getString("sex")); //性别
        user.put("type", "3");//类型：1超级管理员，2管理员，3学校超管，4:学校用户
        user.put("sourceId", shcoolId);//资源id,关联学校，和其他账号
        dao.save("WfUserMapper.addUser", user);
        int user_id = Integer.parseInt(user.get("id") + "");


        //保存用户表角色关联表
        PageData userRole = new PageData();
        userRole.put("userId", user_id);
        userRole.put("roleId", pageData.get("role"));
        dao.save("WfUserMapper.addUserRoles", userRole);


    }

    @Override
    public void update(PageData pageData) throws Exception {
        PageData result = new PageData();
      /*  User user = userRepository.findById(SecurityUtils.getCurrentUserId()).orElseGet(User::new);
        pageData.put("sourceId",user.getSourceId());*/

                    //保存学校表
                    PageData shcool = new PageData();
                    shcool.put("id", pageData.getString("schoolId"));
                     shcool.put("railwayId", pageData.getString("railwayId"));
                    shcool.put("tunnelId", pageData.getString("tunnelId"));
                    shcool.put("pushMessage", pageData.getString("pushMessage"));
                    /*shcool.put("sourceId", pageData.getString("sourceId"));*/
                    shcool.put("updateTime", DateUtils.getTime());
                    shcool.put("remarks", pageData.getString("remarks"));
                    dao.update("AdministratorMapper.update", shcool);

                    shcool.put("enabled", pageData.getString("enabled"));
                    shcool.put("phone", pageData.getString("phone"));


                    //保存用户表
                    PageData user = new PageData();
                    user.put("id", pageData.get("userId"));
                    user.put("email", pageData.get("email"));//邮箱
                    user.put("enabled", "true".equals(pageData.getString("enabled")) ? 1 : 0);//状态：1启用、0禁用
                    //当密码是edit时  默认不修改密码
                    if (!"edit".equals(pageData.getString("password")) && !"".equals(pageData.getString("password"))) {
                        user.put("password", passwordEncoder.encode(pageData.getString("password")));
                    }
                    user.put("username", pageData.getString("username"));
                    user.put("phone", pageData.getString("phone"));

                    user.put("nickName", pageData.getString("nickName")); //昵称
                    user.put("sex", pageData.getString("sex")); //性别
                    user.put("sourceId", pageData.getString("sourceId"));//资源id,关联学校，和其他账号
                    dao.update("WfUserMapper.updateUser", user);


                    //保存用户表角色关联表
                    PageData userRole = new PageData();
                    userRole.put("userId", pageData.get("userId"));
                    userRole.put("roleId", pageData.get("role"));
                    dao.update("WfUserMapper.updateUserRoles", userRole);

    }

    @Override
    public void deleteAll(String[] ids) throws Exception {

        if (ids.length == 0) {
            throw new Exception("删除数据有无唯一条件");
        }


      /*  PageData schoolAdministrator = new PageData();
        schoolAdministrator.put("id", ids[0]);
        List<PageData> schoolList = (List<PageData>) dao.findForList("AdministratorMapper.findByParams", schoolAdministrator);
        if (schoolList.size() != 1) {

            throw new Exception("管理员信息异常，请联系管理员");
        }*/


        //删除 学校表
        dao.delete("AdministratorMapper.deleteAll", ids);
        //查询user表信息
        PageData user = new PageData();
        user.put("sourceId", ids[0]);
        PageData userData = (PageData) dao.findForObject("WfUserMapper.getUser", user);
        if (userData == null) {
            throw new Exception("用户信息表异常，请先查询");
        }
        if (!userData.containsKey("id")) {
            throw new Exception("用户信息表异常，请先查询");
        }
        if (!userData.containsKey("role")) {
            throw new Exception("角色信息异常，请查询");
        }

        //删除user_role表信息
        PageData userRole = new PageData();
        userRole.put("userId", userData.get("id"));
        userRole.put("roleId", userData.get("role"));
        dao.delete("WfUserMapper.deleteUserRole", userRole);

        //删除user表信息
        PageData userInfo = new PageData();
        userInfo.put("id", userData.get("id"));
        dao.delete("WfUserMapper.deleteUser", userInfo);


    }

    @Override
    public List<PageData> getTunnelNum(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("AdministratorMapper.getCheck", pageData);
    }

    @Override
    public List<PageData> getSchoolList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("AdministratorMapper.getSchoolList", pageData);
    }

    @Override
    public List<PageData> queryAllTunnelAdministratorList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("AdministratorMapper.queryAllTunnelAdministratorList", pageData);
    }
}
