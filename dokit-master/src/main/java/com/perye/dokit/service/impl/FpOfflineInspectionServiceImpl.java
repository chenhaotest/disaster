package com.perye.dokit.service.impl;


import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.FpOfflineInspectionService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpOfflineInspection")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpOfflineInspectionServiceImpl implements FpOfflineInspectionService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpOfflineInspectionMapper.queryAll", pageData);
    }

    @Override
    public void add(PageData pageData) throws Exception {
        //线下巡检id
        String id = IdUtil.simpleUUID();
        pageData.put("id", id);

        ArrayList list=  (ArrayList)pageData.get("handleData");//获取集合
        toListOfflineInspection(list,id);//保存问题人员绑定


        pageData.put("createTime", DateUtils.getTime());
        dao.save("FpOfflineInspectionMapper.insert", pageData);
    }


    public void toListOfflineInspection( ArrayList list,String id) throws Exception {
        //问题人员绑定
        for (int i = 0; i <list.size() ; i++) {
            ArrayList data= (ArrayList)list.get(i);  //0 问题 1 人
            PageData param=new PageData();
            param.put("inspectionId",id);  //线下巡检id
            param.put("problemId",data.get(0)); //巡检人员id
            param.put("personId",data.get(1)); //巡检问题
            param.put("state",1); //巡检问题状态
            param.put("createTime", DateUtils.getTime());
            param.put("id", IdUtil.simpleUUID());
            dao.save("FpOfflineInspectionProblemMapper.insert", param);
        }
    }

    @Override
    public void update(PageData pageData) throws Exception {

        //根据巡检任务 删除任务信息



        if(StringUtils.equals("1",pageData.getString("state"))){
            PageData param=new PageData();
            param.put("id",pageData.getString("id"));
            dao.update("FpOfflineInspectionProblemMapper.deleteById", param);
            ArrayList list=  (ArrayList)pageData.get("handleData");//获取集合
            toListOfflineInspection(list,pageData.getString("id"));//保存问题人员绑定
        }





        dao.update("FpOfflineInspectionMapper.update", pageData);
    }

    @Override
    public void deleteAll(String[] ids) throws Exception {
        dao.update("FpOfflineInspectionMapper.deleteAll", ids);
    }

    @Override
    public void createInspectionPerson(PageData pageData) throws Exception {
        String id = IdUtil.simpleUUID();
        pageData.put("id", id);
        pageData.put("createTime", DateUtils.getTime());
        dao.save("FpOfflineInspectionPersonMapper.insert", pageData);
    }

    @Override
    public void updateInspectionPerson(PageData pageData) throws Exception {
        dao.update("FpOfflineInspectionPersonMapper.update", pageData);
    }

    @Override
    public void createInspectionProblem(PageData pageData) throws Exception {
        String id = IdUtil.simpleUUID();
        pageData.put("id", id);
        pageData.put("state", 1);
        pageData.put("createTime", DateUtils.getTime());
        dao.save("FpOfflineInspectionProblemMapper.insert", pageData);
    }

    @Override
    public void updateInspectionProblem(PageData pageData) throws Exception {
        dao.update("FpOfflineInspectionProblemMapper.update", pageData);
    }
}
