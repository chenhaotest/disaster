package com.perye.dokit.service;


import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface AdministratorService {
    /**
     * \* User: chenhao
     * \* Date: 2023/6/20 9:29
     * \* Description: 查询 所有数据
     */



    List<PageData> getAll(PageData pageData) throws Exception;

    /**
     * \* User: chenhao
     * \* Date: 2023/6/20 9:30
     * \* Description: 新增
     */



    void add(PageData pageData) throws Exception;

    /**
     * \* User: chenhao
     * \* Date: 2023/6/20 14:10
     * \* Description:  修改
     */



    void update(PageData pageData) throws Exception;

    /**
     * \* User: chenhao
     * \* Date: 2023/6/20 15:40
     * \* Description:   删除
     */



    void deleteAll(String[] ids) throws Exception;

    List<PageData> getTunnelNum(PageData pageData) throws Exception;

    /**
     * \* User: chenhao
     * \* Date: 2023/6/25 10:12
     * \* Description: 查询所有 可用学校
     */



    List<PageData> getSchoolList(PageData pageData) throws Exception;

    List<PageData> queryAllTunnelAdministratorList(PageData pageData) throws Exception;
}