package com.perye.dokit.service.impl;

import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.perye.dokit.config.ClassExcelVerifyHandler;
import com.perye.dokit.dto.FpEquipmentDto;
import com.perye.dokit.entity.FpEquipment;
import com.perye.dokit.mapper.FpEquipmentMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpEquipmentQueryCriteria;
import com.perye.dokit.repository.FpEquipmentRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpEquipmentService;
import com.perye.dokit.service.FpResourceService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.vo.FpResourceVo;
import com.perye.dokit.vo.FpequipmentExcel;
import org.apache.commons.compress.utils.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpEquipment")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpEquipmentServiceImpl implements FpEquipmentService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private DictDetailService dictDetailService;
    private final FpEquipmentRepository fpEquipmentRepository;
    //不解析空白行
    @Autowired
    private  ClassExcelVerifyHandler verifyHandler;

    @Autowired
    private FpResourceService fpResourceService;

    private final FpEquipmentMapper fpEquipmentMapper;

    public FpEquipmentServiceImpl(FpEquipmentRepository fpEquipmentRepository, FpEquipmentMapper fpEquipmentMapper) {
        this.fpEquipmentRepository = fpEquipmentRepository;
        this.fpEquipmentMapper = fpEquipmentMapper;
    }

    /**
     * 下载
     *
     * @param fileName 文件名称
     * @param response
     * @param request excel数据
     */
    @Override
    public void downLoadExcel(HttpServletResponse response, HttpServletRequest request, String fileName) throws IOException{
        //模板名称
        OutputStream out = null;
        InputStream input =null;
        try {
            ApplicationHome h = new ApplicationHome(getClass());
            String dirPath = h.getSource().toString();
            String path= dirPath+"/template"+File.separator+"excel"+File.separator+fileName;
            File outFile = new File(path);
            input = new BufferedInputStream(new FileInputStream(outFile));
            response.setCharacterEncoding("UTF-8");
            response.setHeader("content-Type", "application/vnd.ms-excel");
//            response.setHeader("Content-Disposition",
//                    "attachment;filename=" + new String((fileName).getBytes(), "iso-8859-1"));
            response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, "utf-8"));

            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
            out = response.getOutputStream();
            byte[] buffer = new byte[1024]; // 缓冲区
            int bytesToRead = -1;
            // 通过循环将读入内容输出到浏览器中
            while ((bytesToRead = input.read(buffer)) != -1) {
                out.write(buffer, 0, bytesToRead);
            }
        } catch (IOException e) {

        } finally {
            IOUtils.closeQuietly(input);
            IOUtils.closeQuietly(out);
        }


    }

    /**
     * 导入excel
     * @param file
     * @return
     * @throws Exception
     */
    @Override
    public PageData importFpEquipment(MultipartFile file) throws Exception {

        PageData  result = new PageData();
        //错位信息列表
        List<String> errorList  =new ArrayList<>();

        ImportParams params = new ImportParams();
        params.setVerifyHandler(verifyHandler);
        params.setTitleRows(0);
        params.setHeadRows(1);
        //params.setNeedVerify(true);//设置需要校验
        // 1、解析Excel里面的数据
        List<FpequipmentExcel> list = ExcelImportUtil.importExcel(file.getInputStream(),FpequipmentExcel.class, params);
       //验证表格中是否存在重复
        checkDepartmentCode(list, errorList);
        if(errorList.size()>0){
            result.put("data",errorList);
            result.put("code", HttpStatus.CREATED.value());
            result.put("message", "导入错误");
            return result;
        }

        //验证是否必填
        verifyEmpty(list, errorList);
         if(errorList.size()>0){
            result.put("data",errorList);
            result.put("code", HttpStatus.CREATED.value());
            result.put("message", "导入错误");
            return result;
          }

        //验证数据库中是否唯一
        verifyDs(list,errorList);
        if(errorList.size()>0){
            result.put("data",errorList);
            result.put("code", HttpStatus.CREATED.value());
            result.put("message", "导入错误");
            return result;
        }

      //如果 错误等于0则插入数据库
        if(errorList.size()==0){
            result.put("data","");
            result.put("code", HttpStatus.OK.value());
            result.put("message", "导入成功");
            return result;
        }

        return result;


    }

    /**
     * 检查重复
     * @param list
     */
    private void checkDepartmentCode(List<FpequipmentExcel> list,  List<String> errorList) {
        // 检查项目编码是否重复
        List<String> assetNumber = ListUtil.getDuplicateElements(list.stream().map(FpequipmentExcel::getAssetNumber).collect(Collectors.toList()));

        // 检查项目编码是否重复
        List<String> specification = ListUtil.getDuplicateElements(list.stream().map(FpequipmentExcel::getSpecification).collect(Collectors.toList()));

        if (assetNumber != null && assetNumber.size() > 0) {
            errorList.add("设备编号【" + assetNumber.get(0) + "】存在重复，请修正!");
        }
        if (specification != null && specification.size() > 0) {
            errorList.add("规格型号【" + assetNumber.get(0) + "】存在重复，请修正!");
        }

    }

    /**
     *验证表格中的数据是否必填
     * @return
     * @throws Exception
     */
    public void verifyEmpty(List<FpequipmentExcel> list,List<String> errorList){
        //判断必填
        for (int i = 0; i <list.size() ; i++) {
          if(StrUtil.isBlank(list.get(i).getSpecification())){

              errorList.add("第"+(i+1)+"行规格型号必填");
          }
            if(ObjectUtil.isEmpty(list.get(i).getPurchaseDate())){
                errorList.add("第"+(i+1)+"行购置日期必填");
            }
        }
    }

    /**
     *验证数据库中是否重复
     * @return
     * @throws Exception
     */
    public void verifyDs(List<FpequipmentExcel> list,List<String> errorList){

        for (int i = 0; i <list.size() ; i++) {

        }
    }



    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */
    @Override
    public List<PageData> fpEquipmentList(PageData pageData) throws Exception {
        List<PageData> pageList = (List<PageData>) dao.findForList("fpEquipmentMapper.selectPageList", pageData);

        //是否远控
        Map<String,String> dmap2 = dictDetailService.queryDictDetailByName("yes_or_not","map");

        //状态：1启用、0禁用
        Map<String,String> enable = dictDetailService.queryDictDetailByName("universal_activation","map");
        //上下行线
        Map<String,String> downLines = dictDetailService.queryDictDetailByName("down_lines","map");

       //设备类型
        Map<String,String> deviceType = dictDetailService.queryDictDetailByName("device_type","map");
        //属性点类型：1风机，2风阀等
        Map<String,String> pointType = dictDetailService.queryDictDetailByName("point_type","map");

        //映射类型：1开，2关等
        Map<String,String> mappingType = dictDetailService.queryDictDetailByName("mapping_type","map");


        //是否告警：1是、0否
        Map<String,String> isAlarm = dictDetailService.queryDictDetailByName("yes_or_not","map");

        //'类型：1写，2读',
        Map<String,String> type = dictDetailService.queryDictDetailByName("point_val_type","map");
        //资源类型[0铁路，1隧道/站房，2区域/几楼站厅,3子系统(如果是子系统则有子系统类型否则没有)]
        List<String> data =new ArrayList<>();
        data.add("0");
        data.add("1");
        data.add("2");
        data.add("3");
        Map<String, String> map = fpResourceService.queryResourceListByType(data).stream().collect(Collectors.toMap(FpResourceVo::getId, FpResourceVo::getName,(key1 , key2)-> key2 ));


        for (int i = 0; i <pageList.size() ; i++) {
            if(ObjectUtil.isNotEmpty(pageList.get(i).get("pointList"))){
                List<PageData> objList =  (List<PageData>)pageList.get(i).get("pointList");
                for (int j = 0; j <objList.size() ; j++) {
                    //点位字典
                    if(ObjectUtil.isNotEmpty((objList.get(j).get("pointType")))){
                        objList.get(j).put("pointTypeLabel", pointType.get(objList.get(j).get("pointType")));
                    }

                    if(ObjectUtil.isNotEmpty((objList.get(j).get("mappingType")))){
                        objList.get(j).put("mappingTypeLabel", mappingType.get(objList.get(j).get("mappingType")));
                    }


                    if(ObjectUtil.isNotEmpty((objList.get(j).get("enable")))){
                        objList.get(j).put("enableLabel", enable.get(objList.get(j).get("enable").toString()));
                    }


                    if(ObjectUtil.isNotEmpty((objList.get(j).get("isAlarm")))){
                        objList.get(j).put("isAlarmLabel", isAlarm.get(objList.get(j).get("isAlarm")));
                    }

                    if(ObjectUtil.isNotEmpty((objList.get(j).get("type")))){
                        objList.get(j).put("typeLabel", type.get(objList.get(j).get("type")));
                    }
                }
            }


            //区域隧道值
            if(StrUtil.isNotBlank((pageList.get(i).getString("resRailwayId")))){
                //获取隧道 站房  区域 子系统 的值
              pageList.get(i).put("resRailwayIdLabel", map.get(pageList.get(i).getString("resRailwayId")));
            }
            //站房
            if(StrUtil.isNotBlank((pageList.get(i).getString("resTunnelId")))){
                //获取隧道 站房  区域 子系统 的值
                pageList.get(i).put("resTunnelIdLabel", map.get(pageList.get(i).getString("resTunnelId")));
            }
         //区域
            if(StrUtil.isNotBlank((pageList.get(i).getString("resRegionId")))){
                //获取隧道 站房  区域 子系统 的值
                pageList.get(i).put("resRegionIdLabel", map.get(pageList.get(i).getString("resRegionId")));
            }
            //子系统
            if(StrUtil.isNotBlank((pageList.get(i).getString("resSysId")))){
                //获取隧道 站房  区域 子系统 的值
                pageList.get(i).put("resSysIdLabel", map.get(pageList.get(i).getString("resSysId")));
            }

            //字典

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("remoteControl")))){
                pageList.get(i).put("remoteControlLabel", dmap2.get(pageList.get(i).get("remoteControl")));
            }
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("enable")))){
                pageList.get(i).put("enableLabel", enable.get(pageList.get(i).get("enable").toString()));
            }

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("downLines")))){
                pageList.get(i).put("downLinesLabel", downLines.get(pageList.get(i).get("downLines")));
            }
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("deviceType")))){
                pageList.get(i).put("deviceTypeLabel", deviceType.get(pageList.get(i).get("deviceType")));
            }
        }
        return pageList;
    }



    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpEquipmentQueryCriteria criteria, Pageable pageable){
        Page<FpEquipment> page = fpEquipmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpEquipmentMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpEquipmentDto> queryAll(FpEquipmentQueryCriteria criteria){

        return fpEquipmentMapper.toDto(fpEquipmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpEquipmentDto findById(String id) {
        FpEquipment fpEquipment = fpEquipmentRepository.findById(id).orElseGet(FpEquipment::new);
        ValidationUtil.isNull(fpEquipment.getId(),"FpEquipment","id",id);
        return fpEquipmentMapper.toDto(fpEquipment);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpEquipmentDto create(FpEquipment resources) {
        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());
        return fpEquipmentMapper.toDto(fpEquipmentRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpEquipment resources) {
        FpEquipment fpEquipment = fpEquipmentRepository.findById(resources.getId()).orElseGet(FpEquipment::new);
        ValidationUtil.isNull( fpEquipment.getId(),"FpEquipment","id",resources.getId());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        fpEquipment.copy(resources);
        fpEquipmentRepository.save(fpEquipment);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("fpEquipmentMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpEquipmentDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpEquipmentDto fpEquipment : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" assetNumber",  fpEquipment.getAssetNumber());
                         map.put(" deviceName",  fpEquipment.getDeviceName());
                         map.put(" deviceType",  fpEquipment.getDeviceType());
                         map.put(" specification",  fpEquipment.getSpecification());
                         map.put(" manufacturer",  fpEquipment.getManufacturer());
                         map.put(" department",  fpEquipment.getDepartment());
                         map.put(" serviceLife",  fpEquipment.getServiceLife());
                         map.put(" usageCondition",  fpEquipment.getUsageCondition());
                         map.put(" purchaseDate",  fpEquipment.getPurchaseDate());
                         map.put(" mileageNumbe",  fpEquipment.getMileageNumbe());
                         map.put(" remoteControl",  fpEquipment.getRemoteControl());
                         map.put(" downLines",  fpEquipment.getDownLines());
                         map.put(" enable",  fpEquipment.getEnable());
                         map.put(" resRailwayId",  fpEquipment.getResRailwayId());
                         map.put(" resTunnelId",  fpEquipment.getResTunnelId());
                         map.put(" resRegionId",  fpEquipment.getResRegionId());
                         map.put(" resSysId",  fpEquipment.getResSysId());
                         map.put(" isDelete",  fpEquipment.getIsDelete());
                         map.put(" createId",  fpEquipment.getCreateId());
                         map.put(" createTime",  fpEquipment.getCreateTime());
                         map.put(" updateTime",  fpEquipment.getUpdateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
