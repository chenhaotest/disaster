package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.exception.BadRequestException;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpSparePartsService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpSpareParts")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpSparePartsServiceImpl implements FpSparePartsService {


    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private DictDetailService dictDetailService;

    @Override
    //@Cacheable
    public List<PageData> queryAll(PageData pageData) throws Exception {

        List<PageData> list = (List<PageData>) dao.findForList("FpSparePartsMapper.selectByPrimaryKey", pageData);
        if (CollectionUtils.isNotEmpty(list)) {
            Map<String, String> deviceTypeMap = dictDetailService.queryDictDetailByName("device_type", "map");
            for (PageData data : list) {
                String deviceType = data.getString("deviceType");
                data.put("deviceTypeStr", deviceTypeMap.get(deviceType));
            }
        }
        return list;
    }


    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void create(PageData pageData) throws Exception {
        pageData.put("id", IdUtil.simpleUUID());
        pageData.put("createId", SecurityUtils.getCurrentUserId().toString());
        pageData.put("isDelete", 0);
        pageData.put("createTime", DateUtils.getTime());
        pageData.put("updateTime", DateUtils.getTime());

        dao.save("FpSparePartsMapper.insert", pageData);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(PageData pageData) throws Exception {

        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpSparePartsMapper.updateByPrimaryKeySelective", pageData);
        //fpSparePartsRepository.save(fpSpareParts);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) throws Exception {
        List<String> list = Arrays.asList(ids);
        dao.update("FpSparePartsMapper.updateByPrimaryKey", list);
    }

    @Override
    public void download(List<PageData> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PageData fpSpareParts : all) {
            /*Map<String, Object> map = new LinkedHashMap<>();
            map.put(" number", fpSpareParts.getNumber());
            map.put(" deviceName", fpSpareParts.getDeviceName());
            map.put(" deviceType", fpSpareParts.getDeviceType());
            map.put(" specification", fpSpareParts.getSpecification());
            map.put(" manufacturer", fpSpareParts.getManufacturer());
            map.put(" department", fpSpareParts.getDepartment());
            map.put(" currentInventory", fpSpareParts.getCurrentInventory());
            map.put(" minInventory", fpSpareParts.getMinInventory());
            map.put(" resRailwayId", fpSpareParts.getResRailwayId());
            map.put(" resTunnelId", fpSpareParts.getResTunnelId());
            map.put(" resRegionId", fpSpareParts.getResRegionId());
            map.put(" isDelete", fpSpareParts.getIsDelete());
            map.put(" createId", fpSpareParts.getCreateId());
            map.put(" createTime", fpSpareParts.getCreateTime());
            map.put(" updateTime", fpSpareParts.getUpdateTime());
            list.add(map);*/
        }
        FileUtil.downloadExcel(list, response);
    }

}
