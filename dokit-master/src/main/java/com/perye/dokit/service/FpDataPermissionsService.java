package com.perye.dokit.service;

import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.vo.FpResourceVo;

import java.util.List;

public interface FpDataPermissionsService {

    /**
     * 查询所有数据
     *
     * @param pageData 条件参数
     * @return List<FpRepairDto>
     */
    List<FpResourceVo> queryAll(PageData pageData) throws Exception;

    List<PageData> queryDataPermissions(PageData pageData) throws Exception;

    int editDataPermissions(PageData pageData) throws Exception;


}
