package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.FpProblemBaseService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpProblemBase")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpProblemBaseServiceImpl implements FpProblemBaseService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpProblemBaseMapper.queryAll", pageData);
    }

    @Override
    public void add(PageData pageData) throws Exception {
        String id = IdUtil.simpleUUID();
        pageData.put("id", id);
        pageData.put("createTime", DateUtils.getTime());
        dao.save("FpProblemBaseMapper.insert", pageData);
    }

    @Override
    public void update(PageData pageData) throws Exception {
        dao.update("FpProblemBaseMapper.update", pageData);
    }

    @Override
    public void deleteAll(String[] ids) throws Exception {
        dao.update("FpProblemBaseMapper.deleteAll", ids);
    }

    @Override
    public List<PageData> getProblemBase(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpProblemBaseMapper.queryAll", pageData);
    }
/*
    private final FpProblemBaseRepository fpProblemBaseRepository;

    private final FpProblemBaseMapper fpProblemBaseMapper;

    public FpProblemBaseServiceImpl(FpProblemBaseRepository fpProblemBaseRepository, FpProblemBaseMapper fpProblemBaseMapper) {
        this.fpProblemBaseRepository = fpProblemBaseRepository;
        this.fpProblemBaseMapper = fpProblemBaseMapper;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpProblemBaseQueryCriteria criteria, Pageable pageable){
        Page<FpProblemBase> page = fpProblemBaseRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpProblemBaseMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpProblemBaseDto> queryAll(FpProblemBaseQueryCriteria criteria){
        return fpProblemBaseMapper.toDto(fpProblemBaseRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpProblemBaseDto findById(String id) {
        FpProblemBase fpProblemBase = fpProblemBaseRepository.findById(id).orElseGet(FpProblemBase::new);
        ValidationUtil.isNull(fpProblemBase.getId(),"FpProblemBase","id",id);
        return fpProblemBaseMapper.toDto(fpProblemBase);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpProblemBaseDto create(FpProblemBase resources) {
        resources.setId(IdUtil.simpleUUID());
        return fpProblemBaseMapper.toDto(fpProblemBaseRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpProblemBase resources) {
        FpProblemBase fpProblemBase = fpProblemBaseRepository.findById(resources.getId()).orElseGet(FpProblemBase::new);
        ValidationUtil.isNull( fpProblemBase.getId(),"FpProblemBase","id",resources.getId());
        fpProblemBase.copy(resources);
        fpProblemBaseRepository.save(fpProblemBase);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        for (String id : ids) {
            fpProblemBaseRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<FpProblemBaseDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpProblemBaseDto fpProblemBase : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" problemName",  fpProblemBase.getProblemName());
                         map.put(" problemType",  fpProblemBase.getProblemType());
                         map.put(" sourceType",  fpProblemBase.getSourceType());
                         map.put(" resRailwayId",  fpProblemBase.getResRailwayId());
                         map.put(" resTunnelId",  fpProblemBase.getResTunnelId());
                         map.put(" sourceId",  fpProblemBase.getSourceId());
                         map.put(" problem",  fpProblemBase.getProblem());
                         map.put(" describe",  fpProblemBase.getDescribe());
                         map.put(" state",  fpProblemBase.getState());
                         map.put(" createTime",  fpProblemBase.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }*/

}
