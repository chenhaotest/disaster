package com.perye.dokit.service;


import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface FpOfflineInspectionService {


    List<PageData> getAll(PageData pageData) throws Exception;

    void add(PageData pageData) throws Exception;

    void update(PageData pageData) throws Exception;

    void deleteAll(String[] ids) throws Exception;

    void createInspectionPerson(PageData pageData) throws Exception;

    void updateInspectionPerson(PageData pageData) throws Exception;

    void createInspectionProblem(PageData pageData) throws Exception;

    void updateInspectionProblem(PageData pageData) throws Exception;
}