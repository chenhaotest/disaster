package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.perye.dokit.dto.FpDepartmentDto;
import com.perye.dokit.entity.FpDepartment;
import com.perye.dokit.mapper.FpDepartmentMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpDepartmentQueryCriteria;
import com.perye.dokit.repository.FpDepartmentRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpDepartmentService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpDepartment")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpDepartmentServiceImpl implements FpDepartmentService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private DictDetailService dictDetailService;
    private final FpDepartmentRepository fpDepartmentRepository;

    private final FpDepartmentMapper fpDepartmentMapper;

    public FpDepartmentServiceImpl(FpDepartmentRepository fpDepartmentRepository, FpDepartmentMapper fpDepartmentMapper) {
        this.fpDepartmentRepository = fpDepartmentRepository;
        this.fpDepartmentMapper = fpDepartmentMapper;
    }

    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */
    @Override
    public List<PageData> fpDepartmentList(PageData pageData) throws Exception {

        List<PageData> pageList = (List<PageData>) dao.findForList("fpDepartmentMapper.selectPageList", pageData);
        // 部门类型
        Map<String,String> dmap = dictDetailService.queryDictDetailByName("department_type","map");
        //是否启用
        Map<String,String> dmap2 = dictDetailService.queryDictDetailByName("universal_activation","map");

        for (int i = 0; i <pageList.size() ; i++) {

            if(ObjectUtil.isNotEmpty((pageList.get(i).get("departmentType")))){
                pageList.get(i).put("departmentTypeLabel", dmap.get(pageList.get(i).get("departmentType").toString()));

            }
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("state")))){
                pageList.get(i).put("stateLabel", dmap2.get(pageList.get(i).get("state")));

            }
        }
        return pageList;
    }



    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpDepartmentQueryCriteria criteria, Pageable pageable){
        Page<FpDepartment> page = fpDepartmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpDepartmentMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpDepartmentDto> queryAll(FpDepartmentQueryCriteria criteria){
        return fpDepartmentMapper.toDto(fpDepartmentRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpDepartmentDto findById(String id) {
        FpDepartment fpDepartment = fpDepartmentRepository.findById(id).orElseGet(FpDepartment::new);
        ValidationUtil.isNull(fpDepartment.getId(),"FpDepartment","id",id);
        return fpDepartmentMapper.toDto(fpDepartment);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpDepartmentDto create(FpDepartment resources) {
        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());

        return fpDepartmentMapper.toDto(fpDepartmentRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpDepartment resources) {
        FpDepartment fpDepartment = fpDepartmentRepository.findById(resources.getId()).orElseGet(FpDepartment::new);
        ValidationUtil.isNull( fpDepartment.getId(),"FpDepartment","id",resources.getId());

        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        fpDepartment.copy(resources);
        fpDepartmentRepository.save(fpDepartment);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("fpDepartmentMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpDepartmentDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpDepartmentDto fpDepartment : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" departmentName",  fpDepartment.getDepartmentName());
                         map.put(" departmentType",  fpDepartment.getDepartmentType());
                         map.put(" state",  fpDepartment.getState());
                         map.put(" description",  fpDepartment.getDescription());
                         map.put(" isDelete",  fpDepartment.getIsDelete());
                         map.put(" createId",  fpDepartment.getCreateId());
                         map.put(" createTime",  fpDepartment.getCreateTime());
                         map.put(" updateTime",  fpDepartment.getUpdateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
