package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.FpInspectionService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.ValidationUtil;
import com.perye.dokit.utils.FileUtil;

import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@Service
//@CacheConfig(cacheNames = "fpInspection")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpInspectionServiceImpl implements FpInspectionService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Override
    public List<PageData> queryAll(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpInspectionMapper.queryAll", pageData);
    }

    @Override
    public void add(PageData pageData) throws Exception {
        String id = IdUtil.simpleUUID();
        pageData.put("id", id);
        pageData.put("createTime", DateUtils.getTime());
        pageData.put("updateTime", DateUtils.getTime());

        dao.save("FpInspectionMapper.insert", pageData);
    }

    @Override
    public void update(PageData pageData) throws Exception {
      String scheduledEndTime=  pageData.getString("scheduledEndTime");
       String scheduledStartTime= pageData.getString("scheduledStartTime");
        pageData.put("scheduledStartTime", DateUtils.dateTime(DateUtils.YYYY_MM_DD,scheduledStartTime));
        pageData.put("scheduledEndTime", DateUtils.dateTime(DateUtils.YYYY_MM_DD,scheduledEndTime));
        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpInspectionMapper.update", pageData);
    }
}
