package com.perye.dokit.service;

import com.perye.dokit.utils.pageData.PageData;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface FpEquipmentMaintainService {

    /**
     * 查询所有数据
     *
     * @param pageData 条件参数
     * @return List<FpRepairDto>
     */
    List<PageData> queryAll(PageData pageData) throws Exception;

    /**
     * 创建
     *
     * @param pageData /
     * @return FpRepairDto
     */
    void create(PageData pageData) throws Exception;

    /**
     * 编辑
     *
     * @param pageData /
     */
    void update(PageData pageData) throws Exception;

    /**
     * 导出数据
     *
     * @param all      待导出的数据
     * @param response /
     * @throws IOException /
     */
    void download(List<PageData> all, HttpServletResponse response) throws IOException;

    /**
     * 多选删除
     *
     * @param ids /
     */
    void deleteAll(String[] ids) throws Exception;
}
