package com.perye.dokit.service.impl;

import com.perye.dokit.entity.TestLi;
import com.perye.dokit.utils.ValidationUtil;
import com.perye.dokit.utils.FileUtil;
import com.perye.dokit.repository.TestLiRepository;
import com.perye.dokit.service.TestLiService;
import com.perye.dokit.dto.TestLiDto;
import com.perye.dokit.query.TestLiQueryCriteria;
import com.perye.dokit.mapper.TestLiMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import cn.hutool.core.util.IdUtil;
// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.perye.dokit.utils.PageUtil;
import com.perye.dokit.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

@Service
//@CacheConfig(cacheNames = "testLi")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class TestLiServiceImpl implements TestLiService {

    private final TestLiRepository testLiRepository;

    private final TestLiMapper testLiMapper;

    public TestLiServiceImpl(TestLiRepository testLiRepository, TestLiMapper testLiMapper) {
        this.testLiRepository = testLiRepository;
        this.testLiMapper = testLiMapper;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(TestLiQueryCriteria criteria, Pageable pageable){
        Page<TestLi> page = testLiRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(testLiMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<TestLiDto> queryAll(TestLiQueryCriteria criteria){
        return testLiMapper.toDto(testLiRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public TestLiDto findById(String id) {
        TestLi testLi = testLiRepository.findById(id).orElseGet(TestLi::new);
        ValidationUtil.isNull(testLi.getId(),"TestLi","id",id);
        return testLiMapper.toDto(testLi);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public TestLiDto create(TestLi resources) {
        resources.setId(IdUtil.simpleUUID());
        return testLiMapper.toDto(testLiRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(TestLi resources) {
        TestLi testLi = testLiRepository.findById(resources.getId()).orElseGet(TestLi::new);
        ValidationUtil.isNull( testLi.getId(),"TestLi","id",resources.getId());
        testLi.copy(resources);
        testLiRepository.save(testLi);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        for (String id : ids) {
            testLiRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<TestLiDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (TestLiDto testLi : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" name",  testLi.getName());
                         map.put(" createTime",  testLi.getCreateTime());
                         map.put(" isDelete",  testLi.getIsDelete());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
