package com.perye.dokit.service.impl;


import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.service.FpAlarmConfirmationService;
import com.perye.dokit.utils.FileUtil;
import com.perye.dokit.utils.PageUtil;
import com.perye.dokit.utils.QueryHelp;
import com.perye.dokit.utils.ValidationUtil;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpAlarmConfirmation")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class FpAlarmConfirmationServiceImpl implements FpAlarmConfirmationService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("FpAlarmConfirmationMapper.queryAll", pageData);
    }

/*    private final FpAlarmConfirmationRepository fpAlarmConfirmationRepository;

    private final FpAlarmConfirmationMapper fpAlarmConfirmationMapper;

    public FpAlarmConfirmationServiceImpl(FpAlarmConfirmationRepository fpAlarmConfirmationRepository, FpAlarmConfirmationMapper fpAlarmConfirmationMapper) {
        this.fpAlarmConfirmationRepository = fpAlarmConfirmationRepository;
        this.fpAlarmConfirmationMapper = fpAlarmConfirmationMapper;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpAlarmConfirmationQueryCriteria criteria, Pageable pageable){
        Page<FpAlarmConfirmation> page = fpAlarmConfirmationRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpAlarmConfirmationMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpAlarmConfirmationDto> queryAll(FpAlarmConfirmationQueryCriteria criteria){
        return fpAlarmConfirmationMapper.toDto(fpAlarmConfirmationRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpAlarmConfirmationDto findById(String id) {
        FpAlarmConfirmation fpAlarmConfirmation = fpAlarmConfirmationRepository.findById(id).orElseGet(FpAlarmConfirmation::new);
        ValidationUtil.isNull(fpAlarmConfirmation.getId(),"FpAlarmConfirmation","id",id);
        return fpAlarmConfirmationMapper.toDto(fpAlarmConfirmation);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpAlarmConfirmationDto create(FpAlarmConfirmation resources) {
        resources.setId(IdUtil.simpleUUID());
        return fpAlarmConfirmationMapper.toDto(fpAlarmConfirmationRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpAlarmConfirmation resources) {
        FpAlarmConfirmation fpAlarmConfirmation = fpAlarmConfirmationRepository.findById(resources.getId()).orElseGet(FpAlarmConfirmation::new);
        ValidationUtil.isNull( fpAlarmConfirmation.getId(),"FpAlarmConfirmation","id",resources.getId());
        fpAlarmConfirmation.copy(resources);
        fpAlarmConfirmationRepository.save(fpAlarmConfirmation);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        for (String id : ids) {
            fpAlarmConfirmationRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<FpAlarmConfirmationDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpAlarmConfirmationDto fpAlarmConfirmation : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" name",  fpAlarmConfirmation.getName());
                         map.put(" sourceType",  fpAlarmConfirmation.getSourceType());
                         map.put(" sourceId",  fpAlarmConfirmation.getSourceId());
                         map.put(" creatorId",  fpAlarmConfirmation.getCreatorId());
                         map.put(" creatorName",  fpAlarmConfirmation.getCreatorName());
                         map.put(" state",  fpAlarmConfirmation.getState());
                         map.put(" createTime",  fpAlarmConfirmation.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }*/

}
