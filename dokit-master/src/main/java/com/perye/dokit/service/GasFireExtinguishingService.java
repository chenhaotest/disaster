package com.perye.dokit.service;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface GasFireExtinguishingService {

    List<PageData> getAll(PageData pageData) throws Exception;

    List<PageData> findAllDevce(PageData pageData) throws Exception;

    PageData findWarningLevel(PageData pageData) throws Exception;

    List<PageData> findWarningTimeLevel(PageData pageData) throws Exception;

    List<PageData> findDeviceNumEchart(PageData pageData) throws Exception;

    PageData findDeviceByCode(PageData pageData) throws Exception;

    List<  List<PageData>> findDevFireImg(PageData pageData) throws Exception;
}
