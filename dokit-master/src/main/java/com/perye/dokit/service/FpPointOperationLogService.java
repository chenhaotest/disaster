package com.perye.dokit.service;


import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface FpPointOperationLogService {


    List<PageData> getAll(PageData pageData) throws Exception;
}