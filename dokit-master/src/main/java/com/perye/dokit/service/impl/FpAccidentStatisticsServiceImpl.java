package com.perye.dokit.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.perye.dokit.dto.FpAccidentStatisticsDto;
import com.perye.dokit.entity.FpAccidentStatistics;
import com.perye.dokit.exception.BadRequestException;
import com.perye.dokit.mapper.FpAccidentStatisticsMapper;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.query.FpAccidentStatisticsQueryCriteria;
import com.perye.dokit.repository.FpAccidentStatisticsRepository;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.service.FpAccidentStatisticsService;
import com.perye.dokit.utils.*;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;

@Service
//@CacheConfig(cacheNames = "fpAccidentStatistics")
@Transactional(propagation = Propagation.SUPPORTS, rollbackFor = Exception.class)
public class FpAccidentStatisticsServiceImpl implements FpAccidentStatisticsService {
    @Resource(name = "daoSupport")
    private DaoSupport dao;
    private final FpAccidentStatisticsRepository fpAccidentStatisticsRepository;

    private final FpAccidentStatisticsMapper fpAccidentStatisticsMapper;
  @Autowired
   private  DictDetailService dictDetailService;
    /**
     * 查询数据分页
     * @param pageData 条件
     * @param pageData 分页参数
     * @return List<PageData>
     */
    @Override
    public List<PageData> fpAccidentStatisticsList(PageData pageData) throws Exception {

        List<PageData> pageList = (List<PageData>) dao.findForList("fpAccidentStatisticsMapper.fpAccidentStatisticsList", pageData);
       // 厂商类型
        Map<String,String> dmap = dictDetailService.queryDictDetailByName("equipment_vendors_type","map");
        //是否有不良信用记录
        Map<String,String> dmap2 = dictDetailService.queryDictDetailByName("bad_credit","map");
        for (int i = 0; i <pageList.size() ; i++) {
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("equipmentVendorsType")))){
                StringBuilder sb =new StringBuilder();
               String [] evtArr = pageList.get(i).get("equipmentVendorsType").toString().split(",");
                for (int j = 0; j <evtArr.length ; j++) {
                    if(j==evtArr.length-1){
                        sb.append(dmap.get(evtArr[j]));
                    }else{
                        sb.append(dmap.get(evtArr[j])+",");
                    }

                }
                pageList.get(i).put("equipmentVendorsTypeLabel", sb.toString());

            }
            if(ObjectUtil.isNotEmpty((pageList.get(i).get("badCredit")))){
                pageList.get(i).put("badCreditLabel", dmap2.get(pageList.get(i).get("badCredit")));

            }
        }
        return pageList;

    }
    public FpAccidentStatisticsServiceImpl(FpAccidentStatisticsRepository fpAccidentStatisticsRepository, FpAccidentStatisticsMapper fpAccidentStatisticsMapper) {
        this.fpAccidentStatisticsRepository = fpAccidentStatisticsRepository;
        this.fpAccidentStatisticsMapper = fpAccidentStatisticsMapper;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(FpAccidentStatisticsQueryCriteria criteria, Pageable pageable){
        Page<FpAccidentStatistics> page = fpAccidentStatisticsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(fpAccidentStatisticsMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<FpAccidentStatisticsDto> queryAll(FpAccidentStatisticsQueryCriteria criteria){
        return fpAccidentStatisticsMapper.toDto(fpAccidentStatisticsRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public FpAccidentStatisticsDto findById(String id) {
        FpAccidentStatistics fpAccidentStatistics = fpAccidentStatisticsRepository.findById(id).orElseGet(FpAccidentStatistics::new);
        ValidationUtil.isNull(fpAccidentStatistics.getId(),"FpAccidentStatistics","id",id);
        return fpAccidentStatisticsMapper.toDto(fpAccidentStatistics);
    }

    /**
     * 新增数据
     * @param resources /
     * @return
     */
    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public FpAccidentStatisticsDto create(FpAccidentStatistics resources) {
        //判断名字是不是唯一
            FpAccidentStatistics fsq =new FpAccidentStatistics();
            fsq.setIsDelete(0);
            fsq.setManufacturerName(resources.getManufacturerName());
            Example<FpAccidentStatistics> example = Example.of(fsq);
        Optional<FpAccidentStatistics> optional = fpAccidentStatisticsRepository.findOne(example);

        if(optional.isPresent()){
            throw new BadRequestException("维保商名称已经存在");
        }

        resources.setId(IdUtil.simpleUUID());
        resources.setIsDelete(0);
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setCreateTime(timestamp);
        resources.setUpdateTime(timestamp);
        resources.setCreateId(SecurityUtils.getCurrentUserId().toString());
        return fpAccidentStatisticsMapper.toDto(fpAccidentStatisticsRepository.save(resources));
    }
    /**
     *  更新数据
     * @param resources /
     * @return
     */
    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(FpAccidentStatistics resources) {
        //判断名字是不是唯一
        FpAccidentStatistics fsq =new FpAccidentStatistics();
        fsq.setIsDelete(0);
        fsq.setManufacturerName(resources.getManufacturerName());
        Example<FpAccidentStatistics> example = Example.of(fsq);
        Optional<FpAccidentStatistics> optional = fpAccidentStatisticsRepository.findOne(example);
        //如果不等于null
        if(optional.isPresent()){
            FpAccidentStatistics fs= optional.get();
            if(!fs.getId().equals(resources.getId())){
                throw new BadRequestException("维保商名称已经存在");
            }

        }

        FpAccidentStatistics fpAccidentStatistics = fpAccidentStatisticsRepository.findById(resources.getId()).orElseGet(FpAccidentStatistics::new);
        ValidationUtil.isNull( fpAccidentStatistics.getId(),"FpAccidentStatistics","id",resources.getId());
        Timestamp timestamp= new Timestamp(System.currentTimeMillis());
        resources.setUpdateTime(timestamp);
        fpAccidentStatistics.copy(resources);
        fpAccidentStatisticsRepository.save(fpAccidentStatistics);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(String[] ids) {
        try {
            //软删除实际是更新
            List<String> idList = new ArrayList<>(Arrays.asList(ids));
            dao.delete("fpAccidentStatisticsMapper.deleteAll", idList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void download(List<FpAccidentStatisticsDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (FpAccidentStatisticsDto fpAccidentStatistics : all) {
            Map<String,Object> map = new LinkedHashMap<>();
                         map.put(" manufacturerName",  fpAccidentStatistics.getManufacturerName());
                         map.put(" personName",  fpAccidentStatistics.getPersonName());
                         map.put(" personPhone",  fpAccidentStatistics.getPersonPhone());
                         map.put(" manufacturersAddress",  fpAccidentStatistics.getManufacturersAddress());
                         map.put(" country",  fpAccidentStatistics.getCountry());
                         map.put(" badCredit",  fpAccidentStatistics.getBadCredit());
                         map.put(" equipmentVendorsType",  fpAccidentStatistics.getEquipmentVendorsType());
                         map.put(" isDelete",  fpAccidentStatistics.getIsDelete());
                         map.put(" createId",  fpAccidentStatistics.getCreateId());
                         map.put(" createTime",  fpAccidentStatistics.getCreateTime());
                         map.put(" updateTime",  fpAccidentStatistics.getUpdateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

 }
