package com.perye.dokit.service;

import com.perye.dokit.dto.FpHotWorkDto;
import com.perye.dokit.entity.FpHotWork;
import com.perye.dokit.query.FpHotWorkQueryCriteria;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface FpHotWorkService {


    /**
     * 更改审批状态
     * @return
     */

    public void updataApprovalStatusById(FpHotWork resources);

    /**
     * 查询数据分页
     * @param pageData pageData
     * @return Map<String,Object>
     */

    public List<PageData> fpHotWorklList(PageData pageData) throws Exception;
    /**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(FpHotWorkQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<FpHotWorkDto>
    */
    List<FpHotWorkDto> queryAll(FpHotWorkQueryCriteria criteria);

    /**
    * 根据ID查询
    * @param id ID
    * @return FpHotWorkDto
    */
    FpHotWorkDto findById(String id);

    /**
    * 创建
    * @param resources /
    * @return FpHotWorkDto
    */
    FpHotWorkDto create(FpHotWork resources);

    /**
    * 编辑
    * @param resources /
    */
    void update(FpHotWork resources);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<FpHotWorkDto> all, HttpServletResponse response) throws IOException;

    /**
    * 多选删除
    * @param ids /
    */
    void deleteAll(String[] ids);
}
