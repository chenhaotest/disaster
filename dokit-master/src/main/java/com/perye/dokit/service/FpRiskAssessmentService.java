package com.perye.dokit.service;

import com.perye.dokit.dto.FpRiskAssessmentDto;
import com.perye.dokit.entity.FpRiskAssessment;
import com.perye.dokit.query.FpRiskAssessmentQueryCriteria;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;


public interface FpRiskAssessmentService {
    /**
     * 查询数据分页
     * @param pageData 条件
     * @return Map<String,Object>
     */
    public List<PageData> fpRiskAssessmentList(PageData pageData) throws Exception;

    /**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(FpRiskAssessmentQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<FpRiskAssessmentDto>
    */
    List<FpRiskAssessmentDto> queryAll(FpRiskAssessmentQueryCriteria criteria);

    /**
    * 根据ID查询
    * @param id ID
    * @return FpRiskAssessmentDto
    */
    FpRiskAssessmentDto findById(String id);

    /**
    * 创建
    * @param resources /
    * @return FpRiskAssessmentDto
    */
    FpRiskAssessmentDto create(FpRiskAssessment resources);

    /**
    * 编辑
    * @param resources /
    */
    void update(FpRiskAssessment resources);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<FpRiskAssessmentDto> all, HttpServletResponse response) throws IOException;

    /**
    * 多选删除
    * @param ids /
    */
    void deleteAll(String[] ids);
}
