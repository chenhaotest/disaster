package com.perye.dokit.restartExe;

import com.perye.dokit.newservice.DeviceManagService;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import java.util.List;

//@Component
public class ApplicationRunnerImpl implements ApplicationRunner {

   @Autowired
    private  DeviceService deviceService;
    @Autowired
    private DeviceManagService deviceManagService;
    @Override
    public void run(ApplicationArguments args) throws Exception {


        /**
         *
         *
         * 查询modubs  管理机连接中，重新启动项目，请求自动连接。
         */
        PageData info=  new PageData();
        info.put("mmId","0");
        info.put("deviceConnectState",1);

        List<PageData> list=  deviceService.selectDevicelistInfo(info);

        for (PageData data:list) {
            deviceManagService.updateDeviceManagStart(data);
        }

    }
}
