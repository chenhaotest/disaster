package com.perye.dokit.newservice.iml;/**
 * @Description //TODO
 * @Date 2023/7/27 13:55
 * @Author chenhao
 **/

import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.analysis.DeviceInitialization;
import com.perye.dokit.analysis.ErrorLog;
import com.perye.dokit.analysis.GasFireExtinguishing;
import com.perye.dokit.config.mqtt.business.MqttTheme;
import com.perye.dokit.config.mqtt.util.DataPacket;
import com.perye.dokit.config.mqtt.util.DataPacketEnum;
import com.perye.dokit.config.mqtt.util.HexStringUtil;
import com.perye.dokit.config.mqtt.util.ImgUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.MqttThemeService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.UuidUtil;
import com.perye.dokit.utils.pageData.PageData;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author chenhao
 * @date 2023/7/27   13:55
 */

@Service
public class MqttThemeServiceImpl implements MqttThemeService {
    @Override
    public void toMqttDataTopic(String topic, MqttMessage message) throws Exception {
       // deviceRequest(topic, message);

        DataPacket packet = DataPacket.getDataPacket(message.getPayload());
        System.out.println("防护门");
    }

    @Resource(name = "daoSupport")
    private DaoSupport dao;


    /**
     * \* Created with IntelliJ IDEA.
     * \* User: chenhao
     * \* Date: 2023/7/27
     * \* Time: 14:03
     * \* Description:  消息主题分类处理
     * \
     */
    public PageData deviceRequest(String topic, MqttMessage message) throws Exception {
        System.out.println(topic);
        PageData result = new PageData();
        String jsonStr = new String(message.getPayload(), "GB2312");
        if (StringUtils.isNotBlank(jsonStr)) {

            PageData json = JSONObject.parseObject(jsonStr, PageData.class);
            String batchCode = UuidUtil.get32UUID();




            if (StringUtils.equals(MqttTheme.TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_ORDINARY, topic)) { //初始化设备
                DeviceInitialization.jsonAnalysis(json, batchCode); //初始化 设备模拟
            }
            /**
             * 气体灭火监控系统正常日志、异常日志记录
             */
            if (StringUtils.equals(MqttTheme.TUNNEL_MAIN_LINE_FIRE_MONITORING_SYSTEM_LOG_ORDINARY, topic)) {


                GasFireExtinguishing.jsonAnalysis(json, batchCode);
                ErrorLog.jsonAnalysis(json, batchCode);
            }
        }


        return result;

    }

}
