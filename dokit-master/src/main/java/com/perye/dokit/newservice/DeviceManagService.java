package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface DeviceManagService {

    List<PageData> getAll(PageData pageData) throws Exception;

    List<PageData> findDevicelist(PageData pDDevice) throws Exception;

    void addDevice(PageData pDDevice) throws Exception;

    void addDeviceList(List<PageData> deviceAddList) throws Exception;

    void add(PageData pageData) throws Exception;

    void update(PageData pageData) throws Exception;



    void delete(PageData pageData) throws Exception;

    void updateDeviceManag(PageData pd) throws Exception;

    void updateDeviceManagStart(PageData pageData) throws Exception;

    void updateDeviceManagStop(PageData pageData) throws Exception;
}
