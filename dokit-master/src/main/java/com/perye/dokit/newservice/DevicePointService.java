package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;
import java.util.Map;

public interface DevicePointService {


    List<PageData> findPoints(PageData pd) throws Exception;


    void updatePointList(List devideList) throws Exception;

    List<PageData> findMaxPoints(PageData pd) throws Exception;
}
