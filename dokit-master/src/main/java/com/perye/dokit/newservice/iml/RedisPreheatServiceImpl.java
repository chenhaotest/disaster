package com.perye.dokit.newservice.iml;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.RedisPreheatService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.RedisUtils;
import com.perye.dokit.utils.ResultState;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class RedisPreheatServiceImpl implements RedisPreheatService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private RedisUtils redisUtils;

    @Override
    public PageData resettingRedis() throws Exception {

        //清除redis
        //清除学校redis信息
        List<String> schoolKeyList = redisUtils.scan("schoolData:*");
        if (CollectionUtil.isNotEmpty(schoolKeyList) && schoolKeyList.size() > 0) {
            for (String schoolKey : schoolKeyList) {
                redisUtils.del(schoolKey);
            }
        }

        //清除浸水规则redis信息
        List<String> immerKeyList = redisUtils.scan("schoolRule:*");
        if (CollectionUtil.isNotEmpty(immerKeyList) && immerKeyList.size() > 0) {
            for (String immerKey : immerKeyList) {
                redisUtils.del(immerKey);
            }
        }


        //清除水流规则redis信息
        List<String> sensorKeyList = redisUtils.scan("sensorRule:*");
        if (CollectionUtil.isNotEmpty(sensorKeyList) && sensorKeyList.size() > 0) {
            for (String sensorKey : sensorKeyList) {
                redisUtils.del(sensorKey);
            }
        }


        //清除浸水硬件redis信息
        List<String> immerDeviceKeyList = redisUtils.scan("schoolImmer:*");
        if (CollectionUtil.isNotEmpty(immerDeviceKeyList)) {
            for (String immerDevice : immerDeviceKeyList) {
                redisUtils.del(immerDevice);
            }
        }

        //清除浸水设备与规则绑定的redis信息
        List<String> ruleImmerKeyList = redisUtils.scan("schoolRuleImmer:*");
        if (CollectionUtil.isNotEmpty(ruleImmerKeyList)) {
            for (String ruleImmer : ruleImmerKeyList) {
                redisUtils.del(ruleImmer);
            }
        }

        //清除学校员工的redis信息
        List<String> schoolAdministratorKeyList = redisUtils.scan("schoolAdministrator:*");
        if (CollectionUtil.isNotEmpty(schoolAdministratorKeyList)) {
            for (String schoolAdministrator : schoolAdministratorKeyList) {
                redisUtils.del(schoolAdministrator);
            }
        }

        //删除异常硬件缓存信息
        List<String> sensorRuleBatchKeyList = redisUtils.scan("SensorRuleBatch:*");
        if (CollectionUtil.isNotEmpty(sensorRuleBatchKeyList)) {
            for (String sensorRuleBatch : sensorRuleBatchKeyList) {
                redisUtils.del(sensorRuleBatch);
            }
        }
        List<String> immerHardwareOneKeyList = redisUtils.scan("immerHardwareOne:*");
        if (CollectionUtil.isNotEmpty(immerHardwareOneKeyList)) {
            for (String immerHardwareOne : immerHardwareOneKeyList) {
                redisUtils.del(immerHardwareOne);
            }
        }
        List<String> schoolRuleBatchImmerKeyList = redisUtils.scan("schoolRuleBatchImmer:*");
        if (CollectionUtil.isNotEmpty(schoolRuleBatchImmerKeyList)) {
            for (String schoolRuleBatchImmer : schoolRuleBatchImmerKeyList) {
                redisUtils.del(schoolRuleBatchImmer);
            }
        }

        //================================================
        //插入redis
        //将学校信息添加到redis中
        PageData info = new PageData();
        List<PageData> schoolList = (List<PageData>) dao.findForList("RedisPreheatMapper.querySchoolList", info);
        if (CollectionUtil.isNotEmpty(schoolList)) {
            for (PageData school : schoolList) {
                String remarks = school.getString("remarks");
                if (StringUtils.isBlank(remarks)) {
                    school.put("remarks", "");
                }
                String key = "schoolData:" + school.getString("schoolNum");//固定字段+学校编号
                redisUtils.hmset(key, school);
            }
        }

        //将浸水规则信息添加到redis中
        List<PageData> immerList = (List<PageData>) dao.findForList("RedisPreheatMapper.queryImmerList", info);
        if (CollectionUtil.isNotEmpty(immerList)) {
            for (PageData immers : immerList) {
                String state = immers.getString("state");
                if (StringUtils.isNotBlank(state)) {
                    immers.put("state", Integer.parseInt(state));
                }
                String key = "schoolRule:" + immers.getString("schoolNum");//固定字段+学校编号
                redisUtils.hmset(key, immers);
            }
        }


        //将水流规则信息添加到redis中
        List<PageData> sensorList = (List<PageData>) dao.findForList("RedisPreheatMapper.querySensorList", info);
        if (CollectionUtil.isNotEmpty(sensorList)) {
            for (PageData sensor : sensorList) {
                ///时间格式转换
                String beginTime = new SimpleDateFormat("HH:mm").format(sensor.get("beginTime"));
                String endTime = new SimpleDateFormat("HH:mm").format(sensor.get("endTime"));
                if (StringUtils.isNotBlank(beginTime)) {
                    sensor.put("beginTime", beginTime);
                }
                if (StringUtils.isNotBlank(endTime)) {
                    sensor.put("endTime", endTime);
                }

                //最大最小阈值格式转换
                BigDecimal lvBig = new BigDecimal(sensor.get("largeValue") + "");
                BigDecimal svBig = new BigDecimal(sensor.get("smallValue") + "");


                String largeValue = lvBig.intValue() + "";
                String smallValue = svBig.intValue() + "";
                if (StringUtils.isNotBlank(largeValue)) {
                    sensor.put("largeValue", largeValue);
                }
                if (StringUtils.isNotBlank(smallValue)) {
                    sensor.put("smallValue", smallValue);
                }

                String key = "sensorRule:" + sensor.getString("sensorRulesType") + ":" + sensor.getString("sensorRulesState") + ":" + sensor.getString("id");//固定字段+水流规则类型(1:主路，2:分路)+水流规则状态(0:启用，1:未启用)+具体规则id
                redisUtils.hmset(key, sensor);
            }
        }


        //将浸水硬件信息添加到redis中
        List<PageData> immerDeviceList = (List<PageData>) dao.findForList("RedisPreheatMapper.queryImmerDeviceList", info);
        if (CollectionUtil.isNotEmpty(immerDeviceList)) {
            for (PageData immerDevice : immerDeviceList) {

                String remarks = immerDevice.getString("remarks");
                if (StringUtils.isBlank(remarks)) {
                    immerDevice.put("remarks", "");
                }
                String key = "schoolImmer:" + immerDevice.getString("hardwareCode") + ":" + immerDevice.getString("type");//固定字段+硬件编码+规则类型（级联还是单独）
                redisUtils.hmset(key, immerDevice);
            }
        }


        //将浸水设备与规则绑定的信息添加到redis中-----数据格式转化待完成
        List<PageData> ruleImmerList = (List<PageData>) dao.findForList("RedisPreheatMapper.queryRuleImmerList", info);
        if (CollectionUtil.isNotEmpty(ruleImmerList)) {
            for (PageData ruleImmer : ruleImmerList) {
                String remarks = ruleImmer.getString("remarks");
                if (StringUtils.isBlank(remarks)) {
                    ruleImmer.put("remarks", "");
                }
                String key = "schoolRuleImmer:" + ruleImmer.getString("sourceId") + ":" + ruleImmer.getString("type") + ":" + ruleImmer.getString("hardwareCode");//固定字段 + 浸水规则id + 规则类型（级联还是单独）+ 硬件编码
                redisUtils.hmset(key, ruleImmer);
            }
        }

        //将学校员工信息添加到redis中
        List<PageData> schoolAdministratorList = (List<PageData>) dao.findForList("RedisPreheatMapper.querySchoolAdministratorList", info);
        if (CollectionUtil.isNotEmpty(schoolAdministratorList)) {
            for (PageData schoolAdministrator : schoolAdministratorList) {
                String remarks = schoolAdministrator.getString("remarks");
                if (StringUtils.isBlank(remarks)) {
                    schoolAdministrator.put("remarks", "");
                }

                long l = (long) schoolAdministrator.get("enabled");
                int enabled = (int) l;
                String enabledStr = enabled == 0 ? "false" : "true";
                if (StringUtils.isNotBlank(enabledStr)) {
                    schoolAdministrator.put("enabled", enabledStr);
                }
                String key = "schoolAdministrator:" + schoolAdministrator.getString("schoolNum") + ":" + schoolAdministrator.getString("id");//固定字段 + 浸水规则id + 规则类型（级联还是单独）+ 硬件编码
                redisUtils.hmset(key, schoolAdministrator);
            }
        }

        PageData result = new PageData();
        result.put("code", "00");
        result.put("msg", ResultState.SuccessUpdate);
        return result;
    }
}
