package com.perye.dokit.newservice.iml;


import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.HqVideoService;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
@Transactional
public class HqVideoServiceImpl implements HqVideoService {


    @Resource(name = "daoSupport")
    private DaoSupport dao;


    @Override
    public Integer create(PageData pageData) throws Exception {
        dao.save("HqVideoMapper.insert", pageData);

        int id = Integer.parseInt(pageData.get("id") + "");
        return id;
    }

    @Override
    public PageData findVideo(PageData imgData) throws Exception {
        PageData result= (PageData)dao.findForObject("HqVideoMapper.findVideo", imgData);
        return result;
    }

    @Override
    public void update(PageData pageData) throws Exception {
        dao.update("HqVideoMapper.updateByPrimaryKeySelective", pageData);
    }

    @Override
    public void deleteAll(String[] ids) throws Exception {
        dao.delete("HqVideoMapper.deleteAll",ids);
    }

    @Override
    public void deleteVideo(String[] ids) throws Exception {
        dao.update("HqVideoMapper.deleteVideo",ids);
    }

}
