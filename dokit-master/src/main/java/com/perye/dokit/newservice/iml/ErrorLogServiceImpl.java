package com.perye.dokit.newservice.iml;

import cn.hutool.core.collection.CollectionUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.ErrorLogService;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ErrorLogServiceImpl implements ErrorLogService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private DictDetailService dictDetailService;

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        List<PageData> logList = (List<PageData>) dao.findForList("ErrorLogMapper.getAll", pageData);

        Map<String, String> stateMap = dictDetailService.queryDictDetailByName("device_state", "map");
        Map<String, String> gfMap = dictDetailService.queryDictDetailByName("gf_is_abnormal", "map");
        Map<String, String> avMap = dictDetailService.queryDictDetailByName("av_is_abnormal", "map");
        Map<String, String> biMap = dictDetailService.queryDictDetailByName("bi_is_abnormal", "map");
        Map<String, String> hbMap = dictDetailService.queryDictDetailByName("hb_is_abnormal", "map");
        Map<String, String> imgMap = dictDetailService.queryDictDetailByName("img_is_abnormal", "map");
        Map<String, String> mbMap = dictDetailService.queryDictDetailByName("mb_is_abnormal", "map");
        Map<String, String> sdMap = dictDetailService.queryDictDetailByName("sd_is_abnormal", "map");
        Map<String, String> sfMap = dictDetailService.queryDictDetailByName("sf_is_abnormal", "map");
        Map<String, String> vrMap = dictDetailService.queryDictDetailByName("vr_is_abnormal", "map");
        Map<String, String> wsMap = dictDetailService.queryDictDetailByName("ws_is_abnormal", "map");

        if (CollectionUtil.isNotEmpty(logList)) {

            Map<String, List> map = new HashMap<>();
            List<PageData> imgList = (List<PageData>) dao.findForList("ErrorLogMapper.queryImgList", pageData);
            if (CollectionUtil.isNotEmpty(imgList)) {
                for (PageData data : imgList) {
                    String deviceId = data.getString("deviceId");
                    List<String> list = map.get(deviceId);
                    if (CollectionUtil.isEmpty(list)) {
                        list = new ArrayList<String>();
                    }
                    list.add(data.getString("url"));
                    map.put(deviceId, list);
                }
            }


            for (PageData log : logList) {

                String id = log.getString("id");
                List<String> list = map.get(id);
                if (CollectionUtil.isNotEmpty(list)) {
                    log.put("imgList", list);
                }

                String state = log.getString("state");
                log.put("deviceState", "");
                if (StringUtils.isNotBlank(state)) {
                    log.put("deviceState", stateMap.get(state));
                    log.put("deviceStateNum",state);
                }

                //设备角色：0：管理机，1：温感，2：烟感，3：图像型火灾探测器，4：人体识别传感器，5：语音提示器，6：悬挂式气体灭火装置，7：手动按钮，8：声光报警器，9：放气指示灯
                String deviceRole = log.getString("deviceRole");
                String isAbnormal = log.getString("isAbnormal");
                if (StringUtils.isNotBlank(deviceRole)) {
                    if ("0".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", gfMap.get(isAbnormal));
                        }
                    } else if ("1".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", wsMap.get(isAbnormal));
                        }
                    } else if ("2".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", sdMap.get(isAbnormal));
                        }
                    } else if ("3".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", imgMap.get(isAbnormal));
                        }
                    } else if ("4".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", hbMap.get(isAbnormal));
                        }
                    } else if ("5".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", vrMap.get(isAbnormal));
                        }
                    } else if ("6".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", sfMap.get(isAbnormal));
                        }
                    } else if ("7".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", mbMap.get(isAbnormal));
                        }
                    } else if ("8".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", avMap.get(isAbnormal));
                        }
                    } else if ("9".equals(deviceRole)) {
                       // log.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            log.put("isAbnormalStr", biMap.get(isAbnormal));
                        }
                    }
                }
            }
        }
        return logList;
    }
}
