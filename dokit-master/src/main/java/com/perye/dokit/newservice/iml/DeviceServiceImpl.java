package com.perye.dokit.newservice.iml;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.modbus.modbusTcp.Modbus4jUtils;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.RedisUtils;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import com.serotonin.modbus4j.ModbusMaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DeviceServiceImpl implements DeviceService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private DictDetailService dictDetailService;

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        List<PageData> deviceList = (List<PageData>) dao.findForList("FpDeviceMapper.queryDeviceList", pageData);
        return deviceList;
    }
    @Override
    public void add(PageData pageData) throws Exception {

        String Id = IdUtil.simpleUUID();
        pageData.put("id", Id);
        pageData.put("isDelete", 0);
        pageData.put("deviceAcquisitionDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("deviceAcquisitionDate")+""));
        pageData.put("mStartDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("mStartDate")+""));
        pageData.put("mEndDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("mEndDate")+""));
        pageData.put("deviceWarrantyDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("deviceWarrantyDate")+""));
        pageData.put("createTime", DateUtils.getTime());
        pageData.put("updateTime", DateUtils.getTime());
        dao.save("FpDeviceMapper.insert", pageData);
    }

    @Override
    public void update(PageData pageData) throws Exception {
        pageData.put("deviceAcquisitionDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("deviceAcquisitionDate")+""));
        pageData.put("mStartDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("mStartDate")+""));
        pageData.put("mEndDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("mEndDate")+""));
        pageData.put("deviceWarrantyDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("deviceWarrantyDate")+""));
        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpDeviceMapper.update", pageData);
    }

    @Override
    public void pointAdd(PageData pageData) throws Exception {
        String Id = IdUtil.simpleUUID();
        pageData.put("id", Id);
        pageData.put("isDelete", 0);
        pageData.put("createTime", DateUtils.getTime());
        pageData.put("updateTime", DateUtils.getTime());
        dao.save("FpDevicePointMapper.insert", pageData);
    }

    @Override
    public void pointEdit(PageData pageData) throws Exception {
        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpDevicePointMapper.update", pageData);
    }

    @Override
    public void pointDelete(PageData pageData) throws Exception {
        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpDevicePointMapper.pointDeleteAll", pageData);
    }

    @Override
    public void delete(PageData pageData) throws Exception {
        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpDeviceMapper.delete", pageData);
    }

    @Override
    public PageData selectDeviceInfo(PageData info) throws Exception {
        PageData result= (PageData)dao.findForObject("FpDeviceMapper.selectDeviceInfo", info);
        return result;
    }

    @Override
    public void addModubsPointsLog(List devideList) throws Exception {
        dao.save("FpModubsPointsLogMapper.insertList", devideList);
    }

    @Override
    public List<PageData> selectByDevicePoints(PageData info) throws Exception {
        List<PageData> deviceList = (List<PageData>) dao.findForList("FpDevicePointMapper.selectByDevicePoints", info);
        return deviceList;
    }

    @Override
    public List<PageData> selectDevicelistInfo(PageData info) throws Exception {

        List<PageData> deviceList = (List<PageData>) dao.findForList("FpDeviceMapper.selectDevicelistInfo", info);
        return deviceList;
    }

    @Override
    public PageData changeModubsVal(PageData pageData) throws Exception {

        PageData resultData=new PageData();
        try {

            //查询当前 点位所在的管理机， 查询所属ip  端口
            PageData modubsInfo=new PageData();
            modubsInfo.put("deviceId",pageData.getString("deviceId"));
            PageData  result = (PageData)dao.findForObject("FpDeviceMapper.selectDeviceParent", modubsInfo);




            String pointId=pageData.getString("pointId");//获取点位id
            String modebustcpIp=result.getString("deviceIp");//获取ip
            Integer port=Integer.parseInt(result.get("devicePort")+"");//获取端口
            Integer registerAddress=Integer.parseInt(pageData.get("registerAddress")+""); //获取偏移量
            Short registerAddressVal=Short.parseShort(pageData.get("registerAddressVal")+"");//获取新的偏移量值

            //连接modubs
            ModbusMaster master = Modbus4jUtils.getMaster(modebustcpIp, port);
            Boolean res1 = Modbus4jUtils.writeRegisters(master,1, registerAddress, new short[]{registerAddressVal});
            //修改成功之后， 修改改偏移量的 值


            PageData point=new PageData();
            point.put("id",pointId);
            point.put("registerAddressVal",registerAddressVal);
            dao.update("FpDevicePointMapper.update", point);

            resultData.put("code", "200");
            resultData.put("data", res1);
            resultData.put("msg", "成功");
        } catch (Exception e) {
            e.printStackTrace();
            resultData.put("code", "201");
            resultData.put("data", "");
            resultData.put("msg", e.getMessage());
        }

        return resultData;
    }

    /**
     *  chenhao
     *  获取当前
     * @param pageData
     * @return
     */

    @Override
    public List<PageData> findDevicelist(PageData pageData) throws Exception {
        List<PageData> deviceList = (List<PageData>) dao.findForList("DeviceMapper.findDevicelist", pageData);
        return deviceList;
    }

    @Override
    public void addDevice(PageData pDDevice) throws Exception {
        dao.save("DeviceMapper.addDevice", pDDevice);
    }

    @Override
    public void addDeviceList(List<PageData> deviceAddList) throws Exception {
        dao.save("DeviceMapper.addDeviceList", deviceAddList);
    }


}
