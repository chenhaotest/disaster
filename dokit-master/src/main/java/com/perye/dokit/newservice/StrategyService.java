package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface StrategyService {

    PageData queryStrategyList(PageData pageData) throws Exception;
}
