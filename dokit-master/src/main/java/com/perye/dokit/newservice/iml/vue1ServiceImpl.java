package com.perye.dokit.newservice.iml;



import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.vue1Service;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class vue1ServiceImpl implements vue1Service {


    @Resource(name = "daoSupport")
    private DaoSupport dao;


    @Override
    public List<PageData> getList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("DemoMapper.getList", pageData);
    }

    @Override
    public void add(PageData pageData) throws Exception {
       // dao.save("DemoMapper.add",pageData);
    }

    @Override
    public List<PageData> getUserList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("vueMapper.getList", pageData);
    }

    @Override
    public List<PageData> findUser(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("vueMapper.findUser", pageData);
    }

    @Override
    public List<PageData> findRole(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("vueMapper.findRole", pageData);
    }

    @Override
    public List<PageData> findMenu(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("vueMapper.findMenu", pageData);
    }

    @Override
    public  List<PageData> findRoleMenu(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("vueMapper.findRoleMenu", pageData);
    }

    @Override
    public List<PageData> findAllMenu(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("vueMapper.findAllMenu", pageData);
    }


}
