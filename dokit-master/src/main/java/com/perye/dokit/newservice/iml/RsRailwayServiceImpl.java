package com.perye.dokit.newservice.iml;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;

import com.perye.dokit.newservice.RsRailwayService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional( rollbackFor = {RuntimeException.class, Exception.class} )
public class RsRailwayServiceImpl implements RsRailwayService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private TransactionTemplate transactionTemplate;

    @Override
    public List<PageData> queryAll(PageData pageData) throws Exception {

        List<PageData> list = (List<PageData>) dao.findForList("RsRailwayMapper.queryRailwayList", pageData);

        return list;
    }

    @Override
    public void create(PageData pageData) throws Exception {

        String Id = IdUtil.simpleUUID();
        PageData railway = new PageData();
        railway.put("id", Id);
        railway.put("isDelete", 0);
        railway.put("name", pageData.getString("name")); //昵称
        railway.put("startPosition", pageData.get("startPosition"));
        railway.put("endPosition", pageData.get("endPosition"));
        railway.put("totalLength", pageData.get("totalLength"));
        railway.put("createTime", DateUtils.getTime());
        railway.put("updateTime", DateUtils.getTime());
        dao.save("RsRailwayMapper.addRailwayInfo", railway);



    }

    @Override
    public void update(PageData pageData) throws Exception {
        PageData railway = new PageData();
        railway.put("id", pageData.getString("id"));
        railway.put("name", pageData.getString("name")); //昵称
        railway.put("startPosition", pageData.get("startPosition"));
        railway.put("endPosition", pageData.get("endPosition"));
        railway.put("totalLength", pageData.get("totalLength"));
        railway.put("updateTime", DateUtils.getTime());
        dao.update("RsRailwayMapper.updateRailwayInfo", railway);




    }

    @Override
    public void delete(String[] ids) throws Exception {
        if (ids.length == 0) {
            throw new Exception("删除数据有无唯一条件");
        }
        //删除 学校表
        List<String> list = Arrays.asList(ids);
        dao.delete("RsRailwayMapper.delRailway", list);

    }

    @Override
    public List<PageData> queryAllRailwayList(PageData pageData) throws Exception {
        List<PageData> list = (List<PageData>) dao.findForList("RsRailwayMapper.queryAllRailwayList", pageData);

        return list;
    }

    @Override
    public List<PageData> querySubLevenList(PageData pageData) throws Exception {
        List<PageData> list = (List<PageData>) dao.findForList("RsRailwayMapper.querySubLevenList", pageData);

        return list;
    }

    @Override
    public List<PageData> getSchoolNum(PageData pageData) throws Exception {
        List<PageData> list = (List<PageData>) dao.findForList("RsRailwayMapper.getCheck", pageData);

        return list;
    }

}
