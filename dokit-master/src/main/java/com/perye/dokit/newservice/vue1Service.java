package com.perye.dokit.newservice;



import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface vue1Service {


    List<PageData> getList(PageData pageData) throws Exception;

    void add(PageData pageData) throws Exception;

    List<PageData> getUserList(PageData pageData) throws Exception;

    List<PageData> findUser(PageData pageData) throws Exception;

    List<PageData> findRole(PageData pageData) throws Exception;

    List<PageData> findMenu(PageData pageData) throws Exception;

    List<PageData> findRoleMenu(PageData pageData) throws Exception;

    List<PageData> findAllMenu(PageData pageData) throws Exception;
}
