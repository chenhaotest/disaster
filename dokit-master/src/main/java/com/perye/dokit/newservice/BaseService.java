package com.perye.dokit.newservice;



import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface BaseService {


    List<PageData> getListBase(PageData pageData) throws Exception;

    Integer create(PageData pageData) throws Exception;

    void deleteAll(String[] ids) throws Exception;

    void update(PageData pageData) throws Exception;
}
