package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface RsRailwayService {

    /**
     * 分页查询
     *
     * @param pageData 条件
     * @return /
     */
    List<PageData> queryAll(PageData pageData) throws Exception;


    /**
     * 创建
     *
     * @param pageData /
     * @return /
     */
    void create(PageData pageData) throws Exception;

    /**
     * 编辑
     *
     * @param pageData /
     */
    void update(PageData pageData) throws Exception;

    /**
     * 删除
     *
     * @param ids /
     */
    void delete(String[] ids) throws Exception;


    /**
     * 查询铁路id和名称
     *
     * @param pageData 条件
     * @return /
     */
    List<PageData> queryAllRailwayList(PageData pageData) throws Exception;

    /**
     * 根据铁路id查询铁路下的隧道id和name
     *
     * @param pageData 条件
     * @return /
     */
    List<PageData> querySubLevenList(PageData pageData) throws Exception;

    List<PageData> getSchoolNum(PageData pageData) throws Exception;
}
