package com.perye.dokit.newservice;



import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface HqWeddingTypesService {


    List<PageData> getList(PageData pageData) throws Exception;

    Integer create(PageData pageData) throws Exception;

    void deleteAll(String[] ids) throws Exception;

    void update(PageData pageData) throws Exception;

    List<PageData> getWeddingTypes(PageData pageData) throws Exception;
}
