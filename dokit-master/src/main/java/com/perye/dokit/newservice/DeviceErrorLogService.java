package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;
import java.util.Map;

public interface DeviceErrorLogService {

    //插入气体灭火设备管理机日志
    void insertGasFireExtinguishingLog(List<PageData> list) throws Exception;

    //插入温感设备日志
    void insertWarmthSensationLog(List<PageData> list) throws Exception;

    //插入烟感设备日志
    void insertSmokeDetectorLog(List<PageData> list) throws Exception;

    //插入图像型火灾探测器设备日志
    void insertImageTypeFireDetectorLog(List<PageData> list) throws Exception;

    //插入人体识别传感器设备日志
    void insertHumanBodyRecognitionSensorLog(List<PageData> list) throws Exception;

    //插入语音提示器设备日志
    void insertVoiceReminderLog(List<PageData> list) throws Exception;

    //插入悬挂式气体灭火装置设备日志
    void insertSuspendedFireExtinguishingLog(List<PageData> list) throws Exception;

    //插入手动按钮设备日志
    void insertManualButtonLog(List<PageData> list) throws Exception;

    //插入声光报警器设备日志
    void insertAudibleVisualAlarmLog(List<PageData> list) throws Exception;

    //插入放气指示灯设备日志
    void insertBleedIndicatorLightLog(List<PageData> list) throws Exception;

    //插入放气指示灯设备日志
    void insertImg(List<PageData> list) throws Exception;

    //查询设备id，通过设备编码
    void updateDeviceList(List<PageData> list) throws Exception;

    //根据管理机编码查询管理机信息
    PageData queryExtinguishingInfo(PageData pageData) throws Exception;

    //查询所有的设备信息
    Map<String, PageData> queryDeviceMap(PageData pageData) throws Exception;

    //插入预警等级日志
    void insertWarningLevelLog(List<PageData> list) throws Exception;
    //前段  3d列表 获取异常硬件数据
    Map<String, PageData> queryDeviceErrorMap(PageData pageData) throws Exception;
    //前段  3d  获取实时统计
    PageData findEchartsTatistics(PageData pageData) throws Exception;
    // 硬件设备统计  报警，离线，故障，正常
    PageData findRealTimeAlarmEquipment(PageData pageData) throws Exception;
}
