package com.perye.dokit.newservice.iml;



import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.BaseService;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class BaseServiceImpl implements BaseService {


    @Resource(name = "daoSupport")
    private DaoSupport dao;

    /**
     *  批量查询
     * @param pageData
     * @return
     */
    @Override
    public List<PageData> getListBase(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("BaseMapper.getListBase", pageData);
    }

    @Override
    public Integer create(PageData pageData) throws Exception {
      dao.save("BaseMapper.insert",pageData);
        int id = Integer.parseInt(pageData.get("id") + "");
        return id;
    }

    @Override
    public void deleteAll(String[] ids) throws Exception {
        dao.delete("BaseMapper.deleteAll",ids);
    }

    @Override
    public void update(PageData pageData) throws Exception {
        dao.update("BaseMapper.updateByPrimaryKeySelective",pageData);
    }
}
