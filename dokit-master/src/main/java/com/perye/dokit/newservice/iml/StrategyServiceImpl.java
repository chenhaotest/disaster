package com.perye.dokit.newservice.iml;

import cn.hutool.core.collection.CollectionUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.StrategyService;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class StrategyServiceImpl implements StrategyService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Override
    public PageData queryStrategyList(PageData pageData) throws Exception {
        PageData result = new PageData();
        Map<String, List> map = new HashMap<String, List>();
        List<PageData> strategyList = (List<PageData>) dao.findForList("StrategyMapper.queryStrategyList", pageData);
        if (CollectionUtil.isNotEmpty(strategyList)) {
            for (PageData strategy : strategyList) {

                List<PageData> list = map.get(strategy.getString("deviceCode"));
                if (CollectionUtil.isEmpty(list)) {
                    list = new ArrayList<PageData>();
                }

                PageData info = new PageData();
                info.put("grade", strategy.getString("grade"));
                info.put("alarmNum", strategy.getString("num"));
                list.add(info);

                map.put(strategy.getString("deviceCode"), list);
            }

            if (CollectionUtil.isNotEmpty(map)) {
                for (String key : map.keySet()) {
                    List<PageData> value = map.get(key);
                    result.put(key, value);
                }
            }
        }
        return result;
    }
}
