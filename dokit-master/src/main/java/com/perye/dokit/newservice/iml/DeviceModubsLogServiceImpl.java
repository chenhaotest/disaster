package com.perye.dokit.newservice.iml;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.DeviceManagService;
import com.perye.dokit.newservice.DeviceModubsLogService;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.RedisUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DeviceModubsLogServiceImpl implements DeviceModubsLogService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Override
    public void addModubsLog(PageData modubsLog) throws Exception {
        dao.save("FpModubsConnectLogMapper.insert", modubsLog);
    }
}
