package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface RsTunnelService {

    /**
     * 分页查询
     *
     * @param pageData 条件
     * @return /
     */
    List<PageData> queryAll(PageData pageData) throws Exception;


    /**
     * 创建
     *
     * @param pageData /
     * @return /
     */
    void create(PageData pageData) throws Exception;

    /**
     * 编辑
     *
     * @param pageData /
     */
    void update(PageData pageData) throws Exception;

    /**
     * 删除
     *
     * @param ids /
     */
    void delete(String[] ids) throws Exception;


    /**
     * 获取list
     * @param pageData
     * @return
     */
    List<PageData> getTunnelByPcList(PageData pageData) throws Exception;
}
