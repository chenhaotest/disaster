package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface RsCommonService {

    List<PageData> queryAllRailwayList(PageData pageData) throws Exception;

    List<PageData> getTunnelByPcList(PageData pageData) throws Exception;

    List<PageData> getExchangeBoardList(PageData pageData) throws Exception;

    List<PageData> getDeviceMachineList(PageData pageData) throws Exception;

    List<PageData> getSubsystemList(PageData pageData) throws Exception;
}
