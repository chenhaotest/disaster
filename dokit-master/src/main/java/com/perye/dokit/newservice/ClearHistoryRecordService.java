package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface ClearHistoryRecordService {

    List<String> queryImmerHistory(PageData pageData) throws Exception;

    void delImmerHistory(List<String> list) throws Exception;

    List<String> querySensorHistory(PageData pageData) throws Exception;

    void delSensorHistory(List<String> list) throws Exception;
}
