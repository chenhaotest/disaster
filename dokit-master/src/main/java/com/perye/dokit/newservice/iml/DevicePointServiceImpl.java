package com.perye.dokit.newservice.iml;

import cn.hutool.core.collection.CollectionUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.DeviceErrorLogService;
import com.perye.dokit.newservice.DevicePointService;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DevicePointServiceImpl implements DevicePointService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;


    @Override
    public List<PageData> findPoints(PageData pd) throws Exception {
        List<PageData>  list = (List<PageData>) dao.findForList("FpDeviceMapper.selectDevicePoint", pd);
        return list;
    }


    @Override
    public void updatePointList(List devideList) throws Exception {
        dao.save("FpDevicePointMapper.updatePointList", devideList);
    }

    @Override
    public List<PageData> findMaxPoints(PageData pd) throws Exception {
        List<PageData>  list = (List<PageData>) dao.findForList("FpDeviceMapper.findMaxPoints", pd);
        return list;
    }
}
