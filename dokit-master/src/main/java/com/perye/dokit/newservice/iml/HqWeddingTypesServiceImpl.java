package com.perye.dokit.newservice.iml;



import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.HqWeddingTypesService;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
@Transactional
public class HqWeddingTypesServiceImpl implements HqWeddingTypesService {


    @Resource(name = "daoSupport")
    private DaoSupport dao;

    /**
     *  批量查询
     * @param pageData
     * @return
     */
    @Override
    public List<PageData> getList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("HqWeddingTypesMapper.getList", pageData);
    }

    @Override
    public Integer create(PageData pageData) throws Exception {
      dao.save("HqWeddingTypesMapper.insert",pageData);
        int id = Integer.parseInt(pageData.get("id") + "");
        return id;
    }

    @Override
    public void deleteAll(String[] ids) throws Exception {
        dao.delete("HqWeddingTypesMapper.deleteAll",ids);
    }

    @Override
    public void update(PageData pageData) throws Exception {
        dao.update("HqWeddingTypesMapper.updateByPrimaryKeySelective",pageData);
    }

    @Override
    public List<PageData> getWeddingTypes(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("HqWeddingTypesMapper.getWeddingTypes", pageData);
    }
}
