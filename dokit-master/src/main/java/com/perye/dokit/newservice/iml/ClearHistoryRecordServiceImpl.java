package com.perye.dokit.newservice.iml;

import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.ClearHistoryRecordService;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ClearHistoryRecordServiceImpl implements ClearHistoryRecordService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Override
    public List<String> queryImmerHistory(PageData pageData) throws Exception {

        List<String> queryImmerHistory = (List<String>) dao.findForList("ClearHistoryRecordMapper.queryImmerHistory", pageData);

        return queryImmerHistory;
    }

    @Override
    public void delImmerHistory(List<String> list) throws Exception {
        dao.delete("ClearHistoryRecordMapper.delImmerHistory", list);
    }

    @Override
    public List<String> querySensorHistory(PageData pageData) throws Exception {

        List<String> querySensorHistory = (List<String>) dao.findForList("ClearHistoryRecordMapper.querySensorHistory", pageData);

        return querySensorHistory;
    }

    @Override
    public void delSensorHistory(List<String> list) throws Exception {
        dao.delete("ClearHistoryRecordMapper.delSensorHistory", list);
    }
}
