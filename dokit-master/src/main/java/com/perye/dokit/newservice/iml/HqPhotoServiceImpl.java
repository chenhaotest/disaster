package com.perye.dokit.newservice.iml;


import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.HqPhotoService;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class HqPhotoServiceImpl implements HqPhotoService {


    @Resource(name = "daoSupport")
    private DaoSupport dao;


    @Override
    public Integer create(PageData pageData) throws Exception {
        dao.save("HqPhotoMapper.insert", pageData);

        int id = Integer.parseInt(pageData.get("id") + "");
        return id;
    }

    @Override
    public PageData findPhoto(PageData imgData) throws Exception {
        PageData result= (PageData)dao.findForObject("HqPhotoMapper.findPhoto", imgData);
        return result;
    }

    @Override
    public void update(PageData pageData) throws Exception {
        dao.update("HqPhotoMapper.updateByPrimaryKeySelective", pageData);
    }

    @Override
    public void deleteAll(String[] ids) throws Exception {
        dao.delete("HqPhotoMapper.deleteAll",ids);
    }

    @Override
    public void addPictureList(List<Map<String, Object>> list) throws Exception {
        dao.save("HqPhotoMapper.addPictureList",list);
    }

    @Override
    public void deletePicture(String[] ids) throws Exception {
        dao.delete("HqPhotoMapper.deletePicture",ids);
    }

    @Override
    public void upPictureList(List<Map<String, Object>> list) throws Exception {
        dao.update("HqPhotoMapper.upPictureList", list);
    }



}
