package com.perye.dokit.newservice.iml;



import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.PersionService;
import com.perye.dokit.utils.pageData.PageData;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class PersionServiceImpl implements PersionService {


    @Resource(name = "daoSupport")
    private DaoSupport dao;

    /**
     *  批量查询
     * @param pageData
     * @return
     */
    @Override
    public List<PageData> getList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("HqPersionMapper.getList", pageData);
    }

    @Override
    public Integer create(PageData pageData) throws Exception {
      dao.save("HqPersionMapper.insert",pageData);
        int id = Integer.parseInt(pageData.get("id") + "");
        return id;
    }

    @Override
    public void deleteAll(String[] ids) throws Exception {
        dao.delete("HqPersionMapper.deleteAll",ids);
    }

    @Override
    public void update(PageData pageData) throws Exception {
        dao.update("HqPersionMapper.updateByPrimaryKeySelective",pageData);
    }

    @Override
    public void addMiaoShuList(@Param(value = "list") List<Map<String, Object>> list) throws Exception {
        dao.save("HqMiaoshuMapper.addMiaoShuList",list);
}

    @Override
    public void deleteMiaoShu(PageData pageData) throws Exception {
        dao.update("HqMiaoshuMapper.updateMiaoShu",pageData);
    }

    @Override
    public void upDateMiaoShuList(List<Map<String, Object>> newmsList) throws Exception {
        dao.update("HqMiaoshuMapper.upDateMiaoShuList",newmsList);

    }
}
