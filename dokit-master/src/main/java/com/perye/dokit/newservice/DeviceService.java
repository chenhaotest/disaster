package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface DeviceService {

    List<PageData> getAll(PageData pageData) throws Exception;

    List<PageData> findDevicelist(PageData pDDevice) throws Exception;

    void addDevice(PageData pDDevice) throws Exception;

    void addDeviceList(List<PageData> deviceAddList) throws Exception;

    void add(PageData pageData) throws Exception;

    void update(PageData pageData) throws Exception;

    void pointAdd(PageData pageData) throws Exception;

    void pointEdit(PageData pageData) throws Exception;

    void pointDelete(PageData pageData) throws Exception;

    void delete(PageData pageData) throws Exception;

    PageData selectDeviceInfo(PageData info) throws Exception;

    void addModubsPointsLog(List devideList) throws Exception;

    List<PageData> selectByDevicePoints(PageData info) throws Exception;

    List<PageData> selectDevicelistInfo(PageData info) throws Exception;

    PageData changeModubsVal(PageData pageData) throws Exception;
}
