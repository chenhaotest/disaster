package com.perye.dokit.newservice;

import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * @Description //TODO
 * @Date 2023/7/27 13:55
 * @Author chenhao
 **/
public interface MqttThemeService {

    /**
     * \* Created with IntelliJ IDEA.
     * \* User: chenhao
     * \* Date: 2023/7/27
     * \* Time: 13:58
     * \* Description: mqtt 硬件数据解析
     * \
     */



    void toMqttDataTopic(String topic, MqttMessage message) throws Exception;
}
