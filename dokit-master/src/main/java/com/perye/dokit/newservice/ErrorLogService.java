package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface ErrorLogService {

    List<PageData> getAll(PageData pageData) throws Exception;

}
