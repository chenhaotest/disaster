package com.perye.dokit.newservice.iml;

import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.newservice.RsCommonService;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.utils.RedisUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ReCommonServiceImpl implements RsCommonService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;


    @Override
    public List<PageData> queryAllRailwayList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("RsCommonMapper.queryAllRailwayList", pageData);
    }

    @Override
    public List<PageData> getTunnelByPcList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("RsCommonMapper.querySubLevenList", pageData);
    }

    @Override
    public List<PageData> getExchangeBoardList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("RsCommonMapper.getExchangeBoardList", pageData);
    }

    @Override
    public List<PageData> getDeviceMachineList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("RsCommonMapper.getDeviceMachineList", pageData);
    }

    @Override
    public List<PageData> getSubsystemList(PageData pageData) throws Exception {
        return (List<PageData>) dao.findForList("RsCommonMapper.getSubsystemList", pageData);
    }
}
