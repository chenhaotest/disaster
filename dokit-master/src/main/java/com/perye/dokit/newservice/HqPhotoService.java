package com.perye.dokit.newservice;



import com.perye.dokit.utils.pageData.PageData;

import java.util.List;
import java.util.Map;

public interface HqPhotoService {

    Integer create(PageData pageData) throws Exception;


    PageData findPhoto(PageData imgData) throws Exception;

    void update(PageData imgData) throws Exception;

    void deleteAll(String[] ids) throws Exception;

    void addPictureList(List<Map<String, Object>> newAddmsList) throws Exception;

    void deletePicture(String[] ids) throws Exception;

    void upPictureList(List<Map<String, Object>> newPuppdateList) throws Exception;


}
