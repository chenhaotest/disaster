package com.perye.dokit.newservice.iml;

import cn.hutool.core.collection.CollectionUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.DeviceErrorLogService;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DeviceErrorLogServiceImpl implements DeviceErrorLogService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private DictDetailService dictDetailService;

    @Override
    public void insertGasFireExtinguishingLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertGasFireExtinguishingLog", list);
    }

    @Override
    public void insertWarmthSensationLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertWarmthSensationLog", list);
    }

    @Override
    public void insertSmokeDetectorLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertSmokeDetectorLog", list);
    }

    @Override
    public void insertImageTypeFireDetectorLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertImageTypeFireDetectorLog", list);
    }

    @Override
    public void insertHumanBodyRecognitionSensorLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertHumanBodyRecognitionSensorLog", list);
    }

    @Override
    public void insertVoiceReminderLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertVoiceReminderLog", list);
    }

    @Override
    public void insertSuspendedFireExtinguishingLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertSuspendedFireExtinguishingLog", list);
    }

    @Override
    public void insertManualButtonLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertManualButtonLog", list);
    }

    @Override
    public void insertAudibleVisualAlarmLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertAudibleVisualAlarmLog", list);
    }

    @Override
    public void insertBleedIndicatorLightLog(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertBleedIndicatorLightLog", list);
    }

    @Override
    public void insertImg(List<PageData> list) throws Exception {
        dao.save("DeviceErrorLogMapper.insertImg", list);
    }

    @Override
    public void updateDeviceList(List<PageData> list) throws Exception {
        dao.update("DeviceErrorLogMapper.updateDeviceList", list);
    }

    @Override
    public PageData queryExtinguishingInfo(PageData pageData) throws Exception {
        return (PageData) dao.findForObject("DeviceErrorLogMapper.queryExtinguishingInfo", pageData);
    }

    @Override
    public Map<String, PageData> queryDeviceMap(PageData pageData) throws Exception {
        Map<String, PageData> deviceMap = new HashMap<String, PageData>();
        int num = 0;
        List<PageData> deviceList = (List<PageData>) dao.findForList("DeviceErrorLogMapper.queryDeviceList", pageData);
        if (CollectionUtil.isNotEmpty(deviceList)) {
            Map<String, String> stateMap = dictDetailService.queryDictDetailByName("device_state", "map");
            Map<String, String> gfMap = dictDetailService.queryDictDetailByName("gf_is_abnormal", "map");
            Map<String, String> avMap = dictDetailService.queryDictDetailByName("av_is_abnormal", "map");
            Map<String, String> biMap = dictDetailService.queryDictDetailByName("bi_is_abnormal", "map");
            Map<String, String> hbMap = dictDetailService.queryDictDetailByName("hb_is_abnormal", "map");
            Map<String, String> imgMap = dictDetailService.queryDictDetailByName("img_is_abnormal", "map");
            Map<String, String> mbMap = dictDetailService.queryDictDetailByName("mb_is_abnormal", "map");
            Map<String, String> sdMap = dictDetailService.queryDictDetailByName("sd_is_abnormal", "map");
            Map<String, String> sfMap = dictDetailService.queryDictDetailByName("sf_is_abnormal", "map");
            Map<String, String> vrMap = dictDetailService.queryDictDetailByName("vr_is_abnormal", "map");
            Map<String, String> wsMap = dictDetailService.queryDictDetailByName("ws_is_abnormal", "map");
            for (PageData data : deviceList) {
                String deviceState = data.getString("deviceState");
                if (StringUtils.isNotBlank(deviceState)) {
                    if (!"0".equals(deviceState)) {
                        num++;
                    }
                    data.put("deviceStateNum",deviceState);
                    data.put("deviceState", stateMap.get(deviceState));
                }
                String deviceRole = data.getString("deviceRole");
                String isAbnormal = data.getString("isAbnormal");
                if (StringUtils.isNotBlank(isAbnormal)) {
                    if ("0".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", gfMap.get(isAbnormal));
                        }
                    } else if ("1".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", wsMap.get(isAbnormal));
                        }
                    } else if ("2".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", sdMap.get(isAbnormal));
                        }
                    } else if ("3".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", imgMap.get(isAbnormal));
                        }
                    } else if ("4".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", hbMap.get(isAbnormal));
                        }
                    } else if ("5".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", vrMap.get(isAbnormal));
                        }
                    } else if ("6".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", sfMap.get(isAbnormal));
                        }
                    } else if ("7".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", mbMap.get(isAbnormal));
                        }
                    } else if ("8".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", avMap.get(isAbnormal));
                        }
                    } else if ("9".equals(deviceRole)) {
                        // data.put("isAbnormal", "");
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", biMap.get(isAbnormal));
                        }
                    }
                }
                deviceMap.put(data.getString("deviceCode"), data);
            }
        }
        PageData info = new PageData();
        info.put("num", num + "");
        deviceMap.put("errorNum", info);
        return deviceMap;
    }

    @Override
    public void insertWarningLevelLog(List<PageData> list) throws Exception {
        dao.update("DeviceErrorLogMapper.insertWarningLevelLog", list);
    }

    @Override
    public Map<String, PageData> queryDeviceErrorMap(PageData pageData) throws Exception {
        Map<String, PageData> deviceMap = new HashMap<String, PageData>();
        List<PageData> deviceList = (List<PageData>) dao.findForList("DeviceErrorLogMapper.queryDeviceErrorMap", pageData);
        if (CollectionUtil.isNotEmpty(deviceList)) {
            Map<String, String> stateMap = dictDetailService.queryDictDetailByName("device_state", "map");
            Map<String, String> gfMap = dictDetailService.queryDictDetailByName("gf_is_abnormal", "map");
            Map<String, String> avMap = dictDetailService.queryDictDetailByName("av_is_abnormal", "map");
            Map<String, String> biMap = dictDetailService.queryDictDetailByName("bi_is_abnormal", "map");
            Map<String, String> hbMap = dictDetailService.queryDictDetailByName("hb_is_abnormal", "map");
            Map<String, String> imgMap = dictDetailService.queryDictDetailByName("img_is_abnormal", "map");
            Map<String, String> mbMap = dictDetailService.queryDictDetailByName("mb_is_abnormal", "map");
            Map<String, String> sdMap = dictDetailService.queryDictDetailByName("sd_is_abnormal", "map");
            Map<String, String> sfMap = dictDetailService.queryDictDetailByName("sf_is_abnormal", "map");
            Map<String, String> vrMap = dictDetailService.queryDictDetailByName("vr_is_abnormal", "map");
            Map<String, String> wsMap = dictDetailService.queryDictDetailByName("ws_is_abnormal", "map");
            for (PageData data : deviceList) {
                String deviceState = data.getString("deviceState");
                if (StringUtils.isNotBlank(deviceState)) {
                    data.put("deviceStateNum",deviceState);
                    data.put("deviceState", stateMap.get(deviceState));
                }
                String deviceRole = data.getString("deviceRole");
                String isAbnormal = data.getString("isAbnormal");
                if (StringUtils.isNotBlank(isAbnormal)) {
                    if ("0".equals(deviceRole)) {

                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", gfMap.get(isAbnormal));
                        }
                    } else if ("1".equals(deviceRole)) {

                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", wsMap.get(isAbnormal));
                        }
                    } else if ("2".equals(deviceRole)) {

                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", sdMap.get(isAbnormal));
                        }
                    } else if ("3".equals(deviceRole)) {

                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", imgMap.get(isAbnormal));
                        }
                    } else if ("4".equals(deviceRole)) {

                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", hbMap.get(isAbnormal));
                        }
                    } else if ("5".equals(deviceRole)) {

                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", vrMap.get(isAbnormal));
                        }
                    } else if ("6".equals(deviceRole)) {

                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", sfMap.get(isAbnormal));
                        }
                    } else if ("7".equals(deviceRole)) {
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", mbMap.get(isAbnormal));
                        }
                    } else if ("8".equals(deviceRole)) {
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", avMap.get(isAbnormal));
                        }
                    } else if ("9".equals(deviceRole)) {
                        if (StringUtils.isNotBlank(isAbnormal)) {
                            data.put("isAbnormalStr", biMap.get(isAbnormal));
                        }
                    }
                }
                deviceMap.put(data.getString("deviceCode"), data);
            }
        }
        return deviceMap;
    }
    @Override
    public PageData findEchartsTatistics(PageData pageData) throws Exception {
        PageData   d1=  (PageData) dao.findForObject("DeviceErrorLogMapper.findEchartsTatistics", pageData);//查询监控天数
        PageData   d2=  (PageData) dao.findForObject("DeviceErrorLogMapper.findDeviceTotal", pageData);//查询设备总数量
        PageData   d3=  (PageData) dao.findForObject("DeviceErrorLogMapper.findDeviceErrorLevelTotal", pageData);//查询报警总次数
        PageData   d4=  (PageData) dao.findForObject("DeviceErrorLogMapper.findDeviceOfflineTotal", pageData);//查询离线总数
        PageData   d5=  (PageData) dao.findForObject("DeviceErrorLogMapper.findTunnelTotal", pageData);//查询隧道数量
        d1.put("deviceTotal",d2.get("deviceTotal"));
        d1.put("deviceErrorLevelTotal",d3.get("deviceErrorLevelTotal"));
        d1.put("deviceOfflineTotal",d4.get("deviceOfflineTotal"));
        d1.put("tunnelTotal",d5.get("tunnelTotal"));
        return d1;
    }

    @Override
    public PageData findRealTimeAlarmEquipment(PageData pageData) throws Exception {
        return (PageData) dao.findForObject("DeviceErrorLogMapper.findRealTimeAlarmEquipment", pageData);
    }
}
