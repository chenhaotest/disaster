package com.perye.dokit.newservice;



import com.perye.dokit.utils.pageData.PageData;

public interface HqVideoService {

    Integer create(PageData pageData) throws Exception;


    PageData findVideo(PageData imgData) throws Exception;

    void update(PageData imgData) throws Exception;

    void deleteAll(String[] ids) throws Exception;

    void deleteVideo(String[] ids) throws Exception;
}
