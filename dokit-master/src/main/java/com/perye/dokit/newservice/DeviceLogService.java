package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

import java.util.List;

public interface DeviceLogService {

    //插入气体灭火设备管理机日志
    void insertGasFireExtinguishingLog(List<PageData> list) throws Exception;

    //插入温感设备日志
    void insertWarmthSensationLog(List<PageData> list) throws Exception;

    //插入烟感设备日志
    void insertSmokeDetectorLog(List<PageData> list) throws Exception;

    //插入图像型火灾探测器设备日志
    void insertImageTypeFireDetectorLog(List<PageData> list) throws Exception;

    //插入人体识别传感器设备日志
    void insertHumanBodyRecognitionSensorLog(List<PageData> list) throws Exception;

    //插入语音提示器设备日志
    void insertVoiceReminderLog(List<PageData> list) throws Exception;

    //插入悬挂式气体灭火装置设备日志
    void insertSuspendedFireExtinguishingLog(List<PageData> list) throws Exception;

    //插入手动按钮设备日志
    void insertManualButtonLog(List<PageData> list) throws Exception;

    //插入声光报警器设备日志
    void insertAudibleVisualAlarmLog(List<PageData> list) throws Exception;

    //插入放气指示灯设备日志
    void insertBleedIndicatorLightLog(List<PageData> list) throws Exception;
    //插入放气指示灯设备日志
    void insertImg(List<PageData> list) throws Exception;

}
