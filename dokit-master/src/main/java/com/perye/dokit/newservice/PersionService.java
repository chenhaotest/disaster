package com.perye.dokit.newservice;



import com.perye.dokit.utils.pageData.PageData;

import java.util.List;
import java.util.Map;

public interface PersionService {


    List<PageData> getList(PageData pageData) throws Exception;

    Integer create(PageData pageData) throws Exception;

    void deleteAll(String[] ids) throws Exception;

    void update(PageData pageData) throws Exception;

    void addMiaoShuList( List<Map<String, Object>> msList) throws Exception;

    void deleteMiaoShu(PageData pageData) throws Exception;

    void upDateMiaoShuList(List<Map<String, Object>> newmsList) throws Exception;
}
