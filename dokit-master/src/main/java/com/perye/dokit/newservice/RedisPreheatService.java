package com.perye.dokit.newservice;

import com.perye.dokit.utils.pageData.PageData;

public interface RedisPreheatService {

    PageData resettingRedis() throws Exception;
}
