package com.perye.dokit.newservice.iml;

import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.DeviceLogService;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class DeviceLogServiceImpl implements DeviceLogService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Override
    public void insertGasFireExtinguishingLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertGasFireExtinguishingLog", list);
    }

    @Override
    public void insertWarmthSensationLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertWarmthSensationLog", list);
    }

    @Override
    public void insertSmokeDetectorLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertSmokeDetectorLog", list);
    }

    @Override
    public void insertImageTypeFireDetectorLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertImageTypeFireDetectorLog", list);
    }

    @Override
    public void insertHumanBodyRecognitionSensorLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertHumanBodyRecognitionSensorLog", list);
    }

    @Override
    public void insertVoiceReminderLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertVoiceReminderLog", list);
    }

    @Override
    public void insertSuspendedFireExtinguishingLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertSuspendedFireExtinguishingLog", list);
    }

    @Override
    public void insertManualButtonLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertManualButtonLog", list);
    }

    @Override
    public void insertAudibleVisualAlarmLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertAudibleVisualAlarmLog", list);
    }

    @Override
    public void insertBleedIndicatorLightLog(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertBleedIndicatorLightLog", list);
    }

    @Override
    public void insertImg(List<PageData> list) throws Exception {
        dao.save("DeviceLogMapper.insertImg", list);
    }
}
