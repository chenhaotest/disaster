package com.perye.dokit.newservice.iml;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.RsTunnelService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional( rollbackFor = {RuntimeException.class, Exception.class} )
public class RsTunnelServiceImpl implements RsTunnelService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public List<PageData> queryAll(PageData pageData) throws Exception {

        List<PageData> list = (List<PageData>) dao.findForList("RsTunnelMapper.queryTunnelList", pageData);

        return list;
    }

    @Override
    public void create(PageData pageData) throws Exception {




        String Id = IdUtil.simpleUUID();
        PageData tunnel = new PageData();
        tunnel.put("id", Id);
        tunnel.put("isDelete", 0);
        tunnel.put("name", pageData.getString("name")); //昵称
        tunnel.put("railwayId", pageData.getString("railwayId")); //昵称
        tunnel.put("startPosition", pageData.get("startPosition"));
        tunnel.put("endPosition", pageData.get("endPosition"));
        tunnel.put("totalLength", pageData.get("totalLength"));
        tunnel.put("createTime", DateUtils.getTime());
        tunnel.put("updateTime", DateUtils.getTime());
        dao.save("RsTunnelMapper.addTunnelInfo", tunnel);





    }

    @Override
    public void update(PageData pageData) throws Exception {



        PageData railway = new PageData();

        railway.put("railwayId", pageData.getString("railwayId"));
        railway.put("id", pageData.getString("id"));
        railway.put("name", pageData.getString("name")); //昵称
        railway.put("startPosition", pageData.get("startPosition"));
        railway.put("endPosition", pageData.get("endPosition"));
        railway.put("totalLength", pageData.get("totalLength"));
        railway.put("updateTime", DateUtils.getTime());
        dao.update("RsTunnelMapper.updateTunnelInfo", railway);



    }

    @Override
    public void delete(String[] ids) throws Exception {

       /* List<String> list = Arrays.asList(ids);
        dao.update("RsTunnelMapper.delTunnel", list);
*/

        if (ids.length == 0) {
            throw new Exception("删除数据有无唯一条件");
        }
        //删除 学校表
        List<String> list = Arrays.asList(ids);
        dao.delete("RsTunnelMapper.delTunnel", list);



    }

    @Override
    public List<PageData> getTunnelByPcList(PageData pageData) throws Exception {

        List<PageData> list = (List<PageData>) dao.findForList("RsTunnelMapper.getTunnelByPcList", pageData);
        return list;
    }
}
