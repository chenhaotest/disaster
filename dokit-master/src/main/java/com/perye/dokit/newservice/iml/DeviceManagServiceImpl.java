package com.perye.dokit.newservice.iml;

import cn.hutool.core.util.IdUtil;
import com.perye.dokit.mapper.dao.DaoSupport;
import com.perye.dokit.newservice.DeviceManagService;
import com.perye.dokit.newservice.DeviceModubsLogService;
import com.perye.dokit.service.DictDetailService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.RedisUtils;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.workcontroller.ExecutePro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

@Service
public class DeviceManagServiceImpl implements DeviceManagService {

    @Resource(name = "daoSupport")
    private DaoSupport dao;

    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private DictDetailService dictDetailService;


    @Autowired
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;




    @Autowired
    private DeviceModubsLogService deviceModubsLogService;



    private ScheduledFuture<?> future;

    private Map<String, ScheduledFuture<?>> ScheduledFutureMap=new HashMap<>();

    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    @Override
    public List<PageData> getAll(PageData pageData) throws Exception {
        List<PageData> deviceList = (List<PageData>) dao.findForList("FpDeviceManagMapper.queryDeviceList", pageData);
        return deviceList;
    }
    @Override
    public void add(PageData pageData) throws Exception {

        String Id = IdUtil.simpleUUID();
        pageData.put("id", Id);
        pageData.put("isDelete", 0);
        pageData.put("deviceAcquisitionDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("deviceAcquisitionDate")+""));
        pageData.put("mStartDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("mStartDate")+""));
        pageData.put("mEndDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("mEndDate")+""));
        pageData.put("deviceWarrantyDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("deviceWarrantyDate")+""));
        pageData.put("createTime", DateUtils.getTime());
        pageData.put("updateTime", DateUtils.getTime());
        dao.save("FpDeviceManagMapper.insert", pageData);
    }

    @Override
    public void update(PageData pageData) throws Exception {
        pageData.put("deviceAcquisitionDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("deviceAcquisitionDate")+""));
        pageData.put("mStartDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("mStartDate")+""));
        pageData.put("mEndDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("mEndDate")+""));
        pageData.put("deviceWarrantyDate", DateUtils.dateTime(DateUtils.YYYY_MM_DD,pageData.get("deviceWarrantyDate")+""));
        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpDeviceManagMapper.update", pageData);
    }


    @Override
    public void delete(PageData pageData) throws Exception {
        pageData.put("updateTime", DateUtils.getTime());
        dao.update("FpDeviceManagMapper.delete", pageData);
    }

    @Override
    public void updateDeviceManag(PageData pageData) throws Exception {


        dao.update("FpDeviceManagMapper.update", pageData);



    }

    @Override
    public void updateDeviceManagStart(PageData pageData) throws Exception {

        String id=pageData.getString("deviceIp");
        Integer port=Integer.parseInt(pageData.get("devicePort")+"");
        //每小时过一分执行策略 整点是采集数据的
        future = threadPoolTaskScheduler.schedule(new ExecutePro(id,port,pageData.getString("id")), new CronTrigger("0 0/1 * * * ? "));
        ScheduledFutureMap.put(id+":"+port,future);

        PageData pd=  new PageData();
        pd.put("id",pageData.getString("id"));
        pd.put("deviceConnectState","1");
        pageData.put("updateTime", DateUtils.getTime());

        dao.update("FpDeviceManagMapper.update", pd);

        //点击执行日志
        toAddModubsLog(pageData,1,"开启连接");//1：开启连接，0：未连接，2：连接失败
    }

    @Override
    public void updateDeviceManagStop(PageData pageData) throws Exception {
        String id=pageData.getString("deviceIp");
        Integer port=Integer.parseInt(pageData.get("devicePort")+"");
        //每小时过一分执行策略 整点是采集数据的
        //future=pageData.getString("pageData");
        future=ScheduledFutureMap.get(id+":"+port);
        if (future != null) {
            future.cancel(true);
        }

        PageData pd=  new PageData();
        pd.put("id",pageData.getString("id"));
        pd.put("deviceConnectState","0");
        pageData.put("updateTime", DateUtils.getTime());

        dao.update("FpDeviceManagMapper.update", pd);
        //点击执行日志
        toAddModubsLog(pageData,0,"关闭连接");//1：开启连接，0：未连接，2：连接失败
    }

    public  void  toAddModubsLog(PageData pageData,int state,String msg) throws Exception {


        String Id = IdUtil.simpleUUID();
        PageData modubsLog= new  PageData();
        modubsLog.put("id", Id);
        modubsLog.put("isDelete", 0);
        modubsLog.put("createTime", DateUtils.getTime());
        modubsLog.put("updateTime", DateUtils.getTime());
        modubsLog.put("tunnelId", pageData.getString("tunnelId"));
        modubsLog.put("railwayId", pageData.getString("railwayId"));
        modubsLog.put("deviceId", pageData.getString("id"));
        modubsLog.put("deviceIp", pageData.getString("deviceIp"));
        modubsLog.put("devicePort", pageData.getString("devicePort"));
        modubsLog.put("state",  state);
        modubsLog.put("msg", msg);
        deviceModubsLogService.addModubsLog(modubsLog);

    }
    /**
     *  chenhao
     *  获取当前
     * @param pageData
     * @return
     */

    @Override
    public List<PageData> findDevicelist(PageData pageData) throws Exception {
        List<PageData> deviceList = (List<PageData>) dao.findForList("DeviceMapper.findDevicelist", pageData);
        return deviceList;
    }

    @Override
    public void addDevice(PageData pDDevice) throws Exception {
        dao.save("DeviceMapper.addDevice", pDDevice);
    }

    @Override
    public void addDeviceList(List<PageData> deviceAddList) throws Exception {
        dao.save("DeviceMapper.addDeviceList", deviceAddList);
    }


}
