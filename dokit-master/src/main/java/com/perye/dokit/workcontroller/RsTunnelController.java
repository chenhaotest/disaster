package com.perye.dokit.workcontroller;

import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;

import com.perye.dokit.newservice.RsRailwayService;
import com.perye.dokit.newservice.RsTunnelService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "隧道管理")
@RestController
@RequestMapping("/api/tunnel")
public class RsTunnelController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(RsTunnelController.class);

    private final RsTunnelService rsTunnelService;
    private final RsRailwayService rsRailwayService;

    public RsTunnelController(RsTunnelService rsTunnelService, RsRailwayService rsRailwayService) {
        this.rsTunnelService = rsTunnelService;
        this.rsRailwayService = rsRailwayService;
    }


    @Log("查询隧道信息")
    @ApiOperation("查询隧道信息")
    @GetMapping
    @PreAuthorize("@dokit.check('tunnel:list')")
    public ResponseEntity<Object> getDicts() {
        PageData result = new PageData();
        PageData pageData = getPageData();

        try {
            ArrayAssembly.getPage(pageData);
            List<PageData> labelList = rsTunnelService.queryAll(pageData);
            result = ArrayAssembly.getDataToObject(labelList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 查询所有铁路信息
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/queryAllRailwayList")
    @ResponseBody
    public ResponseEntity<Object> queryAllRailwayList() throws Exception {
        PageData result = new PageData();
        try {
            PageData pageData = getPageData();
            List<PageData> list = rsRailwayService.queryAllRailwayList(pageData);

            result.put("code", "200");
            result.put("msg", "查询成功");
            result.put("data", list);
        } catch (Exception e) {
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 查询铁路下的隧道信息
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/querySubLevenList")
    @ResponseBody
    public ResponseEntity<Object> querySubLevenList(@Validated @RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = rsRailwayService.querySubLevenList(pageData);

            result.put("code", "200");
            result.put("msg", "查询成功");
            result.put("data", list);
        } catch (Exception e) {
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Log("新增隧道信息")
    @ApiOperation("新增隧道信息")
    @PostMapping
    @PreAuthorize("@dokit.check('tunnel:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PageData pageData) throws Exception {
        rsTunnelService.create(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);


    }

    @Log("修改隧道信息")
    @ApiOperation("修改隧道信息")
    @PutMapping
    @PreAuthorize("@dokit.check('tunnel:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PageData pageData) throws Exception {
        rsTunnelService.update(pageData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除隧道信息")
    @ApiOperation("删除隧道信息")
    @DeleteMapping
    @PreAuthorize("@dokit.check('tunnel:del')")
    public ResponseEntity<Object> delete(@RequestBody String[] ids) throws Exception {
        rsTunnelService.delete(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    /*
     *
     * 硬件交互指令
     *
     * @param pageData "execute":"是否执行消音操作：0:消音（设备暂停发出声响），1:停止灭火"
     * @return
     */

    @RequestMapping(value = "/getTunnelByPcList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public PageData getTunnelByPcList(@RequestBody PageData pageData) {
        PageData result = new PageData();
        try {
            List<PageData> list = rsTunnelService.getTunnelByPcList(pageData);
            result.put("code", "200");
            result.put("msg", "查询成功");
            result.put("data", list);
        } catch (Exception e) {
            result.put("msg", "发送失败");
            result.put("code", "201");
            logger.error(e.getMessage());
        }
        return result;
    }


}
