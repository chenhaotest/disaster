package com.perye.dokit.workcontroller;



import javax.xml.transform.Result;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;


import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.perye.dokit.newservice.DeviceManagService;
import com.perye.dokit.newservice.DeviceModubsLogService;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.ScheduledFuture;


@Api(tags = "开启定时modubs")
@RestController
@RequestMapping("/api/taskModubs")
public class TaskController {
    private static Logger logger = LoggerFactory.getLogger(TaskController.class);


    @Autowired
    private DeviceManagService deviceManagService;

    @PostMapping("/start")
    public ResponseEntity<Object> start(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {


         //   deviceManagService.updateDeviceManag(pageData);
            deviceManagService.updateDeviceManagStart(pageData);

            logger.info("策略已经启动");
            result.put("code", "200");
            result.put("msg", "查询成功");
        } catch (Exception e) {

            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    @PostMapping("/stop")
    public ResponseEntity<Object> stop(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {


           // deviceManagService.updateDeviceManag(pd);
            deviceManagService.updateDeviceManagStop(pageData);
            //点击执行日志
          //  toAddModubsLog(pageData,0,"关闭连接");//1：开启连接，0：未连接，2：连接失败

            logger.info("策略已经停止");
            result.put("code", "200");
            result.put("msg", "查询成功");
        } catch (Exception e) {

            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }





}
