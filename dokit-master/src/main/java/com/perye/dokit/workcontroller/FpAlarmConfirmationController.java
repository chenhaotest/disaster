package com.perye.dokit.workcontroller;

import cn.hutool.json.JSONArray;
import com.alibaba.fastjson.JSON;

import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpAlarmConfirmationService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "问题任务")
@RestController
@RequestMapping("/api/fpAlarmConfirmation")
public class FpAlarmConfirmationController  extends BaseController {
    @Autowired
    public FpAlarmConfirmationService fpAlarmConfirmationService;
    private static Logger logger = LoggerFactory.getLogger(FpAlarmConfirmationController.class);

    @Log("查询问题任务")
    @ApiOperation("查询问题任务")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpAlarmConfirmation:list')")
    public ResponseEntity<Object> getFpAlarmConfirmations() {
        PageData result = new PageData();
        PageData pageData = getPageData();
        try {

            ArrayAssembly.getPage(pageData);
            List<PageData> list = fpAlarmConfirmationService.getAll(pageData);
            String ss = JSONObject.toJSONString(list);

            System.out.println(ss);


            result = ArrayAssembly.getDataToObject(list);



        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }




/*
    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpAlarmConfirmation:list')")
    public void download(HttpServletResponse response, FpAlarmConfirmationQueryCriteria criteria) throws IOException {
        fpAlarmConfirmationService.download(fpAlarmConfirmationService.queryAll(criteria), response);
    }

    @Log("查询alarm_confirmation")
    @ApiOperation("查询alarm_confirmation")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpAlarmConfirmation:list')")
    public ResponseEntity<Object> getFpAlarmConfirmations(FpAlarmConfirmationQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(fpAlarmConfirmationService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @Log("新增alarm_confirmation")
    @ApiOperation("新增alarm_confirmation")
    @PostMapping
    @PreAuthorize("@dokit.check('fpAlarmConfirmation:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody FpAlarmConfirmation resources){
        return new ResponseEntity<>(fpAlarmConfirmationService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改alarm_confirmation")
    @ApiOperation("修改alarm_confirmation")
    @PutMapping
    @PreAuthorize("@dokit.check('fpAlarmConfirmation:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody FpAlarmConfirmation resources){
    fpAlarmConfirmationService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除alarm_confirmation")
    @ApiOperation("删除alarm_confirmation")
    @PreAuthorize("@dokit.check('fpAlarmConfirmation:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
    fpAlarmConfirmationService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }*/

}
