package com.perye.dokit.workcontroller;

import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.newservice.RsCommonService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * api常用工具
 */
@RestController
@RequestMapping("/api/common")
public class RsCommonController extends BaseController {


    private final RsCommonService rsCommonService;

    public RsCommonController(RsCommonService rsCommonService) {
        this.rsCommonService = rsCommonService;
    }


    @PostMapping("/queryAllRailwayList")
    public ResponseEntity<Object> queryAllRailwayList(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = rsCommonService.queryAllRailwayList(pageData);
            result.put("data", list);
            result.put("code", "200");
            result.put("msg", "查询成功");
        } catch (Exception e) {

            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/getTunnelByPcList")
    public ResponseEntity<Object> getTunnelByPcList(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = rsCommonService.getTunnelByPcList(pageData);
            result.put("data", list);
            result.put("code", "200");
            result.put("msg", "查询成功");
        } catch (Exception e) {

            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    /**
     * 查询 所有交换机
     * @param pageData
     * @return
     * @throws Exception
     */
    @PostMapping("/getExchangeBoardList")
    public ResponseEntity<Object> getExchangeBoardList(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = rsCommonService.getExchangeBoardList(pageData);
            result.put("data", list);
            result.put("code", "200");
            result.put("msg", "查询成功");
        } catch (Exception e) {

            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    /**
     * 查询 所有管理机设备
     * @param pageData
     * @return
     * @throws Exception
     */
    @PostMapping("/getDeviceMachineList")
    public ResponseEntity<Object> getDeviceMachineList(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = rsCommonService.getDeviceMachineList(pageData);
            result.put("data", list);
            result.put("code", "200");
            result.put("msg", "查询成功");
        } catch (Exception e) {

            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    /**
     * 查询 所有子系统
     * @param pageData
     * @return
     * @throws Exception
     */
    @PostMapping("/getSubsystemList")
    public ResponseEntity<Object> getSubsystemList(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = rsCommonService.getSubsystemList(pageData);
            result.put("data", list);
            result.put("code", "200");
            result.put("msg", "查询成功");
        } catch (Exception e) {

            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


}
