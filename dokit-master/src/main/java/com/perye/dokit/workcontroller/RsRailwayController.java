package com.perye.dokit.workcontroller;

import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;

import com.perye.dokit.newservice.RsRailwayService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "铁路管理")
@RestController
@RequestMapping("/api/railway")
public class RsRailwayController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(RsRailwayController.class);

    private final RsRailwayService rsRailwayService;

    public RsRailwayController(RsRailwayService rsRailwayService) {
        this.rsRailwayService = rsRailwayService;
    }


    @Log("查询铁路信息")
    @ApiOperation("查询铁路信息")
    @GetMapping
    @PreAuthorize("@dokit.check('railway:list')")
    public ResponseEntity<Object> getDicts() {
        PageData result = new PageData();
        PageData pageData = getPageData();

        try {
            ArrayAssembly.getPage(pageData);
            List<PageData> labelList = rsRailwayService.queryAll(pageData);
            result = ArrayAssembly.getDataToObject(labelList);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Log("新增铁路信息")
    @ApiOperation("新增铁路信息")
    @PostMapping
    @PreAuthorize("@dokit.check('railway:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PageData pageData) throws Exception {
        rsRailwayService.create(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改铁路信息")
    @ApiOperation("修改铁路信息")
    @PutMapping
    @PreAuthorize("@dokit.check('railway:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PageData pageData) throws Exception {
        rsRailwayService.update(pageData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    @Log("删除铁路信息")
    @ApiOperation("删除铁路信息")
    @DeleteMapping
    @PreAuthorize("@dokit.check('railway:del')")
    public ResponseEntity<Object> delete(@RequestBody String[] ids) throws Exception {
        rsRailwayService.delete(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/getSchoolNum")
    public ResponseEntity<Object> getSchoolNum(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = rsRailwayService.getSchoolNum(pageData);


            if (StringUtils.equals("1", pageData.getString("state")) && list.size() == 0) {
                result.put("code", "200");
                result.put("msg", "此数据不存在");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
            if (StringUtils.equals("1", pageData.getString("state")) && list.size() > 0) {
                result.put("code", "201");
                result.put("msg", "已有此信息，请重新填写");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }

            if (list.size() > 1) {
                result.put("code", "201");
                result.put("msg", "存在多条此信息，请查询");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }

            if (StringUtils.equals("0", pageData.getString("state")) && list.size() == 0) {
                result.put("code", "200");
                result.put("msg", "此数据不存在");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }

            if (StringUtils.equals("0", pageData.getString("state")) && StringUtils.equals(pageData.getString("dataId"), list.get(0).getString("id"))) {
                result.put("code", "200");
                result.put("msg", "此数据不存在");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
            if (StringUtils.equals("0", pageData.getString("state")) && !StringUtils.equals(pageData.getString("dataId"), list.get(0).getString("id"))) {
                result.put("code", "201");
                result.put("msg", "已有此信息，请重新填写");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
            result.put("content", list);
            result.put("status", 0);
            result.put("msg", "查询成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
