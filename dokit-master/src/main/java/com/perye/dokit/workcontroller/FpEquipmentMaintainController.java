package com.perye.dokit.workcontroller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpEquipmentMaintainService;
import com.perye.dokit.service.FpEquipmentService;
import com.perye.dokit.service.FpRepairService;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Api(tags = "fpEquipmentMaintain管理")
@RestController
@RequestMapping("/api/fpEquipmentMaintain")
public class FpEquipmentMaintainController extends BaseController {

    private final FpEquipmentMaintainService fpEquipmentMaintainService;

    public FpEquipmentMaintainController(FpEquipmentMaintainService fpEquipmentMaintainService) {
        this.fpEquipmentMaintainService = fpEquipmentMaintainService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpEquipmentMaintain:list')")
    public void download(HttpServletResponse response, PageData criteria) throws IOException {
        /*fpEquipmentMaintainService.download(fpEquipmentMaintainService.queryAll(criteria), response);*/
    }

    @Log("查询fpEquipmentMaintain")
    @ApiOperation("查询fpEquipmentMaintain")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpEquipmentMaintain:list')")
    public ResponseEntity<Object> getfpEquipmentMaintains() throws Exception {
        PageData pageData = getPageData();

        int page = Integer.parseInt(pageData.getString("page")) + 1;
        PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

        List<PageData> resourceList = fpEquipmentMaintainService.queryAll(pageData);

        PageInfo<PageData> pageInfo = new PageInfo<PageData>(resourceList);
        int count = (int) pageInfo.getTotal();

        PageData pageDataList = new PageData();
        pageDataList.put("content", resourceList);
        pageDataList.put("totalElements", count);

        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增fpEquipmentMaintain")
    @ApiOperation("新增fpEquipmentMaintain")
    @PostMapping
    @PreAuthorize("@dokit.check('fpEquipmentMaintain:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PageData pageData) throws Exception {
        fpEquipmentMaintainService.create(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改fpEquipmentMaintain")
    @ApiOperation("修改fpEquipmentMaintain")
    @PutMapping
    @PreAuthorize("@dokit.check('fpEquipmentMaintain:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PageData pageData) throws Exception {
        fpEquipmentMaintainService.update(pageData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除fpEquipmentMaintain")
    @ApiOperation("删除fpEquipmentMaintain")
    @PreAuthorize("@dokit.check('fpEquipmentMaintain:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) throws Exception {
        fpEquipmentMaintainService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
