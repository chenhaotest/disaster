package com.perye.dokit.workcontroller;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.modbus.modbusTcp.Locator;
import com.perye.dokit.modbus.modbusTcp.Modbus4jUtils;
import com.perye.dokit.newservice.DeviceErrorLogService;
import com.perye.dokit.newservice.DeviceModubsLogService;
import com.perye.dokit.newservice.DevicePointService;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.SpringContextUtils;
import com.perye.dokit.utils.pageData.PageData;
import com.serotonin.modbus4j.BatchRead;
import com.serotonin.modbus4j.BatchResults;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.locator.BaseLocator;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.beans.beancontext.BeanContext;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecutePro implements Runnable  {

    private String modebustcpIp;

    private Integer port;
    private String deviceId;

    private static DeviceModubsLogService deviceModubsLogService= SpringContextUtils.getApplicationContext().getBean(DeviceModubsLogService.class);;

    private static DeviceService deviceService= SpringContextUtils.getApplicationContext().getBean(DeviceService.class);;

    private static DevicePointService devicePointService= SpringContextUtils.getApplicationContext().getBean(DevicePointService.class);;


    public ExecutePro(String modebustcpIp, Integer port,String id) {
        this.modebustcpIp = modebustcpIp;
        this.port = port;
        this.deviceId = id;
    }

    @SneakyThrows
    @Override
    public void run() {
        ModbusMaster master = Modbus4jUtils.getMaster(modebustcpIp, port);

        try {



            PageData pd= new  PageData();
            pd.put("mmId",deviceId);
            List<PageData> devPoints =     devicePointService.findMaxPoints(pd);
            Integer val=Integer.parseInt(  devPoints.get(0).get("register_address")+"");
            List<PageData> pointMaxList =  getPointMaxList(val);


            for (PageData pointMaxData : pointMaxList) {
                int start=(Integer) pointMaxData.get("start");
                int end=(Integer) pointMaxData.get("end");

                BatchRead<Integer> batch = new BatchRead<>();

                for (int i = start; i <= end; i++) {
                    Locator locator= new Locator();
                    locator.setId(i);
                    locator.setSlaveId(1);
                    locator.setOffset(i);
                    batch.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(), DataType.TWO_BYTE_INT_SIGNED));
                }
                BatchResults<Integer> batchResults = Modbus4jUtils.batchRead(master, batch);
                toSavePoints(batchResults,start,end);

                System.out.println("ip="+modebustcpIp+"port="+port+"="+batchResults);



            }




        } catch (ModbusTransportException e) {
            e.printStackTrace();
            toAddModubsLog(e.getMessage());
        } catch (ErrorResponseException e) {
            e.printStackTrace();
            toAddModubsLog(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            toAddModubsLog(e.getMessage());
        }
    }


    /**
     *  MODUBS  传参数 必须是连续数字，并且 测试 最多传120 ， 这里获取醉倒偏移量值， 以100划分
     * @param num
     * @return
     */

    public List<PageData>  getPointMaxList(int num){



        List list = new ArrayList<>();
        int start = 0;
        int end = 0;
        for (int i = 0; i < num; i = i + 100) {
            PageData data = new PageData();
            start = i;
            end = i + 99;
            if (end > num) {
                end = num;
            }
            data.put("start", start);
            data.put("end", end);
            list.add(data);
            // System.out.println("statr="+start+"end="+end);
        }
        return list;
    }

    public void toSavePoints(BatchResults<Integer> batchResults,Integer start,Integer end) throws Exception {



        List  devicePointsList=new ArrayList<>();
        List  devideList=new ArrayList<>();
        List <PageData>  listPoints=  getAlldevicePointsInfo();

        for (PageData points: listPoints) {

            Integer registerAddress=(Integer)points.get("registerAddress");
            Short  registerAddressVal= (Short)batchResults.getValue(registerAddress);
            if(registerAddressVal!=null){
                String Id = IdUtil.simpleUUID();
                PageData modubsPoints= new  PageData();
                modubsPoints.put("id", Id);
                modubsPoints.put("isDelete", 0);
                modubsPoints.put("createTime", DateUtils.getTime());
                modubsPoints.put("updateTime", DateUtils.getTime());
                modubsPoints.put("tunnelId", points.get("tunnelId"));
                modubsPoints.put("railwayId", points.get("railwayId"));
                modubsPoints.put("pointId", points.get("id") );//点位id
                modubsPoints.put("deviceId",  points.get("deviceId"));//设备id
                modubsPoints.put("deviceModubsId",  deviceId);//设备管理机id
                modubsPoints.put("deviceIp", modebustcpIp);
                modubsPoints.put("devicePort", port);
                modubsPoints.put("registerAddress", registerAddress );
                modubsPoints.put("registerAddressVal", registerAddressVal);
                devideList.add(modubsPoints);


                PageData pointInfo= new  PageData();
                pointInfo.put("id", points.get("id") );//点位id
                pointInfo.put("registerAddressVal", registerAddressVal);//点位id
                devicePointsList.add(pointInfo);
            }


        }


        deviceService.addModubsPointsLog(devideList);//更新点位 日志表
        devicePointService.updatePointList(devicePointsList);//更新点位 偏移量值
        System.out.println("4444");


    }

    public    List <PageData>  getAlldevicePointsInfo() throws Exception {
        PageData info=  new PageData();
        info.put("mmId",deviceId);
        List <PageData>  pageData=  deviceService.selectByDevicePoints(info);
        return pageData;
    }

   public PageData  deviceInfo() throws Exception {
       PageData info=  new PageData();
       info.put("deviceId",deviceId);
       PageData  pageData=  deviceService.selectDeviceInfo(info);
       return pageData;
   }



    public  void  toAddModubsLog(String msg) throws Exception {
        PageData  pageData=  deviceInfo();
        if(pageData==null){
            msg="根据该设备的唯一标识查不到该信息";
        }

        String Id = IdUtil.simpleUUID();
        PageData modubsLog= new  PageData();
        modubsLog.put("id", Id);
        modubsLog.put("isDelete", 0);
        modubsLog.put("createTime", DateUtils.getTime());
        modubsLog.put("updateTime", DateUtils.getTime());
        modubsLog.put("tunnelId", pageData.get("tunnelId"));
        modubsLog.put("railwayId", pageData.get("railwayId"));
        modubsLog.put("deviceId", deviceId);
        modubsLog.put("deviceIp", modebustcpIp);
        modubsLog.put("devicePort", port);
        modubsLog.put("state",  2);
        modubsLog.put("msg", msg);
        deviceModubsLogService.addModubsLog(modubsLog);

    }
}
