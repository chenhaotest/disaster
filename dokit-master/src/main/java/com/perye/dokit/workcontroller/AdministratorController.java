package com.perye.dokit.workcontroller;

import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.AdministratorService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api(tags = "学校管理员")
@RestController
@RequestMapping("/api/administrator")
public class AdministratorController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(AdministratorController.class);
    private final AdministratorService administratorService;

    public AdministratorController(AdministratorService administratorService) {
        this.administratorService = administratorService;
    }


    @PostMapping("/getTunnelNum")
    public ResponseEntity<Object> getSchoolNum(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = administratorService.getTunnelNum(pageData);


            if (StringUtils.equals("1", pageData.getString("state")) && list.size() == 0) {
                result.put("code", "200");
                result.put("msg", "此数据不存在");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
            if (StringUtils.equals("1", pageData.getString("state")) && list.size() > 0) {
                result.put("code", "201");
                result.put("msg", "已有此信息，请重新填写");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }

            if (list.size() > 1) {
                result.put("code", "201");
                result.put("msg", "存在多条此信息，请查询");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }

            if (StringUtils.equals("0", pageData.getString("state")) && list.size() == 0) {
                result.put("code", "200");
                result.put("msg", "此数据不存在");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }

            if (StringUtils.equals("0", pageData.getString("state")) && StringUtils.equals(pageData.getString("dataId"), list.get(0).getString("id"))) {
                result.put("code", "200");
                result.put("msg", "此数据不存在");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
            if (StringUtils.equals("0", pageData.getString("state")) && !StringUtils.equals(pageData.getString("dataId"), list.get(0).getString("id"))) {
                result.put("code", "201");
                result.put("msg", "已有此信息，请重新填写");
                return new ResponseEntity<>(result, HttpStatus.OK);
            }
            result.put("content", list);
            result.put("status", 0);
            result.put("msg", "查询成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/getSchoolList")
    public ResponseEntity<Object> getSchoolList(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            List<PageData> list = administratorService.getSchoolList(pageData);
            result.put("data", list);
            result.put("code", "200");
            result.put("msg", "查询成功");
        } catch (Exception e) {
            logger.error(e.getMessage());
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }





    /**
     * 查询隧道下 所属人员
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/queryAllTunnelAdministratorList")
    @ResponseBody
    public ResponseEntity<Object> queryAllRailwayAdministratorList(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {

            List<PageData> list = administratorService.queryAllTunnelAdministratorList(pageData);

            result.put("code", "200");
            result.put("msg", "查询成功");
            result.put("data", list);
        } catch (Exception e) {
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }




    @Log("查询学校管理员")
    @ApiOperation("查询学校管理员")
    @GetMapping()
    @PreAuthorize("@dokit.check('administrator:list')")
    public ResponseEntity<Object> getWfSchools() {
        PageData result = new PageData();
        PageData pageData = getPageData();
        try {

            ArrayAssembly.getPage(pageData);
            List<PageData> list = administratorService.getAll(pageData);
            result = ArrayAssembly.getDataToObject(list);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Log("新增学校管理员")
    @ApiOperation("新增学校管理员")
    @PostMapping
    @PreAuthorize("@dokit.check('administrator:add')")
    public ResponseEntity<Object> create(@RequestBody PageData pageData) throws Exception {
        administratorService.add(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改学校管理员")
    @ApiOperation("修改学校管理员")
    @PutMapping
    @PreAuthorize("@dokit.check('administrator:edit')")
    public ResponseEntity<Object> update(@RequestBody PageData pageData) throws Exception {
        administratorService.update(pageData);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除学校管理员")
    @ApiOperation("删除学校管理员")
    @PreAuthorize("@dokit.check('administrator:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) throws Exception {
        PageData result = new PageData();
         administratorService.deleteAll(ids);
        return new ResponseEntity<>(result, HttpStatus.NO_CONTENT);
    }





}
