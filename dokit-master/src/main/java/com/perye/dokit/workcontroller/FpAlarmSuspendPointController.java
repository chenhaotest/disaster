package com.perye.dokit.workcontroller;


import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpAlarmSuspendPointService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "设备告警暂停处理")
@RestController
@RequestMapping("/api/fpAlarmSuspendPoint")
public class FpAlarmSuspendPointController extends BaseController {

    private final FpAlarmSuspendPointService fpAlarmSuspendPointService;
    private static Logger logger = LoggerFactory.getLogger(FpAlarmSuspendPointController.class);
    public FpAlarmSuspendPointController(FpAlarmSuspendPointService fpAlarmSuspendPointService) {
        this.fpAlarmSuspendPointService = fpAlarmSuspendPointService;
    }


    @Log("设备告警暂停处理")
    @ApiOperation("设备告警暂停处理")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:list')")
    public ResponseEntity<Object> getFpAlarmSuspendPoints() {
        PageData result = new PageData();
        PageData pageData = getPageData();
        try {

            ArrayAssembly.getPage(pageData);
            List<PageData> list = fpAlarmSuspendPointService.getAll(pageData);
            String ss = JSONObject.toJSONString(list);

            System.out.println(ss);


            result = ArrayAssembly.getDataToObject(list);



        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }



    @Log("新增设备告警暂停处理")
    @ApiOperation("新增设备告警暂停处理")
    @PostMapping
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:add')")
    public ResponseEntity<Object> create(@RequestBody PageData pageData) throws Exception {
        fpAlarmSuspendPointService.add(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改设备告警暂停处理")
    @ApiOperation("修改设备告警暂停处理")
    @PutMapping
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:edit')")

    public ResponseEntity<Object> update(@RequestBody PageData pageData) throws Exception {
        fpAlarmSuspendPointService.update(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @Log("删除设备告警暂停处理")
    @ApiOperation("删除设备告警暂停处理")
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) throws Exception {
        fpAlarmSuspendPointService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }


/*    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:list')")
    public void download(HttpServletResponse response, FpAlarmSuspendPointQueryCriteria criteria) throws IOException {
        fpAlarmSuspendPointService.download(fpAlarmSuspendPointService.queryAll(criteria), response);
    }

    @Log("查询alarmSuspendPoint")
    @ApiOperation("查询alarmSuspendPoint")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:list')")
    public ResponseEntity<Object> getFpAlarmSuspendPoints(FpAlarmSuspendPointQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(fpAlarmSuspendPointService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @Log("新增alarmSuspendPoint")
    @ApiOperation("新增alarmSuspendPoint")
    @PostMapping
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody FpAlarmSuspendPoint resources){
        return new ResponseEntity<>(fpAlarmSuspendPointService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改alarmSuspendPoint")
    @ApiOperation("修改alarmSuspendPoint")
    @PutMapping
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody FpAlarmSuspendPoint resources){
    fpAlarmSuspendPointService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除alarmSuspendPoint")
    @ApiOperation("删除alarmSuspendPoint")
    @PreAuthorize("@dokit.check('fpAlarmSuspendPoint:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
    fpAlarmSuspendPointService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }*/

}
