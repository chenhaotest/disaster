package com.perye.dokit.workcontroller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpRepairService;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "fpRepair管理")
@RestController
@RequestMapping("/api/fpRepair")
public class FpRepairController extends BaseController {

    private final FpRepairService fpRepairService;

    public FpRepairController(FpRepairService fpRepairService) {
        this.fpRepairService = fpRepairService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpRepair:list')")
    public void download(HttpServletResponse response, PageData criteria) throws IOException {
        /*fpRepairService.download(fpRepairService.queryAll(criteria), response);*/
    }

    @Log("查询fpRepair")
    @ApiOperation("查询fpRepair")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpRepair:list')")
    public ResponseEntity<Object> getFpRepairs() throws Exception {
        PageData pageData = getPageData();

        int page = Integer.parseInt(pageData.getString("page")) + 1;
        PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

        List<PageData> resourceList = fpRepairService.queryAll(pageData);

        PageInfo<PageData> pageInfo = new PageInfo<PageData>(resourceList);
        int count = (int) pageInfo.getTotal();

        PageData pageDataList = new PageData();
        pageDataList.put("content", resourceList);
        pageDataList.put("totalElements", count);

        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增fpRepair")
    @ApiOperation("新增fpRepair")
    @PostMapping
    @PreAuthorize("@dokit.check('fpRepair:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PageData pageData) throws Exception {
        fpRepairService.create(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改fpRepair")
    @ApiOperation("修改fpRepair")
    @PutMapping
    @PreAuthorize("@dokit.check('fpRepair:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PageData pageData) throws Exception {
        fpRepairService.update(pageData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除fpRepair")
    @ApiOperation("删除fpRepair")
    @PreAuthorize("@dokit.check('fpRepair:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) throws Exception {
        fpRepairService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
