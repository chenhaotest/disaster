package com.perye.dokit.workcontroller;

import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpProblemBaseService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "问题库管理")
@RestController
@RequestMapping("/api/fpProblemBase")
public class FpProblemBaseController extends BaseController {

    private final FpProblemBaseService fpProblemBaseService;
    private static Logger logger = LoggerFactory.getLogger(FpProblemBaseController.class);

    public FpProblemBaseController(FpProblemBaseService fpProblemBaseService) {
        this.fpProblemBaseService = fpProblemBaseService;
    }


    @Log("查询问题库")
    @ApiOperation("查询问题库")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpProblemBase:list')")
    public ResponseEntity<Object> getFpProblemBases() {
        PageData result = new PageData();
        PageData pageData = getPageData();
        try {

            ArrayAssembly.getPage(pageData);
            List<PageData> list = fpProblemBaseService.getAll(pageData);
            String ss = JSONObject.toJSONString(list);

            System.out.println(ss);


            result = ArrayAssembly.getDataToObject(list);


        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @Log("新增问题库")
    @ApiOperation("新增问题库")
    @PostMapping
    @PreAuthorize("@dokit.check('fpProblemBase:add')")
    public ResponseEntity<Object> create(@RequestBody PageData pageData) throws Exception {
        fpProblemBaseService.add(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改问题库")
    @ApiOperation("修改问题库")
    @PutMapping
    @PreAuthorize("@dokit.check('fpProblemBase:edit')")

    public ResponseEntity<Object> update(@RequestBody PageData pageData) throws Exception {
        fpProblemBaseService.update(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @Log("删除问题库")
    @ApiOperation("删除问题库")
    @PreAuthorize("@dokit.check('fpProblemBase:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) throws Exception {
        fpProblemBaseService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * 查询问题
     * @param pageData
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getProblemBase", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public PageData getProblemBase() throws Exception {

        PageData pageData= this.getPageData();

        PageData result = new PageData();
        List<PageData> list =  fpProblemBaseService.getProblemBase(pageData);
        result.put("code", "00");
        result.put("msg", "查询成功");
        result.put("data", list);

        return result;
    }


    /*@Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpProblemBase:list')")
    public void download(HttpServletResponse response, FpProblemBaseQueryCriteria criteria) throws IOException {
        fpProblemBaseService.download(fpProblemBaseService.queryAll(criteria), response);
    }

    @Log("查询problembase")
    @ApiOperation("查询problembase")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpProblemBase:list')")
    public ResponseEntity<Object> getFpProblemBases(FpProblemBaseQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(fpProblemBaseService.queryAll(criteria,pageable),HttpStatus.OK);
    }



    @Log("修改problembase")
    @ApiOperation("修改problembase")
    @PutMapping
    @PreAuthorize("@dokit.check('fpProblemBase:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody FpProblemBase resources){
    fpProblemBaseService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除problembase")
    @ApiOperation("删除problembase")
    @PreAuthorize("@dokit.check('fpProblemBase:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
    fpProblemBaseService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }*/

}
