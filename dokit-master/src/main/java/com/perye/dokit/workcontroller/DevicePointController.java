package com.perye.dokit.workcontroller;

import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "设备管理")
@RestController
@RequestMapping("/api/point")
public class DevicePointController extends BaseController {


    private final DeviceService deviceService;

    public DevicePointController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }



    @Log("新增硬件点位")
    @ApiOperation("新增硬件点位")
    @PostMapping
    @PreAuthorize("@dokit.check('point:add')")
    public ResponseEntity<Object> create(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {

            deviceService.pointAdd(pageData);
            result.put("code", "200");
            result.put("msg", "新增成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }




    @Log("修改硬件点位")
    @ApiOperation("修改硬件点位")
    @PutMapping
    @PreAuthorize("@dokit.check('point:edit')")
    public ResponseEntity<Object> pointEdit(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {

            deviceService.pointEdit(pageData);
            result.put("code", "200");
            result.put("msg", "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Log("删除硬件点位")
    @ApiOperation("删除硬件点位")
    @DeleteMapping
    @PreAuthorize("@dokit.check('point:delete')")
    public ResponseEntity<Object> pointDelete(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {
            deviceService.pointDelete(pageData);
            result.put("code", "200");
            result.put("msg", "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }




}
