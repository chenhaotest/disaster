package com.perye.dokit.workcontroller;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpResourceService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.StringUtils;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.vo.FpResourceVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@Api(tags = "fpResource管理")
@RestController
@RequestMapping("/api/fpResource")
public class FpResourceController extends BaseController {

    private final FpResourceService fpResourceService;

    public FpResourceController(FpResourceService fpResourceService) {
        this.fpResourceService = fpResourceService;
    }

    /*@Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpResource:list')")
    public void download(HttpServletResponse response, FpResourceQueryCriteria criteria) throws IOException {
        fpResourceService.download(fpResourceService.queryAll(criteria), response);
    }*/

    @Log("查询fpResource")
    @ApiOperation("查询fpResource")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpResource:list')")
    public ResponseEntity<Object> getFpResources() throws Exception {
        PageData pageData = getPageData();

        int page = Integer.parseInt(pageData.getString("page")) + 1;
        PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

        List<FpResourceVo> resourceList = fpResourceService.queryAll(pageData);

        PageInfo<FpResourceVo> pageInfo = new PageInfo<FpResourceVo>(resourceList);
        int count = (int) pageInfo.getTotal();

        PageData pageDataList = new PageData();
        pageDataList.put("content", resourceList);
        pageDataList.put("totalElements", count);

        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增fpResource")
    @ApiOperation("新增fpResource")
    @PostMapping
    @PreAuthorize("@dokit.check('fpResource:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PageData pageData) throws Exception {
        Boolean vool = fpResourceService.create(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改fpResource")
    @ApiOperation("修改fpResource")
    @PutMapping
    @PreAuthorize("@dokit.check('fpResource:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PageData pageData) throws Exception {
        fpResourceService.update(pageData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除fpResource")
    @ApiOperation("删除fpResource")
    @PreAuthorize("@dokit.check('fpResource:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) throws Exception {
        fpResourceService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @RequestMapping(value = "/queryResourceList", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public PageData queryResourceList(@Validated @RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();

        String parentId = pageData.getString("parentId");
        if (StringUtils.isBlank(parentId)) {
            result.put("code", "01");
            result.put("msg", "未接收到父级信息");
            result.put("data", null);
            return result;
        }

        List<PageData> queryDropdownBoxList = fpResourceService.queryDropdownBoxList(pageData);

        if (CollectionUtils.isEmpty(queryDropdownBoxList)) {
            queryDropdownBoxList = new ArrayList<PageData>();
        }

        result.put("code", "00");
        result.put("msg", "查询成功");
        result.put("data", queryDropdownBoxList);

        return result;
    }

    @RequestMapping(value = "/testtttt", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public PageData test(@Validated @RequestBody PageData pageData) throws Exception {
        return fpResourceService.queryResourceInfo(pageData);
    }
    @RequestMapping(value = "/queryResourceById", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public PageData queryResourceById(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        List<PageData> list =  fpResourceService.queryResourceById(pageData);
        result.put("code", "00");
        result.put("msg", "查询成功");
        result.put("data", list);

        return result;
    }




}
