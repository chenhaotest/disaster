package com.perye.dokit.workcontroller;

import cn.hutool.core.collection.CollectionUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpDataPermissionsService;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.vo.FpResourceVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FpDataPermissionsController
 * @Description 数据权限管理
 * @Author zd
 * @Date 2024/4/29 13:10
 * @Version 1.0
 **/
@Api(tags = "fpDataPermissions管理")
@RestController
@RequestMapping("/api/fpDataPermissions")
public class FpDataPermissionsController extends BaseController {

    @Autowired
    private FpDataPermissionsService fpDataPermissionsService;

    @Log("查询fpDataPermissions")
    @ApiOperation("查询fpDataPermissions")
    @RequestMapping(value = "/queryDataPermissions", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public ResponseEntity<Object> queryDataPermissions(@RequestBody PageData info) throws Exception {
        PageData pageData = getPageData();
        PageData result = new PageData();

        if (CollectionUtil.isNotEmpty(info) && CollectionUtil.isEmpty(pageData)) {
            pageData = info;
        }

        List<FpResourceVo> resourceList = fpDataPermissionsService.queryAll(pageData);
        List<String> dataPermissions = new ArrayList<>();
        List<PageData> list = fpDataPermissionsService.queryDataPermissions(pageData);
        if (CollectionUtils.isNotEmpty(list)) {
            for (PageData data : list) {
                dataPermissions.add(data.getString("sourceId"));
            }
        }


        if (CollectionUtils.isEmpty(resourceList)) {
            resourceList = new ArrayList<FpResourceVo>();
        }

        result.put("expandedKeys", resourceList);
        result.put("checkedKeys", dataPermissions);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    @Log("编辑fpDataPermissions")
    @ApiOperation("编辑fpDataPermissions")
    @RequestMapping(value = "/editDataPermissions", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public ResponseEntity<Object> editDataPermissions(@RequestBody PageData info) throws Exception {
        PageData pageData = getPageData();
        PageData result = new PageData();

        if (CollectionUtil.isNotEmpty(info) && CollectionUtil.isEmpty(pageData)) {
            pageData = info;
        }

        int num = fpDataPermissionsService.editDataPermissions(pageData);

        result.put("code", "00");
        result.put("msg", "提交成功");

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
