package com.perye.dokit.workcontroller;

import com.perye.dokit.base.BaseController;
import com.perye.dokit.modbus.modbusTcp.Locator;
import com.perye.dokit.modbus.modbusTcp.LocatorConfig;
import com.perye.dokit.modbus.modbusTcp.Modbus4jUtils;
import com.perye.dokit.service.FpDevicePointNewService;
import com.perye.dokit.utils.pageData.PageData;
import com.serotonin.modbus4j.BatchRead;
import com.serotonin.modbus4j.BatchResults;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.locator.BaseLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

//import static com.perye.dokit.modbus.ModbusUtils.writeRegister;


@RestController
@RequestMapping("/api/wfModubsController")
public class WfModubsController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(WfModubsController.class);


    private String modebustcpIp = "192.168.1.205";

    private Integer port = 502;

    @Autowired
    private LocatorConfig locatorConfig;

    @Autowired
    private  FpDevicePointNewService fpDevicePointNewService;

    @PostMapping("/toLampClose")
    public PageData toLampClose() throws Exception {
        PageData result = new PageData();
        try {

            ModbusMaster master = Modbus4jUtils.getMaster(modebustcpIp, port);

            BatchRead<Integer> batch = new BatchRead<>();
            List<Locator> locatorList = locatorConfig.getLocatorList();
            locatorList.forEach(locator -> batch.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(), DataType.TWO_BYTE_INT_SIGNED)));
            short a[] ={0};
//            short b[] ={0};
            Boolean  a1 = Modbus4jUtils.writeRegisters(master,1,200,a);
//            Thread.sleep(500);
//            Boolean  a2 = Modbus4jUtils.writeRegisters(master,1,201,b);
            result.put("status", 0);

            result.put("code", "200");

        } catch (Exception e) {
            logger.error(e.getMessage());
            result.put("msg", "发送失败");
            result.put("code", "201");
        }
        return result;
    }
    @PostMapping("/toLampOpen")
    public PageData toLampOpen() throws Exception {
        PageData result = new PageData();
        try {

            ModbusMaster master = Modbus4jUtils.getMaster(modebustcpIp, port);

            BatchRead<Integer> batch = new BatchRead<>();
            List<Locator> locatorList = locatorConfig.getLocatorList();
            locatorList.forEach(locator -> batch.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(), DataType.TWO_BYTE_INT_SIGNED)));
            short a[] ={1};
//            short b[] ={0};
            Boolean  a1 = Modbus4jUtils.writeRegisters(master,1,200,a);
//            Thread.sleep(500);
//            Boolean  a2 = Modbus4jUtils.writeRegisters(master,1,201,b);
            result.put("status", 0);

            result.put("code", "200");

        } catch (Exception e) {
            logger.error(e.getMessage());
            result.put("msg", "发送失败");
            result.put("code", "201");
        }
        return result;
    }
    /**
     * 查询状态
     * 读取状态
     */
    @PostMapping("/selChangeModbusState")
    public PageData getChangeModbusState() throws ModbusInitException,Exception {
        PageData result = new PageData();
        try {
        PageData pageData = this.getPageData();

        //获取点位表信息
            //1. 先分组查询有多少个设备（每个设备连接的modubs 肯定是一样的）
            List<PageData> pd = fpDevicePointNewService.selectListGroupBy(pageData);
            //根据分组得到的设备id查询设备情况
            for (int e = 0; e <pd.size() ; e++) {
                //设备id
                String equipmentId=pd.get(e).getString("equipmentId");
                //modebusIP
                String modebustcpIp=pd.get(e).getString("networkAddress");
                //modebus 端口号
                Integer port=Integer.parseInt(pd.get(e).getString("ports"));
                // 类型：1写，2读
                //获取每个设备的点位信息 读的信息
                pageData.put("type","2");
                pageData.put("equipmentId",equipmentId);
                List<PageData> dataList = fpDevicePointNewService.fpDevicePointNewList(pageData);
                 PageData data =selDeviceState( modebustcpIp,  port,dataList);
                result.put("data", data);
            }


            result.put("status", 0);
            result.put("msg", "成功");
            result.put("code", "200");

        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * 获取每个设备下的设备状态
     * @param modebustcpIp
     * @param port
     * @param dataList
     * @return
     * @throws ModbusInitException
     * @throws Exception
     */
    public PageData selDeviceState(String modebustcpIp, Integer port,List<PageData> dataList) throws ModbusInitException,Exception {

        PageData result = new PageData();
        try {
                ModbusMaster master = Modbus4jUtils.getMaster(modebustcpIp, port);
                BatchRead<Integer> batch = new BatchRead<>();
                //偏移量
                List<Locator> locatorList = new ArrayList<>();
            for (int i=0;i<dataList.size();i++) {
                Locator ct = new Locator();
                Integer registerAddress = Integer.parseInt(dataList.get(i).getString("registerAddress"));
                ct.setId(registerAddress);
                ct.setSlaveId(1);
                //偏移量
                ct.setOffset(registerAddress);
                locatorList.add(ct);
            }
                locatorList.forEach(locator -> batch.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(), DataType.TWO_BYTE_INT_SIGNED)));
               BatchResults<Integer> batchResults = Modbus4jUtils.batchRead(master, batch);
                logger.info("----------------"+batchResults.toString());

                PageData data = new PageData();
                //漏电回路电源指示灯

//            data.put("leakage", batchResults.getValue(5).toString());
////            data.put("leakage","1");
//
//            //过载回路电源指示灯
//            data.put("overload", batchResults.getValue(6).toString());
//
//
//            //漏电电流
//            float num = Float.parseFloat(batchResults.getValue(7).toString());
//            data.put("lean", num / 10);
//
//            //过载电流
//            data.put("gean", batchResults.getValue(8).toString());
//
//            data.put("type", "e");



                result.put("data", batchResults.toString());
                result.put("status", 0);
                result.put("msg", "成功");
                result.put("code", "200");






            //  }
        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
