package com.perye.dokit.workcontroller;

import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpPointOperationLogService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api(tags = "硬件操作记录")
@RestController
@RequestMapping("/api/fpPointOperationLog")
public class FpPointOperationLogController  extends BaseController {

    private final FpPointOperationLogService fpPointOperationLogService;

    public FpPointOperationLogController(FpPointOperationLogService fpPointOperationLogService) {
        this.fpPointOperationLogService = fpPointOperationLogService;
    }

    private static Logger logger = LoggerFactory.getLogger(FpAlarmConfirmationController.class);

    @Log("查询硬件操作记录")
    @ApiOperation("查询硬件操作记录")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpPointOperationLog:list')")
    public ResponseEntity<Object> getFpPointOperationLogs() {
        PageData result = new PageData();
        PageData pageData = getPageData();
        try {

            ArrayAssembly.getPage(pageData);
            List<PageData> list = fpPointOperationLogService.getAll(pageData);
            String ss = JSONObject.toJSONString(list);

            System.out.println(ss);


            result = ArrayAssembly.getDataToObject(list);



        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


   /* @Log("查询pointOperationLog")
    @ApiOperation("查询pointOperationLog")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpPointOperationLog:list')")
    public ResponseEntity<Object> getFpPointOperationLogs(FpPointOperationLogQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(fpPointOperationLogService.queryAll(criteria,pageable),HttpStatus.OK);
    }
*/
}
