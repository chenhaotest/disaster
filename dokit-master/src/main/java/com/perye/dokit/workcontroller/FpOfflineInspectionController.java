package com.perye.dokit.workcontroller;

import com.alibaba.fastjson.JSONObject;
import com.perye.dokit.aop.log.Log;

import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpOfflineInspectionService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "线下巡检管理")
@RestController
@RequestMapping("/api/fpOfflineInspection")
public class FpOfflineInspectionController extends BaseController {
    private static Logger logger = LoggerFactory.getLogger(FpOfflineInspectionController.class);
    private final FpOfflineInspectionService fpOfflineInspectionService;

    public FpOfflineInspectionController(FpOfflineInspectionService fpOfflineInspectionService) {
        this.fpOfflineInspectionService = fpOfflineInspectionService;
    }


    @Log("查询线下巡检")
    @ApiOperation("查询线下巡检")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpOfflineInspection:list')")
    public ResponseEntity<Object> getFpOfflineInspections() {
        PageData result = new PageData();
        PageData pageData = getPageData();
        try {

            ArrayAssembly.getPage(pageData);
            List<PageData> list = fpOfflineInspectionService.getAll(pageData);
            String ss = JSONObject.toJSONString(list);

            System.out.println(ss);


            result = ArrayAssembly.getDataToObject(list);



        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Log("新增线下巡检")
    @ApiOperation("新增线下巡检")
    @PostMapping
    @PreAuthorize("@dokit.check('fpOfflineInspection:add')")
    public ResponseEntity<Object> create(@RequestBody PageData pageData) throws Exception {
        fpOfflineInspectionService.add(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改线下巡检")
    @ApiOperation("修改线下巡检库")
    @PutMapping
    @PreAuthorize("@dokit.check('fpOfflineInspection:edit')")

    public ResponseEntity<Object> update(@RequestBody PageData pageData) throws Exception {
        fpOfflineInspectionService.update(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


    @Log("删除线下巡检")
    @ApiOperation("删除线下巡检")
    @PreAuthorize("@dokit.check('fpOfflineInspection:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) throws Exception {
        fpOfflineInspectionService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }



    /**
     * 巡检人员新增
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/createInspectionPerson")
    @ResponseBody
    public ResponseEntity<Object> createInspectionPerson(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {

          fpOfflineInspectionService.createInspectionPerson(pageData);

            result.put("code", "200");
            result.put("msg", "成功");

        } catch (Exception e) {
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }


    /**
     * 巡检人员修改
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/updateInspectionPerson")
    @ResponseBody
    public ResponseEntity<Object> updateInspectionPerson(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {

            fpOfflineInspectionService.updateInspectionPerson(pageData);
            result.put("code", "200");
            result.put("msg", "成功");

        } catch (Exception e) {
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    /**
     * 巡检人员问题新增
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/createInspectionProblem")
    @ResponseBody
    public ResponseEntity<Object> createInspectionProblem(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {

            fpOfflineInspectionService.createInspectionProblem(pageData);

            result.put("code", "200");
            result.put("msg", "成功");

        } catch (Exception e) {
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    /**
     * 巡检人员问题修改
     *
     * @return
     * @throws Exception
     */
    @PostMapping("/updateInspectionProblem")
    @ResponseBody
    public ResponseEntity<Object> updateInspectionProblem(@RequestBody PageData pageData) throws Exception {
        PageData result = new PageData();
        try {

            fpOfflineInspectionService.updateInspectionProblem(pageData);

            result.put("code", "200");
            result.put("msg", "成功");

        } catch (Exception e) {
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

  /*  @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpOfflineInspection:list')")
    public void download(HttpServletResponse response, FpOfflineInspectionQueryCriteria criteria) throws IOException {
        fpOfflineInspectionService.download(fpOfflineInspectionService.queryAll(criteria), response);
    }

    @Log("查询线下巡检")
    @ApiOperation("查询线下巡检")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpOfflineInspection:list')")
    public ResponseEntity<Object> getFpOfflineInspections(FpOfflineInspectionQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(fpOfflineInspectionService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @Log("新增线下巡检")
    @ApiOperation("新增线下巡检")
    @PostMapping
    @PreAuthorize("@dokit.check('fpOfflineInspection:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody FpOfflineInspection resources){
        return new ResponseEntity<>(fpOfflineInspectionService.create(resources),HttpStatus.CREATED);
    }

    @Log("修改线下巡检")
    @ApiOperation("修改线下巡检")
    @PutMapping
    @PreAuthorize("@dokit.check('fpOfflineInspection:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody FpOfflineInspection resources){
    fpOfflineInspectionService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除线下巡检")
    @ApiOperation("删除线下巡检")
    @PreAuthorize("@dokit.check('fpOfflineInspection:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) {
    fpOfflineInspectionService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }*/

}
