package com.perye.dokit.workcontroller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.service.FpSparePartsService;
import com.perye.dokit.utils.pageData.PageData;
import com.perye.dokit.vo.FpResourceVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Api(tags = "fpSpareParts管理")
@RestController
@RequestMapping("/api/fpSpareParts")
public class FpSparePartsController extends BaseController {

    private final FpSparePartsService fpSparePartsService;

    public FpSparePartsController(FpSparePartsService fpSparePartsService) {
        this.fpSparePartsService = fpSparePartsService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@dokit.check('fpSpareParts:list')")
    public void download(HttpServletResponse response, PageData pageData) throws Exception {
        fpSparePartsService.download(fpSparePartsService.queryAll(pageData), response);
    }

    @Log("查询fpSpareParts")
    @ApiOperation("查询fpSpareParts")
    @GetMapping()
    @PreAuthorize("@dokit.check('fpSpareParts:list')")
    public ResponseEntity<Object> getFpSparePartss() throws Exception {
        PageData pageData = getPageData();

        int page = Integer.parseInt(pageData.getString("page")) + 1;
        PageHelper.startPage(page, Integer.parseInt(pageData.getString("size")));

        List<PageData> resourceList = fpSparePartsService.queryAll(pageData);

        PageInfo<PageData> pageInfo = new PageInfo<PageData>(resourceList);
        int count = (int) pageInfo.getTotal();

        PageData pageDataList = new PageData();
        pageDataList.put("content", resourceList);
        pageDataList.put("totalElements", count);

        return new ResponseEntity<>(pageDataList, HttpStatus.OK);
    }

    @Log("新增fpSpareParts")
    @ApiOperation("新增fpSpareParts")
    @PostMapping
    @PreAuthorize("@dokit.check('fpSpareParts:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PageData pageData) throws Exception {
        fpSparePartsService.create(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改fpSpareParts")
    @ApiOperation("修改fpSpareParts")
    @PutMapping
    @PreAuthorize("@dokit.check('fpSpareParts:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PageData pageData) throws Exception {
        fpSparePartsService.update(pageData);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除fpSpareParts")
    @ApiOperation("删除fpSpareParts")
    @PreAuthorize("@dokit.check('fpSpareParts:del')")
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody String[] ids) throws Exception {
        fpSparePartsService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
