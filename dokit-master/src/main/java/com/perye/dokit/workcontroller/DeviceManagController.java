package com.perye.dokit.workcontroller;

import com.perye.dokit.aop.log.Log;
import com.perye.dokit.base.BaseController;
import com.perye.dokit.newservice.DeviceManagService;
import com.perye.dokit.newservice.DeviceService;
import com.perye.dokit.utils.ArrayAssembly;
import com.perye.dokit.utils.pageData.PageData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "设备管理")
@RestController
@RequestMapping("/api/deviceManag")
public class DeviceManagController extends BaseController {


    private final DeviceManagService deviceManagService;

    public DeviceManagController(DeviceManagService deviceManagService) {
        this.deviceManagService = deviceManagService;
    }


    @GetMapping()
    @PreAuthorize("@dokit.check('deviceManag:list')")
    public ResponseEntity<Object> getRsFloodlightLog() {
        PageData result = new PageData();
        PageData pageData = getPageData();
        try {
            ArrayAssembly.getPage(pageData);
            List<PageData> list = deviceManagService.getAll(pageData);
            result = ArrayAssembly.getDataToObject(list);

        } catch (Exception e) {

        }
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    @Log("新增硬件")
    @ApiOperation("新增硬件")
    @PostMapping
    @PreAuthorize("@dokit.check('deviceManag:add')")
    public ResponseEntity<Object> create(@RequestBody PageData pageData) throws Exception {
        deviceManagService.add(pageData);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @Log("修改硬件")
    @ApiOperation("修改硬件")
    @PutMapping
    @PreAuthorize("@dokit.check('deviceManag:edit')")
    public ResponseEntity<Object> update(@RequestBody PageData pageData) throws Exception {
        deviceManagService.update(pageData);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除硬件")
    @ApiOperation("删除硬件")
    @DeleteMapping
    @PreAuthorize("@dokit.check('deviceManag:del')")
    public ResponseEntity<Object> del(@RequestBody PageData pageData) throws Exception {

        PageData result = new PageData();
        try {
            deviceManagService.delete(pageData);
            result.put("code", "200");
            result.put("msg", "删除成功");
        } catch (Exception e) {
            e.printStackTrace();
            result.put("code", "201");
            result.put("msg", e.getMessage());
            return new ResponseEntity<>(result, HttpStatus.OK);
        }
        return new ResponseEntity<>(result, HttpStatus.OK);

    }



}
