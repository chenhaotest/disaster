package com.perye.dokit.repository;

import com.perye.dokit.entity.FpDevicePointNew;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpDevicePointNewRepository extends JpaRepository<FpDevicePointNew, String>, JpaSpecificationExecutor<FpDevicePointNew> {
}
