package com.perye.dokit.repository;

import com.perye.dokit.entity.FpFireDrill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpFireDrillRepository extends JpaRepository<FpFireDrill, String>, JpaSpecificationExecutor<FpFireDrill> {
}
