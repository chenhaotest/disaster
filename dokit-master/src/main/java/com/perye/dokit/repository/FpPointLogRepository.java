package com.perye.dokit.repository;

import com.perye.dokit.entity.FpPointLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpPointLogRepository extends JpaRepository<FpPointLog, String>, JpaSpecificationExecutor<FpPointLog> {
}
