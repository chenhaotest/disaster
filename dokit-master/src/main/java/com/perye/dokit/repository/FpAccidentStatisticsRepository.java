package com.perye.dokit.repository;

import com.perye.dokit.entity.FpAccidentStatistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpAccidentStatisticsRepository extends JpaRepository<FpAccidentStatistics, String>, JpaSpecificationExecutor<FpAccidentStatistics> {
}
