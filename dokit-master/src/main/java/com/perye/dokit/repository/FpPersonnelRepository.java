package com.perye.dokit.repository;

import com.perye.dokit.entity.FpPersonnel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpPersonnelRepository extends JpaRepository<FpPersonnel, String>, JpaSpecificationExecutor<FpPersonnel> {
}
