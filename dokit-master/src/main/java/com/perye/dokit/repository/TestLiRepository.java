package com.perye.dokit.repository;

import com.perye.dokit.entity.TestLi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface TestLiRepository extends JpaRepository<TestLi, String>, JpaSpecificationExecutor<TestLi> {
}
