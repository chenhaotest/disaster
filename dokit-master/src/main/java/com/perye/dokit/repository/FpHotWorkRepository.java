package com.perye.dokit.repository;

import com.perye.dokit.entity.FpHotWork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpHotWorkRepository extends JpaRepository<FpHotWork, String>, JpaSpecificationExecutor<FpHotWork> {
}
