package com.perye.dokit.repository;

import com.perye.dokit.entity.FpEquipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpEquipmentRepository extends JpaRepository<FpEquipment, String>, JpaSpecificationExecutor<FpEquipment> {
}
