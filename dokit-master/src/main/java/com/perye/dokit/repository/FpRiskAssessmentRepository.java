package com.perye.dokit.repository;

import com.perye.dokit.entity.FpRiskAssessment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpRiskAssessmentRepository extends JpaRepository<FpRiskAssessment, String>, JpaSpecificationExecutor<FpRiskAssessment> {
}
