package com.perye.dokit.repository;

import com.perye.dokit.entity.FpModubs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpModubsRepository extends JpaRepository<FpModubs, String>, JpaSpecificationExecutor<FpModubs> {
}
