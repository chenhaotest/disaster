package com.perye.dokit.repository;

import com.perye.dokit.entity.FpDepartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpDepartmentRepository extends JpaRepository<FpDepartment, String>, JpaSpecificationExecutor<FpDepartment> {
}
