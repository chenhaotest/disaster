package com.perye.dokit.repository;

import com.perye.dokit.entity.FpAlarmPointLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;


public interface FpAlarmPointLogRepository extends JpaRepository<FpAlarmPointLog, String>, JpaSpecificationExecutor<FpAlarmPointLog> {
}
