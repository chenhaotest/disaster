package com.perye.dokit.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author LiBo
* @date 2020-08-11
*/
@Entity
@Data
@Table(name="test_li")
public class TestLi implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "name")
    private String name;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "is_delete")
    private Integer isDelete;

    public void copy(TestLi source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
