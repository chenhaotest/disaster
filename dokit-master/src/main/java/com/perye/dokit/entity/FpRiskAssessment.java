package com.perye.dokit.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author libo
* @date 2024-05-06
*/
@Entity
@Data
@Table(name="fp_risk_assessment")
public class FpRiskAssessment implements Serializable {


    @Id
    @Column(name = "id")
    private String id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "evaluation_time")
    private Timestamp evaluationTime;


    @Column(name = "assessor")
    private String assessor;


    @Column(name = "contact_phone")
    private String contactPhone;


    @Column(name = "rectification_situation")
    private String rectificationSituation;


    @Column(name = "assessment_report")
    private String assessmentReport;


    @Column(name = "evaluation_unit")
    private String evaluationUnit;


    @Column(name = "evaluate")
    private String evaluate;


    @Column(name = "create_id")
    private String createId;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;

    @Column(name = "is_delete")
    private Integer isDelete;

    public void copy(FpRiskAssessment source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
