package com.perye.dokit.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author libo
* @date 2024-03-28
*/
@Entity
@Data
@Table(name="fp_equipment")
public class FpEquipment implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "asset_number")
    private String assetNumber;


    @Column(name = "device_name")
    private String deviceName;


    @Column(name = "device_type")
    private String deviceType;


    @Column(name = "specification")
    private String specification;


    @Column(name = "manufacturer")
    private String manufacturer;


    @Column(name = "department")
    private String department;


    @Column(name = "service_life")
    private String serviceLife;


    @Column(name = "usage_condition")
    private String usageCondition;


    @Column(name = "purchase_date")
    private Timestamp purchaseDate;


    @Column(name = "mileage_numbe")
    private String mileageNumbe;


    @Column(name = "remote_control")
    private String remoteControl;


    @Column(name = "down_lines")
    private String downLines;


    @Column(name = "enable")
    private String enable;


    @Column(name = "res_railway_id")
    private String resRailwayId;


    @Column(name = "res_tunnel_id")
    private String resTunnelId;


    @Column(name = "res_region_id")
    private String resRegionId;


    @Column(name = "res_sys_id")
    private String resSysId;


    @Column(name = "is_delete")
    private Integer isDelete;


    @Column(name = "create_id")
    private String createId;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;
    /**
     * 下次保养日期
     */
    @Column(name = "next_maintenance_date")
    private Timestamp nextMaintenanceDate;

    /**
     * 保养周期(天)
     */
    @Column(name = "maintenance_cycle")
    private String maintenanceCycle;

    public void copy(FpEquipment source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
