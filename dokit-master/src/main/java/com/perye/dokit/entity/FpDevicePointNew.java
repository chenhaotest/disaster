package com.perye.dokit.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author libo
* @date 2024-03-28
*/
@Entity
@Data
@Table(name="fp_device_point_new")
public class FpDevicePointNew implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "point_name")
    private String pointName;


    @Column(name = "point_type")
    private String pointType;


    @Column(name = "mapping_type",nullable = false)
    @NotBlank
    private String mappingType;


    @Column(name = "register_address",nullable = false)
    @NotBlank
    private String registerAddress;


    @Column(name = "register_address_byte",nullable = false)
    @NotBlank
    private String registerAddressByte;


    @Column(name = "modbus_id")
    private String modbusId;


    @Column(name = "res_railway_id")
    private String resRailwayId;


    @Column(name = "res_tunnel_id")
    private String resTunnelId;


    @Column(name = "res_region_id")
    private String resRegionId;


    @Column(name = "res_sys_id")
    private String resSysId;


    @Column(name = "equipment_id")
    private String equipmentId;


    @Column(name = "connection_type")
    private String connectionType;


    @Column(name = "enable")
    private String enable;


    @Column(name = "is_alarm")
    private String isAlarm;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;


    @Column(name = "is_delete")
    private Integer isDelete;


    @Column(name = "type")
    private String type;


    @Column(name = "register_address_val")
    private String registerAddressVal;

    @Column(name = "register_type")
    private String registerType;

    public void copy(FpDevicePointNew source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
