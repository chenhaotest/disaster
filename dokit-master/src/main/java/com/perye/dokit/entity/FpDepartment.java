package com.perye.dokit.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author libo
* @date 2024-03-27
*/
@Entity
@Data
@Table(name="fp_department")
public class FpDepartment implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "department_name")
    private String departmentName;


    @Column(name = "department_type")
    private String departmentType;


    @Column(name = "state")
    private String state;


    @Column(name = "description")
    private String description;


    @Column(name = "is_delete")
    private Integer isDelete;


    @Column(name = "create_id")
    private String createId;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;

    public void copy(FpDepartment source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
