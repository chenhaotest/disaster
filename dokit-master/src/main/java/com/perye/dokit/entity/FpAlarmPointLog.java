package com.perye.dokit.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author libo
* @date 2024-03-29
*/
@Entity
@Data
@Table(name="fp_alarm_point_log")
public class FpAlarmPointLog implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "point_name")
    private String pointName;


    @Column(name = "point_type")
    private String pointType;


    @Column(name = "mapping_type",nullable = false)
    @NotBlank
    private String mappingType;


    @Column(name = "register_address",nullable = false)
    @NotBlank
    private String registerAddress;


    @Column(name = "register_address_byte",nullable = false)
    @NotBlank
    private String registerAddressByte;


    @Column(name = "modbus_id")
    private String modbusId;


    @Column(name = "res_railway_id")
    private String resRailwayId;


    @Column(name = "res_tunnel_id")
    private String resTunnelId;


    @Column(name = "res_region_id")
    private String resRegionId;


    @Column(name = "res_sys_id")
    private String resSysId;


    @Column(name = "equipment_id")
    private String equipmentId;


    @Column(name = "batch")
    private String batch;


    @Column(name = "state")
    private String state;


    @Column(name = "register_address_val")
    private String registerAddressVal;


    @Column(name = "update_time")
    private Timestamp updateTime;


    @Column(name = "create_time")
    private Timestamp createTime;

    public void copy(FpAlarmPointLog source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
