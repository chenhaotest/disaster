package com.perye.dokit.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author libo
* @date 2024-03-27
*/
@Entity
@Data
@Table(name="fp_accident_statistics")
public class FpAccidentStatistics implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "manufacturer_name")
    private String manufacturerName;


    @Column(name = "person_name")
    private String personName;


    @Column(name = "person_phone")
    private String personPhone;


    @Column(name = "manufacturers_address")
    private String manufacturersAddress;


    @Column(name = "country")
    private String country;


    @Column(name = "bad_credit")
    private String badCredit;


    @Column(name = "equipment_vendors_type")
    private String equipmentVendorsType;


    @Column(name = "is_delete")
    private Integer isDelete;


    @Column(name = "create_id")
    private String createId;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;

    public void copy(FpAccidentStatistics source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
