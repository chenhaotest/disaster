package com.perye.dokit.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author libo
* @date 2024-04-25
*/
@Entity
@Data
@Table(name="fp_hot_work")
public class FpHotWork implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "work_definition")
    private String workDefinition;


    @Column(name = "work_name")
    private String workName;


    @Column(name = "use_tool")
    private String useTool;


    @Column(name = "work_address_range")
    private String workAddressRange;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "work_begin_time")
    private Timestamp workBeginTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "work_end_time")
    private Timestamp workEndTime;


    @Column(name = "work_unit")
    private String workUnit;


    @Column(name = "work_people_type")
    private String workPeopleType;


    @Column(name = "work_peoples")
    private String workPeoples;


    @Column(name = "inform")
    private String inform;


    @Column(name = "work_supervise")
    private String workSupervise;


    @Column(name = "regulatory_records")
    private String regulatoryRecords;


    @Column(name = "approval_status")
    private String approvalStatus;


    @Column(name = "approval_opinions")
    private String approvalOpinions;


    @Column(name = "approval_by")
    private String approvalBy;


    @Column(name = "is_delete")
    private Integer isDelete;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;


    @Column(name = "create_id")
    private String createId;

    public void copy(FpHotWork source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
