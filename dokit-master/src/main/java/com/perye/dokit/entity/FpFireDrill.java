package com.perye.dokit.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author libo
* @date 2024-04-28
*/
@Entity
@Data
@Table(name="fp_fire_drill")
public class FpFireDrill implements Serializable {


    @Id
    @Column(name = "id")
    private String id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "exercise_time")
    private Timestamp exerciseTime;


    @Column(name = "exercise_addr")
    private String exerciseAddr;


    @Column(name = "exercise_describe")
    private String exerciseDescribe;


    @Column(name = "contact")
    private String contact;

    @Column(name = "contact_info")
    private String contactInfo;


    @Column(name = "state")
    private String state;


    @Column(name = "participate_num")
    private String participateNum;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;


    @Column(name = "create_id")
    private String createId;

    @Column(name = "is_delete")
    private Integer isDelete;

    public void copy(FpFireDrill source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
