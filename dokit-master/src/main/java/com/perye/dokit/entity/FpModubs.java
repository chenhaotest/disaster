package com.perye.dokit.entity;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author libo
* @date 2024-03-28
*/
@Entity
@Data
@Table(name="fp_modubs")
public class FpModubs implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "network_address")
    private String networkAddress;


    @Column(name = "ports")
    private String ports;


    @Column(name = "res_railway_id")
    private String resRailwayId;


    @Column(name = "res_tunnel_id")
    private String resTunnelId;


    @Column(name = "state")
    private String state;


    @Column(name = "description")
    private String description;


    @Column(name = "read_max")
    private Integer readMax;


    @Column(name = "is_delete")
    private Integer isDelete;


    @Column(name = "create_id")
    private String createId;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;

    public void copy(FpModubs source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
