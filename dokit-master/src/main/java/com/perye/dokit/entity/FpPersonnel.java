package com.perye.dokit.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author libo
* @date 2024-03-27
*/
@Entity
@Data
@Table(name="fp_personnel")
public class FpPersonnel implements Serializable {


    @Id
    @Column(name = "id")
    private String id;


    @Column(name = "name")
    private String name;


    @Column(name = "sex")
    private String sex;


    @Column(name = "department_id")
    private String departmentId;


    @Column(name = "personnel_type")
    private String personnelType;


    @Column(name = "idcard")
    private String idcard;


    @Column(name = "phone_number")
    private String phoneNumber;


    @Column(name = "emergency_contact")
    private String emergencyContact;


    @Column(name = "emergency_contact_phone")
    private String emergencyContactPhone;


    @Column(name = "state")
    private String state;


    @Column(name = "affiliated_company")
    private String affiliatedCompany;


    @Column(name = "is_delete")
    private Integer isDelete;


    @Column(name = "create_id")
    private String createId;


    @Column(name = "create_time")
    private Timestamp createTime;


    @Column(name = "update_time")
    private Timestamp updateTime;

    @Column(name = "people_post")
    private String peoplePost;
    public void copy(FpPersonnel source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}
