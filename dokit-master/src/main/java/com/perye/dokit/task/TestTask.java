package com.perye.dokit.task;

import com.perye.dokit.utils.UuidUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class TestTask {

    public void run(){
        log.info("执行成功");
    }

    public void run1(String str){
        log.info("执行成功，参数为： {}" , str);
    }

    public static void main(String[] args) {
        System.out.println(UuidUtil.get32UUID());
        System.out.println(UuidUtil.get32UUID());
        System.out.println(UuidUtil.get32UUID());
        System.out.println(UuidUtil.get32UUID());
        System.out.println(UuidUtil.get32UUID());
    }
}
