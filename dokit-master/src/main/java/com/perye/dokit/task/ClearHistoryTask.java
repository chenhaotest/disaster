package com.perye.dokit.task;

import cn.hutool.core.collection.CollectionUtil;
import com.perye.dokit.newservice.ClearHistoryRecordService;
import com.perye.dokit.utils.DateUtils;
import com.perye.dokit.utils.pageData.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.List;

@Component
public class ClearHistoryTask {

    @Autowired
    private ClearHistoryRecordService clearHistoryRecordService;

    public void clearHistoryRecord() throws Exception {
        //获取上个月今天的日期
        String time = DateUtils.oneMonthGet(DateUtils.getTime(), -1);
        PageData info = new PageData();
        info.put("time", time);
        //删除上个月的浸水传感器数据
        clearImmerHistory(info);

        //删除上个月的水流量传感器数据
        clearSensorHistory(info);
    }

    //浸水传感器
    public void clearImmerHistory(PageData pageData) throws Exception {

        List<String> immerHistory = clearHistoryRecordService.queryImmerHistory(pageData);

        if (CollectionUtil.isNotEmpty(immerHistory)) {
            clearHistoryRecordService.delImmerHistory(immerHistory);
        }

    }


    //水流量传感器
    public void clearSensorHistory(PageData pageData) throws Exception {
        List<String> sensorHistory = clearHistoryRecordService.querySensorHistory(pageData);
        if (CollectionUtil.isNotEmpty(sensorHistory)) {
            clearHistoryRecordService.delSensorHistory(sensorHistory);
        }
    }
}
