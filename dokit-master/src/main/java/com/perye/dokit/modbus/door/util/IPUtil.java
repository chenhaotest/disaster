package com.perye.dokit.modbus.door.util;

import lombok.extern.slf4j.Slf4j;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * @author 王杰
 * @Description()
 * @ClassName IPUtil
 * @date 2021.08.12 14:59
 */
@Slf4j
public class IPUtil {

    public static String getLocalIp() {
        String ip = "";
        if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1) {
            try {
                ip = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException e) {
                log.error("UnknownHostException {}", e);
            }
        } else {
            try {
                for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                    NetworkInterface interf = en.nextElement();
                    String name = interf.getName();
                    if (!name.contains("docker") && !name.contains("lo")) {
                        for (Enumeration<InetAddress> enumeAddress = interf.getInetAddresses(); enumeAddress.hasMoreElements(); ) {
                            InetAddress address = enumeAddress.nextElement();
                            if (!address.isLoopbackAddress()) {
                                String ipAddress = address.getHostAddress();
                                if (!ipAddress.contains("::") && !ipAddress.contains("0:0") && !ipAddress.contains("fe80")) {
                                    ip = ipAddress;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                log.error("get Linux local ip error {}", e);
            }
        }
        return ip;
    }

    /**
     * 把字符串IP转换成long
     *
     * @param ipStr 字符串IP
     * @return IP对应的long值
     */
    public static long ip2Long(String ipStr) {
        String[] ip = ipStr.split("\\.");
        return (Long.valueOf(ip[0]) << 24) + (Long.valueOf(ip[1]) << 16) + (Long.valueOf(ip[2]) << 8) + Long.valueOf(ip[3]);
    }

    /**
     * 把字节数组IP转换成long
     *
     * @param ip 字符串IP
     * @return IP对应的long值
     */
    public static long ip2Long(byte[] ip) {
        return (Long.valueOf(String.valueOf(Byte.toUnsignedLong(ip[0]))) << 24) + (Long.valueOf(String.valueOf(Byte.toUnsignedLong(ip[1]))) << 16) + (Long.valueOf(String.valueOf(Byte.toUnsignedLong(ip[2]))) << 8) + (Long.valueOf(String.valueOf(Byte.toUnsignedLong(ip[3]))));
    }

    /**
     * 把IP的long值转换成字节数组
     *
     * @param x IP的long值
     * @return long值对应的字符串
     */
    public static byte[] byte2Ip(long x) {
        byte[] arr = new byte[4];
        arr[3] = (byte) (x & 0xff);
        arr[2] = (byte) (x >> 8 & 0xff);
        arr[1] = (byte) (x >> 16 & 0xff);
        arr[0] = (byte) (x >> 24 & 0xff);
        return arr;
    }

    /**
     * 把IP的long值转换成字符串
     *
     * @param ipLong IP的long值
     * @return long值对应的字符串
     */
    public static String long2Ip(long ipLong) {
        StringBuilder ip = new StringBuilder();
        ip.append(ipLong >>> 24).append(".");
        ip.append((ipLong >>> 16) & 0xFF).append(".");
        ip.append((ipLong >>> 8) & 0xFF).append(".");
        ip.append(ipLong & 0xFF);
        return ip.toString();
    }
}
