package com.perye.dokit.modbus.door.util;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

/**
 * 快速傅里叶变换工具类
 *
 * @author wangjie
 */
public class FFTUtil {

    /**
     * 创建傅里叶方法实例
     */
    private static FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.STANDARD);

    /**
     * 返回一个新数组,不足的数据进行补零
     *
     * @param data
     * @return
     */
    public static double[] supplementData(double[] data) {
        int rebuildNum = rebuildNum(data.length);
        double[] newData = new double[rebuildNum];
        for (int i = 0; i < rebuildNum; i++) {
            if (i > data.length - 1) {
                newData[i] = 0;
            } else {
                newData[i] = data[i];
            }
        }
        return newData;
    }

    /**
     * 获取最靠近num的2次幂的数
     *
     * @param num
     * @return
     */
    public static int rebuildNum(int num) {
        if (num <= 0) {
            //如果num<=0，直接返回2的0次幂
            return 1;
        } else if ((num & (num - 1)) == 0) {
            //如果num是2的乘方，则直接返回num
            return num;
        } else {
            //找到大于以2为底num的对数最近的整数
            //例：num=7,以2为底7的对数为：2.807...，强转int再加1结果为：3
            //int tmp = (int) (Math.log(num) / Math.log(2)) + 1;
            int tmp = (int) (Math.log(num) / Math.log(2));
            //求2的tmp次幂
            return (int) Math.pow(2, tmp);
        }
    }


    /**
     * 逆傅里叶变换
     *
     * @param data
     * @return
     */
    public static Complex[] ifft(double[] data) {
        double[] supplementData = supplementData(data);
        Complex[] result = fft.transform(supplementData, TransformType.INVERSE);
        return result;
    }
}
