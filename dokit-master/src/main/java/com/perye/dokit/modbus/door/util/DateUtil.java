package com.perye.dokit.modbus.door.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author wangjie
 */
public class DateUtil {
    /**
     * 获取当前时间的秒数(int类型)
     *
     * @return
     */
    public static int getSeconds() {
        return (int) (System.currentTimeMillis() / 1000);
    }

    public static long getStartTimeByDate(String date) throws ParseException {
        String startDate = date + " 00:00:00";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = sdf.parse(startDate);
        return parse.getTime();
    }

    public static long getEndTimeByDate(String date) throws ParseException {
        String endDate = date + " 23:59:59";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = sdf.parse(endDate);
        return parse.getTime();
    }

    public static String dateFormat(long date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date(date));
    }

}
