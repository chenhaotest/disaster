package com.perye.dokit.modbus.modbusTcp;


import lombok.Data;

@Data
public class Locator {
    private Integer id;
    private Integer slaveId;
    private Integer offset;
}