package com.perye.dokit.modbus.door.util;

import com.intelligt.modbus.jlibmodbus.Modbus;
import com.intelligt.modbus.jlibmodbus.master.ModbusMaster;
import com.intelligt.modbus.jlibmodbus.master.ModbusMasterFactory;
import com.intelligt.modbus.jlibmodbus.tcp.TcpParameters;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class ModbusMasterUtils {

    //获取连接
    public static ModbusMaster getModbusMaster(String ip, Integer port)  {
        try {
            TcpParameters tcpParameters = new TcpParameters();
            //ip
            InetAddress adress = InetAddress.getByName(ip);
            tcpParameters.setHost(adress);
            // TCP设置长连接
            tcpParameters.setKeepAlive(true);
            // TCP设置端口，这里设置是默认端口502
            tcpParameters.setPort(port);

            ModbusMaster master = ModbusMasterFactory.createModbusMasterTCP(tcpParameters);
            Modbus.setAutoIncrementTransactionId(true);
            return master;
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;

    }

    /**
     * 读04
     * @param master 配置的mater主机
     * @param slaveId 从机id
     * @param offset 寄存器读取开始地址  0059的话则为58
     * @param quantity 读取的寄存器数量
     * @return float类型的值
     */
    public static List<Float> readInputRegisters(ModbusMaster master, int slaveId, int offset, int quantity){
        List<Float> floatList=new ArrayList<>();
        try {

            if (!master.isConnected()) {
                master.connect();// 开启连接
            }

            // 读取对应从机的数据，readInputRegisters读取的写寄存器，功能码04
            int[] registerValues = master.readInputRegisters(slaveId, offset, quantity);

           // floatList = ByteArrayConveter.getFloatArray(registerValues);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return floatList;
    }

    /**
     * 读03
     * @param master 配置的mater主机
     * @param slaveId 从机id
     * @param offset 寄存器读取开始地址  0059的话则为58
     * @param quantity 读取的寄存器数量
     * @return float类型的值
     */
    public static List<Float> readHoldingRegisters(ModbusMaster master,int slaveId,int offset,int quantity){
        List<Float> floatList=new ArrayList<>();
        try {

            if (!master.isConnected()) {
                master.connect();// 开启连接
            }

            // 读取对应从机的数据，readInputRegisters读取的写寄存器，功能码04
            int[] registerValues = master.readHoldingRegisters(slaveId, offset, quantity);

         //   floatList = ByteArrayConveter.getFloatArray(registerValues);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return floatList;
    }

    public static void close(ModbusMaster master){
        try {
            master.disconnect();

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

