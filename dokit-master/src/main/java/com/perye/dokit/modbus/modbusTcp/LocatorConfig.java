package com.perye.dokit.modbus.modbusTcp;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Data
@ConfigurationProperties(prefix = "locators")
public class LocatorConfig {
    private List<Locator> locatorList;
}
