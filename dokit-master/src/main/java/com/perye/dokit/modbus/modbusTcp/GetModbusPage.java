

package com.perye.dokit.modbus.modbusTcp;

import cn.hutool.core.util.PageUtil;
import com.perye.dokit.websocket.WebSocketServer;
import com.serotonin.modbus4j.BatchRead;
import com.serotonin.modbus4j.BatchResults;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.locator.BaseLocator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


/**
 * 启动打开
 */
@Slf4j
//@Configuration
//@EnableScheduling
@RestController

@RequestMapping("/app/getModbusPage")
public class GetModbusPage {

    private String modebustcpIp = "192.168.1.205";

    private Integer port = 503;
    @Autowired
    private LocatorConfig locatorConfig;

    private static WebSocketServer webSocketServer;

@PostMapping("/toChangeModbusData")
    public void toChangeModbusData() throws ModbusInitException {

    // 测试的逻辑内容开始---------


        long stime =System.currentTimeMillis();
        log.info("开始时间"+stime);
        //最大偏移量个数
        int totalCount =1000;
        //每次最多请求的数量
        int pageSize =120;
        try {
            List<BatchResults<Integer>>  results =  new ArrayList<BatchResults<Integer>>();
            //每次根据最大偏移量和最大和最大连续请求 来确定循环几次
            int totalPage = PageUtil.totalPage(totalCount, pageSize);//9
            for (int j = 1; j <=totalPage ; j++) {
                Thread.sleep(10);
                int[] startEnd = PageUtil.transToStartEnd(j, pageSize);//[0, 10]
                int start= startEnd[0];
                int end= startEnd[1];

                //最后一页
                if(j==totalPage){
                    end =totalCount;

                }
                log.info("J=:"+j+"start=:"+start+"end=:"+end);
                ModbusMaster master2 = Modbus4jUtils.getMaster(modebustcpIp, port);
                master2.setTimeout(5000);
                BatchRead<Integer> batch2 = new BatchRead<>();
                List<Locator> locatorList2 = new ArrayList<>();
                for (int i = start+1; i <=end ; i++) {
                    Locator ct =new Locator();
                    ct.setId(i);
                    ct.setSlaveId(1);
                    ct.setOffset(i);
                    locatorList2.add(ct);
                }
                locatorList2.forEach(locator -> batch2.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(), DataType.TWO_BYTE_INT_SIGNED)));
                BatchResults<Integer> batchResults2 = Modbus4jUtils.batchRead(master2, batch2);
//                log.info("结果2==="+batchResults2);
                results.add(batchResults2);
            }
            long etime =  System.currentTimeMillis()-stime;
            log.info("结束时间时间"+etime);
            log.info("最终结果"+results);

        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    // 测试的逻辑内容结束---------


    }

    @PostMapping("/getDataPage")
    public void getDataPage() throws ModbusInitException {
        try {
            ModbusMaster master = Modbus4jUtils.getMaster(modebustcpIp, port);

        BatchRead<Integer> batch = new BatchRead<>();

        Thread.sleep(10);
        log.info("开始-----------------------");
        List<Locator> locatorList = new ArrayList<>();
        for (int i = 5; i <11 ; i++) {
            Locator ct =new Locator();
            ct.setId(i);
            ct.setSlaveId(1);
            ct.setOffset(i);
            locatorList.add(ct);
        }
        locatorList.forEach(locator -> batch.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(), DataType.TWO_BYTE_INT_SIGNED)));
        log.info("结束-----------------------");



            BatchResults<Integer> batchResults = Modbus4jUtils.batchRead(master, batch);
            log.info("结果==="+batchResults);




        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
