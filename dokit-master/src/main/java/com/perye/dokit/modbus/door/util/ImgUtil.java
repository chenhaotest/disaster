package com.perye.dokit.modbus.door.util;

/**
 * @author 王杰
 * @Description()
 * @ClassName ImgUtil
 * @date 2021.08.04 11:20
 */
public class ImgUtil {

    /**
     * 获取图片路径(去掉开头的双斜杠)
     *
     * @param data
     * @return
     */
    public static String getStartImgUrl(byte[] data) {
        byte[] img = new byte[data.length - 6];
        System.arraycopy(data, 6, img, 0, img.length);
        String imgUrl = new String(img).trim();
        String[] split = imgUrl.split("//");
        imgUrl = split[1].split(":")[0] + "/img/" + split[2];
        return imgUrl;
    }

    /**
     * 获取完成图片路径
     *
     * @param data
     * @return
     */
    public static String getFinishImgUrl(byte[] data) {
        byte[] img = new byte[data.length - 1];
        System.arraycopy(data, 1, img, 0, img.length);
        String imgUrl = new String(img).trim();
        String[] split = imgUrl.split("//");
        imgUrl = split[1].split(":")[0] + "/img/" + split[2];
        return imgUrl;
    }

    /**
     * 获取原始图片路径
     *
     * @param data
     * @return
     */
    public static String getOriginalImgUrl(byte[] data) {
        byte[] img = new byte[data.length - 6];
        System.arraycopy(data, 6, img, 0, img.length);
        String imgUrl = new String(img).trim();
        return imgUrl;
    }

}
