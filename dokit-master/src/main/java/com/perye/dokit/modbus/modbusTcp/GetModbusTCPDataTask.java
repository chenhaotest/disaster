package com.perye.dokit.modbus.modbusTcp;


import com.perye.dokit.newservice.DevicePointService;
import com.perye.dokit.utils.pageData.PageData;
import com.serotonin.modbus4j.BatchRead;
import com.serotonin.modbus4j.BatchResults;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.locator.BaseLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.method.P;

import java.util.ArrayList;
import java.util.List;

/*@Configuration
@EnableScheduling*/
public class GetModbusTCPDataTask {

    private String modebustcpIp = "192.168.1.205";

    private Integer port = 504;
    @Autowired
    private LocatorConfig locatorConfig;

    @Autowired
    public DevicePointService devicePointService;


    @Scheduled(fixedRateString = "5000")
    public void getData() throws ModbusInitException {
        ModbusMaster master = Modbus4jUtils.getMaster(modebustcpIp, port);
        BatchRead<Integer> batch = new BatchRead<>();
       /* List<Locator> locatorList =new ArrayList<>();
       locatorList.forEach(locator -> batch.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(),DataType.TWO_BYTE_INT_SIGNED)));
*/
         try {

     /*        PageData pd= new  PageData();
             List<PageData> devPoints =     devicePointService.findPoints(pd);

             for (PageData points: devPoints) {
                 Locator locator= new Locator();
                 locator.setId(Integer.parseInt(points.get("id")+"") );
                 locator.setSlaveId(1);
                 locator.setOffset(Integer.parseInt(points.get("register_address")+""));
                 batch.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(),DataType.TWO_BYTE_INT_SIGNED));
             }

             BatchResults<Integer> batchResults = Modbus4jUtils.batchRead(master, batch);
             System.out.println("valueOne222111:"+batchResults);
*/


             PageData pd= new  PageData();
            /* List<PageData> devPoints =     devicePointService.findMaxPoints(pd);//获取  偏移量最大的值
             Integer VAL=Integer.parseInt(  devPoints.get(0).get("register_address")+"");*/

             for (int i = 100; i < 220; i++) {
                 Locator locator= new Locator();
                 locator.setId(i);
                 locator.setSlaveId(1);
                 locator.setOffset(i);
                 batch.addLocator(locator.getId(), BaseLocator.holdingRegister(locator.getSlaveId(), locator.getOffset(),DataType.TWO_BYTE_INT_SIGNED));
             }
             BatchResults<Integer> batchResults = Modbus4jUtils.batchRead(master, batch);
             System.out.println("valueOne222111:"+batchResults);

        } catch (ModbusTransportException e) {
            e.printStackTrace();
        } catch (ErrorResponseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}