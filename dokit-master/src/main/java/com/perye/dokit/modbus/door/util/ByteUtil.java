package com.perye.dokit.modbus.door.util;

/**
 * @author gaoyf
 * @since 2020/7/7 0007 15:15
 * byte工具类
 */
public class ByteUtil {

    /**
     * 获取二进制byte指定索引的数值
     *
     * @param b     byte
     * @param index 索引
     * @return 返回0-1
     */
    public static int getIndex(byte b, int index) {
        return (b >> (8 - index)) & 1;
    }

    /**
     * CRC8校验成功
     *
     * @return
     */
    public static byte[] CRC8VerificationSuccee() {
        return new byte[]{0x01, 0x00, 0x00};
    }

    /**
     * CRC8校验失败
     *
     * @return
     */
    public static byte[] CRC8VerificationFailed() {
        return new byte[]{0x02, 0x00, 0x00};
    }

    /**
     * 数字转字符串
     *
     * @param b
     * @return
     */
    public static String byte2String(byte[] b) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < b.length; i++) {
            sb.append((char) b[i]);
        }
        return sb.toString();
    }

    /**
     * 将字节数组转为long<br>
     * 如果input为null,或offset指定的剩余数组长度不足8字节则抛出异常
     *
     * @param input
     * @param offset       起始偏移量
     * @param littleEndian 输入数组是否小端模式
     * @return
     */
    public static long longFrom8Bytes(byte[] input, int offset, boolean littleEndian) {
        long value = 0;
        // 循环读取每个字节通过移位运算完成long的8个字节拼装
        for (int count = 0; count < Long.BYTES; ++count) {
            int shift = (littleEndian ? count : (7 - count)) << 3;
            value |= ((long) 0xff << shift) & ((long) input[offset + count] << shift);
        }
        return value;
    }

    /**
     * 获取byte中单个bit值
     *
     * @param b
     * @param i
     * @return
     */
    public static int getBit(byte b, int i) {
        int bit = (int) ((b >> i) & 0x1);
        return bit;
    }

    /**
     * 获取int中单个bit值
     *
     * @param b
     * @param i
     * @return
     */
    public static int getBit(int b, int i) {
        int bit = (int) ((b >> i) & 0x1);
        return bit;
    }
}
