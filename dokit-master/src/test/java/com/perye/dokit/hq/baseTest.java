package com.perye.dokit.hq;

import com.perye.dokit.newservice.BaseService;
import com.perye.dokit.utils.pageData.PageData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * \* Created with IntelliJ IDEA.
 * \* User: chenhao
 * \* Date: 2021/7/19
 * \* Time: 11:42
 * \* Description:
 * \
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class baseTest {
    @Autowired
    private BaseService baseService;

    @Test
    public void contextLoads() {
        PageData result = new PageData();


        try {
            List<PageData> list = baseService.getListBase(result);
            result.put("data", list);
            result.put("status", 0);
            result.put("msg", "查询成功");
        } catch (Exception e) {

        }

    }
}