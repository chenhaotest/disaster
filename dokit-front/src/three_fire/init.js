import cameraModule from "./camera";
import rendererModule from "./renderer";

// 更新摄像头
export function initData(){
  var threeDiv=document.getElementById('three_id');
 var height = threeDiv.offsetHeight;
  var width = threeDiv.offsetWidth;
  cameraModule.activeCamera.aspect = width/ height;
//   更新摄像机的投影矩阵
  cameraModule.activeCamera.updateProjectionMatrix();

// 监听屏幕大小改变的变化，设置渲染的尺寸
  window.addEventListener("resize", () => {
    //   console.log("resize");
    // 更新摄像头
    cameraModule.activeCamera.aspect =width/ height;
    //   更新摄像机的投影矩阵
    cameraModule.activeCamera.updateProjectionMatrix();

    //   更新渲染器
    rendererModule.renderer.setSize(width, height);
    //   设置渲染器的像素比例
    rendererModule.renderer.setPixelRatio(width, height);

    //   更新css3d渲染器
    rendererModule.css3drender.setSize(width, height);
  });

}

