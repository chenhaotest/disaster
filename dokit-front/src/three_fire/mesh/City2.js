import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'

import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'

import { CSS3DObject } from 'three/examples/jsm/renderers/CSS3DRenderer'



import scene from '../scene'

var list = []
export default class City {
  constructor(scene) {

    var that = this
    // 载入模型
    this.scene = scene
    this.loader = new GLTFLoader()


    //模型加载解压
    const dracoLoader = new DRACOLoader();
    dracoLoader.setDecoderPath("/draco/");
    this.loader.setDRACOLoader(dracoLoader);
    this.loader.load("/model/dem0814022.glb", (gltf) => {
  //  gltf.scene.position.set(200, -30, -500);
/*    gltf.scene.rotation.set(200, -30, -500);*/

      scene.add(gltf.scene)
      gltf.scene.children.forEach(function(child) {
        if(child.name=="baipai"){
          child.children.forEach(function(md) {
       // md.material=material2;
          })
        }
        if(  child.children.length==0){
          list.push(child.name)
          that.createTag(child)
        }

      })
      sessionStorage.setItem('sdList', list)

    })

 // this.createModel()

  }



  createTag(object3d) {


    const element = document.createElement('div')
    element.className = 'elementTag'
    element.id = object3d.name
    element.innerHTML = `
      <div class="elementContent " onclick="toShowThree(${object3d.name})">
 
        
        <i class="el-icon-location three_icon_tag"></i>
       <!-- <i class="el-icon-tickets three_el_icon" onclick="toShowThree()"></i>-->
      </div>
    `
    const objectCSS3D = new CSS3DObject(element)
    objectCSS3D.scale.set(0.2, 0.2, 0.2)
    objectCSS3D.position.copy(object3d.position)
    this.scene.add(objectCSS3D)
  }

  createModel(object3d) {


    const element = document.createElement('div')
    element.className = 'elementTagModel'
    element.innerHTML = `
      <div class="elementContentModel"  id="element_center_one">
 
        <div >
        <span class="title_jg_h">火警预警</span>
            <h5 class="title_jg_jb">一</h5>
    </div>
      </div>
    `

    const objectCSS3D = new CSS3DObject(element)
    objectCSS3D.scale.set(0.2, 0.2, 0.2)
    objectCSS3D.position.copy([10,0,0])
    this.scene.add(objectCSS3D)
  }
}
