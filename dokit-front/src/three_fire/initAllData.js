import * as THREE from 'three'
import TWEEN from '@tweenjs/tween.js'

// 导入轨道控制器
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'

import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader'

export function initAllData() {
  let sceneDiv = document.getElementById('three_id')

  var height=sceneDiv.offsetHeight;
  var width=sceneDiv.offsetWidth;
// 初始化场景
  const scene = new THREE.Scene()

  const ambient = new THREE.HemisphereLight(0xffffff, 0xffffff, 1)
  ambient.position.set(0, 50, 0) //这个也是默认位置
  scene.add(ambient)

// 载入模型
  var loader = new GLTFLoader()
//模型加载解压
  const dracoLoader = new DRACOLoader()
  dracoLoader.setDecoderPath('../draco/')
  loader.setDRACOLoader(dracoLoader)
  loader.load('../model/suidao0002.glb', (gltf) => {
    scene.add(gltf.scene)
  })

// 第二步   实例化一个透视投影相机对象
//视场角度, width / height:Canvas画布宽高比, 1:近裁截面, 3000：远裁截面
  const camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000)
//相机在Three.js三维坐标系中的位置
// 根据需要设置相机位置具体值
  camera.position.set(10, 10, 10)
  scene.add(camera)

// 第四步  创建渲染器对象
  const renderer = new THREE.WebGLRenderer()
// 设置渲染的尺寸大小
  renderer.setSize(width, height)
 /* renderer.setClearColor(0x3da0ff, 1) //设置背景颜色*/
//第五步将webgl渲染的canvas内容添加到body
  /*renderer.setClearColor(0x3da0ff, 0.1); //设置背景颜色*/
//document.body.appendChild(renderer.domElement);

  sceneDiv.appendChild(renderer.domElement)

// 设置相机控件轨道控制器OrbitControls
// 创建轨道控制器
  const controls = new OrbitControls(camera, renderer.domElement)



  const texture = new THREE.TextureLoader().load(url);
  texture.mapping = THREE.EquirectangularReflectionMapping
  scene.background = texture
 scene.environment = texture



  function animate(t) {
    renderer.render(scene, camera)
    requestAnimationFrame(animate)
  }

  animate()


}


