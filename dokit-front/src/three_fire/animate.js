import * as THREE from "three";
import cameraModule from "./camera";
import rendererModule from "./renderer";
import controlsModule from "./controls";
import scene from "./scene";
import { cylinderMesh} from "./planRing";


import TWEEN from "@tweenjs/tween.js";
//const points = createPoints("1",0.3 );//粒子播放


const clock = new THREE.Clock();
function animate(t) {
  const time = clock.getDelta();
  controlsModule.controls.update(time);
  TWEEN.update();
  cylinderAnimate()
 // points.rotation.x =  clock.getElapsedTime() * 0.3;

  // 使用渲染器渲染相机看这个场景的内容渲染出来
  rendererModule.renderer.render(scene, cameraModule.activeCamera);
  rendererModule.css3drender.render(scene, cameraModule.activeCamera);

  requestAnimationFrame(animate);
}

let cylinderRadius = 0;
let cylinderOpacity = 1;
//圆柱光圈扩散动画
function cylinderAnimate() {
  cylinderRadius += 0.01;
  cylinderOpacity -= 0.003;
  if (cylinderRadius > 4) {
    cylinderRadius = 0;
    cylinderOpacity = 1;
  }
  if (cylinderMesh) {
    cylinderMesh.scale.set(1 + cylinderRadius, 1, 1 + cylinderRadius); //圆柱半径增大
    cylinderMesh.material[0].opacity = cylinderOpacity; //圆柱可见度减小
  }



}

export default animate;
