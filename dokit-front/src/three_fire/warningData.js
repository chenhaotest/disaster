












export function DevceChangeDataTable(devceList,tableList,that) {

  var  addDevceList=[];

  var data= JSON.parse(localStorage.getItem('DeviceErrorData'));
  for (let i = 0, len = devceList.length; i < len; i++) {   //当前  websocket传来的数据
    var webData=devceList[i]
    var d_deviceCode=webData.deviceCode
    var state=webData.state  //0正常 1异常
    if(tableList.length==0){
      data[d_deviceCode]=webData  //当数据异常， 缓存中添加  异常数据
      addDevceList.push(webData);
    }


    for (let j = 0, len = tableList.length; j < len; j++) {   //当前  table展示的数据
      var tableData=tableList[j]
      var t_deviceCode=tableData.deviceCode
        if(state==0){
          if(d_deviceCode==t_deviceCode){
            tableList.splice(j, 1);//删除list中的数据
            console.log("错误修复-----------------------------------------------------"+t_deviceCode)
            delete data[d_deviceCode]  //当数据正常， 删掉缓存中  异常数据
            break;
          }

        }else{
         // tableList[tableList.length]=webData
          addDevceList.push(webData);
          data[d_deviceCode]=webData  //当数据异常， 缓存中添加  异常数据
          break;
        }
    }
  }
  localStorage.removeItem('DeviceErrorData')
  localStorage.setItem('DeviceErrorData', JSON.stringify(data))
  return  addDevceList;
}





/**
 * 批量设备预警
 * @param devceList
 * @constructor   type: 1:初始化加载，2websocket
 */
export function MaintenanceMonitoring(devceList,type) {

  for (let i = 0, len = devceList.length; i < len; i++) {
    var item=devceList[i]
    MaintenanceObject(item,type)

  }

}

/**
 * 单独设备预警
 * @param item
 * @constructor    type: 1:初始化加载，2websocket
 */
export function MaintenanceObject(item,type) {
  let elementTag = document.getElementById(item.deviceCode)

  if(elementTag!=null){
    if(item.state=="0" && type=="2"){
      elementTag.classList.remove('elementTagOne')
      elementTag.classList.add('elementTag')
      let elementIcon = elementTag.childNodes[1].childNodes[1]
      elementIcon.classList.remove('three_icon_tagOne')
      elementIcon.classList.add('three_icon_tag')
    }else if(item.state=="1"){

      if(elementTag!=null){
        elementTag.classList.remove('elementTag')
        elementTag.classList.add('elementTagOne')
        let elementIcon = elementTag.childNodes[1].childNodes[1]
        elementIcon.classList.remove('three_icon_tag')
        elementIcon.classList.add('three_icon_tagOne')
      }
    }else if(item.state=="2"){

    }

  }

}

/**
 * 火警预警
 * @param item
 * @constructor
 */

export function MaintenanceTunnelMonitoring(item,type) {
  /*let elementTagModel = document.getElementById("element_center_one")*/
  let elementTagModel = document.getElementById("center_level_id")
  if(item.state=="0"){
    $(elementTagModel).hide()
  }else if(item.state=="1"){
    $(elementTagModel).show()
    $(".level_text").html("一级")

    $(elementTagModel).css("background-image","linear-gradient(to left, rgb(61, 160, 255), rgb(110, 0, 20), rgb(61, 160, 255))")
  }else if(item.state=="2"){
    $(elementTagModel).show()
    $(".level_text").html("二级")
    $(elementTagModel).css("background-image","linear-gradient(to left, rgb(230, 196, 29), rgb(110, 0, 20), rgb(230, 196, 29))")
  }else if(item.state=="3"){
    $(elementTagModel).show()
    $(".level_text").html("三级")
    $(elementTagModel).css("background-image","linear-gradient(to left, rgb(255, 8, 22), rgb(110, 0, 20), rgb(255, 8, 22))")
  }



  /*
      $(elementTagModel).css("background-color","#3da0ff")
    }else if(item.state=="2"){
      $(elementTagModel).show()
      $(".title_jg_jb").html("二")
      $(elementTagModel).css("background-color","#e6c41d")
    }else if(item.state=="3"){
      $(elementTagModel).show()
      $(".title_jg_jb").html("三")
      $(elementTagModel).css("background-color","#FF0816")
    }
  */




}
//弹框
export function  toShowTableHtml(deviceCode){
    var data= JSON.parse(localStorage.getItem('DeviceErrorData'));
    var item=  data[deviceCode]

    $.alert({
      title: '设备详情',
      content: '<div class="tunnel_form">\n' +
        '  <form class="layui-form" action="">\n' +
        '    <div class="layui-form-item">\n' +
        '      <div class="layui-inline">\n' +
        '        <label class="layui-form-label">所属隧道：</label>\n' +
        '        <div class="layui-form-mid layui-word-aux   ">'+item.tunnelName+'</div>\n' +
        '      </div>\n' +
        '      <div class="layui-inline">\n' +
        '        <label class="layui-form-label">设备状态：</label>\n' +
        '        <div class="layui-form-mid layui-word-aux">'+item.deviceState+'</div>\n' +
        '      </div>\n' +
        '    </div>\n' +
        '\n' +
        '    <div class="layui-form-item">\n' +
        '      <div class="layui-inline">\n' +
        '        <label class="layui-form-label">设备名称：</label>\n' +
        '        <div class="layui-form-mid layui-word-aux">'+item.deviceName+'</div>\n' +
        '      </div>\n' +
        '      <div class="layui-inline">\n' +
        '        <label class="layui-form-label">设备编码：</label>\n' +
        '        <div class="layui-form-mid layui-word-aux">'+item.deviceCode+'</div>\n' +
        '      </div>\n' +
        '    </div>\n' +
        '    <div class="layui-form-item">\n' +
        '      <div class="layui-inline">\n' +
        '        <label class="layui-form-label">设备位置：</label>\n' +
        '        <div class="layui-form-mid layui-word-aux">'+item.devicePosition+'</div>\n' +
        '      </div>\n' +
        '      <div class="layui-inline">\n' +
        '        <label class="layui-form-label">是否异常：</label>\n' +
        '        <div class="layui-form-mid layui-word-aux">'+item.isAbnormalStr+'</div>\n' +
        '      </div>\n' +
        '    </div>\n' +
        '  </form>\n' +
        '</div>'
    })



}


//离线数量动态统计
export function MaintenanceDeviceErrorNum(data) {




}
