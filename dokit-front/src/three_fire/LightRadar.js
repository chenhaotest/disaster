import * as THREE from "three";
import gsap from "gsap";
import vertex from "./shader/vertex.glsl";
import fragment from "./shader/fragment.glsl";

export default class LightRadar {

  constructor(radius = 2, position = { x: 0, z: 0 }, color = 0xff0000) {


    let positions = new Float32Array([
      -10, 0, 0, // 0
      10, 0, 0, // 1
      0, 10, 0, // 2
      0, 0, 5, // 3
      0, 10, 5, // 4
      0, 0, 15 // 5
    ]);
    this.geometry  = new THREE.BufferGeometry();
    this.geometry.attributes.position = new THREE.BufferAttribute(positions, 3);


   // this.geometry = new THREE.PlaneBufferGeometry(radius, radius);
    this.material = new THREE.ShaderMaterial({
      uniforms: {
        uColor: { value: new THREE.Color(color) },
        uTime: {
          value: 0,
        },
      },
      vertexShader: vertex,
      fragmentShader: fragment,
      transparent: true,
      side: THREE.DoubleSide,
    });

    this.mesh = new THREE.Mesh(this.geometry, this.material);
    this.mesh.position.set(position.x, 1, position.z);
    this.mesh.rotation.x = -Math.PI / 2;

    gsap.to(this.material.uniforms.uTime, {
      value: 1,
      duration: 1,
      repeat: -1,
      ease: "none",
    });
  }

  remove() {
    this.mesh.remove();
    this.mesh.removeFromParent();
    this.mesh.geometry.dispose();
    this.mesh.material.dispose();
  }
}
