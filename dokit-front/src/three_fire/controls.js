import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";
import { FlyControls } from "three/examples/jsm/controls/FlyControls";
import { FirstPersonControls } from "three/examples/jsm/controls/FirstPersonControls";
import cameraModule from "./camera";
import rendererModule from "./renderer";
import eventHub from "@/utils/eventHub";

class ControlsModule {
  constructor() {
    this.setOrbitControls();
    eventHub.on("toggleControls", (name) => {
      this[`set${name}Controls`]();
    });
  }
  setOrbitControls() {
    // 初始化控制器
    this.controls = new OrbitControls(
      cameraModule.activeCamera,
      rendererModule.renderer.domElement
    );
    // 设置控制器阻尼
    this.controls.enableDamping = false;
    this.controls.dampingFactor = 0.000001;
    //是否能使用键盘
    this.controls.enableKeys = true;
    //默认键盘控制上下左右的键
    this.controls.keys = { LEFT: 37, UP: 38, RIGHT: 39, BOTTOM: 40 };
    this.controls.maxPolarAngle = Math.PI / 2.08;
     /*  this.controls.minPolarAngle =  Math.PI / 2;*/
  }
  setFlyControls() {
    this.controls = new FlyControls(
      cameraModule.activeCamera,
      rendererModule.renderer.domElement
    );

    this.controls.movementSpeed = 18;
    this.controls.rollSpeed = Math.PI / 60;
    this.controls.lookSpeed=0.06
 /*   this.controls.heightMax=5;*/
  }
  setFirstPersonControls() {
    this.controls = new FirstPersonControls(
      cameraModule.activeCamera,
      rendererModule.renderer.domElement
    );
    this.controls.movementSpeed = 18;
    this.controls.rollSpeed = Math.PI / 60;
    this.controls.lookSpeed=0.01
    this.controls.enableDamping = false;
  }
}

export default new ControlsModule();
