import * as THREE from "three";

// 初始化场景

import uv_grid_opengl from  '../../public/plane.jpg';
//import uv_grid_opengl from  '../../public/beijing.gif';

const textureLoader = new THREE.TextureLoader();
const doorColorTexture = textureLoader.load( uv_grid_opengl );

const PlaneBufferGeometry=new THREE.PlaneGeometry(80, 80,1)//平面物体
// 材质
const basicMaterial = new THREE.MeshBasicMaterial({
  color: "#eaebff",
  map: doorColorTexture,
  //   alphaMap: doorAplhaTexture,
  transparent: true,
  opacity: 0.1,
  //   side: THREE.DoubleSide,
});
basicMaterial.side = THREE.DoubleSide;   //物体设置双面

// 添加平面
const plane = new THREE.Mesh(
  PlaneBufferGeometry,
  basicMaterial
);
plane.position.set(0, 0, 0);
//围绕x轴旋转45度
plane.rotation.set(-Math.PI / 2, 0,0, "XZY");


export default plane;
