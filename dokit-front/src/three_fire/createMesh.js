import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader.js'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import * as THREE from 'three'
import scene from './scene'
import gsap from 'gsap'

import City2 from './mesh/City2'
import LightRadar from './LightRadar'
import $ from 'jquery'

let city2

export function createMeshCity2() {
  // 创建城市

  removeDevelopmentType()
  city2 = new City2(scene)
}

export function removeDevelopmentType() {

  var allChildren = scene.children
  for (var i = allChildren.length - 1; i >= 0; i--) {
    if (allChildren[i].type == 'Object3D' || allChildren[i].type == 'Group') {
      scene.remove(allChildren[i])
    }
  }

}

export function toChangeScene(obj) {
//创建一个材质对象Material
  const material2 = new THREE.MeshBasicMaterial({
    color: 0xff9933//0xff0000设置材质颜色为红色
  })
  var allChildren = scene.children
  for (var i = allChildren.length - 1; i >= 0; i--) {
    if (allChildren[i].type == 'Group') {
      for (var j = allChildren[i].children.length - 1; j >= 0; j--) {
        var dd = allChildren[i].children[j]
        if (dd.name == obj) {
          dd.material = material2
        }
      }

    }
  }

}


export function createLightRadar() {
  // // 添加雷达

}
