import * as THREE from "three";

// 初始化场景




import uv_grid_opengl from  '../../public/plane1.jpg';


//创建圆柱
let geometry = new THREE.CylinderGeometry(80, 80, 2, 30);

//加载纹理
let texture = new THREE.TextureLoader().load(uv_grid_opengl);
texture.wrapS = texture.wrapT = THREE.RepeatWrapping; //每个都重复
texture.repeat.set(1, 1);
texture.needsUpdate = true;

let materials = [
  //圆柱侧面材质，使用纹理贴图
  new THREE.MeshBasicMaterial({
    map: texture,
    side: THREE.DoubleSide,
    transparent: true
  }),
  //圆柱顶材质
  new THREE.MeshBasicMaterial({
    transparent: true,
    opacity: 0,
    side: THREE.DoubleSide
  }),
  //圆柱底材质
  new THREE.MeshBasicMaterial({
    transparent: true,
    opacity: 0,
    side: THREE.DoubleSide
  })
];
var cylinderMesh = new THREE.Mesh(geometry, materials);


cylinderMesh.position.set(0, -3, 0);











export  { cylinderMesh }
