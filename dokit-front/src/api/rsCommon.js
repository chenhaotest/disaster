import request from '@/utils/request'


export function findAllDevce(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/findAllDevce',
    method: 'post',
    data
  })
}

export function findWarningLevel(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/findWarningLevel',
    method: 'post',
    data
  })
}

export function findWarningTimeLevel(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/findWarningTimeLevel',
    method: 'post',
    data
  })
}

export function findDeviceNumEchart(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/findDeviceNumEchart',
    method: 'post',
    data
  })
}
//ecahrt  查询所有异常设备数据
export function findDeviceError(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/findDeviceError',
    method: 'post',
    data
  })
}
//ecahrt  查询实时统计
export function findEchartsTatistics(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/findEchartsTatistics',
    method: 'post',
    data
  })
}
// 硬件设备统计  报警，离线，故障，正常
export function findRealTimeAlarmEquipment(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/findRealTimeAlarmEquipment',
    method: 'post',
    data
  })
}

// 否执行消音操作：0:消音（设备暂停发出声响），1:停止灭火"
export function interactive(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/interactive',
    method: 'post',
    data
  })
}


// 火灾检测图片
export function findDevFireImg(data) {
  return request({
    url: 'api/gasFireExtinguishingLog/findDevFireImg',
    method: 'post',
    data
  })
}

//查询铁路
export function queryAllRailwayList(data) {
  return request({
    url: 'api/common/queryAllRailwayList',
    method: 'post',
    data
  })
}
//查询隧道
export function getTunnelByPcList(data) {
  return request({
    url: 'api/common/getTunnelByPcList',
    method: 'post',
    data
  })
}
//查询交换机
export function getExchangeBoardList(data) {
  return request({
    url: 'api/common/getExchangeBoardList',
    method: 'post',
    data
  })
}
//查询管理机
export function getDeviceMachineList(data) {
  return request({
    url: 'api/common/getDeviceMachineList',
    method: 'post',
    data
  })
}

//查询子系统
export function getSubsystemList(data) {
  return request({
    url: 'api/common/getSubsystemList',
    method: 'post',
    data
  })
}



//查询人员
export function queryAllTunnelAdministratorList(data) {
  return request({
    url: 'api/administrator/queryAllTunnelAdministratorList',
    method: 'post',
    data
  })
}


export function taskModubsStart(data) {
  return request({
    url: 'api/taskModubs/start',
    method: 'post',
    data
  })
}
export function taskModubsStop(data) {
  return request({
    url: 'api/taskModubs/stop',
    method: 'post',
    data
  })
}

export function queryResourceById(data) {
  return request({
    url: 'api/fpResource/queryResourceById',
    method: 'post',
    data
  })
}





export default { findAllDevce,findWarningLevel,findWarningTimeLevel,findDeviceNumEchart,findDeviceError,findEchartsTatistics,findRealTimeAlarmEquipment,interactive,findDevFireImg,queryAllRailwayList,getTunnelByPcList,queryAllTunnelAdministratorList,getExchangeBoardList,getDeviceMachineList,getSubsystemList,taskModubsStart,taskModubsStop,queryResourceById}
