import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/fpSpareParts',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/fpSpareParts/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/fpSpareParts',
    method: 'put',
    data
  })
}

export function fpAccidentStatisticsListAll(data) {
  return request({
    url: 'api/fpAccidentStatistics/fpAccidentStatisticsListAll',
    method: 'post',
    data
  })
}
export function fpEquipmentListAll(data) {
  return request({
    url: 'api/fpEquipment/fpEquipmentListAll',
    method: 'post',
    data
  })
}
export function addDict(data) {
  return request({
    url: `api/dict?page=0&size=9999&sort=id,desc`,
    method: 'get',
    data
  })
}
export default { add, edit, del, fpAccidentStatisticsListAll, addDict, fpEquipmentListAll }
