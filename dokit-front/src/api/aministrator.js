import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/administrator',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/administrator/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/administrator',
    method: 'put',
    data
  })
}

export function getTunnelByPcList(data) {
  return request({
    url: 'api/tunnel/getTunnelByPcList',
    method: 'post',
    data
  })
}
export default { add, edit, del,getTunnelByPcList }
