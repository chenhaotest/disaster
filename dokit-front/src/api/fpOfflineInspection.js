import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/fpOfflineInspection',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/fpOfflineInspection/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/fpOfflineInspection',
    method: 'put',
    data
  })
}

export function fpPersonnel() {
  return request({
    url: 'api/fpPersonnel?page=0&size=1000000&state=1&sort=id,desc',
    method: 'get'
  })
}

export function getProblemBase() {
  return request({
    url: 'api/fpProblemBase/getProblemBase',
    method: 'post'
  })
}
export default { add, edit, del, fpPersonnel, getProblemBase }
