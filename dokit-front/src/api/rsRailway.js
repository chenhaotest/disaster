import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/railway',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/railway/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/railway',
    method: 'put',
    data
  })
}

export default { add, edit, del }
