import request from '@/utils/request'

export function fqadd(data) {
  return request({
    url: 'api/fpResource',
    method: 'post',
    data
  })
}

export function fpdel(ids) {
  return request({
    url: 'api/fpResource/',
    method: 'delete',
    data: ids
  })
}

export function fqedit(data) {
  return request({
    url: 'api/fpResource',
    method: 'put',
    data
  })
}

export function findByType(data) {
  return request({
    url: 'api/dict/findByType',
    method: 'post',
    data
  })
}
export function queryResourceList(data) {
  return request({
    url: 'api/fpResource/queryResourceList',
    method: 'post',
    data
  })
}
export function fpEquipment(data) {
  return request({
    url: 'api/fpEquipment/',
    method: 'delete',
    data
  })
}

export function fpModubs(data) {
  return request({
    url: `api/fpModubs?page=0&size=9999&sort=id,desc`,
    method: 'get',
    data
  })
}

export function fpDevicePointNew(data) {
  return request({
    url: 'api/fpDevicePointNew',
    method: 'post',
    data
  })
}

export function putfpDevicePointNew(data) {
  return request({
    url: 'api/fpDevicePointNew',
    method: 'put',
    data
  })
}
export function delfpDevicePointNew(data) {
  return request({
    url: 'api/fpDevicePointNew',
    method: 'delete',
    data
  })
}
export default { fqadd, fqedit, fpdel, queryResourceList, findByType, fpEquipment, fpModubs, fpDevicePointNew, delfpDevicePointNew, putfpDevicePointNew }
