import request from '@/utils/request'

export function pointAdd(data) {
  return request({
    url: 'api/point',
    method: 'post',
    data
  })
}

export function poinDel(ids) {
  return request({
    url: 'api/point/',
    method: 'delete',
    data: ids
  })
}

export function pointEdit(data) {
  return request({
    url: 'api/point',
    method: 'put',
    data
  })
}

export default { pointAdd, poinDel, pointEdit }
