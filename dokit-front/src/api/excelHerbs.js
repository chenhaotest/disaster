import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/wfexcel',
    method: 'get',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/wfexcel/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/wfexcel',
    method: 'put',
    data
  })
}

export default { add, edit, del }
