import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/fpRepair',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/fpRepair/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/fpRepair',
    method: 'put',
    data
  })
}

export function findByType(data) {
  return request({
    url: 'api/dict/findByType',
    method: 'post',
    data
  })
}

export function fpDepartment(data) {
  return request({
    url: `api/fpDepartment?page=0&size=9999&sort=id,sort&state=1`,
    method: 'get',
    data
  })
}
export default { add, edit, del, findByType, fpDepartment }
