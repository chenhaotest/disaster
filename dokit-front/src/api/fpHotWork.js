import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/fpHotWork',
    method: 'post',
    data
  })
}

export function del(ids) {
  return request({
    url: 'api/fpHotWork/',
    method: 'delete',
    data: ids
  })
}

export function edit(data) {
  return request({
    url: 'api/fpHotWork',
    method: 'put',
    data
  })
}

export function updataApprovalStatusById(data) {
  return request({
    url: 'api/fpHotWork/updataApprovalStatusById',
    method: 'post',
    data
  })
}
export default { add, edit, del, updataApprovalStatusById }
