import * as THREE from "three";
import {camera} from "./camera";
import { renderer } from "./renderer";
import scene from "./scene";
import TWEEN from "@tweenjs/tween.js";
import controlsModule from "./controls";
var isDisabled = true
var composer = null // 控制发光
var outlinePass = null
var renderPass = null

var controls =controlsModule.controls
var selectedObjects = []
var mouse = new THREE.Vector2()
var raycaster = new THREE.Raycaster()

var tween = null
//定义模型架子的长度
var long = 100
//定义模型架子的宽度
var tall = 24
//定义模型架子横纵板的宽度
var thickness = 0.4

var positionObj = null

// 鼠标点击模型
function onMouseClick(event) {
  //通过鼠标点击的位置计算出raycaster所需要的点的位置，以屏幕中心为原点，值的范围为-1到1
  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / (window.innerHeight - 50)) * 2 + 1;
  // 通过鼠标点的位置和当前相机的矩阵计算出raycaster
  raycaster.setFromCamera(mouse, camera);
  // 获取raycaster直线和所有模型相交的数组集合
  let intersects = raycaster.intersectObjects(scene.children);

  if (!intersects[0]) {
    return;
  } else {
    /*if (!intersects[0].object.name == "") {*/
      selectedObjects = [];
      selectedObjects.push(intersects[0].object);

      positionObj = {
        x: intersects[0].point.x,
        y: intersects[0].point.y,
        z: intersects[0].point.z,
      };

      initTween(
        positionObj.x,
        positionObj.y,
        positionObj.z
      );
      isDisabled = false;

      controls.autoRotate = false;
    }
 /* }*/
}

// 相机移动动画
function initTween(targetX, targetY, targetZ) {

  // 需要保留this
  let initPosition = {
    x: camera.position.x,
    y: camera.position.y,
    z: camera.position.z,
  };
  let tween = new TWEEN.Tween(initPosition)
    .to({x: targetX, y: targetY, z: targetZ}, 2000)
    .easing(TWEEN.Easing.Sinusoidal.InOut);
  let onUpdate = (pos) => {
    let x = pos.x;
    let y = pos.y;
    let z = pos.z;
    if (pos.z < 0) {
      camera.position.set(x, y, z - 12);
    } else {
      camera.position.set(x, y, z + 12);
    }
  };
  tween.onUpdate(onUpdate);
  tween.start();
  // controls.target.set(0, 0, 0);
  if (positionObj.z < 0) {
    controls.target.set(
      positionObj.x,
      positionObj.y,
      -12
    );
  } else {
    controls.target.set(
      positionObj.x,
      positionObj.y,
      12
    );
  }
}
function onWindowResize() {
  //camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
}
window.onresize = onWindowResize;
window.oncontextmenu = onMouseClick;
