import * as THREE from "three";

// 初始化场景

import rendererModule from "./renderer";
import controlsModule from "./controls";

import uv_grid_opengl from  '../../public/ring2.jpg';

import scene from './scene'
import cameraModule from './camera'



//创建圆柱


//加载纹理
let texture = new THREE.TextureLoader().load(uv_grid_opengl);
texture.wrapS = texture.wrapT = THREE.RepeatWrapping; //每个都重复
texture.repeat.set(1, 1);
texture.needsUpdate = true;
let cylinderMesh2,geometry;
let materials = [
  //圆柱侧面材质，使用纹理贴图
  new THREE.MeshBasicMaterial({
    map: texture,
    side: THREE.DoubleSide,
    transparent: true
  }),
  //圆柱顶材质
  new THREE.MeshBasicMaterial({
    transparent: true,
    opacity: 0,
    side: THREE.DoubleSide
  }),
  //圆柱底材质
  new THREE.MeshBasicMaterial({
    transparent: true,
    opacity: 0,
    side: THREE.DoubleSide
  })
];

export  function  jg(scene,x,y,z){
   geometry = new THREE.CylinderGeometry(10, 10, 2, 30);

   cylinderMesh2 = new THREE.Mesh(geometry, materials);
  cylinderMesh2.position.set(x, y, z);
  scene.add(cylinderMesh2);

}




export  { cylinderMesh2 }










