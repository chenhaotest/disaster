import * as THREE from "three";

// 初始化场景

import * as GeometryUtils from 'three/examples/jsm/utils/GeometryUtils.js';



//const  s22phereGeometry=new  THREE.BoxGeometry()


/*
const  sphereGeometry=new  THREE.BoxGeometry(1000,1000,2,500,500,1);
const  pointsMaterial=new THREE.PointsMaterial(
  {
    color: 0xff0000,//0xff0000设置材质颜色为红色
    transparent: true,
    opacity: 0.1,

  }
);
pointsMaterial.size=0.1;
const  plane=new THREE.Points(sphereGeometry,pointsMaterial);
plane.position.set(0, 0, 0);
plane.rotation.set(-Math.PI / 2, 0, 0, "XZY");
*/




const points = GeometryUtils.gosper( 1000 );
const geometry4 = new THREE.BufferGeometry();
const positionAttribute = new THREE.Float32BufferAttribute( points, 3 );
geometry4.setAttribute( 'position', positionAttribute );
geometry4.center();


const material4 = new THREE.LineDashedMaterial( {
  vertexColors: false,
  color: 0xffffff,
  transparent: true,
  opacity: 0.8,

} );
const material5 = new THREE.LineDashedMaterial( {
  vertexColors: false,
  color: 0x6280f2,//0xff0000设置材质颜色为红色
  transparent: true,
  opacity: 0.1,

} );
 var plane = new THREE.Line( geometry4, material4 );
plane.scale.setScalar(0.01 );
plane.position.set(0, -5, 0);
plane.rotation.set(-Math.PI / 2, 0, 0, "XZY");





var plane2 = new THREE.Line( geometry4, material5 );
plane2.scale.setScalar(0.01 );
plane2.position.set(0, -25, 0);
plane2.rotation.set(-Math.PI / 2, 0, 0, "XZY");






/*
const geometry = new THREE.PlaneGeometry( 100, 100 );
const material = new THREE.MeshMatcapMaterial( {
  color: 0x6280f2,
  transparent: true,
  opacity: 0.8,
} );

 const plane2 = new THREE.Mesh( geometry, material );
plane2.position.set( 0, - 1, 0 );
plane2.rotation.x = - Math.PI / 2;
/!*plane.receiveShadow = true;*!/
*/





export  { plane, plane2 }
