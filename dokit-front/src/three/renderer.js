import * as THREE from "three";
import { CSS3DRenderer } from "three/examples/jsm/renderers/CSS3DRenderer";
// 初始化渲染器
const renderer = new THREE.WebGLRenderer({
  // 设置抗锯齿
  antialias: true,
  // depthbuffer
  logarithmicDepthBuffer: true,
  physicallyCorrectLights: true,
});
// 设置渲染尺寸大小
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.shadowMap.enabled = true;
renderer.toneMapping = THREE.ACESFilmicToneMapping;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
renderer.toneMappingExposure = 1;
/*renderer.setClearColor(0x3da0ff, 0.1); //设置背景颜色*/




const css3drender = new CSS3DRenderer();
css3drender.setSize(window.innerWidth, window.innerHeight);

document.querySelector("#cssrender").appendChild(css3drender.domElement);

export default { renderer, css3drender };
