import * as THREE from "three";


import { plane, plane2 } from "./planBox";
import { cylinderMesh} from "./planRing";
import uv_grid_opengl from  '../../public/plane1.jpg';
import { Lensflare } from 'three/examples/jsm/objects/Lensflare.js';
import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader.js";
// 初始化场景
const scene = new THREE.Scene();






const ambient = new THREE.HemisphereLight( 0xffffff, 0xffffff, 1);
ambient.position.set(0, 50, 0); //这个也是默认位置
scene.add( ambient );
/*const helper = new THREE.HemisphereLightHelper( ambient, 100 );
scene.add( helper );*/



const ambient2 = new THREE.HemisphereLight( 0x00ffcc, 0xffffff, 1);
/*ambient2.position.set(-100, 50, -350); //这个也是默认位置*/
ambient2.position.set(10, 50,0); //这个也是默认位置
scene.add( ambient2 );
/*const helper2 = new THREE.HemisphereLightHelper( ambient2, 100 );
scene.add( helper2 );*/


var spotLight = new THREE.SpotLight( 0xffffff, 500 );
spotLight.position.set( 2.5, 60, 2.5 );

spotLight.angle = 2;
spotLight.penumbra = 1;
spotLight.decay = 0.9;
spotLight.distance = 0;


spotLight.castShadow = true;
spotLight.shadow.mapSize.width = 1024;
spotLight.shadow.mapSize.height = 1024;
spotLight.shadow.camera.near = 1;
spotLight.shadow.camera.far = 10;
spotLight.shadow.focus = 0;
scene.add( spotLight );
/*var lightHelper = new THREE.SpotLightHelper( spotLight );
scene.add( lightHelper );*/



//cylinderRingMesh(uv_grid_opengl,scene)


scene.add(cylinderMesh);

//scene.add(plane2);

scene.add(plane);
//scene.add(plane2);





/*

const  sphereGeometry=new  THREE.BoxGeometry(150,150,1,10,10,1);
const  pointsMaterial=new THREE.PointsMaterial(
  {
    color: 0x00ffcc,//0xff0000设置材质颜色为红色
    transparent: true,
    opacity: 0.5,

  }
);
pointsMaterial.size=0.1;
const  plane3=new THREE.Points(sphereGeometry,pointsMaterial);
plane3.position.set(0, -30, 0);
plane3.rotation.set(-Math.PI / 2, 0, 0, "XZY");
scene.add(plane3);
*/





export default scene;
